ALTER TABLE `users` ADD COLUMN `IsVerifyPhone` TINYINT NOT NULL  AFTER `IsVerifyEmail` ;
UPDATE users SET IsVerifyPhone = 1;
CREATE  TABLE `notificationusers` (
  `NotificationUserId` INT NOT NULL AUTO_INCREMENT ,
  `NotificationId` INT NOT NULL ,
  `UserId` INT NOT NULL ,
  `IsRead` TINYINT NOT NULL ,
  `UpdateDateTime` DATETIME NULL ,
  PRIMARY KEY (`NotificationUserId`) );

ALTER TABLE `registers`
ADD `IsAlpha` TINYINT(4) NOT NULL AFTER `UpdateDateTime`,
 ADD `IsBeta` TINYINT(4) NOT NULL AFTER `IsAlpha`,
 ADD `IsGamma` TINYINT(4) NOT NULL AFTER `IsBeta`,
 ADD `CourseType` TINYINT(4) NOT NULL AFTER `IsGamma`;
 INSERT INTO `configs`
 (`ConfigId`, `ConfigCode`, `ConfigName`, `ConfigValue`, `ConfigDecimal`, `AutoLoad`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`)
 VALUES
('16', 'ALPHA_VALUE', 'Hệ số Zticket Alpha (%)', '30', '0', '2', '1', '2017-11-27 00:00:00', '1', '2018-03-18 13:07:04'),
('17', 'BETA_VALUE', 'Hệ số Zticket Beta (%)', '50', '0', '2', '1', '2017-11-27 00:00:00', '1', '2018-03-18 13:07:04'),
('18', 'BETA_VALUE', 'Hệ số Zticket Gamma (%)', '40', '0', '2', '1', '2017-11-27 00:00:00', '1', '2018-03-18 13:07:04');

ALTER TABLE `users` ADD COLUMN `TokenFCM` VARCHAR(45) NULL  AFTER `TokenEmail` ;
ALTER TABLE `users` CHANGE COLUMN `TokenFCM` `TokenFCM` TEXT NULL DEFAULT NULL  ;
