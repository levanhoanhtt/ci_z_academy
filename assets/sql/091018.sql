ALTER TABLE `notifications` ADD `ArticleId` INT NOT NULL AFTER `IsFromStudent`;
ALTER TABLE `notifications` ADD `ArticleContent` TEXT NOT NULL AFTER `ArticleId`;