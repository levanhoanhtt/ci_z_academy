$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('.btnRegisterService').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#getServiceDetailUrl').val(),
            data: {
                ServiceId: btn.attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    var serviceCost = formatDecimal(data.ServiceCost);
                    $('input#serviceId').val(data.ServiceId);
                    $('input#serviceTypeId').val(data.ServiceTypeId);
                    $('#spanServiceName').text(data.ServiceName);
                    $('#spanServiceDesc').text(data.ServiceDesc);
                    $('#spanServiceCost').text(serviceCost);
                    $('#spanServiceTypeName').text(data.ServiceTypeName);
                    $('input#endDate').val('');
                    $('input#paidCost').val(serviceCost);
                    $('input#comment').val('');
                    $('input#isRequireCarNumber').val(data.IsRequireCarNumber);
                    if(data.IsRequireCarNumber == 2){
                        $('#divCarNumber').show();
                        $('#divServiceComment').removeClass('col-sm-12').addClass('col-sm-8');
                    }
                    else{
                        $('#divCarNumber').hide();
                        $('#divServiceComment').removeClass('col-sm-8').addClass('col-sm-12');
                    }
                    $('#modalRegisterService').modal('show');
                }
                else showNotification(json.message, json.code);
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
        return false;
    });
    $('input#beginDate, input#endDate').change(function(){
        $('input#paidCost').val($('#spanServiceCost').text());
        var beginDate = $('input#beginDate').val().trim();
        if(beginDate != ''){
            var beginDates = beginDate.split('/');
            if(beginDates.length != 3) return false;
            beginDates[0] = parseInt(beginDates[0]);
            beginDates[1] = parseInt(beginDates[1]);
            beginDates[2] = parseInt(beginDates[2]);
            if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0) return false;
            var serviceTypeId = parseInt($('input#serviceTypeId').val());
            var endDate = $('input#endDate').val().trim();
            if(endDate != ''){
                var endDates = endDate.split('/');
                if(endDates.length != 3) return false;
                endDates[0] = parseInt(endDates[0]);
                endDates[1] = parseInt(endDates[1]);
                endDates[2] = parseInt(endDates[2]);
                if (endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0) return false;
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function (servicePeriod) {
                    if (servicePeriod > 0) {
                        var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                });
            }
            else{
                var now = new Date();
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                    if (servicePeriod > 0) {
                        var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                });
            }
        }
    });
    $('#btnRegisterService').click(function(){
        var inputBeginDate = $('input#beginDate');
        var inputEndDate = $('input#endDate');
        var beginDate = inputBeginDate.val().trim();
        if(beginDate == ''){
            showNotification('Ngày bắt đầu không được bỏ trống', 0);
            inputBeginDate.focus();
            return false;
        }
        var beginDates = beginDate.split('/');
        if(beginDates.length != 3){
            showNotification('Ngày bắt đầu không đúng định dạng', 0);
            inputBeginDate.focus();
            return false;
        }
        beginDates[0] = parseInt(beginDates[0]);
        beginDates[1] = parseInt(beginDates[1]);
        beginDates[2] = parseInt(beginDates[2]);
        if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0){
            showNotification('Ngày bắt đầu không đúng định dạng', 0);
            inputBeginDate.focus();
            return false;
        }
        var serviceTypeId = parseInt($('input#serviceTypeId').val());
        var serviceCost = replaceCost($('#spanServiceCost').text(), true);
        var endDate = inputEndDate.val().trim();
        if(endDate != ''){
            var endDates = endDate.split('/');
            if(endDates.length != 3){
                showNotification('Ngày kết thúc không đúng định dạng', 0);
                inputEndDate.focus();
                return false;
            }
            endDates[0] = parseInt(endDates[0]);
            endDates[1] = parseInt(endDates[1]);
            endDates[2] = parseInt(endDates[2]);
            if(endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0){
                showNotification('Ngày kết thúc không đúng định dạng', 0);
                inputEndDate.focus();
                return false;
            }
            timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function(servicePeriod){
                if(servicePeriod > 0){
                    var paidCost = servicePeriod * serviceCost;
                    $('input#paidCost').val(formatDecimal(paidCost.toString()));
                }
                else{
                    showNotification('Ngày kết thúc phải lớn hơn ngày bắt đầu', 0);
                    inputEndDate.focus();
                    $('input#paidCost').val($('#spanServiceCost').text());
                }
            });
        }
        else{
            if(serviceTypeId == 1){
                showNotification('Dịch vụ này bắt buộc phải có ngày kết thúc', 0);
                inputEndDate.focus();
                return false;
            }
            var now = new Date();
            timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                if (servicePeriod > 0) {
                    var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                    $('input#paidCost').val(formatDecimal(paidCost.toString()));
                }
            });
        }
        var carNumber = '';
        if($('input#isRequireCarNumber').val() == '2'){
            carNumber = $('input#carNumber').val().trim();
            if(carNumber == ''){
                showNotification('Biển số không được bỏ trống', 0);
                return false;
            }
        }
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#registerServiceUrl').val(),
            data: {
                ServiceId: $('input#serviceId').val(),
                ServiceTypeId: serviceTypeId,
                ServiceCost: serviceCost,
                BeginDate: beginDate,
                EndDate: endDate,
                PaidCost: replaceCost($('input#paidCost').val(), true),
                Comment: $('input#comment').val().trim(),
                CarNumber: carNumber
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $('#modalRegisterService').modal('hide');
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
        return false;
    });
});

function timeDiff(beginDate, endDate, serviceTypeId, func){
    $.ajax({
        type: "POST",
        url: $('input#getTimeDiffUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate,
            ServiceTypeId: serviceTypeId
        },
        success: function (response) {
            func(parseInt(response));
        },
        error: function (response) {
            func(0);
        }
    });
}