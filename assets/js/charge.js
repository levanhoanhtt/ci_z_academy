$(document).ready(function () {
    var btnBuyTicket = $('#btnBuyTicket');
    if($('input#buyAble').val() == '0') btnBuyTicket.removeClass('btn-buy').addClass('btn-n');
    else btnBuyTicket.removeClass('btn-n').addClass('btn-buy');
    btnBuyTicket.click(function(){
        if($('input#buyAble').val() == '1'){
            $('#money_cost').val('0');
            $('#ticket_buy').val('0');
            $('#buyModal').modal('show');
        }
        else showNotification('Bạn không đủ tiền để mua. Vui lòng nạp tiền vào tài khoản', 0);
    });
    var ticketCost = parseInt($('input#ticketCost').val());
    var ticketPiece = parseInt($('input#ticketPiece').val());
    var decimalCount = 2;
    var minTicket = 0.01;
    if(ticketPiece == 1000){
        decimalCount = 3;
        minTicket = 0.001;
    }
    else if(ticketPiece == 10){
        decimalCount = 1;
        minTicket = 0.1;
    }
    $('#divSell').on('keyup', '#ticketCountSell', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        if(getDecimalCount(value) > decimalCount) showNotification('1 vé chỉ được chia làm ' + ticketPiece + ' phần', 0);
    }).on('keydown', '#ticketCountSell', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8|| e.keyCode == 190 || e.keyCode == 110)) {
            e.preventDefault();
        }
    });
    $('#buyModal').on('keyup', '#money_cost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var ticket = replaceCost(value, true) / ticketCost;
        if(getDecimalCount(ticket) > decimalCount) showNotification('1 vé chỉ được chia làm ' + ticketPiece + ' phần. Vui lòng chọn số tiền tương ứng', 0);
        $('#ticket_buy').val(formatDecimal(ticket.toString()));
    }).on('keydown', '#money_cost', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    }).on('keyup', '#ticket_buy', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        if(getDecimalCount(value) > decimalCount) showNotification('1 vé chỉ được chia làm ' + ticketPiece + ' phần', 0);
        var money = value * ticketCost;
        money = Math.ceil(money);
        $('#money_cost').val(formatDecimal(money.toString()));
    }).on('keydown', '#ticket_buy', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8|| e.keyCode == 190 || e.keyCode == 110)) {
            e.preventDefault();
        }
    });

    $('#sendMoney').on('keyup', '#money-s', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keydown', '#money-s', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });

    $('.tagsinput-typeahead').tagsinput({
        typeahead: {
            //source: places.map(function(item) { return item.name }),
            source: function (typeahead, query) {
                return $.ajax({
                    url: $('input#searchByNameOrPhoneUrl').val(),
                    type: 'POST',
                    data: {
                        SearchText: typeahead,
                        IsCheckUser: 1
                    },
                    dataType: 'json',
                    success: function (json) {
                        return typeof json.options == 'undefined' ? false : process(json.options);
                    }
                });
            },
            afterSelect: function () {
                this.$element[0].value = '';
            }
        }
    });

    $('#btnBuy').click(function(){
        if($('input#buyAble').val() == '1'){
            var ticket = replaceCost($('#ticket_buy').val(), false);
            if(getDecimalCount(ticket) > decimalCount){
                showNotification('1 vé chỉ được chia làm ' + ticketPiece + ' phần. Vui lòng chọn số tiền tương ứng', 0);
                return false;
            }
            var paidVN = replaceCost($('#money_cost').val(), true);
            if(paidVN > 0) {
                var btn = $(this);
                btn.prop("disabled", true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertChargeUrl').val(),
                    data: {
                        ChargeTypeId: 2,
                        PaidVN: paidVN,
                        TicketCount: ticket
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) redirect(true, '');
                        else btn.prop("disabled", false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            else showNotification('Vui lòng nhập số tiền cần mua', 0);
        }
        else showNotification('Bạn không đủ tiền để mua. Vui lòng nạp tiền vào tài khoản', 0);
        return false;
    });

    $('#btnSell').click(function(){
        if(validateEmpty('#divSell')){
            var ticketCountSell = replaceCost($('input#ticketCountSell').val(), false);
            if(getDecimalCount(ticketCountSell) > decimalCount){
                showNotification('1 vé chỉ được chia làm ' + ticketPiece + ' phần. Vui lòng chọn lại số vé cần bán', 0);
                return false;
            }
            if(ticketCountSell >= minTicket && ticketCountSell > 0){
                var btn = $(this);
                btn.prop("disabled", true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertChargeUrl').val(),
                    data: {
                        PhoneNumber: $('input#phoneNumber').val().trim(),
                        UserPass: $('input#userPass').val().trim(),
                        UserCode: $('input#userCode').val().trim(),
                        ChargeTypeId: 1,
                        TicketCount: ticketCountSell
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 0 || json.code == 1) redirect(true, '');
                        else btn.prop("disabled", false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            else showNotification('Số lượng vé bán phải lớn hơn ' + minTicket);
        }
        return false;
    });

    $('#btnSendMoney').click(function(){
        if(validateEmpty('#sendMoney')){
            var paidVN = replaceCost($('input#money-s').val(), true);
            if(paidVN > 0) {
                var btn = $(this);
                btn.prop("disabled", true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertChargeUrl').val(),
                    data: {
                        PhoneNumber: $('input#phoneNumber-s').val().trim(),
                        UserPass: $('input#userPass-s').val().trim(),
                        UserCode: $('input#userCode-s').val().trim(),
                        ChargeTypeId: 4,
                        PaidVN: paidVN,
                        Comment: $('input#reason-s').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 0 || json.code == 1) redirect(true, '');
                        else btn.prop("disabled", false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            else showNotification('Vui lòng nhập số tiền cần chuyển', 0);
        }
        return false;
    });
});

function getDecimalCount(num) {
    return (num.toString().split('.')[1] || []).length;
}