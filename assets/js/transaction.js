var container = {
    page: 1
};
function getProof(page) {
    $.ajax({
        type: 'POST',
        url: $('input#getListProofUrl').val(),
        data: {
            page: page
        },
        success: function (result) {
            result = JSON.parse(result);
            var data = result.data;
            container.page = result.page;
            container.maxPage = result.maxPage;
            var html = '';
            var imagePath = $('input#imagePath').val();
            data.forEach(function (item) {
                html += '<tr><td>' + item.CrDateTime + '</td>';
                html += '<td>' + item.FullName + '</td>';
                html += '<td>' + item.PhoneNumber + '</td>';
                html += '<td style="width: 20%"><a href="' + imagePath + item.Image + '" target="_blank"><img src="' + imagePath + item.Image + '"></a></td>';
                html += '<td>' + item.PaymentTypeName + '</td></tr>';
            });
            if (container.page > 1 || container.page < container.maxPage) {
                html += '<tr>' +
                    '<td colspan="5" class="text-right">' +
                    '<ul>' +
                    '<li style="display: inline-block"><a id="prevPage" style="cursor:' + (container.page > 1 ? 'pointer' : 'not-allowed' ) + ';color: #35a6ff;padding:3px 8px;display: block;border: 1px solid #EEEEEE"><i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>' +
                    '<li style="display: inline-block"><a id="nextPage" style="cursor:' + (container.page < container.maxPage ? 'pointer' : 'not-allowed' ) + ';color: #35a6ff;padding:3px 8px;display: block;border: 1px solid #EEEEEE;margin-left: 5px"><i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>' +
                    '</ul>' +
                    '</td>' +
                    '</tr>';
            }
            $('#tblProof').html(html);
        },
        error: function () {

        }
    });
}
$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#showPopupProof').click(function () {
        getProof();
        $('#modalProof').modal('show');
        return false;
    });

    $('#modalProof').on('click', '#prevPage', function (e) {
        if (container.page > 1) {
            getProof(container.page - 1);
        }
    }).on('click', '#nextPage', function (e) {
        if (container.page < container.maxPage) {
            getProof(container.page + 1);
        }
    });
    $('#transactionForm').on('keyup', 'input#paidVN', function () {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).submit(function () {
        var form = $('#transactionForm');
        if (validateEmpty('#transactionForm')) {
            form.find('button').prop("disabled", true);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 0 || json.code == 1) redirect(true, '');
                    else form.find('button').prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    form.find('button').prop("disabled", false);
                }
            });
        }
        return false;
    });
    /*var places = [
        {name: "New York"},
        {name: "Los Angeles"},
        {name: "Copenhagen"},
        {name: "Albertslund"},
        {name: "Skjern"}
    ];*/
    $('.tagsinput-typeahead').tagsinput({
        typeahead: {
            //source: places.map(function(item) { return item.name }),
            source: function (typeahead, query) {
                return $.ajax({
                    url: $('input#searchByNameOrPhoneUrl').val(),
                    type: 'POST',
                    data: {
                        SearchText: typeahead,
                        IsCheckUser: 0
                    },
                    dataType: 'json',
                    success: function (json) {
                        return typeof json.options == 'undefined' ? false : process(json.options);
                    }
                });
            },
            afterSelect: function () {
                this.$element[0].value = '';
            }
        }
    });
});