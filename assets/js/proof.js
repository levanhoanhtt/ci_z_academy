$(document).ready(function() {
    $('.send-proof').click(function () {
        $('#popupProof').modal('show');
    });

    $('#selectImage').click(function (e) {
        $('#inputImage').trigger('click');
    });

    $('#inputImage').change(function (e) {
        var file = this.files[0];
        var typeFile = file.type.replace('image/', '');
        var whiteList = ['jpeg', 'jpg', 'png', 'bmp'];
        if (whiteList.indexOf(typeFile) === -1) {
            showNotification('Tệp tin phải là ảnh có định dạng , jpeg/jpg/png/bmp', 0);
            return;
        }
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            $('#preView').attr('src', reader.result);
        }, false);
        if (file) reader.readAsDataURL(file);
    });

    $('#sendProof').click(function (e) {
        var imageBase64 = $('#preView').attr('src');
        if (imageBase64.length === 0) {
            showNotification('Xin mời gửi ảnh bằng chứng', 0);
            return false;
        }
        $('#selectImage').hide();
        $('#progress').show();
        var btn = $(this);
        btn.prop("disabled", true);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        var prb = $('#progress .progress-bar');
                        prb.text(percentComplete + '%');
                        prb.css({
                            width: percentComplete + '%'
                        });
                        if (percentComplete === 100) {
                            setTimeout(function () {
                                prb.text('Tải ảnh lên hoàn thành');
                            }, 1000);
                        }
                    }
                }, false);
                return xhr;
            },
            type: 'post',
            url: $('#urlSend').val(),
            data: {
                PaymentTypeId: $('select#paymentTypeId').val(),
                ImageBase64: imageBase64
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    setTimeout(function () {
                        $('#popupProof').modal('hide');
                        $('#selectImage').show();
                        $('#progress').hide();
                        $('#preView').attr('src', '');
                    }, 2000);
                }
                btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
        return false;
    });
});