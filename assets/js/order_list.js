$(document).ready(function(){
	$('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $("#tbodyOrder").on("click", "a.link_status", function(){
        $('input#orderId, input#statusId, input#statusTypeId').val('0');
        var id = $(this).attr('data-id');
        var statusId = parseInt($(this).attr('data-status'));
        var statusTypeId = parseInt($('input#statusTypeId_' + id).val());
        var typeId = 0;
        //if(statusTypeId == 2 && statusId == 2) typeId = 1;
        //else if(statusTypeId == 3 && (statusId == 4 || statusId == 5)) typeId = 2;
        if(typeId > 0){
            $('input#orderId').val(id);
            $('input#statusId').val(statusId);
            $('input#statusTypeId').val(statusTypeId);
            var txt = 'Bằng chứng đã giao hàng';
            if(typeId == 2){
                if(statusId == 4) txt = 'Bằng chứng yêu câu hoàn tiền';
                else txt = 'Bằng chứng hủy đơn';
            }
            $('#modalProof h4').text(txt);
            $('input#comment').val(txt);
            $('#modalProof').modal('show');
        }
        else{
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    OrderId: id,
                    StatusId: statusId,
                    StatusTypeId: statusTypeId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        var tr = $('#trOrder_' + id);
                        var data = json.data;
                        if(statusTypeId == 1) tr.find('.tdZOrderStatus').html(data.StatusName);
                        else if(statusTypeId == 2) tr.find('.tdSellerOrderStatus').html(data.StatusName);
                        else if(statusTypeId == 3) tr.find('.tdCustomerOrderStatus').html(data.StatusName);
                        if(data.ZStatusName != ''){
                            tr.find('.tdZOrderStatus').html(data.ZStatusName);
                            tr.find('a.link_evident').show();
                        }
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    }).on('click', 'a.link_evident', function(){
        $('input#orderId, input#statusId, input#statusTypeId').val('0');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#getConflictUrl').val(),
            data: {
                OrderId: id
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    $('input#orderId').val(id);
                    var data = json.data;
                    var html = '';
                    for(var i = 0; i < data.length; i++){
                        html += '<tr><td>' + data[i].CrDateTime + '</td><td>';
                        if(data[i].IsCustomerSend == '2') html += $('#customerName_' + id).text();
                        else html += $('#sellerName_' + id).text();
                        html += '</td><td style="width: 20%">';
                        html += '<a href="' + data[i].ConflictImage + '" target="_blank"><img src="' + data[i].ConflictImage + '"></a>';
                        html += '</td><td>' + data[i].Comment + '</td></tr>';
                    }
                    $('#tblProof').html(html);
                    html = 'Quyết định từ ⓩ: ';
                    var isCustomerWin = $('input#isCustomerWin_' + id).val();
                    if(isCustomerWin == '1'){
                        html += 'Không phù hợp để hoàn tiền';
                        $('#divConflictNotify').css({'background' :'red','border': '1px solid red'});
                    }
                    else if(isCustomerWin == '2'){
                        html += 'Hoàn tiền cho bên mua';
                        $('#divConflictNotify').css({'background' :'green','border': '1px solid green'});
                    }
                    $('#divConflictNotify span').html(html);
                    if($('input#zOrderStatusId_' + id).val() != '2' && $('input#userLoginId').val() == '1') $('.btnSolve').remove();
                    $('#modalProof').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
    if($('input#roleLoginId').val() == '2'){
        $('#selectImage').click(function (e) {
            $('#inputImage').trigger('click');
        });
        $('#inputImage').change(function (e) {
            var file = this.files[0];
            var typeFile = file.type.replace('image/', '');
            var whiteList = ['jpeg', 'jpg', 'png', 'bmp'];
            if (whiteList.indexOf(typeFile) === -1) {
                showNotification('Tệp tin phải là ảnh có định dạng , jpeg/jpg/png/bmp', 0);
                return;
            }
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                $('#preView').attr('src', reader.result);
            }, false);
            if (file) reader.readAsDataURL(file);
        });
        $('#sendProof').click(function(){
            var statusId = parseInt($('input#statusId').val());
            var statusTypeId = parseInt($('input#statusTypeId').val());
            var imageBase64 = $('#preView').attr('src');
            var hasImage = imageBase64.length > 0;
            /*if(imageBase64.length === 0) {
                if(statusTypeId == 3 && statusId == 5) hasImage = false;
                else {
                    showNotification('Xin mời gửi ảnh bằng chứng', 0);
                    return false;
                }
            }*/
            var isCustomerSend = 0;
            var orderId = $('input#orderId').val();
            var userLoginId = $('input#userLoginId').val();
            if(userLoginId == $('input#sellerId_' + orderId).val()) isCustomerSend = 1;
            else if(userLoginId == $('input#customerId_' + orderId).val()) isCustomerSend = 2;
            var btn = $(this);
            btn.prop("disabled", true);
            if(hasImage) {
                $('#selectImage').hide();
                $('#progress').show();
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                var prb = $('#progress .progress-bar');
                                prb.text(percentComplete + '%');
                                prb.css({
                                    width: percentComplete + '%'
                                });
                                if (percentComplete === 100) {
                                    setTimeout(function () {
                                        prb.text('Tải ảnh lên hoàn thành');
                                    }, 1000);
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    type: 'post',
                    url: $('input#insertConflictUrl').val(),
                    data: {
                        OrderId: orderId,
                        IsCustomerSend: isCustomerSend,
                        ConflictImage: imageBase64,
                        Comment: $('input#comment').val().trim(),

                        StatusId: statusId,
                        StatusTypeId: statusTypeId
                    },
                    success: function (response) {
                        handleSuccessConflict(response, isCustomerSend, orderId, statusId, statusTypeId, btn);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: $('input#insertConflictUrl').val(),
                    data: {
                        OrderId: orderId,
                        IsCustomerSend: isCustomerSend,
                        ConflictImage: imageBase64,
                        Comment: $('input#comment').val().trim(),

                        StatusId: statusId,
                        StatusTypeId: statusTypeId
                    },
                    success: function (response) {
                        handleSuccessConflict(response, isCustomerSend, orderId, statusId, statusTypeId, btn);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            return false;
        });
    }
    else{
        var btnSolve = $('.btnSolve');
        btnSolve.click(function(){
            var typeId = $(this).attr('data-id');
            var orderId = $('input#orderId').val();
            btnSolve.prop("disabled", true);
            $.ajax({
                type: "POST",
                url: $('input#solveConflictUrl').val(),
                data: {
                    OrderId: orderId,
                    SolveTypeId: typeId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) {
                        $('#trOrder_' + orderId).find('.tdZOrderStatus').html(json.data.StatusName);
                        $('input#zOrderStatusId_' + orderId).val(3);
                        $('input#isCustomerWin_' + orderId).val(typeId);
                        btnSolve.remove();
                        $('#modalProof').modal('hide');
                    }
                    btnSolve.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btnSolve.prop("disabled", false);
                }
            });
        });
    }
});

function handleSuccessConflict(response, isCustomerSend, orderId, statusId, statusTypeId, btn){
    var json = $.parseJSON(response);
    showNotification(json.message, json.code);
    if(json.code == 1) {
        $('input#comment').val('');
        var data = json.data;
        var html = '<tr><td>' + data.CrDateTime + '</td><td>';
        if(isCustomerSend == 2) html += $('#customerName_' + orderId).text();
        else html += $('#sellerName_' + orderId).text();
        html += '</td><td style="width: 20%">';
        if(data.ConflictImage != '') html += '<a href="' + data.ConflictImage + '" target="_blank"><img src="' + data.ConflictImage + '"></a>';
        html += '</td><td>' + data.Comment + '</td></tr>';
        $('#tblProof').prepend(html);
        var tr = $('#trOrder_' + orderId);
        if (statusId == 2 && statusTypeId == 2) tr.find('.tdSellerOrderStatus').html(data.SellerOrderStatusName);
        else if (statusTypeId == 3 && (statusId == 4 || statusId == 5)) {
            tr.find('.tdCustomerOrderStatus').html(data.CustomerOrderStatusName);
            tr.find('.tdZOrderStatus').html(data.ZOrderStatusName);
        }
    }
    btn.prop("disabled", false);
    $('#selectImage').show();
    $('#progress').hide();
    $('#preView').attr('src', '');
}