$(document).ready(function(){
    var isRegiterPage = $('div.g-recaptcha').length > 0;
    if(isRegiterPage) {
        var APP_ID = $('input#appFbId').val();
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.fbAsyncInit = function () {
            FB.init({
                appId: APP_ID,
                cookie: true,
                xfbml: true,
                version: 'v2.10'
            });
            FB.AppEvents.logPageView();
        };

        AccountKit_OnInteractive = function () {
            AccountKit.init(
                {
                    appId: APP_ID,
                    state: new Date().toString(),
                    version: "v1.1",
                    fbAppEventsEnabled: true,
                    debug: true,
                    display: 'modal'
                }
            );
        };

        function loginCallback(response) {
            if (response.status === "PARTIALLY_AUTHENTICATED") {
                $.ajax({
                    type: "POST",
                    url: $("#verifyPhoneUrl").val(),
                    data: {
                        PhoneNumber: $("#phoneNumber").val().trim(),
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
                /*var code = response.code;
                var csrf = response.state;
                var phoneNumber = $("#phoneNumber").val();
                var url = $("#verifyPhoneUrl").val();
                $.post( url, {PhoneNumber: phoneNumber}).done(function( data ) {
                    var json = $.parseJSON(data);
                    showNotification(json.message, json.code);
                });*/
            }
            else if (response.status === "NOT_AUTHENTICATED") showNotification('Bạn chưa xác thực số điện thoại', 0);
            else if (response.status === "BAD_PARAMS") showNotification('Bạn chưa xác thực số điện thoại', 0);
        }

        function smsLogin() {
            var countryCode = '+84';
            var phoneNumber = $("#phoneNumber").val();
            if(phoneNumber.indexOf('0') == 0) phoneNumber  = (phoneNumber.slice(1, 11));
            AccountKit.login(
                'PHONE',
                {countryCode: countryCode, phoneNumber: phoneNumber},
                loginCallback
            );
        }
    }

    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.chooseImage').click(function(){
        chooseFile('Users', function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        });
    });
    $('a#generatorPass').click(function(){
        var pass = randomPassword(10);
        $('input#newPass').val(pass);
        $('input#rePass').val(pass);
        return false;
    });
    var userId = parseInt($('input#userId').val());
    $('.submit').click(function (){
        if(validateEmpty('#userForm')) {
            var phoneInput = $("#phoneNumber");
            if(!validatePhoneNumber(phoneInput)){
                phoneInput.focus();
                return false;
            }
            var idInput = $('input#iDCardNumber');
            if(!validateIdCartNumber(idInput)){
                idInput.focus();
                return false;
            }
            if($('input#newPass').val() != $('input#rePass').val()) {
                showNotification('Mật khẩu không trùng', 0);
                return false;
            }
            /*if($('input#userName').length > 0 && $('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('UserName không được có khoảng trắng', 0);
                $('input#userName').focus();
                return false;
            }*/
            if(isRegiterPage){
                var v = grecaptcha.getResponse();
                if(v.length == 0){
                    showNotification('Chưa veryfy captcha', 0);
                    return false;
                }
            }
            var btn = $(this);
            btn.prop('disabled', true);
            var form = $('#userForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        smsLogin();
                        showNotification(json.message, json.code);
                        if(userId == 0) {
                            if ($('.register-success').length > 0) {
                                $('.login-box-body, .register-success').hide();
                                $('.login-box').css({"margin-top": "10%", "width": "470px"});
                                $('#divRegister').fadeIn();
                            }
                        }
                        else redirect(false, $('input#userEditUrl').val() + '/' + json.data);

                    }
                    else if(json.code == -2){
                        $('.register-success, #userForm').hide();
                        $(".login-box-body, #notificationForm").fadeIn();
                    }
                    else if(json.code == -3){
                        showNotification(json.message, json.code);
                        redirect(false, $('#aLogin').attr('href'));
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });

    $('#btnComplaint').click(function(){
        if(validateEmpty('#notificationForm')){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('#notificationForm').attr('action'),
                data: {
                    StudentId: -1,
                    AdminId: 0,
                    PhoneNumber: $("#phoneNumber").val().trim(),
                    Email: $("#email").val().trim(),
                    Message: $('#message').val().trim(),
                    IsFromStudent: 2
                },
                success: function(response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $('.login-box-body, .register-success').hide();
                        $('.login-box').css({"margin-top": "10%", "width": "470px"});
                        $('#divNotification').fadeIn();
                    }
                    else showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    })
});

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    //var chars = "ABCDEFGHIJKLMNOPQRSTXYZ1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

