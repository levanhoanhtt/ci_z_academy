$(document).ready(function(){
	var btnDraw = $('#btnDraw');
	if($('input#buyAble').val() == '0') btnDraw.removeClass('btn-buy').addClass('btn-n');
	else btnDraw.removeClass('btn-n').addClass('btn-buy');
	btnDraw.click(function(){
		if($('input#buyAble').val() == '1'){
			if(validateEmpty('#withdrawalForm')){
				var fullNameLoginId = change_alias($('input#fullNameLoginId').val()).toLocaleLowerCase();
				if(change_alias($('input#bankHolder').val().trim().toLowerCase()) == fullNameLoginId) {
					$('input#withdrawalCost, input#receiveCost').val('0');
					$('#drawModal').modal('show');
				}
				else showNotification('Chủ tài khoản ngân hàng phải trùng tên với chủ tài khoản Z', 0);
			}
		}
		else showNotification('Bạn không đủ tiền để rút', 0);
	});
	$('#drawModal').on('keyup', 'input#withdrawalCost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
		value = replaceCost(value, true) * (100 - parseInt($('input#totalPercent').val())) / 100;
		value = Math.ceil(value);
		$('input#receiveCost').val(formatDecimal(value.toString()));
    }).on('keydown', 'input#withdrawalCost', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });
    $("#btnWithdrawal").click(function(){
    	var withdrawalCost = replaceCost($('input#withdrawalCost').val(), true);
		if(withdrawalCost < 10000000){
			showNotification('Số tiền rút tối thiểu phải là 10.000.000 đ', 0);
			return false;
		}
		if($('input#buyAble').val() == '1'){
			var btn = $(this);
			btn.prop("disabled", true);
			$.ajax({
				type: "POST",
				url: $('#withdrawalForm').attr('action'),
				data: {
					WithdrawalCost: withdrawalCost,
					ReceiveCost: replaceCost($('input#receiveCost').val(), true),
					BankName: $('input#bankName').val().trim(),
					BankHolder: $('input#bankHolder').val().trim(),
					BankNumber: $('input#bankNumber').val().trim(),
					BranchName: $('input#branchName').val().trim(),
					AffPercent: parseInt($('input#totalPercent').val()) - 1
				},
				success: function (response) {
					var json = $.parseJSON(response);
					showNotification(json.message, json.code);
					if(json.code == 1) redirect(true, '');
					else btn.prop("disabled", false);
				},
				error: function (response) {
					showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
					btn.prop("disabled", false);
				}
			});
    	}
		else showNotification('Bạn không đủ tiền để rút', 0);
    	return false;
    });
});

function change_alias(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    str = str.trim(); 
    return str;
}
