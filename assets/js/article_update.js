$(document).ready(function () {
    CKEDITOR.replace('ArticleContent', {
        language: 'vi',
        height: 450 
    });
    CKEDITOR.replace('ArticleLead', {
        language: 'vi',
        toolbar : 'ShortToolbar',
        height: 150
    });
    $('input.datetimepicker').datetimepicker({
        format: 'dd/mm/yyyy hh:ii'
    }).on('changeDate', function(e){
        $(this).datetimepicker('hide');
    });
    $('input#isPublic').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    }).on('ifToggled', function (e) {
        if (e.currentTarget.checked) $('#divStudent').fadeIn();
        else $('#divStudent').fadeOut();
    });
    var articleId = parseInt($('input#articleId').val());
    if(articleId == 0) $('select#articleStatusId option[value="4"]').hide();
    $('#articleForm').submit(function(){
        var articleTitle = $('input#articleTitle').val().trim();
        if(articleTitle == ''){
            showNotification('Tiêu đề không được bỏ trống', 0);
            $('input#articleTitle').focus();
            return false;
        }
        var articleContent = CKEDITOR.instances['ArticleContent'].getData();
        if(!checkEmptyEditor(articleContent)){
            showNotification('Nội dung không được bỏ trống', 0);
            return false;
        }
        var isPublic = $("input#isPublic").parent().hasClass('checked') ? 1 : 2;
        var userIds = JSON.stringify($('select#userId').val());
        if(isPublic == 1 && (userIds == null || userIds == 'null')){
            showNotification('Danh sách học viên không để trồng', 0);
            $('select#userId').focus();
            return false;
        }
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: {
                ArticleId: articleId,
                ArticleTitle: articleTitle,
                ArticleSlug: '',
                ArticleLead: CKEDITOR.instances['ArticleLead'].getData(),
                ArticleContent: articleContent,
                ArticleTypeId: $('input#articleTypeId').val(),
                ArticleStatusId: $('select#articleStatusId').val(),
                ArticleImage: '',
                PublishDateTime: $('input#publishDateTime').val().trim(),
                AutoPublishDateTime: $('input#autoPublishDateTime').val().trim(),
                IsPublic: isPublic,
                UserIds: userIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1 && articleId == 0) redirect(false, $('a#articleListUrl').attr('href'));
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
});

/*/function checkAutoPublishDateTime(autoPublishDateTime){
    if(autoPublishDateTime != ''){
        var parts = autoPublishDateTime.split(' ');
        if(parts.length == 2){
            var days = parts[0].split('/');
            var times = parts[1].split(':');
            if(days.length == 3 && times.length == 2){
                var now = new Date();
                var date = now.getDate();
                var month = now.getMonth() + 1;
                var year = now.getFullYear();
                var hour = now.getHours();
                var minute = now.getMinutes();
            }
            else{
                showNotification('Định dạng ngày không đúng', 0);
                $('input#autoPublishDateTime').focus();
                return false;
            }
        }
        else{
            showNotification('Định dạng ngày không đúng', 0);
            $('input#autoPublishDateTime').focus();
            return false;
        }
    }
    return true;
}*/