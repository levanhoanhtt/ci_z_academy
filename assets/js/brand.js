$(document).ready(function(){
    var roleLoginId = parseInt($('input#roleLoginId').val());
    if(roleLoginId == 2) $('select#itemStatusId').prop("disabled", true);
    $("#tbodyBrand").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#brandId').val(id);
        $('input#brandName').val($('td#brandName_' + id).text());
        $('select#itemStatusId').val($('input#itemStatusId_' + id).val());
        $('input#userId').val($('input#userId_' + id).val());
        if(roleLoginId == 1){
            $('td#fullName').html($('td#fullName_' + id).html());
            $('td#phoneNumber').html($('td#phoneNumber_' + id).html());
        }
        scrollTo('input#brandName');
        return false;
    }).on("click", "a.link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        return false;
    }).on('click', 'a.link_status', function(){
        changeStatus($(this).attr('data-id'), $(this).attr('data-status'));
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#brandForm').trigger("reset");
        if(roleLoginId == 1) $('td#fullName, td#phoneNumber').html('');
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#brandForm')) {
            var form = $('#brandForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        if(roleLoginId == 1) $('td#fullName, td#phoneNumber').html('');
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="brand_' + data.BrandId + '">';
                            html += '<td id="brandName_' + data.BrandId + '">' + data.BrandName + '</td>';
                            html += '<td id="statusName_' + data.BrandId + '" class="text-center">' + data.StatusName + '</td>';
                            html += '<td class="actions text-center">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.BrandId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.BrandId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="itemStatusId_' + data.BrandId + '" value="' + data.ItemStatusId + '">' +
                                '<input type="text" hidden="hidden" id="userId_' + data.BrandId + '" value="' + data.UserId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyBrand').prepend(html);
                        }
                        else{
                            $('td#brandName_' + data.BrandId).text(data.BrandName);
                            $('td#statusName_' + data.BrandId).html(data.StatusName);
                            $('input#itemStatusId_' + data.BrandId).val(data.ItemStatusId);
                            $('input#userId_' + data.BrandId).val(data.UserId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});

function changeStatus(brandId, itemStatusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            BrandId: brandId,
            ItemStatusId: itemStatusId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                var tr = $('tr#brand_' + brandId);
                if(itemStatusId == 0) tr.remove();
                else{
                    $('td#statusName_' + brandId).html(json.data.StatusName);
                    if(itemStatusId == 2 || itemStatusId == 3) tr.find('a.link_status').attr('data-status', itemStatusId == 2 ? 3 : 2).text(itemStatusId == 2 ? 'Ngừng kinh doanh' : 'Kinh doanh');
                }
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}