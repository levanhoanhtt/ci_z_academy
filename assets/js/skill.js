$(document).ready(function(){
    $("#tbodySkill").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#skillId').val(id);
        $('input#skillName').val($('td#skillName_' + id).text());
        $('select#skillTypeId').val($('input#skillTypeId_' + id).val());
        scrollTo('input#skillName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteSkillUrl').val(),
                data: {
                    SkillId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#skill_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#skillForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#skillForm')) {
            var form = $('#skillForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="skill_' + data.SkillId + '">';
                            html += '<td id="skillName_' + data.SkillId + '">' + data.SkillName + '</td>';
                            html += '<td id="skillTypeName_' + data.SkillId + '">' + data.SkillTypeName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.SkillId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.SkillId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="skillTypeId_' + data.SkillId + '" value="' + data.SkillTypeId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodySkill').prepend(html);
                        }
                        else{
                            $('td#skillName_' + data.SkillId).text(data.SkillName);
                            $('td#skillTypeName_' + data.SkillId).html(data.SkillTypeName);
                            $('input#skillTypeId_' + + data.SkillId).val(data.SkillTypeId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});