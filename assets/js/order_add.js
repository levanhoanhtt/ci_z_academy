$(document).ready(function(){
    $('#tbodyOrder').on('click', '.aOrder', function(){
        var id = $(this).attr('data-id');
        $('input#productId').val(id);
        var tr = $(this).parent().parent();
        $('input#productName').val(tr.find('.tdProductName').text());
        $('input#brandName').val(tr.find('.tdBrandName').text());
        $('input#seller').val(tr.find('.tdSeller').text());
        $('input#promotionCode, input#password, input#2FACode, input#address, input#comment').val('');
        $('input#cost, input#sumCost').val(tr.find('.spanCost').text());
        $('input#quantity').val('1');        
        $('input#sellerId').val($('input#sellerId_' + id).val());
        $('#orderModal').modal('show');
        return false;
    });
    $('#orderModal').on('keyup', 'input#quantity', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var quantity = replaceCost(value, true);
        value = quantity * replaceCost($('input#cost').val(), true);
        var discountCost = parseInt($('input#discountCost').val());
        if(discountCost > 0) value = value - quantity * discountCost;
        $('input#sumCost').val(formatDecimal(value.toString()));
    }).on('keydown', 'input#quantity', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    }).on('blur', 'input#promotionCode', function(){
        var promotionCode = $(this).val();
        $.ajax({
            type: "POST",
            url: $('input#checkPromotionCodeUrl').val(),
            data: {
                PromotionCode: promotionCode,
                ProductId: $('input#productId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    $('input#promotionCodeId').val(data.PromotionCodeId);
                    $('input#discountCost').val(data.DiscountCost);
                    var discountCost = parseInt(data.DiscountCost);
                    if(discountCost > 0){
                        var quantity = replaceCost($('input#quantity').val(), true);
                        var sumCost = quantity * (replaceCost($('input#cost').val(), true) - discountCost);
                        $('input#sumCost').val(formatDecimal(sumCost.toString()));
                    }
                    else showNotificationPromotion('Mã ưu đãi không tồn tại hoặc đã hết hạn');
                }
            },
            error: function (response) {
                showNotificationPromotion('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    $('#btnPayOrder').click(function(){
        if(validateEmpty('#orderModal')){
            var quantity = replaceCost($('input#quantity').val(), true);
            if(quantity > 0){
                var btn = $(this);
                btn.prop("disabled", true);
                $.ajax({
                    type: "POST",
                    url: $('input#updateOrderUrl').val(),
                    data: {
                        ProductId: $('input#productId').val().trim(),
                        SellerId: $("input#sellerId").val().trim(),
                        Quantity: quantity,
                        Cost: $("input#cost").val(),
                        Address: $("input#address").val().trim(),
                        Comment: $("input#comment").val(),
                        PromotionCode: $("input#promotionCode").val().trim(),
                        DiscountCost: $("input#discountCost").val(),

                        PromotionCodeId: $('input#promotionCodeId').val(),

                        UserPass: $('input#password').val().trim(),
                        UserCode: $('input#userCode').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) $('#orderModal').modal('hide');
                        btn.prop("disabled", false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        btn.prop("disabled", false);
                    }
                });
            }
            else showNotification('Số lượng phải lớn hơn 0', 0);
        }
    });

    var productId = parseInt($("input#productBuyId").val());
    if(productId > 0 && $('#tbodyOrder tr').length == 1) $('#tbodyOrder').find('.aOrder').trigger('click');
});

function showNotificationPromotion(msg){
    $('#divPromotionError').text(msg).show();
    setTimeout(function(){
        $('#divPromotionError').text('').hide();
    }, 10000);
}