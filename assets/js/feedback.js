$(document).ready(function() {
    $('#tbodyFeedback').on('click', 'a.link_edit', function(){
        $.ajax({
            type: "POST",
            url: $('input#getFeedbackUrl').val(),
            data: {
                FeedbackId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    $('input#feedbackId').val(json.data.FeedbackId);
                    $('input#feedbackTitle2').val(json.data.Title);
                    $('#feedbackQuestion2').val(json.data.Question);
                    $('#feedbackReply2').val(json.data.Reply);
                    $('#modalReplyFeedback').modal('show');
                    if(json.data.FeedbackStatusId == 2){
                        $('#btnReplyFeedback').hide();
                        $('#feedbackReply2').prop("disabled", true);
                    }
                    else{
                        $('#btnReplyFeedback').show();
                        $('#feedbackReply2').prop("disabled", false);
                    }
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
    $('#btnReplyFeedback').click(function(){
        var reply = $('#feedbackReply2').val().trim();
        if(reply == ''){
            showNotification('Bạn chưa trả lời hỗ trợ', 0);
            return false;
        }
        var btn = $(this);
        btn.prop("disabled", true);
        $.ajax({
            type: "POST",
            url: $('input#updateFeedbackUrl').val(),
            data: {
                FeedbackId: $('input#feedbackId').val(),
                Title: $('input#feedbackTitle2').val(),
                Question: $('#feedbackQuestion2').val(),
                Reply: reply,
                FeedbackStatusId: 2
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
        return false;
    });
    $('#btnAddFeedback').click(function(){
        var title = $('input#feedbackTitle1').val().trim();
        var question = $('#feedbackQuestion1').val().trim();
        if(title == ''){
            showNotification('Tiêu đề không được bỏ trồng', 0);
            $('input#feedbackTitle1').focus();
            return false;
        }
        if(question == ''){
            showNotification('Nội dung không được bỏ trồng', 0);
            $('input#feedbackQuestion1').focus();
            return false;
        }
        var btn = $(this);
        btn.prop("disabled", true);
        $.ajax({
            type: "POST",
            url: $('input#updateFeedbackUrl').val(),
            data: {
                FeedbackId: 0,
                Title: title,
                Question: question,
                Reply: '',
                FeedbackStatusId: 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
        return false;
    });
});