$(document).ready(function(){
    $('#enable2fa').click(function () {
        $('.div2FA').show()
    });
    $('#disable2fa').click(function () {
        $('.div2FA').hide()
    });
    $('#btn2FA').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#checkCode2FaUrl').val(),
            data: {
                IsEnabled2FA: $('input[name="enable2fa"]:checked').val(),
                UserCode: $('input#userCode').val().trim(),
                SecretCode: $('input#secretCode').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $('input#userCode').val('');
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
        return false;
    });
});