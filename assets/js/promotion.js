$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#productId').change(function(){
        var brandId = $(this).find('option[value="' + $(this).val() + '"]').attr('data-id');
        $('select#productBrandId').val(brandId);
        $('input#brandId').val(brandId);
    });
    var roleLoginId = parseInt($('input#roleLoginId').val());
    $("#tbodyPromotion").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#promotionId').val(id);
        $('#productId').val($('input#productId_' + id).val());
        $('select#productBrandId, input#brandId').val($('input#brandId_' + id).val());
        $('input#discountCost').val($('td#discountCost_' + id).text()).prop("disabled", true);
        $('input#quantity').val($('td#quantity_' + id).text()).prop("disabled", true);
        $('input#beginDate').val($('td#beginDate_' + id).text());
        $('input#endDate').val($('td#endDate_' + id).text());
        $('input#userId').val($('input#userId_' + id).val());
        if(roleLoginId == 1){
            $('td#fullName').html($('td#fullName_' + id).html());
            $('td#phoneNumber').html($('td#phoneNumber_' + id).html());
            $('td#productName').html($('td#productName_' + id).html());
            $('td#brandName').html($('td#brandName_' + id).html());
        }
        else $('#productId').prop("disabled", true);
        scrollTo('input#discountCost');
        return false;
    }).on("click", "a.link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')){
            var promotionId = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deletePromotionUrl').val(),
                data: {
                    PromotionId: promotionId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('tr#promotion_' + promotionId).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    }).on('keyup', 'input.cost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keydown', 'input.cost', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });
    $('a#link_cancel').click(function(){
        $('#promotionForm').trigger("reset");
        if(roleLoginId == 1) $('td#fullName, td#phoneNumber, td#productName, td#brandName').html('');
        else $('#productId').prop("disabled", false);
        $('input#quantity, input#discountCost').prop("disabled", false);
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#promotionForm')) {
            var discountCost = replaceCost($('input#discountCost').val(), true);
            var productId = $('#productId').val();
            if(roleLoginId == 2){
                if(discountCost > parseInt($('input#price_' + productId).val())){
                    showNotification('Số tiền giảm không được lớn hơn giá sản phẩm', 0);
                    return false;
                }
            }
            var form = $('#promotionForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: {
                    PromotionId: $('input#promotionId').val(),
                    UserId: $('input#userId').val(),
                    ProductId: productId,
                    BrandId: $('input#brandId').val(),
                    Quantity: replaceCost($('input#quantity').val(), true),
                    DiscountCost: discountCost,
                    BeginDate: $('input#beginDate').val().trim(),
                    EndDate: $('input#endDate').val().trim()
                }, ///form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        if(roleLoginId == 1) $('td#fullName, td#phoneNumber, td#productName, td#brandName').html('');
                        else $('#productId').prop("disabled", false);
                        $('input#quantity, input#discountCost').prop("disabled", false);
                        var data = json.data;
                        var productName = $('select#productId option[value="' + data.ProductId + '"]').text();
                        var brandName = $('select#productBrandId option[value="' + data.BrandId + '"]').text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="promotion_' + data.PromotionId + '">';
                            html += '<td id="productName_' + data.PromotionId + '">' + productName + '</td>';
                            html += '<td id="brandName_' + data.PromotionId + '">' + brandName + '</td>';
                            html += '<td id="discountCost_' + data.PromotionId + '">' + formatDecimal(data.DiscountCost.toString()) + '</td>';
                            html += '<td id="quantity_' + data.PromotionId + '">' + formatDecimal(data.Quantity.toString()) + '</td>';
                            html += '<td id="beginDate_' + data.PromotionId + '">' + data.BeginDate + '</td>';
                            html += '<td id="endDate_' + data.PromotionId + '">' + data.EndDate + '</td>';
                            html += '<td class="actions text-center">' +
                                '<a href="' + $('input#downloadPromotionUrl').val() + '/' + data.PromotionId + '" target="_blank" title="Tải về"><i class="fa fa-download"></i></a>' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.PromotionId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.PromotionId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="userId_' + data.PromotionId + '" value="' + data.UserId + '">' +
                                '<input type="text" hidden="hidden" id="productId_' + data.PromotionId + '" value="' + data.ProductId + '">' +
                                '<input type="text" hidden="hidden" id="brandId_' + data.PromotionId + '" value="' + data.BrandId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyPromotion').prepend(html);
                        }
                        else{
                            $('td#productName_' + data.PromotionId).text(productName);
                            $('td#brandName_' + data.PromotionId).text(brandName);
                            $('td#discountCost_' + data.PromotionId).text(formatDecimal(data.DiscountCost.toString()));
                            $('td#quantity_' + data.PromotionId).text(formatDecimal(data.Quantity.toString()));
                            $('td#beginDate_' + data.PromotionId).text(data.BeginDate);
                            $('td#endDate_' + data.PromotionId).text(data.EndDate);
                            $('input#userId_' + data.PromotionId).val(data.UserId);
                            $('input#productId_' + data.PromotionId).val(data.ProductId);
                            $('input#brandId_' + data.PromotionId).val(data.BrandId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});