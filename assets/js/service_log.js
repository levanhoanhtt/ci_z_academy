$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var roleId = parseInt($('input#roleId').val());
    $('#tbodyUserService').on('click', '.link_delete', function(){
        var $this = $(this);
        var id = $this.attr('data-id');
        var serviceId = $('input#serviceId_' + id).val();
        var userId = $('input#userId_' + id).val();
        if(roleId == 2){
            if(confirm('Bạn có thực sự muốn hủy ?')) cancelStatus(id, serviceId, userId, '', roleId);
        }
        else{
            $("#btnCancelService").attr('data-id', id).attr('data-service', serviceId).attr('data-user', userId);
            $("#modalCancelService").modal('show');
        }
        return false;
    });
    if(roleId == 1) {
        $('#btnCancelService').click(function () {
            var btn = $(this);
            var id = btn.attr('data-id');
            var serviceId = btn.attr('data-service');
            var userId = btn.attr('data-user');
            var comment = $('#comment1').val().trim();
            if (comment == '') {
                showNotification("Lý do không được bỏ trổng", 0);
                $('#comment1').focus();
            }
            else {
                btn.prop("disabled", true);
                cancelStatus(id, serviceId, userId, comment, roleId);
            }
        });

        $("#tbodyUserService").on('click', '.link_edit', function(){
            var tr = $(this).parent().parent();
            var id = $(this).attr("data-id");
            var isRequireCarNumber = $(this).attr('data-car');
            var btn = $('#btnUpdateService');
            btn.attr('data-id', id).attr('data-type', $(this).attr('data-type')).attr('data-user', $('input#userId_' + id).val()).attr('data-service', $('input#serviceId_' + id).val()).attr('data-car', isRequireCarNumber);
            $('#spanStudentName').text(tr.find('td:eq(1)').text());
            $('#spanStudentPhone').text(tr.find('td:eq(2)').text());
            var td = tr.find('td:eq(3)');
            $("#spanServiceName").text(td.find('span.serviceName').text());
            $("#spanServiceDesc").text(td.find('span.serviceDesc').text());
            $("#spanServiceCost").text(td.find('span.serviceCost').text());
            $("#spanServiceTypeName").text(td.find('span.serviceTypeName').text());
            $("input#paidCost").val(td.find('span.paidCost').text());
            $("input#beginDate").val(tr.find('td:eq(4)').text());
            $("input#endDate").val(tr.find('td:eq(5)').text());
            $("select#serviceStatusId2").val($(this).attr("data-status"));
            $('input#carNumber, input#comment2').val('');
            td = tr.find('td:eq(7)');
            if(isRequireCarNumber == '2'){
                if(td.find('.spanCarNumber').length > 0) $('input#carNumber').val(td.find('.spanCarNumber').text().trim());
                if(td.find('.spanComment').length > 0) $('input#comment2').val(td.find('.spanComment').text().trim());
                $('#divCarNumber').show();
            }
            else{
                $("input#comment2").val(td.text().trim());
                $('#divCarNumber').hide();
            }
            $("#modalRegisterService").modal('show');
        });

        $('input#beginDate, input#endDate').change(function(){
            $('input#paidCost').val($('#spanServiceCost').text());
            var beginDate = $('input#beginDate').val().trim();
            if(beginDate != ''){
                var beginDates = beginDate.split('/');
                if(beginDates.length != 3) return false;
                beginDates[0] = parseInt(beginDates[0]);
                beginDates[1] = parseInt(beginDates[1]);
                beginDates[2] = parseInt(beginDates[2]);
                if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0) return false;
                var serviceTypeId = parseInt($('#btnUpdateService').attr('data-type'));
                var endDate = $('input#endDate').val().trim();
                if(endDate != ''){
                    var endDates = endDate.split('/');
                    if(endDates.length != 3) return false;
                    endDates[0] = parseInt(endDates[0]);
                    endDates[1] = parseInt(endDates[1]);
                    endDates[2] = parseInt(endDates[2]);
                    if (endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0) return false;
                    timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function (servicePeriod) {
                        if (servicePeriod > 0) {
                            var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                            $('input#paidCost').val(formatDecimal(paidCost.toString()));
                        }
                    });
                }
                else{
                    var now = new Date();
                    timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                        if (servicePeriod > 0) {
                            var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                            $('input#paidCost').val(formatDecimal(paidCost.toString()));
                        }
                    });
                }
            }
        });

        $('#btnUpdateService').click(function(){
            var btn = $(this);
            var inputBeginDate = $('input#beginDate');
            var inputEndDate = $('input#endDate');
            var beginDate = inputBeginDate.val().trim();
            if(beginDate == ''){
                showNotification('Ngày bắt đầu không được bỏ trống', 0);
                inputBeginDate.focus();
                return false;
            }
            var beginDates = beginDate.split('/');
            if(beginDates.length != 3){
                showNotification('Ngày bắt đầu không đúng định dạng', 0);
                inputBeginDate.focus();
                return false;
            }
            beginDates[0] = parseInt(beginDates[0]);
            beginDates[1] = parseInt(beginDates[1]);
            beginDates[2] = parseInt(beginDates[2]);
            if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0){
                showNotification('Ngày bắt đầu không đúng định dạng', 0);
                inputBeginDate.focus();
                return false;
            }
            var serviceTypeId = parseInt(btn.attr('data-type'));
            var serviceCost = replaceCost($('#spanServiceCost').text(), true);
            var endDate = inputEndDate.val().trim();
            if(endDate != ''){
                var endDates = endDate.split('/');
                if(endDates.length != 3){
                    showNotification('Ngày kết thúc không đúng định dạng', 0);
                    inputEndDate.focus();
                    return false;
                }
                endDates[0] = parseInt(endDates[0]);
                endDates[1] = parseInt(endDates[1]);
                endDates[2] = parseInt(endDates[2]);
                if(endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0){
                    showNotification('Ngày kết thúc không đúng định dạng', 0);
                    inputEndDate.focus();
                    return false;
                }
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function(servicePeriod){
                    if(servicePeriod > 0){
                        var paidCost = servicePeriod * serviceCost;
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                    else{
                        showNotification('Ngày kết thúc phải lớn hơn ngày bắt đầu', 0);
                        inputEndDate.focus();
                        $('input#paidCost').val($('#spanServiceCost').text());
                    }
                });
            }
            else{
                if(serviceTypeId == 1){
                    showNotification('Dịch vụ này bắt buộc phải có ngày kết thúc', 0);
                    inputEndDate.focus();
                    return false;
                }
                var now = new Date();
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                    if (servicePeriod > 0) {
                        var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                });
            }
            var carNumber = '';
            if(btn.attr('data-car') == '2'){
                carNumber = $('input#carNumber').val().trim();
                if(carNumber == ''){
                    showNotification('Biển số không được bỏ trống', 0);
                    return false;
                }
            }
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateRegisterUrl').val(),
                data: {
                    UserServiceId: btn.attr('data-id'),
                    UserId: btn.attr('data-user'),
                    ServiceId: btn.attr('data-service'),
                    BeginDate: beginDate,
                    EndDate: endDate,
                    ServiceStatusId: $('select#serviceStatusId2').val(),
                    Comment: $('input#comment2').val().trim(),
                    CarNumber: carNumber,
                    CommentUpdate: $('input#commentTmp').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
            return false;
        });
    }
});

function cancelStatus(id, serviceId, userId, comment, roleId){
    $.ajax({
        type: "POST",
        url: $('input#cancelServiceUrl').val(),
        data: {
            UserServiceId: id,
            ServiceId: serviceId,
            UserId: userId,
            Comment: comment
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                var tr = $('tr#userService_' + id);
                tr.find('.tdEndDate').text(json.data.EndDate);
                tr.find('.tdStatusName').html(json.data.StatusName);
                tr.find('.link_delete').remove();
                if(roleId == 1) $("#modalCancelService").modal('hide');
            }
            $('#btnCancelService').prop("disabled", false);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            $('#btnCancelService').prop("disabled", false);
        }
    });
    return false;
}

function timeDiff(beginDate, endDate, serviceTypeId, func){
    $.ajax({
        type: "POST",
        url: $('input#getTimeDiffUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate,
            ServiceTypeId: serviceTypeId
        },
        success: function (response) {
            func(parseInt(response));
        },
        error: function (response) {
            func(0);
        }
    });
}