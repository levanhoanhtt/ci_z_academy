$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#tbodyWithdrawal').on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#withdrawalId').val(id);
        $('select#withdrawalStatusIdUpdate option').show();
        $('select#withdrawalStatusIdUpdate').val('0');
        $('select#withdrawalStatusIdUpdate option[value="' + $('input#withdrawalStatusId_' + id).val() + '"]').hide();
        $('#modalWithdrawalStatus').modal('show');
        return false;
    });
    $('#btnUpdateStatus').click(function(){
        var withdrawalStatusId = parseInt($('select#withdrawalStatusIdUpdate').val());
        if(withdrawalStatusId > 0) {
            var withdrawalId = $('input#withdrawalId').val();
            var btn = $(this);
            btn.prop("disabled", true);
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    WithdrawalId: withdrawalId,
                    WithdrawalStatusId: withdrawalStatusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        var tr = $('#withdrawal_' + withdrawalId);
                        tr.find('.tdStatus').html(json.data.StatusName);
                        if(withdrawalStatusId > 2){
                            tr.find('td.actions').html('');
                            if(withdrawalStatusId == 3){
                                var spanCost = $('#spanWithdrawalCost');
                                var cost = replaceCost(spanCost.text(), true) + replaceCost(tr.find('.spanWithdrawalCost').text(), true);
                                spanCost.text(formatDecimal(cost.toString()));
                                spanCost = $('#spanReceiveCost');
                                cost = replaceCost(spanCost.text(), true) + replaceCost(tr.find('.spanReceiveCost').text(), true);
                                spanCost.text(formatDecimal(cost.toString()));
                            }
                        }
                        $('#modalWithdrawalStatus').modal('hide');
                    }
                    btn.prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop("disabled", false);
                }
            });
        }
        else showNotification('Vui lòng chọn trạng thái rút tiền', 0);
    });
});