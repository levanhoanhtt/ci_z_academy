$(document).ready(function(){
    var roleLoginId = parseInt($('input#roleLoginId').val());
    if(roleLoginId == 2) $('select#itemStatusId').prop("disabled", true);
    $("#tbodyProduct").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productId').val(id);
        $('input#productName').val($('td#productName_' + id).text());
        var brandId = $('input#brandId_' + id).val();
        $('select#productBrandId').val(brandId);
        $('input#price').val($('td#price_' + id).text());
        $('td#shortLink').text($('td#shortLink_' + id).text());
        $('select#itemStatusId').val($('input#itemStatusId_' + id).val());
        $('input#userId').val($('input#userId_' + id).val());
        if(roleLoginId == 1){
            $('td#fullName').html($('td#fullName_' + id).html());
            $('td#phoneNumber').html($('td#phoneNumber_' + id).html());
            $('select#productBrandId option').hide();
            $('select#productBrandId option[value="0"]').show();
            $('select#productBrandId option[value="' + brandId + '"]').show();
        }
        scrollTo('input#productName');
        return false;
    }).on("click", "a.link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        return false;
    }).on('click', 'a.link_status', function(){
        changeStatus($(this).attr('data-id'), $(this).attr('data-status'));
        return false;
    }).on('keyup', 'input#price', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keydown', 'input#price', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });
    $('a#link_cancel').click(function(){
        $('#productForm').trigger("reset");
        $('td#shortLink').html('');
        if(roleLoginId == 1) $('td#fullName, td#phoneNumber').html('');
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productForm')) {
            var form = $('#productForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        $('td#shortLink').html('');
                        if(roleLoginId == 1) $('td#fullName, td#phoneNumber').html('');
                        var data = json.data;
                        var brandName = $('#productBrandId option[value="' + data.BrandId + '"]').text();
                        if(data.IsAdd == 1){
                            var html = '<tr id="product_' + data.ProductId + '">';
                            html += '<td id="productName_' + data.ProductId + '">' + data.ProductName + '</td>';
                            html += '<td id="brandName_' + data.ProductId + '">' + brandName + '</td>';
                            html += '<td id="price_' + data.ProductId + '">' + formatDecimal(data.Price.toString()) + '</td>';
                            html += '<td id="shortLink_' + data.ProductId + '">' + data.PaymentLink + '</td>';
                            html += '<td id="statusName_' + data.ProductId + '" class="text-center">' + data.StatusName + '</td>';
                            html += '<td class="actions text-center">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="itemStatusId_' + data.ProductId + '" value="' + data.ItemStatusId + '">' +
                                '<input type="text" hidden="hidden" id="userId_' + data.ProductId + '" value="' + data.UserId + '">' +
                                '<input type="text" hidden="hidden" id="brandId_' + data.ProductId + '" value="' + data.BrandId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyProduct').prepend(html);
                        }
                        else{
                            $('td#productName_' + data.ProductId).text(data.ProductName);
                            $('td#brandName_' + data.ProductId).text(brandName);
                            $('td#price_' + data.ProductId).text(formatDecimal(data.Price.toString()));
                            $('td#shortLink_' + data.ProductId).text(data.PaymentLink);
                            $('td#statusName_' + data.ProductId).html(data.StatusName);
                            $('input#itemStatusId_' + data.ProductId).val(data.ItemStatusId);
                            $('input#userId_' + data.ProductId).val(data.UserId);
                            $('input#brandId_' + data.ProductId).val(data.BrandId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});

function changeStatus(productId, itemStatusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            ProductId: productId,
            ItemStatusId: itemStatusId,
            BrandId: $('input#brandId_' + productId).val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1){
                var tr = $('tr#product_' + productId);
                if(itemStatusId == 0) tr.remove();
                else{
                    $('td#statusName_' + productId).html(json.data.StatusName);
                    if(itemStatusId == 2 || itemStatusId == 3) tr.find('a.link_status').attr('data-status', itemStatusId == 2 ? 3 : 2).text(itemStatusId == 2 ? 'Ngừng kinh doanh' : 'Kinh doanh');
                }
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}