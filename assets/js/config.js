$(document).ready(function() {
	$('.datepicker').datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
	});
	$('body').on('keyup', 'input.input-number', function(){
		var value = $(this).val();
		$(this).val(formatDecimal(value));
	});
	$('.change_config').click(function(event) {
		var _this = $(this);
		var parent = $(this).parents('.item');
		var input = $(parent).find('input');
		var id = $(input).attr('data-id');
		var value = 0;
		if(id == '5') value = replaceCost(input.val(), false);
		else value = replaceCost(input.val(), true);
		if(value == 0){
			showNotification('Phải nhập số lớn hơn 0 hoặc nhỏ hơn 0', 0);
			input.focus();
			return false;
		}
		if(id == '7'){
			if(value != 10 && value != 100 && value != 1000){
				showNotification('Chỉ chấp nhận 10, 100 hoặc 1000', 0);
				input.focus();
				return false;
			}
		}
		else if(id == '9'){
			if(value > 100){
				showNotification('Không được vượt quá 100', 0);
				input.focus();
				return false;
			}
		}
		var data = {};
		data[id] = value;
		_this.prop("disabled", true);
		$.ajax({
		    url: $('input#updateConfigUrl').val(),
			type: 'POST',
			dataType: 'json',
			data: {data},
		    success: function (json) {
		    	_this.prop("disabled", false);
				showNotification(json.message, json.code);
				if(json.code == 1) redirect(true, '');
		    },
		    error: function (response) {
		    	_this.prop("disabled", false);
		        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		    }
		});
		return false;
	});
	$('.change_config_text').click(function(event) {
		var _this = $(this);
		var parent = $(this).parents('.item');
		var input = $(parent).find('input');
		var value = input.val().trim();
		if(value == ''){
			showNotification('Xin mới nhập giá trị', 0);
			input.focus();
			return false;
		}
		var data = {};
		data[$(input).attr('data-id')] = value;
		_this.prop("disabled", true);
		$.ajax({
			url: $('input#updateConfigTextUrl').val(),
			type: 'POST',
			dataType: 'json',
			data: {data},
			success: function (json) {
				_this.prop("disabled", false);
				showNotification(json.message, json.code);
			},
			error: function (response) {
				_this.prop("disabled", false);
				showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
			}
		});
		return false;
	});
});