$(document).ready(function() {
	$('.tdSelect select').change(function(){
		var skillLevelId = $(this).val();
		if(skillLevelId != '0') {
			var td = $(this).parent('td');
			$.ajax({
				type: "POST",
				url: $('input#updateUserSkillUrl').val(),
				data: {
					SkillId: td.attr('data-id'),
					SkillLevelId: skillLevelId,
					SkillTypeId: td.attr('data-type')
				},
				success: function (response) {
					var json = $.parseJSON(response);
					showNotification(json.message, json.code);
				},
				error: function (response) {
					showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
				}
			});
		}
		else showNotification('Vui lòng chọn mức độ', 0);
	});

	$('a#link_cancel').click(function(){
		$('#relationshipForm').trigger("reset");
		return false;
	});
	$("#tbodyRelationship").on("click", "a.link_edit", function(){
		var id = $(this).attr('data-id');
		$('input#relationshipId').val(id);
		$('select#fieldId').val($('input#fieldId_' + id).val());
		$('input#comment').val($('td#comment_' + id).text());
		scrollTo('input#comment');
		return false;
	}).on("click", "a.link_delete", function(){
		if (confirm('Bạn có thực sự muốn xóa ?')) {
			var id = $(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: $('input#deleteRelationshipUrl').val(),
				data: {
					RelationshipId: id
				},
				success: function (response) {
					var json = $.parseJSON(response);
					showNotification(json.message, json.code);
					if (json.code == 1) $('tr#relationship_' + id).remove();
				},
				error: function (response) {
					showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
				}
			});
		}
		return false;
	});
	$('a#link_update').click(function(){
		if (validateEmpty('#relationshipForm')) {
			var form = $('#relationshipForm');
			$.ajax({
				type: "POST",
				url: form.attr('action'),
				data: form.serialize(),
				success: function (response) {
					var json = $.parseJSON(response);
					if(json.code == 1){
						form.trigger("reset");
						var data = json.data;
						data.FieldName = $('select#fieldId option[value="' + data.FieldId + '"]').text();
						if(data.IsAdd == 1){
							var html = '<tr id="relationship_' + data.RelationshipId + '">';
							html += '<td class="tdText" id="fieldName_' + data.RelationshipId + '">' + data.FieldName + '</td>';
							html += '<td class="tdText" id="comment_' + data.RelationshipId + '">' + data.Comment + '</td>';
							html += '<td class="actions">' +
								'<a href="javascript:void(0)" class="link_edit" data-id="' + data.RelationshipId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
								'<a href="javascript:void(0)" class="link_delete" data-id="' + data.RelationshipId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
								'<input type="text" hidden="hidden" id="fieldId_' + data.RelationshipId + '" value="' + data.FieldId + '">' +
								'</td>';
							html += '</tr>';
							$('#tbodyRelationship').prepend(html);
						}
						else{
							$('td#fieldName_' + data.RelationshipId).text(data.FieldName);
							$('td#comment_' + data.RelationshipId).html(data.Comment);
							$('input#fieldId_' + + data.RelationshipId).val(data.FieldId);
						}
					}
					showNotification(json.message, json.code);
				},
				error: function (response) {
					showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
				}
			});
		}
		return false;
	});
});