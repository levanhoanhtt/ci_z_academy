$(document).ready(function(){
	$("body").on('keyup', '.cost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keydown', '.cost', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });
	$("button.link_order").click(function(){
		var td = $(this).parent().parent().find('td');
		var priceProduct = td.eq(5).find('input#priceProduct').val();
		$("input#productName").val(td.eq(1).text());
		$("input#brandName").val(td.eq(2).text());
		$("input#seller").val(td.eq(3).text());
		$("input#cost").val(formatDecimal(priceProduct));
		$("input#total").val(formatDecimal(priceProduct));
		$('input#productId').val($(this).attr('data-id'));
		$("input#sellerId").val($(this).attr('seller-id'))
		//var withdrawalCost = replaceCost($('input#withdrawalCost').val(), true);
		$("#orderModal").modal('show');
		
	});

	$("#orderModal").on('keyup', '#promotionCode', function(){
		var quantity = replaceCost($("input#quantity").val());
		var price = parseInt(replaceCost($("input#cost").val()));
		var promotionCode = $(this).val().trim();
		discount(promotionCode,$('input#productId').val().trim(),quantity, price);
	});

	$("#orderModal").on('keyup', '#quantity', function(){
		var quantity = replaceCost($(this).val());
		var price = parseInt(replaceCost($("input#cost").val()));
		var promotionCode = $("input#promotionCode").val().trim();
		if(promotionCode == ""){
			var total = quantity * price;
			$("input#total").val(formatDecimal(total.toString()));
		}else{
			discount(promotionCode,$('input#productId').val().trim(),quantity, price);
		}
	});

	$("#orderModal").on('click', '#btn-pay', function(){
		if(validateEmpty('#orderForm')) {
			var form = $('#orderForm');
			$.ajax({
		        type: "POST",
		        url: form.attr('action'),
		        data: {
		            OrderId: 0 ,
		            ProductId: $('input#productId').val().trim(),
		            SellerId: $("input#sellerId").val().trim(),
		            CustomerId: $("input#userId").val().trim(),
		            Quantity: replaceCost($("input#quantity").val()),
		            Cost: replaceCost($("input#cost").val()),
		            Address: $("input#address").val().trim(),
		            Comment: $("input#comment").val(),
		            SellerOrderStatusId: 1,
		            CustomerOrderStatusId: 1,
		            ZOrderStatusId: 1,
		            PromotionCode: $("input#promotionCode").val().trim(),
		            RefundTime:0 ,
		        },
		        success: function (response) {
		        	console.log(response)
		            var json = $.parseJSON(response);
		            showNotification(json.message, json.code);
		            if (json.code == 1){

		            };
		        },
		        error: function (response) {
		            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
		        }
		    });
		}
		return false;
	})
})

function discount(promotionCode, productId,quantity, price){
	$.ajax({
        type: "POST",
        url: $('input#promotionCodeUrl').val(),
        data: {
            PromotionCode: promotionCode,
            ProductId: productId,
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
            	var total = (quantity * price) - parseInt(json.data.DiscountCost);
				$("input#total").val(formatDecimal(total.toString()));
				$("input#promotionCodeId").val(json.data.PromotionCodeId)
            };
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
    return false;
}