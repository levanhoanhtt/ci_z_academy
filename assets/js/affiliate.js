$(document).ready(function(){
	$('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#aCopyLink').click(function(){
        copyToClipboard($('#bLinkAff').text());
        return false;
    });

    $('#tbodyUser').on('click', '.link_mail', function(){
        $.ajax({
            type: "POST",
            url: $('input#verifyEmailUrl').val(),
            data: {
                UserId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
});

function copyToClipboard(text) {
    if (window.clipboardData && window.clipboardData.setData) {
        // IE specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", text);
    }
    else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        }
        catch (ex) {
            //console.warn("Copy to clipboard failed.", ex);
            return false;
        }
        finally {
            document.body.removeChild(textarea);
        }
    }
}