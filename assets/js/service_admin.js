$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#cbIsRequireCarNumber').bootstrapSwitch({size: 'mini'});

    $('#aAddService').click(function(){
        $('input#serviceId').val('0');
        $('input#serviceName').val('');
        $('#serviceDesc').val('');
        $('input#serviceCost').val('0');
        $('img#preViewUpdate').attr('src', $('input#noImage').val());
        $('#modalUpdateService').modal('show');
        return false;
    });

    $('#tbodyService').on('click', '.link_edit', function(){
        $.ajax({
            type: "POST",
            url: $('input#getServiceDetailUrl').val(),
            data: {
                ServiceId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1){
                    var data = json.data;
                    $('input#serviceId').val(data.ServiceId);
                    $('input#serviceName').val(data.ServiceName);
                    $('#serviceDesc').val(data.ServiceDesc);
                    $('input#serviceCost').val(formatDecimal(data.ServiceCost));
                    $('select#serviceTypeId1').val(data.ServiceTypeId);
                    $('img#preViewUpdate').attr('src', $('input#imagePath').val() + data.ServiceImage);
                    if(data.IsRequireCarNumber == 2) $('#cbIsRequireCarNumber').bootstrapSwitch('state', true);
                    else $('#cbIsRequireCarNumber').bootstrapSwitch('state', false);
                    $('#modalUpdateService').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    }).on("click", ".link_delete", function(){
        if(confirm('Bạn có thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        return false;
    }).on('click', '.link_status', function(){
        var statusId = parseInt($(this).attr('data-status'));
        if(confirm(statusId == 3 ? 'Bạn có muốn dừng dịch vụ này?':'Bạn có muốn kích hoạt lại dịch vụ này?')) changeStatus($(this).attr('data-id'), statusId);
        return false;
    }).on("click", ".link_register", function(){
        var isRequireCarNumber = $(this).attr('data-car');
        $('#btnRegisterService').attr('data-type', $(this).attr('data-type')).attr('data-car', isRequireCarNumber);
        $('input#serviceId').val($(this).attr('data-id'));
        var tr = $(this).parent().parent();
        var serviceCost = tr.find('td:eq(3)').children('span').text();
        $("#spanServiceName").text(tr.find('td:eq(1)').text());
        $("#spanServiceDesc").text(tr.find('td:eq(2)').text());
        $("#spanServiceCost").text(serviceCost);
        $("#spanServiceTypeName").text(tr.find('td:eq(4)').text());
        $('input#paidCost').val(serviceCost);
        $('input#endDate, input#comment, input#carNumber').val('');
        $("select#studentId").val('0').trigger('change');
        if(isRequireCarNumber == '2'){
            $('#divCarNumber').show();
            $('#divServiceComment').removeClass('col-sm-12').addClass('col-sm-8');
        }
        else{
            $('#divCarNumber').hide();
            $('#divServiceComment').removeClass('col-sm-8').addClass('col-sm-12');
        }
        $("#modalRegisterService").modal('show');
    });

    $('input#beginDate, input#endDate').change(function(){
        $('input#paidCost').val($('#spanServiceCost').text());
        var beginDate = $('input#beginDate').val().trim();
        if(beginDate != ''){
            var beginDates = beginDate.split('/');
            if(beginDates.length != 3) return false;
            beginDates[0] = parseInt(beginDates[0]);
            beginDates[1] = parseInt(beginDates[1]);
            beginDates[2] = parseInt(beginDates[2]);
            if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0) return false;
            var serviceTypeId = parseInt($('#btnRegisterService').attr('data-type'));
            var endDate = $('input#endDate').val().trim();
            if(endDate != ''){
                var endDates = endDate.split('/');
                if(endDates.length != 3) return false;
                endDates[0] = parseInt(endDates[0]);
                endDates[1] = parseInt(endDates[1]);
                endDates[2] = parseInt(endDates[2]);
                if (endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0) return false;
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function (servicePeriod) {
                    if (servicePeriod > 0) {
                        var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                });
            }
            else{
                var now = new Date();
                timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                    if (servicePeriod > 0) {
                        var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                        $('input#paidCost').val(formatDecimal(paidCost.toString()));
                    }
                });
            }
        }
    });

    $('#btnRegisterService').click(function(){
        var btn = $(this);
        var inputBeginDate = $('input#beginDate');
        var inputEndDate = $('input#endDate');
        var userId =  $('select#studentId').val().trim();
        var beginDate = inputBeginDate.val().trim();
        if(userId == '0'){
            showNotification('Học viên không được bỏ trống', 0);
            return false;
        }
        if(beginDate == ''){
            showNotification('Ngày bắt đầu không được bỏ trống', 0);
            inputBeginDate.focus();
            return false;
        }
        var beginDates = beginDate.split('/');
        if(beginDates.length != 3){
            showNotification('Ngày bắt đầu không đúng định dạng', 0);
            inputBeginDate.focus();
            return false;
        }
        beginDates[0] = parseInt(beginDates[0]);
        beginDates[1] = parseInt(beginDates[1]);
        beginDates[2] = parseInt(beginDates[2]);
        if(beginDates[0] < 0 || beginDates[0] > 31 || beginDates[1] < 0 || beginDates[1] > 12 || beginDates[2] < 0){
            showNotification('Ngày bắt đầu không đúng định dạng', 0);
            inputBeginDate.focus();
            return false;
        }
        var serviceTypeId = parseInt(btn.attr('data-type'));
        var serviceCost = replaceCost($('#spanServiceCost').text(), true);
        var endDate = inputEndDate.val().trim();
        if(endDate != ''){
            var endDates = endDate.split('/');
            if(endDates.length != 3){
                showNotification('Ngày kết thúc không đúng định dạng', 0);
                inputEndDate.focus();
                return false;
            }
            endDates[0] = parseInt(endDates[0]);
            endDates[1] = parseInt(endDates[1]);
            endDates[2] = parseInt(endDates[2]);
            if(endDates[0] < 0 || endDates[0] > 31 || endDates[1] < 0 || endDates[1] > 12 || endDates[2] < 0){
                showNotification('Ngày kết thúc không đúng định dạng', 0);
                inputEndDate.focus();
                return false;
            }
            timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], endDates[2] + '-' + endDates[1] + '-' + endDates[0], serviceTypeId, function(servicePeriod){
                if(servicePeriod > 0){
                    var paidCost = servicePeriod * serviceCost;
                    $('input#paidCost').val(formatDecimal(paidCost.toString()));
                }
                else{
                    showNotification('Ngày kết thúc phải lớn hơn ngày bắt đầu', 0);
                    inputEndDate.focus();
                    $('input#paidCost').val($('#spanServiceCost').text());
                }
            });
        }
        else{
            if(serviceTypeId == 1){
                showNotification('Dịch vụ này bắt buộc phải có ngày kết thúc', 0);
                inputEndDate.focus();
                return false;
            }
            var now = new Date();
            timeDiff(beginDates[2] + '-' + beginDates[1] + '-' + beginDates[0], now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate(), serviceTypeId, function (servicePeriod) {
                if (servicePeriod > 0) {
                    var paidCost = servicePeriod * replaceCost($('#spanServiceCost').text(), true);
                    $('input#paidCost').val(formatDecimal(paidCost.toString()));
                }
            });
        }
        var carNumber = '';
        if(btn.attr('data-car') == '2'){
            carNumber = $('input#carNumber').val().trim();
            if(carNumber == ''){
                showNotification('Biển số không được bỏ trống', 0);
                return false;
            }
        }
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#registerServiceUrl').val(),
            data: {
                UserId: userId,
                ServiceId: $('input#serviceId').val(),
                ServiceTypeId: serviceTypeId,
                ServiceCost: serviceCost,
                BeginDate: beginDate,
                EndDate: endDate,
                PaidCost: replaceCost($('input#paidCost').val(), true),
                Comment: $('input#comment').val().trim(),
                CarNumber: carNumber
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $('#modalRegisterService').modal('hide');
                btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop('disabled', false);
            }
        });
        return false;
    });

    $('#modalUpdateService').on('keyup', 'input#serviceCost', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    }).on('keydown', 'input#serviceCost', function(e){
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8)) {
            e.preventDefault();
        }
    });

    $('#selectUpdateImage').click(function (e) {
        $('#inputUpdateImage').trigger('click');
    });

    $('#inputUpdateImage').change(function (e) {
        var file = this.files[0];
        var typeFile = file.type.replace('image/', '');
        var whiteList = ['jpeg', 'jpg', 'png', 'bmp'];
        if (whiteList.indexOf(typeFile) === -1) {
            showNotification('Tệp tin phải là ảnh có định dạng , jpeg/jpg/png/bmp', 0);
            return;
        }
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            $('img#preViewUpdate').attr('src', reader.result);
        }, false);
        if (file) reader.readAsDataURL(file);
    });

    $('#btnUpdateService').click(function(){
        var serviceName = $('input#serviceName').val();
        if(serviceName == ''){
            showNotification('Tên dịch vụ không được bỏ trống', 0);
            $('input#serviceName').focus();
            return false;
        }
        var serviceCost = replaceCost($('input#serviceCost').val(), true);
        if(serviceCost <= 0){
            showNotification('Phí dịch vụ không được nhỏ hơn 0', 0);
            $('input#serviceCost').focus();
            return false;
        }
        $('#selectUpdateImage').hide();
        $('#progressUpdateImage').show();
        var btn = $(this);
        btn.prop("disabled", true);
        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        var prb = $('#progressUpdateImage .progress-bar');
                        prb.text(percentComplete + '%');
                        prb.css({
                            width: percentComplete + '%'
                        });
                        if (percentComplete === 100) {
                            setTimeout(function () {
                                prb.text('Tải ảnh lên hoàn thành');
                            }, 1000);
                        }
                    }
                }, false);
                return xhr;
            },
            type: "POST",
            url: $('input#updateServiceUrl').val(),
            data: {
                ServiceId: $('input#serviceId').val(),
                ServiceName: serviceName,
                ServiceDesc: $('#serviceDesc').val(),
                ServiceCost: serviceCost,
                ServiceTypeId: $('select#serviceTypeId1').val(),
                ServiceImage: $('img#preViewUpdate').attr('src'),
                IsRequireCarNumber: $('.bootstrap-switch-id-cbIsRequireCarNumber').hasClass('bootstrap-switch-on') ? 2 : 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(true, '');
                else btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
        return false;
    });

});

function timeDiff(beginDate, endDate, serviceTypeId, func){
    $.ajax({
        type: "POST",
        url: $('input#getTimeDiffUrl').val(),
        data: {
            BeginDate: beginDate,
            EndDate: endDate,
            ServiceTypeId: serviceTypeId
        },
        success: function (response) {
            func(parseInt(response));
        },
        error: function (response) {
            func(0);
        }
    });
}

function changeStatus(id, statusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            ServiceId: id,
            StatusId: statusId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
                var tr = $('tr#service_' + id);
                if(statusId == 0) tr.remove();
                else{
                    tr.find('.tdStatusName').html(json.data.StatusName);
                    tr.find('.link_status').attr('data-status', statusId == 3 ? 2 : 3);
                }
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}