$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#tbodyNotification').on('click', '.link_isRead', function(){
        var id = $(this).attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#changeIsReadUrl').val(),
            data: {
                NotificationId: id
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('td#isRead_' + id).html(json.data.StatusName);
                    $('tr#notification_' + id).find('td.actions').html('');
                    var spanNotificationCount = $('#spanNotificationCount');
                    var notificationCount = parseInt(spanNotificationCount.text().trim()) - 1;
                    if(notificationCount > 0) spanNotificationCount.text(notificationCount);
                    else spanNotificationCount.text('0').hide();
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
        return false;
    });
    $('#btnReadAll').click(function(){
        var btn = $(this);
        btn.prop("disabled", true);
        $.ajax({
            type: "POST",
            url: $('input#changeIsReadAllUrl').val(),
            data: {},
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('.tdIsRead').html(json.data.StatusName);
                    $('td.actions').html('');
                    $('#spanNotificationCount').text('0').hide();
                }
                btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                btn.prop("disabled", false);
            }
        });
    });
});