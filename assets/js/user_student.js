$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('.tooltip1').tooltip();
    $('.js-switch').bootstrapSwitch({size: 'mini'});
    $('.cbMember').on('switchChange.bootstrapSwitch', function(event, state) {
        $.ajax({
            type: "POST",
            url: $('input#changeIsMemberUrl').val(),
            data: {
                UserId: $(this).val(),
                IsMember: state ? 2 : 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    $('.cbBlock').on('switchChange.bootstrapSwitch', function(event, state) {
        changeStatus($(this).val(), state ? 3 : 2);
    });
    $('#tbodyUser').on('click', '.link_delete', function(){
        if (confirm('Bạn thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        return false;
    }).on('click', '.link_mail', function(){
        $.ajax({
            type: "POST",
            url: $('input#verifyEmailUrl').val(),
            data: {
                UserId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    /*var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
        new Switchery(html, {size: 'small'});
    });
    var cbMember = document.querySelector('.cbMember');
    cbMember.onchange = function() {
        $.ajax({
            type: "POST",
            url: $('input#changeIsMemberUrl').val(),
            data: {
                UserId: cbMember.value,
                IsMember: cbMember.checked ? 2 : 1
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    };
    var cbBlock = document.querySelector('.cbBlock');
    cbBlock.onchange = function() {
        changeStatus(cbBlock.value, cbBlock.checked ? 3 : 2);
    };*/
});

function changeStatus(userId, statusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            UserId: userId,
            StatusId: statusId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
                if(statusId == 0) $('tr#user_' + userId).remove();
                else if(statusId == 2) $('#tdStatusName_' + userId).html('<span class="label label-success">Đã duyệt</span>');
                else if(statusId == 3) $('#tdStatusName_' + userId).html('<span class="label label-warning">Block</span>');
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}