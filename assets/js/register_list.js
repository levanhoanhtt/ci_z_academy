$(document).ready(function() {
	$('input.iCheck').iCheck({
	    checkboxClass: 'icheckbox_square-blue',
	    radioClass: 'iradio_square-blue',
	    increaseArea: '20%'
	});
	$(document).on('click','.editRegister',function(){
		var id = $(this).attr('data-id');
		var course = $(this).attr('data-course');
		var studentName = $(this).attr('data-name');
		$('#courseIdMd').val(course);
		$('#nameStudentMd').text(studentName);
		$('#RegisterIdMd').val(id);
		$('#changeRegisterModal').modal('show');
		return false;
	});
	$('#btnUpdateRegiter').click(function(event) {
		var id = $('#RegisterIdMd').val();
		var courseId = $('#courseIdMd').val();
		if(isNaN(courseId)){
			showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
			return false;
		}
		var btn = $(this);
		btn.prop("disabled", true);
		var isDelete = 0;
		if($('#deleteRegisterMd').parent().hasClass('checked')) isDelete = 1;
    	$.ajax({
            type: "POST",
            dataType: "json",
            url: $('input#updateRegisterUrl').val(),
          	data: {
          		RegisterId : id,
          		CourseId : courseId,
          		IsDelete : isDelete
          	},
            success: function (response) {
          		var json = response;
          		showNotification(json.message, json.code);
          		if(json.code == 1){
          			$('#changeRegisterModal').modal('hide');
          			$('#deleteRegisterMd').iCheck('uncheck');
					var tr = $('#register_' + id);
          			if(isDelete == 0){
          				tr.find('td').eq(1).text(courseId);
          				tr.find('.editRegister').attr('data-course', courseId);
          			}
          			else tr.remove();
          		}
				btn.prop("disabled", false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
				btn.prop("disabled", false);
            }
        });
	});
	$('.tooltip1').tooltip();
	$('.js-switch').bootstrapSwitch({size: 'mini'});
    $('.cbFinish').on('switchChange.bootstrapSwitch', function(event, state) {
		$.ajax({
            type: "POST",
            dataType: "json",
            url: $('input#updateGraduating').val(),
          	data: {
          		RegisterId :$(this).val(),
				IsFinish: state ? 1 : 0
          	},
            success: function (response) {
          		var json = response;
          		showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
				});
});