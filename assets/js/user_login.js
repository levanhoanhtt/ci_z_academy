$(document).ready(function() {
    var APP_ID = $('input#appFbId').val();
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    window.fbAsyncInit = function() {
        FB.init({
            appId : APP_ID,
            cookie : true,
            xfbml : true,
            version : 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    AccountKit_OnInteractive = function(){
        AccountKit.init(
            {
                appId:APP_ID,
                state: new Date().toString(),
                version:"v1.1",
                fbAppEventsEnabled:true,
                debug:true,
                display: 'modal'
            }
        );
    };

    function loginCallback(response) {
        if(response.status === "PARTIALLY_AUTHENTICATED") {
            $.ajax({
                type: "POST",
                url: $("#verifyPhoneUrl").val(),
                data: {
                    PhoneNumber: $("#phoneNumber").val().trim(),
                },
                success: function(response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('#userForm').trigger('submit');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else if(response.status === "NOT_AUTHENTICATED") showNotification('Bạn chưa xác thực số điện thoại', 0);
        else if(response.status === "BAD_PARAMS") showNotification('Bạn chưa xác thực số điện thoại', 0);
    }

    function smsLogin() {
        var countryCode = '+84';
        var phoneNumber = $("#phoneNumber").val();
        if(phoneNumber.indexOf('0') == 0) phoneNumber  = (phoneNumber.slice(1, 11));
        AccountKit.login(
            'PHONE',
            {countryCode: countryCode, phoneNumber: phoneNumber},
            loginCallback
        );
    }

    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $(document).on('submit','#userForm',function (){
        if(validateEmpty('#userForm')) {
            if(validateEmpty('#userForm')) {
                var phoneInput = $("#phoneNumber");
                if(!validatePhoneNumber(phoneInput)){
                    phoneInput.focus();
                    return false;
                }
                var form = $('#userForm');
                var data = form.serialize();
                form.find('input, button').prop("disabled", true);
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: form.attr('action'),
                    data: data,
                    success: function (response) {
                        var json = response;// $.parseJSON(response);
                        if(json.code != -2 && json.code != -3) showNotification(json.message, json.code);
                        if(json.code == 1){
                            var redirectUrl = getURLParameter('redirectUrl');
                            if(redirectUrl.indexOf('http') == -1) redirectUrl = $('input#dashboardUrl').val();
                            redirect(false, redirectUrl);
                        }
                        else if(json.code == -2){
                            $('.login-box-body, .login-block').hide();
                            $('.login-box').css({"margin-top": "10%", "width": "470px"});
                            $('.register-success').fadeIn();
                        }
                        else if(json.code == -3){
                            $('.login-box-body, .register-success').hide();
                            $('.login-box').css({"margin-top": "10%", "width": "470px"});
                            $('.login-block').fadeIn();
                        }
                        else if(json.code == -4){
                            smsLogin();
                            showNotification(json.message, json.code);
                        }
                        form.find('input, button').prop("disabled", false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        form.find('input, button').prop("disabled", false);
                    }
                });
            }
        }
        return false;
    });
    $(document).on('submit','#checkPhoneForm',function (){
        if(validateEmpty('#checkPhoneForm')) {
            var phoneInput = $("#phoneNumber1");
            if(!validatePhoneNumber(phoneInput)){
                phoneInput.focus();
                return false;
            }
            var form = $('#checkPhoneForm');
            var data = form.serialize();
            form.find('input, button').prop("disabled", true);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: data,
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $("#h4Notice").text(json.message);
                        var data = json.data;
						var studyA = "Chưa học";
						var studyB = "Chưa học";
						var studyC = "Chưa học";
						var IsFinishC = "Chưa đạt";
						var IsFinishB = "Chưa đạt";
						var IsFinishA = "Chưa đạt";
						if(data.CourseTypeId == 3){
							studyA = "Đã học";
							studyB = "Đã học";
							studyC = "Đã học";
							IsFinishC = (data.IsFinish == 1) ? "Đạt" : "Chưa đạt";
						}
						else if (data.CourseTypeId == 2){
							studyA = "Đã học";
							studyB = "Đã học";
							IsFinishB = (data.IsFinish == 1) ? "Đạt" : "Chưa đạt";
						}
						else if (data.CourseTypeId == 1){
							studyA = "Đã học";
							IsFinishA = (data.IsFinish == 1) ? "Đạt" : "Chưa đạt";
						}
                        $('#bFullName').text(data.StudentName);
                        $('#bBirthDay').text(data.BirthDay);
                        $('#bPhoneNumber').text(data.PhoneNumber);
                        $('#bIDCardNumber').text(data.IDCardNumber);
						$('#IsAlpha').text(studyA);
						$('#IsBeta').text(studyB);
						$('#IsGamma').text(studyC);
						$('#IsFinishA').text(IsFinishA);
						$('#IsFinishB').text(IsFinishB);
						$('#IsFinishG').text(IsFinishC);
                        $("#modalRegisterNotice").modal("show");
					}
                    else showNotification(json.message, json.code);
                    form.find('input, button').prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    form.find('input, button').prop("disabled", false);
                }
            });
        }
        return false;
    });
});

function getURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
    return '';
}