$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $("#phoneNumber").blur(function () {
        var phoneInput = $(this);
        if(!validatePhoneNumber(phoneInput)) return false;
        var phone = phoneInput.val().trim();
        if (phone == $('input#yourPhone').val()) {
            $('input#studentName').val($('input#yourName').val());
            $('input#birthDay').val($('input#yourBirthDay').val());
            $('input#iDCardNumber').val($('input#yourIDCardNumber').val());
            if (parseInt($('input#yourYearOld').val()) > 26) {
                $("input#ageType2").prop("checked", true);
                $("input#CourseType1").prop("checked", true);
                setTicketPay();
            }
            else {
                $("input#ageType1").prop("checked", true);
                $("input#CourseType1").prop("checked", true);
                setTicketPay();
            }
            //$('input[name=AgeTypeId]').prop("disabled", true);
        }
        else {
            $.ajax({
                type: "POST",
                url: $('input#getUserByPhoneUrl').val(),
                data: {
                    PhoneNumber: phone
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var data = json.data;
                        $('input#studentName').val(data.FullName);
                        $('input#birthDay').val(data.BirthDay);
                        $('input#iDCardNumber').val(data.IDCardNumber);
                        if (data.YearOld > 26 ) {
                            $("input#ageType2").prop("checked", true);
							$("input#CourseType1").prop("checked", true);
                            setTicketPay();
                        }
                        else {
                            $("input#ageType1").prop("checked", true);
							$("input#CourseType1").prop("checked", true);
                            setTicketPay();
                        }
                        //$('input[name=AgeTypeId]').prop("disabled", true);
                    }
                    //else $('input[name=AgeTypeId]').prop("disabled", false);
                },
                error: function (response) {
                    //$('input[name=AgeTypeId]').prop("disabled", false);
                }
            });
        }
    });
    $('.regiter-submit').click(function() {
        var form = $('#registerForm');
        form.submit(function(e){
            e.preventDefault();
        });
        if(validateEmpty('#registerForm')) {
            var phoneInput = $("#phoneNumber");
            if(!validatePhoneNumber(phoneInput)){
                phoneInput.focus();
                return false;
            }
            var idInput = $('input#iDCardNumber');
            if(!validateIdCartNumber(idInput)){
                idInput.focus();
                return false;
            }
            var data = form.serialize();
            form.find('input, button').prop("disabled", true);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: form.attr('action'),
                data: data,
                success: function (response) {
                    var json = response;
                    if(json.code == 1) {
                        if(json.bonus == 0) $('.congrat-modal h3').hide();
                        else $('.congrat-modal h3').show();
                        $('.congrat-modal').modal('show');
                        $('#registerForm')[0].reset();
                    }
                    else showNotification(json.message, json.code);
                    form.find('input, button').prop("disabled", false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    form.find('input, button').prop("disabled", false);
                }
            });
        }
        return false;
    });

    $('.congrat-modal').on('hidden.bs.modal', function () {
        window.location.reload(true);
    })

});

    $('[name=CourseTypeId]').change(function(){
		 setTicketPay();
	 });
     $('[name=AgeTypeId]').change(function(){
		 setTicketPay();
	 });


//Trung add
function setTicketPay(){
	$.ajax({
        type: "POST",
        url: $('input#getCourse').val(),
		data:{
			Age: $('[name="AgeTypeId"]:radio:checked').val(),
			CourseTypeId: $('[name="CourseTypeId"]:radio:checked').val()
		},
        success: function (response) {
            $('.ticketPay').text(response);
        },
        error: function (response) {
            
        }
    });
}
//