$(document).ready(function() {
    $('#tbodyUser').on('click', '.link_delete', function(){
        if (confirm('Bạn thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        return false;
    });
});

function changeStatus(userId, statusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            UserId: userId,
            StatusId: statusId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1 && statusId == 0) $('tr#user_' + userId).remove();
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}