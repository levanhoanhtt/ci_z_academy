$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#btnProfile').click(function(){
        if(validateEmpty('#divProfile')){
            var phoneInput = $("#phoneNumber");
            if(!validatePhoneNumber(phoneInput)){
                phoneInput.focus();
                return false;
            }
            var idInput = $('input#iDCardNumber');
            if(!validateIdCartNumber(idInput)){
                idInput.focus();
                return false;
            }
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateProfileUrl').val(),
                data: {
                    //UserName: $('input#userName').val().trim(),
                    Email: $('input#email').val().trim(),
                    FullName: $('input#fullName').val().trim(),
                    PhoneNumber: phoneInput.val().trim(),
                    IDCardNumber: idInput.val().trim(),
                    BirthDay: $('input#birthDay').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });

    $('#btnPassword').click(function(){
        if(validateEmpty('#divPass')){
            var newPass = $('input#newPass').val().trim();
            var rePass = $('input#rePass').val().trim();
            if(newPass != rePass){
                showNotification('Mật khẩu mới không trùng');
                return false;
            }
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changePassUrl').val(),
                data: {
                    UserPass: $('input#userPass').val().trim(),
                    NewPass: newPass
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input#userPass').val('');
                        $('input#newPass').val('');
                        $('input#rePass').val('');
                    }
                    btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    btn.prop('disabled', false);
                }
            });
        }
        return false;
    });
});