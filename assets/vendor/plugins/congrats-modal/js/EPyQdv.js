var AnimateCssEvent = (function() {

  var eventNamespace = 'animateCssEvent';

  var addAnimCls = function($elemAnim, effect) {
    $elemAnim.addClass('animated ' + effect);
  }
  var removeAnimCls = function($elemAnim, effect) {
    $elemAnim.removeClass('animated ' + effect);
  }

  var bindEventOne = function(eventType, $elemEvent, $elemAnim, effect) {
    $elemEvent.on(eventType, function() {
      addAnimCls($elemAnim, effect);
      $elemAnim.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        removeAnimCls($elemAnim, effect)
      });
    });
  };

  var bindEventInfinite = function(eventType, $elemEvent, $elemAnim, effect, action) {
    $elemEvent.on(eventType, function() {
      if (action === 'add') {
        addAnimCls($elemAnim, effect + ' infinite');
      } else {
        removeAnimCls($elemAnim, effect + ' infinite');
      }
    });
  };

  return {
    attachEvent: function(eventType, effect, $elemEvent, $elemAnim) {

      $elemAnim = $elemAnim || $elemEvent;

      if (!$elemEvent || !$elemAnim || !effect) {
        throw "AnimateCssEvent.attachEvent: unexpected parameters [$elemEvent, $elemAnim, effect]";
      }

      switch (eventType) {
        case 'click':
          bindEventOne('click.animateCssEvent', $elemEvent, $elemAnim, effect);
          break;
        case 'hover':
          bindEventInfinite('mouseenter.' + eventNamespace, $elemEvent, $elemAnim, effect, 'add');
          bindEventInfinite('mouseleave.' + eventNamespace, $elemEvent, $elemAnim, effect, 'remove');
          break;
        default:
          throw "AnimateCssEvent.attachEvent: unexpected parameter eventType. Should be ['click', 'hover']";
          break;
      }
    },

    dettachEvent: function(eventType, $elemEvent) {
      $elemEvent.off(eventType + '.' + eventNamespace);
    }
  };
})();

AnimateCssEvent.attachEvent('click', 'fadeIn', jQuery('.btn-one'), jQuery('.one'));
AnimateCssEvent.attachEvent('click', 'slideInDown', jQuery('.btn-two'), jQuery('.two'));
AnimateCssEvent.attachEvent('hover', 'color', jQuery('.two'));
AnimateCssEvent.attachEvent('hover', 'pulse', jQuery('.btn-three'), jQuery('.three'));
AnimateCssEvent.attachEvent('hover', 'pulse', jQuery(jQuery('.three').get(1)));