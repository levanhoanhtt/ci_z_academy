<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class MY_Controller extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(function_exists('date_default_timezone_set')) date_default_timezone_set('Asia/Bangkok');
        //$user = $this->Musers->get(1);$this->session->set_userdata('user', $user);
    }

    protected function commonData($user, $title, $data = array()){
        $data['user'] = $user;
        $data['title'] = $title;
        $data['listActions'] = $this->Mactions->getByUserId($user['UserId'], $user['RoleId']);
        $configs = $this->Mconfigs->getListMap(2);
        $data['configs'] = $configs;
        return $data;
    }

    protected function checkUserLogin($isApi = false){
        $user = $this->session->userdata('user');
        if($user){
            if($user['RoleId'] == 2) $user = $this->Musers->get($user['UserId']);
            if($user['StatusId'] != STATUS_ACTIVED){
                $fields = array('user', 'configs');
                foreach($fields as $field) $this->session->unset_userdata($field);
                redirect('user');
                die();
            }
            return $user;
        }
        else{
            if($isApi) echo json_encode(array('code' => -1, 'message' => "Bạn cần login lại"));
            else redirect('admin?redirectUrl='.current_url());
            die();
        }
    }

    protected function loadModel($models = array()){
        foreach($models as $model) $this->load->model($model);
    }

    protected function arrayFromPost($fields) {
        $data = array();
        foreach ($fields as $field) $data[$field] = trim($this->input->post($field));
        return $data;
    }

    protected function getDecimalCount($ticketPiece = 0){
        $retVal = 3;
        if($ticketPiece == 0) $ticketPiece = $this->Mconfigs->getConfigValue('PIECES_PER_TICKET', 1000);
        if($ticketPiece == 100) $retVal = 2;
        elseif($ticketPiece == 10) $retVal = 1;
        return $retVal;
    }

    protected function sendMail($emailFrom, $nameFrom, $emailTo, $subject, $message){
        //$this->load->library('email');
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => '465',
            'smtp_user' => 'minhngocbka@gmail.com',
            'smtp_pass' => 'sitaukmegypymsta',
            'mailtype'  => 'html',
            'starttls'  => true,
            'newline'   => "\r\n"
        );
        $this->load->library('email', $config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from($emailFrom, $nameFrom);
        $this->email->to($emailTo);
        $this->email->subject($subject);
        $this->email->message($message);
        //$this->email->send();return true;
        if($this->email->send()) return true;
        return false;
    }
}