<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Quản lý sản phẩm',
			array(
				'scriptFooter' => array('js' => 'js/product.js'),
				'pageIcon' => 'product.png'
			)
		);
		if($user['RoleId'] == 2 && $user['IsMember'] != 2) $this->load->view('user/permission', $data);
		else{
			$this->loadModel(array('Mbrands', 'Mproducts'));
			$postData = $this->arrayFromPost(array('ProductName', 'BrandId', 'ItemStatusId'));
			if($user['RoleId'] == 2){
				$postData['UserId'] = $user['UserId'];
				$data['listBrands'] = $this->Mbrands->getBy(array('ItemStatusId >' => 0, 'UserId' => $user['UserId']));
			}
			else{
				$postData['UserId'] = $this->input->post('UserId');
				$data['listUsers'] = $this->Musers->getListForSelect(2, 2);
				$data['listBrands'] = $this->Mbrands->getBy(array('ItemStatusId >' => 0));
			}
			$rowCount = $this->Mproducts->getCount($postData);
			$data['listProducts'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listProducts'] = $this->Mproducts->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('product/list', $data);
		}
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ProductName', 'BrandId', 'Price', 'ItemStatusId', 'UserId'));
		$postData['Price'] = replacePrice($postData['Price']);
		if(!empty($postData['ProductName']) && $postData['BrandId'] > 0 && $postData['Price'] > 0){
			$productId = $this->input->post('ProductId');
            $this->loadModel(array('Mbrands', 'Mproducts'));
			if($user['RoleId'] == 1){
				if($productId > 0){
					$flag = $this->Mproducts->checkExist($postData['ProductName'], $postData['BrandId'], $productId);
					if(!$flag){
						$postData['UpdateUserId'] = $user['UserId'];
						$postData['UpdateDateTime'] = getCurentDateTime();
						$this->updateProduct($postData, $productId, $user);
					}
					else echo json_encode(array('code' => -1, 'message' => "Sản phẩm này đã tồn tại"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Quản lý không có quyền thêm sản phẩm"));
			}
			elseif($user['RoleId'] == 2 && $user['IsMember'] == 2){
				$flag = $this->Mproducts->checkExist($postData['ProductName'], $postData['BrandId'], $productId);
				if(!$flag){
					$isValid = false;
					if($productId > 0){
						$itemStatusId = $this->Mproducts->getFieldValue(array('ProductId' => $productId), 'ItemStatusId', 0);
						if($itemStatusId == 1){
							$postData['ItemStatusId'] = 1;
							$postData['UpdateUserId'] = $user['UserId'];
							$postData['UpdateDateTime'] = getCurentDateTime();
							$isValid = true;
						}
						else echo json_encode(array('code' => -1, 'message' => "Sản phẩm này không có quyền sửa"));
					}
					else{
						$postData['UserId'] = $user['UserId'];
						$postData['ItemStatusId'] = 1;
						$postData['CrUserId'] = $user['UserId'];
						$postData['CrDateTime'] = getCurentDateTime();
						$isValid = true;
					}
					if($isValid) $this->updateProduct($postData, $productId, $user);
				}
				else echo json_encode(array('code' => -1, 'message' => "Sản phẩm này đã tồn tại"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$productId = $this->input->post('ProductId');
		$itemStatusId = $this->input->post('ItemStatusId');
		$brandId = $this->input->post('BrandId');
		if($productId > 0 && $brandId > 0 && $itemStatusId >= 0 && $itemStatusId < 4){
			$this->loadModel(array('Mbrands', 'Mproducts'));
			if($itemStatusId == 2){
				$brandStatusId = $this->Mbrands->getFieldValue(array('BrandId' => $brandId), 'ItemStatusId', 0);
				if($brandStatusId != 2){
					echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu sản phẩm chưa được duyệt"));
					die();
				}
			}
			if($user['RoleId'] == 1) $this->changeStatusProduct($productId, $itemStatusId, $user['UserId']);
			elseif($user['RoleId'] == 2 && $user['IsMember'] == 2){
				$itemStatusIdOld = $this->Mproducts->getFieldValue(array('ProductId' => $productId), 'ItemStatusId', 0);
				if($itemStatusId == 0){
					if($itemStatusIdOld == 1) $this->changeStatusProduct($productId, $itemStatusId, $user['UserId']);
					else echo json_encode(array('code' => -1, 'message' => "Sản phẩm này không có quyền xóa"));
				}
				elseif($itemStatusId == 2){
					if($itemStatusIdOld == 3) $this->changeStatusProduct($productId, $itemStatusId, $user['UserId']);
					else echo json_encode(array('code' => -1, 'message' => "Bạn không có quyền kích hoạt sản phẩm"));
				}
				elseif($itemStatusId == 3){
					if($itemStatusIdOld == 2) $this->changeStatusProduct($productId, $itemStatusId, $user['UserId']);
					else echo json_encode(array('code' => -1, 'message' => "Bạn không có quyền ngừng kinh doanh sản phẩm"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}	

	private function updateProduct($postData, $productId, $user){
	    $brand = $this->Mbrands->get($postData['BrandId'], true, '', 'BrandName, ItemStatusId');
	    if($brand){
            if($postData['ItemStatusId'] == 2){
                if($brand['ItemStatusId'] != 2){
                    echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu sản phẩm chưa được duyệt"));
                    die();
                }
            }
            $notifications = array();
            $crDateTime = getCurentDateTime();
            if($user['RoleId'] == 1){
                $student = $this->Musers->get($postData['UserId'], false, '', 'FullName, PhoneNumber');
				if($postData['ItemStatusId'] == 2 || $postData['ItemStatusId'] == 3) {
					$notifications[] = array(
						'StudentId' => 0,
						'AdminId' => $user['UserId'],
						'IsRead' => 1,
						'IsFromStudent' => 1,
						'Message' => 'Chợ ⓩ ' . ($postData['ItemStatusId'] == 2 ? 'mở' : 'ngừng') . ' bán sản phẩm ' . $postData['ProductName'] . ' thuộc thương hiệu ' . $brand['BrandName'] . ' của ' . $student['FullName'] . ' (' . $student['PhoneNumber'] . ')',
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
				}
				if($postData['ItemStatusId'] == 2 || $postData['ItemStatusId'] == 4) {
					$notifications[] = array(
						'StudentId' => $postData['UserId'],
						'AdminId' => $user['UserId'],
						'IsRead' => 1,
						'IsFromStudent' => 1,
						'Message' => 'Đăng kí Sản phẩm ' . $postData['ProductName'] . ' của bạn ' . ($postData['ItemStatusId'] == 2 ? 'đã' : 'không') . '  được phê duyệt',
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
				}
            }
            else{
                if($productId == 0) {
                    $notifications[] = array(
                        'StudentId' => $user['UserId'],
                        'AdminId' => ADMIN_ID,
                        'IsRead' => 1,
                        'IsFromStudent' => 2,
                        'Message' => 'Thành viên ' . $user['FullName'] . ' (' . $user['PhoneNumber'] . ') đăng kí sản phẩm ' . $postData['ProductName'] . ' thuộc nhẫn hiệu ' . $brand['BrandName'],
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => getCurentDateTime()
                    );
                }
            }
            $flag = $this->Mproducts->update($postData, $productId, $notifications);
            if($flag > 0) {
                $postData['ProductId'] = $flag;
                $postData['PaymentLink'] = base_url('thanhtoan/'.($flag + 10000));
                $postData['StatusName'] = '<span class="'.$this->Mconstants->labelCss[$postData['ItemStatusId']].'">'.$this->Mconstants->itemStatus[$postData['ItemStatusId']].'</span>';
				$postData['IsAdd'] = ($productId > 0) ? 0 : 1;
				$tokenFCMs = array($user['UserId'] => $user['TokenFCM']);
				foreach($notifications as $n){
					if ($n['IsFromStudent'] == 1){
						if($n['StudentId'] > 0){
							if(!isset($tokenFCMs[$n['StudentId']])) $tokenFCMs[$n['StudentId']] = $this->Musers->getFieldValue(array('UserId' => $n['StudentId']), 'TokenFCM');
							sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
						} 
						else sendFCM($n['Message'], DEFAULT_TOPIC_NOTIFICATION, true);
					}
				}
                echo json_encode(array('code' => 1, 'message' => "Cập nhật sản phẩm thành công", 'data' => $postData));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy nhãn hiệu"));
	}
	
	private function changeStatusProduct($productId, $itemStatusId, $userId){
		$flag = $this->Mproducts->save(array('ItemStatusId' => $itemStatusId, 'UpdateUserId' => $userId, 'UpdateDateTime' => getCurentDateTime()), $productId);
		if($flag){
			if($itemStatusId > 0) echo json_encode(array('code' => 1, 'message' => 'Cập nhật sản phẩm thành công', 'data' => array('StatusName' => '<span class="'.$this->Mconstants->labelCss[$itemStatusId].'">'.$this->Mconstants->itemStatus[$itemStatusId].'</span>')));
			else echo json_encode(array('code' => 1, 'message' => "Xóa sản phẩm thành công"));
		}
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}