<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Hỗ trợ',
			array(
				'scriptFooter' => array('js' => 'js/feedback.js'),
				'pageIcon' => 'light-icon.png'
			)
		);
		$postData = $this->arrayFromPost(array('FeedbackStatusId'));
		if($user['RoleId'] == 2) $postData['StudentId'] = $user['UserId'];
		$this->load->model('Mfeedbacks');
		$rowCount = $this->Mfeedbacks->getCount($postData);
		$data['listFeedbacks'] = array();
		if($rowCount > 0){
			$perPage = DEFAULT_LIMIT;
			$pageCount = ceil($rowCount / $perPage);
			$page = $this->input->post('PageId');
			if(!is_numeric($page) || $page < 1) $page = 1;
			$data['listFeedbacks'] = $this->Mfeedbacks->search($postData, $perPage, $page);
			$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
		}
		$this->load->view('feedback/list', $data);
	}

	public function get(){
		$this->checkUserLogin(true);
		$feedbackId = $this->input->post('FeedbackId');
		if($feedbackId > 0){
			$this->load->model('Mfeedbacks');
			$feedback = $this->Mfeedbacks->get($feedbackId);
			if($feedback) echo json_encode(array('code' => 1, 'data' => $feedback));
			else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy phản hồi"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('Title', 'Question', 'Reply', 'FeedbackStatusId'));
		if(!empty($postData['Title']) && !empty($postData['Question']) && $postData['FeedbackStatusId'] > 0){
			$feedbackId = $this->input->post('FeedbackId');
			if($feedbackId > 0){
				unset($postData['Title']);
				unset($postData['Question']);
				$postData['AdminId'] = $user['UserId'];
				$postData['ReplyDateTime'] = getCurentDateTime();
			}
			else{
				$postData['StudentId'] = $user['UserId'];
				$postData['CrDateTime'] = getCurentDateTime();
			}
			$this->load->model('Mfeedbacks');
			$flag = $this->Mfeedbacks->save($postData, $feedbackId);
			if($flag > 0){
				$message = $feedbackId > 0 ? 'Trả lời hỗ trợ thành công' : 'Gửi hỗ trợ thành công';
				echo json_encode(array('code' => 1, 'message' => $message));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getCountUnRead(){
		$user = $this->session->userdata('user');
		if($user && $user['RoleId'] == 1){
			$this->load->model('Mfeedbacks');
			echo $this->Mfeedbacks->getCount(array('FeedbackStatusId' => 1));
		}
		else echo '0';
	}
}