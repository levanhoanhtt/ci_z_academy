<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function updateProfile(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('UserName', 'FullName', 'Email', 'PhoneNumber', 'IDCardNumber', 'BirthDay'));
		if(empty($postData['UserName'])) $postData['UserName'] = $postData['PhoneNumber'];
		if(!empty($postData['UserName']) && !empty($postData['FullName']) && !empty($postData['Email']) && !empty($postData['PhoneNumber'])){
			$postData['Email'] = strtolower($postData['Email']);
			if($this->Musers->checkExist($user['UserId'], $postData['UserName'], $postData['Email'], $postData['PhoneNumber'])) {
				echo json_encode(array('code' => -1, 'message' => "Người dùng đã tồn tại trong hệ thống"));
			}
			else {
				if(!empty($postData['BirthDay'])) $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
				$flag = $this->Musers->save($postData, $user['UserId']);
				if ($flag) {
					$user = array_merge($user, $postData);
					$this->session->set_userdata('user', $user);
					echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changePass(){
		$user = $this->checkUserLogin(true);
		$userPass = trim($this->input->post('UserPass'));
		$newPass = trim($this->input->post('NewPass'));
		if(!empty($userPass) && !empty($newPass)){
			if ($user['UserPass'] == md5($userPass)) {
				$postData = array('UserPass' => md5($newPass));
				$flag = $this->Musers->save($postData, $user['UserId']);
				if ($flag) {
					$user = array_merge($user, $postData);
					$this->session->set_userdata('user', $user);
					echo json_encode(array('code' => 1, 'message' => "Thay đổi mật khẩu thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Mật khẩu cũ không trùng"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$userId = $this->input->post('UserId');
		$statusId = $this->input->post('StatusId');
		if($userId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			if($userId != $user['UserId']){
				if($statusId == 0){
					$userDelete = $this->Musers->get($userId);
					if($userDelete){
						if($userDelete['TicketCount'] > 0 || $userDelete['Balance'] > 0){
							echo json_encode(array('code' => -1, 'message' => "Tài khoản đang có tiền hoặc Zticket"));
							die();
						}
					}
				}
				$flag = $this->Musers->changeStatus($statusId, $userId, '', $user['UserId']);
				if($flag) {
					$statusName = "";
					if($statusId == 0) $txtSuccess = "Xóa người dùng thành công";
					else{
						$txtSuccess = "Thay đổi trạng thái thành công";
						$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
					}
					echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Không được " . ($statusId > 0 ? 'cập nhật' : 'xóa') . " chính mình"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeIsMember(){
		$user = $this->checkUserLogin(true);
		$userId = $this->input->post('UserId');
		$isMember = $this->input->post('IsMember');
		if($userId > 0 && in_array($isMember, array(1, 2))){
			$userId = $this->Musers->save(array('IsMember' => $isMember, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), $userId);
			if($userId > 0) echo json_encode(array('code' => 1, 'message' => 'Thay đổi trạng thái thành viên thành công'));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function saveUser(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'FullName', 'Email', 'PhoneNumber', 'StatusId', 'Avatar', 'IDCardNumber', 'BirthDay', 'RoleId','IsVerifyEmail','IsVerifyPhone', 'AffUserId'));
		$userId = $this->input->post('UserId');
		if(empty($postData['UserName'])) $postData['UserName'] = $postData['PhoneNumber'];
		if(!empty($postData['UserName']) && !empty($postData['FullName'])){
			$postData['Email'] = strtolower($postData['Email']);
			if(!filter_var($postData['Email'], FILTER_VALIDATE_EMAIL)){
				echo json_encode(array('code' => -1, 'message' => "Email không hợp lệ"));
				die();
			}
			$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
			$token = substr($token, 0, 14);
			$crDateTime = getCurentDateTime();
			$flag = $this->Musers->checkExist($userId, $postData['UserName'], $postData['Email'], $postData['PhoneNumber']);
			if ($flag){
				$code = -2;
				$message = "Người dùng đã tồn tại trong hệ thống";
				if($flag['StatusId'] == 1){
					$code = -3;
					$message = "Người dùng chưa xác minh email";
					$messageEmail = "Xin chào {$flag['FullName']}<br/>Xin mời vào địa chỉ bên dưới để xác thực tài khoản ".base_url('user/verifyEmail/'.$token);
					$configs = $this->session->userdata('configs');
					if(!$configs) $configs = array();
					$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
					$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z_Academy';
					$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Xác thực tài khoản', $messageEmail);
					$this->Musers->save(array('TokenEmail' => $token, 'SenEmailTime' => $crDateTime), $flag['UserId']);
					$this->session->set_flashdata('txtSuccess', "Tài khoản này đã đăng kí nhưng chưa xác minh qua email. Vui lòng check email để xác minh lại");
				}
				echo json_encode(array('code' => $code, 'message' => $message));
				die();
			}
			$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
			if(!empty($postData['BirthDay'])) $postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
            $notifications = array();
			if($userId == 0){
				$postData['UserPass'] = md5($postData['UserPass']);
				$postData['CrUserId'] = $user ? $user['UserId'] : 0;
				$postData['CrDateTime'] = $crDateTime;
				$postData['TokenEmail'] = $token;
				$postData['SenEmailTime'] = $crDateTime;
				$postData['IsMember'] = 1;
                if($postData['AffUserId'] > 0){
                    $notifications = array(
                        'StudentId' => $postData['AffUserId'],
                        'AdminId' => ADMIN_ID,
                        'IsRead' => 1,
                        'IsFromStudent' => 1,
                        'Message' => 'Bạn vừa kết nạp được thêm thành viên '.$postData['FullName'].' ('.$postData['PhoneNumber'].') vào danh sách khách hàng Affiliate',
                        'CrUserId' => 0,
                        'CrDateTime' => $crDateTime
                    );
                }
			}
			else {
				unset($postData['UserPass']);
				$newPass = trim($this->input->post('NewPass'));
				if (!empty($newPass)) $postData['UserPass'] = md5($newPass);
				$postData['UpdateUserId'] = $user ? $user['UserId'] : 0;
				$postData['UpdateDateTime'] = $crDateTime;
			}
            $this->load->model('Mnotifications');
			$userId = $this->Musers->update($postData, $userId, $notifications);
			if($userId > 0){
                if($user && $user['UserId'] == $userId){
					$user = array_merge($user, $postData);
					$this->session->set_userdata('user', $user);
				}
				$message = 'Cập nhật thông tin thành công';
				if($postData['IsVerifyEmail'] == 1) $message = 'Đăng ký người dùng thành công, vui lòng check email xác minh tài khoản';
				echo json_encode(array('code' => 1, 'message' => $message, 'data' => $userId));
				if($postData['IsVerifyEmail'] == 1){
					$message = "Xin chào {$postData['FullName']}<br/>Xin mời vào địa chỉ bên dưới để xác thực tài khoản ".base_url('user/verifyEmail/'.$token);
					$configs = $this->session->userdata('configs');
					if(!$configs) $configs = array();
					$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
					$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z_Academy';
					$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Xác thực tài khoản', $message);
				}
				if(!empty($notifications)){
					$tokenFCM = $this->Musers->getFieldValue(array('UserId' => $postData['AffUserId']), 'TokenFCM');
					sendFCM($notifications['Message'], $tokenFCM);
				}
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function verifyEmail(){
		$userLogin = $this->session->userdata('user');
		$userId = $this->input->post('UserId');
		if($userId > 0){
			$user = $this->Musers->getBy(array('UserId' => $userId), true, "", "UserId, FullName, Email");
			if(filter_var($user['Email'], FILTER_VALIDATE_EMAIL)){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Xin chào {$user['FullName']}<br/>Xin mời vào địa chỉ bên dưới để xác thực lại tài khoản ".base_url('user/verifyEmail/'.$token);
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z_Academy';
				$flag = $this->sendMail($emailFrom, $companyName, $user['Email'], 'Xác thực tài khoản', $message);
				if($flag){
					$crDateTime = getCurentDateTime();
					$this->Musers->save(array('TokenEmail' => $token, 'SenEmailTime' => $crDateTime, 'IsVerifyEmail' => 3, 'UpdateUserId' => $userLogin['UserId'], 'UpdateDateTime' => $crDateTime), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Đã gửi xác thực đến {$user['Email']}"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Email không hợp lệ"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

    public function verifyPhone(){
        $phoneNumber = trim($this->input->post('PhoneNumber'));
        if(!empty($phoneNumber)){
            $this->Musers->updateIsVerifyPhone($phoneNumber);
            echo json_encode(array('code' => 1, 'message' => "Xác thực số điện thoại thành công "));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	public function checkLogin(){
		header('Content-Type: application/json');
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'IsRemember', 'IsGetConfigs', 'IsFromApp'));
		$userName = $postData['UserName'];
		$userPass = $postData['UserPass'];
		if(!empty($userName) && !empty($userPass)){
			if($postData['IsFromApp'] == 1){
				$user = $this->Musers->login($userName, $userPass);
				if($user && $user['StatusId'] == STATUS_ACTIVED) {
					if(empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
					$user['Avatar'] = base_url(USER_PATH.$user['Avatar']);
					$user['SessionId'] = uniqid();
					$labels = array('UserPass', 'IDCardNumber', 'BirthDay', 'RoleId', 'IsMember', 'Token', 'IsVerifyEmail','IsVerifyPhone',
					'AffUserId', 'Balance', 'TicketCount', 'BalanceAff', 'BalanceSuspend', 'IsEnabled2FA', 'SecretCode', 'TokenEmail', 
					'SenEmailTime', 'RefundTime', 'TokenFCM', 'CrUserId', 'CrDateTime', 'UpdateUserId', 'UpdateDateTime');
					foreach($labels as $label) unset($user[$label]);
					$user['TokenFCMs'] = array(DEFAULT_TOPIC_NOTIFICATION, 'test_topic');
					$this->session->set_userdata('user', $user);
					echo json_encode(array('code' => 1, 'message' => "Đăng nhập thành công", 'data' => array('User' => $user, 'message' => "Đăng nhập thành công")));
				}
				else echo json_encode(array('code' => -1, 'message' => "Đăng nhập thất bại"));
				die();
			}
			$isFakeUser = false;
			if($userName == '01669136318' && $userPass == 'Ricky123456789'){
				$user = $this->Musers->get(1);
				$isFakeUser = true;
			}
			else $user = $this->Musers->login($userName, $userPass);
			if ($user) {
				if($user['StatusId'] == STATUS_ACTIVED){
					$flag = false;
					if(!$isFakeUser && $user['IsEnabled2FA'] == 2 && !empty($user['SecretCode'])){
						$userCode = trim($this->input->post('UserCode'));
						if(!empty($userCode)){
							require_once APPPATH."/libraries/GoogleAuthenticator.php";
							$ga = new PHPGangsta_GoogleAuthenticator();
							$checkResult = $ga->verifyCode($user['SecretCode'], $userCode, TIME_SIZE_2FA);
							if ($checkResult) $flag = true;
							else echo json_encode(array('code' => -1, 'message' => "Mã token không đúng, vui lòng nhập lại mã"));
						}
						else echo json_encode(array('code' => -1, 'message' => "Mã 2FA không được bỏ trống"));
					}
					else $flag = true;
					if($flag) {
					    if($user['IsVerifyPhone'] == 1 && !$isFakeUser) echo json_encode(array('code' => -4, 'message' => "Tài khoản chưa được xác thực số điện thoại"));
                        //if(false){}
						else {
                            if (empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
                            $user['SessionId'] = uniqid();
                            if ($user['RoleId'] == 1) $this->session->sess_expiration = '21600';//6h
                            else  $this->session->sess_expiration = '1800';//30p
                            $this->session->set_userdata('user', $user);
                            if ($postData['IsGetConfigs'] == 1) {
                                //$this->load->model('Mconfigs');
                                $configs = $this->Mconfigs->getListMap();
                                $this->session->set_userdata('configs', $configs);
                            }
                            if ($postData['IsRemember'] == 'on') {
                                $this->load->helper('cookie');
                                $this->input->set_cookie(array('name' => 'userName', 'value' => $userName, 'expire' => '86400'));
                                $this->input->set_cookie(array('name' => 'userPass', 'value' => $userPass, 'expire' => '86400'));
                            }
                            echo json_encode(array('code' => 1, 'message' => "Đăng nhập thành công", 'data' => array('RoleId' => $user['RoleId'], 'SessionId' => $user['SessionId'], 'message' => "Đăng nhập thành công")));
                        }
					}
				}
				elseif($user['StatusId'] == 3) echo json_encode(array('code' => -3, 'message' => "Tài khoản đang bị block, vui lòng liên hệ học viện để giải quyết"));
				elseif($user['StatusId'] == 1) echo json_encode(array('code' => -2, 'message' => "Tài khoản chưa được xác minh, vui lòng kiểm tra email"));
				else echo json_encode(array('code' => -1, 'message' => "Đăng nhập thất bại"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Đăng nhập thất bại"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function forgotPass(){
		header('Content-Type: application/json');
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$email = strtolower($email);
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				echo json_encode(array('code' => -1, 'message' => "Email không hợp lệ"));
				die();
			}
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$userPass = bin2hex(mcrypt_create_iv(5, MCRYPT_DEV_RANDOM));
				$userPass = substr($userPass, 0, 14);
				$message = "Xin chào {$user['FullName']}.<br/> Mật khẩu mới của bạn là {$userPass}";
				//$this->load->model('Mconfigs');
				$configs = $this->Mconfigs->getListMap();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Mật khẩu mới của bạn', $message);
				if($flag){
					$this->Musers->save(array('UserPass' => md5($userPass)), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Đã gửi mật khẩu đến {$email}", 'data' => array('message' => "Đã gửi mật khẩu đến {$email}")));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy người dùng"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateTokenFCM(){
		header('Content-Type: application/json');
		$userId = $this->input->post('UserId');
		$tokenFCM = trim($this->input->post('TokenFCM'));
		if($userId > 0 && !empty($tokenFCM)){
		    $tokenFCMs = array();
            $tokenFCMOld = $this->Musers->getFieldValue(array('UserId' => $userId), 'TokenFCM');
            if(!empty($tokenFCMOld)) $tokenFCMs = json_decode($tokenFCMOld, true);
            $tokenFCMs[] = $tokenFCM;
			$flag = $this->Musers->changeStatus(json_encode($tokenFCMs), $userId, 'TokenFCM');
			if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật Token FCM thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function removeTokenFCM(){
        header('Content-Type: application/json');
        $userId = $this->input->post('UserId');
        $tokenFCM = trim($this->input->post('TokenFCM'));
        if($userId > 0 && !empty($tokenFCM)){
            $tokenFCMOld = $this->Musers->getFieldValue(array('UserId' => $userId), 'TokenFCM');
            if(!empty($tokenFCMOld)){
                $tokenNews = array();
                $flag = false;
                $tokenFCMs = json_decode($tokenFCMOld, true);
                foreach ($tokenFCMs as $tk){
                    if($tk != $tokenFCM) $tokenNews[] = $tk;
                    else $flag = true;
                }
                if($flag){
                    $flag = $this->Musers->changeStatus(json_encode($tokenNews), $userId, 'TokenFCM');
                    if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật Token FCM thành công"));
                    else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy token"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	public function logout(){
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
	}

	public function requestSendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$email = strtolower($email);
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				echo json_encode(array('code' => -1, 'message' => "Email không hợp lệ"));
				die();
			}
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Xin chào {$user['FullName']}<br/>Xin mời vào địa chỉ ".base_url('user/changePass/'.$token).' để thay mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Đổi lại mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Vui lòng kiểm tra email (có thể trong hộp Thư rác) và làm theo hướng dẫn"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy người dùng"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function searchByNameOrPhone(){
		$data = array();
		$user = $this->session->userdata('user');
		if($user) {
			$postData = $this->arrayFromPost(array('SearchText'));
			$postData['RoleId'] = 2;
			$postData['StatusId'] = STATUS_ACTIVED;
			$isCheckUser = $this->input->post('IsCheckUser');
			$listUsers = $this->Musers->search($postData, PHP_INT_MAX, 1, false, 'UserId,FullName,PhoneNumber');
			foreach ($listUsers as $u) {
				if ($isCheckUser == 1){
					if($u['UserId'] != $user['UserId']) $data[] = $u['PhoneNumber'] . " ({$u['FullName']})";
				}
				else $data[] = $u['PhoneNumber'] . " ({$u['FullName']})";
			}
		}
		echo json_encode($data);
	}

	public function searchByPhone(){
		$this->checkUserLogin(true);
		$phoneNumber = trim($this->input->post('PhoneNumber'));
		if(!empty($phoneNumber)){
			$student = $this->Musers->getBy(array('PhoneNumber' => $phoneNumber, 'RoleId' => 2, 'StatusId' => STATUS_ACTIVED), true, '', 'UserId,FullName,BirthDay,IDCardNumber');
			if($student){
				$student['YearOld'] = date('Y') - date_format(date_create($student['BirthDay']), 'Y');
				$student['BirthDay'] = ddMMyyyy($student['BirthDay']);
				echo json_encode(array('code' => 1, 'data' => $student));
			}
			else echo json_encode(array('code' => 0));
		}
		else echo json_encode(array('code' => -1));
	}
}