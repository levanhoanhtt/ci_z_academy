<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller{

    public function getList(){
		header('Content-Type: application/json');
		$studentId = $this->input->post('UserId');
		$isRead = 0;//$this->input->post('IsRead');
		if($studentId > 0 && $isRead >= 0){
			$postData = array(
				'StudentId' => $studentId,
				'RoleId' => 2,
				'IsFromStudent' => 1,
				'IsRead' => $isRead
			);
			$page = $this->input->post('PageId');
			if($page < 1) $page = 1;
			$perPage = 10;
			$this->load->model('Mnotifications');
			$listNotifications = $this->Mnotifications->search($postData, $perPage, $page);
			$now = new DateTime(date('Y-m-d'));
			$notifications = array();		
			foreach($listNotifications as $n){
				$time = '';
				$dayDiff = getDayDiff($n['CrDateTime'], $now);
				$dateTime = ddMMyyyy($n['CrDateTime'], $dayDiff > 2 ? 'd/m/Y' : '');
				$dayText = getDayDiffText($dayDiff) . $dateTime;
				if($dayDiff != 0) $time = getDayDiffText($dayDiff) . $dateTime;
				else{
					$times = getTime($n['CrDateTime']);
					if($times['hours'] < 6){
						if($times['hours'] == 0 && $times['minutes'] == 0 && $times['seconds'] > 0) $time = $times['seconds']. ' giây trước';
						else if($times['hours'] == 0 && $times['minutes'] > 0) $time = $times['minutes']. ' phút trước';
						else $time = $times['hours']. ' giờ trước';
					}
					else $time= getDayDiffText($dayDiff) . $dateTime;
				}
				$notification = array(
					'id' => $n['NotificationId'],
					'name' => 'Học viện Z',
					'avatar_url' => base_url(USER_PATH.NO_IMAGE),
					'subtitle' => $n['Message'],
                    'status' => $n['UserIsRead'] > 0 ? $n['UserIsRead'] : 1,
                    'time' => $time
				);
				if(isset($notifications[$dayText])) $notifications[$dayText][] = $notification;
				else $notifications[$dayText] = array($notification);
			}
			$data = array();
			foreach($notifications as $label => $lists){
				$data[] = array(
					'title' => $label,
					'data' => $lists
				);
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function get(){
    	header('Content-Type: application/json');
		$notificationId = $this->input->post('NotificationId');
		$studentId = $this->input->post('UserId');
    	if($notificationId > 0 && $studentId > 0){
    		$this->loadModel(array('Mnotifications', 'Mnotificationusers'));
			$detail = $this->Mnotifications->get($notificationId);
			if($detail){
				$now = new DateTime(date('Y-m-d'));
				$dayDiff = getDayDiff($detail['CrDateTime'], $now);
				$dateTime = ddMMyyyy($detail['CrDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
				if($dayDiff != 0) $time = getDayDiffText($dayDiff) . $dateTime;
				else{
					$times = getTime($detail['CrDateTime']);
					if($times['hours'] < 6){
						if($times['hours'] == 0 && $times['minutes'] == 0 && $times['seconds'] > 0) $time = $times['seconds']. ' giây';
						else if($times['hours'] == 0 && $times['minutes'] > 0) $time = $times['minutes']. ' phút';
						else $time = $times['hours']. ' giờ';
					}
					else $time= getDayDiffText($dayDiff) . $dateTime;	
				}
				$data = array(
					'name' => 'Học viện Z',
					'avatar_url' => base_url(USER_PATH.NO_IMAGE),
					'content' => $detail['ArticleId'] > 0 ? $detail['ArticleContent'] : '<p>' . $detail['Message'] . '</p>',
					'time' => $time
				);
				$notificationUser = $this->Mnotificationusers->getBy(array('NotificationId' => $detail['NotificationId'], 'UserId' => $studentId), true);
				if($notificationUser){
					if($notificationUser['IsRead'] == 1){
						$this->Mnotificationusers->changeStatus(2, $notificationUser['NotificationUserId'], 'IsRead', $studentId);
						$data['seenText'] = 'Đã xem '.date('H:i');
					}
					else{
						$dayDiff = getDayDiff($notificationUser['UpdateDateTime'], $now);
						if($dayDiff == 0) $data['seenText'] = 'Đã xem '.ddMMyyyy($notificationUser['UpdateDateTime'], 'H:i');
						else{
							$dateTime = ddMMyyyy($notificationUser['UpdateDateTime'], $dayDiff > 2 ? 'd/m/Y H:i' : 'H:i');
							$data['seenText'] = getDayDiffText($dayDiff) . $dateTime;
						}
					}
				}
				else{
					$this->Mnotificationusers->save(array(
						'NotificationId' => $notificationId,
						'UserId' => $studentId,
						'IsRead' => 2,
						'UpdateDateTime' => getCurentDateTime()
					));
					$data['seenText'] = 'Đã xem '.date('H:i');
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}	
			else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy thông báo"));    		
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeIsRead(){
        header('Content-Type: application/json');
        $notificationId = $this->input->post('NotificationId');
        $studentId = $this->input->post('UserId');
        if ($notificationId > 0 && $studentId > 0) {
			$this->load->model('Mnotificationusers');
			$id = $this->Mnotificationusers->getFieldValue(array('NotificationId' => $notificationId, 'UserId' => $studentId), 'NotificationUserId', 0);
			if($id == 0) $flag = $this->Mnotificationusers->save(array('NotificationId' => $notificationId, 'IsRead' => 2, 'UserId' => $studentId, 'UpdateDateTime' => getCurentDateTime())) > 0;
			else $flag = true;
            if ($flag) echo json_encode(array('code' => 1, 'message' => 'Thay đổi trạng thái thành công'));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}