<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userskill extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Hồ Sơ Z',
			array(
				'scriptFooter' => array('js' => 'js/user_skill.js'),
				'pageIcon' => 'userskill.png'
			)
		);
		if($user['RoleId'] == 1 || ($user['RoleId'] == 2 && $user['IsMember'] == 2)) {
			$this->loadModel(array('Mskills', 'Mfields'));
			$data['listFields'] = $this->Mfields->getBy(array('StatusId' => STATUS_ACTIVED));
			$listSkills = $this->Mskills->getBy(array('StatusId' => STATUS_ACTIVED));
			$listTechSkills = $listSoftSkills = array();
			foreach ($listSkills as $s) {
				if ($s['SkillTypeId'] == 1) $listTechSkills[] = $s;
				else $listSoftSkills[] = $s;
			}
			$data['listTechSkills'] = $listTechSkills;
			$data['listSoftSkills'] = $listSoftSkills;
			$postData = $this->arrayFromPost(array('SkillId_1', 'SkillLevelId_1', 'SkillId_2', 'SkillLevelId_2', 'FieldId','SearchText'));
			$postData['RoleId'] = 2;
			$postData['IsMember'] = 2;
			$data['listStudents'] = array();
			$rowCount = $this->Musers->getCount($postData);
			$data['listStudents'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listStudents'] = $this->Musers->search($postData, $perPage, $page, true);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('userskill/admin', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function profile(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Hồ Sơ Z',
			array(
				'scriptFooter' => array('js' => 'js/user_skill.js'),
				'pageIcon' => 'userskill.png'
			)
		);
		if($user['IsMember'] == 2){
			$this->loadModel(array('Mregisters', 'Mskills', 'Mfields', 'Muserskills', 'Mrelationships'));
			$user['CountRegister'] = $this->Mregisters->countRows(array('ByUserId' => $user['UserId']));
			$data['user'] = $user;
			$data['listSkills'] = $this->Mskills->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listUserSkills'] = $this->Muserskills->getBy(array('UserId' => $user['UserId']));
			$data['listFields'] = $this->Mfields->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listRelationships'] = $this->Mrelationships->getBy(array('UserId' => $user['UserId']));
			$this->load->view('userskill/student', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function view($userId = 0){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Hồ Sơ Z',
			array('pageIcon' => 'userskill.png')
		);
		if($userId > 0){
			$userEdit = $this->Musers->get($userId);
			if($userEdit){
				$data['userId'] = $userId;
				$configs = $data['configs'];
				$decimalCount = $this->getDecimalCount($configs['PIECES_PER_TICKET']);
				$userEdit['TicketCount'] = priceFormat(round($userEdit['TicketCount'], $decimalCount), true);
				$this->loadModel(array('Mregisters', 'Mskills', 'Mfields', 'Muserskills', 'Mrelationships'));
				$userEdit['CountRegister'] = $this->Mregisters->getCount(array('ByUserId' => $userId));
				$data['userEdit'] = $userEdit;
				if($userEdit['IsMember'] == 2) {
					$data['listSkills'] = $this->Mskills->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['listUserSkills'] = $this->Muserskills->getBy(array('UserId' => $userId));
					$data['listFields'] = $this->Mfields->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['listRelationships'] = $this->Mrelationships->getBy(array('UserId' => $userId));
				}
				else{
					$data['listSkills'] = array();
					$data['listUserSkills'] = array();
					$data['listFields'] = array();
					$data['listRelationships'] = array();
				}
			}
			else{
				$data['userId'] = 0;
				$data['txtError'] = "Không tìm thấy thành viên";
			}
			$this->load->view('userskill/view', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('SkillId', 'SkillLevelId', 'SkillTypeId'));
		if($user['IsMember'] == 2 && $postData['SkillId'] > 0 && $postData['SkillLevelId'] > 0 && $postData['SkillTypeId'] > 0){
			$postData['UserId'] = $user['UserId'];
			$postData['CrDateTime'] = getCurentDateTime();
			$this->load->model('Muserskills');
			$userSkillId = $this->Muserskills->getFieldValue(array('UserId' => $postData['UserId'], 'SkillId' => $postData['SkillId']), 'UserSkillId', 0);
			$userSkillId = $this->Muserskills->save($postData, $userSkillId);
			if($userSkillId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật kỹ năng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateRelationship(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FieldId', 'Comment'));
		if($user['IsMember'] == 2 && $postData['FieldId'] > 0 && !empty($postData['Comment'])){
			$postData['UserId'] = $user['UserId'];
			$postData['CrDateTime'] = getCurentDateTime();
			$flag = 0;
			$this->load->model('Mrelationships');
			$relationshipId = $this->input->post('RelationshipId');
			if($relationshipId == 0){
				$flag = $this->Mrelationships->getFieldValue(array('UserId' => $postData['UserId'], 'FieldId' => $postData['FieldId']), 'RelationshipId', 0);
				if($flag > 0) echo json_encode(array('code' => 0, 'message' => "Mối quan hệ đã tồn tại"));
			}
			if($flag == 0) {
				$flag = $this->Mrelationships->save($postData, $relationshipId);
				if ($flag > 0) {
					$postData['RelationshipId'] = $flag;
					$postData['IsAdd'] = ($relationshipId > 0) ? 0 : 1;
					echo json_encode(array('code' => 1, 'message' => "Cập nhật mối quan hệ thành công", 'data' => $postData));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function deleteRelationship(){
		$user = $this->checkUserLogin(true);
		$relationshipId = $this->input->post('RelationshipId');
		if($user['IsMember'] == 2 && $relationshipId > 0){
			$this->load->model('Mrelationships');
			$flag = $this->Mrelationships->delete($relationshipId);
			if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa mối quan hệ thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
