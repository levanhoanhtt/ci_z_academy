<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1){
			$data = $this->commonData($user,
				'Nạp tiền cho thành viên',
				array(
					'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css','vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js','vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js','vendor/plugins/bootstrap-tagsinput/bootstrap3-typeahead.js', 'js/transaction.js')),
					'pageIcon' => 'cart-icon.png'
				)
			);
			if($this->session->flashdata('messageTransaction')) $data['messageTransaction'] = $this->session->flashdata('messageTransaction');
			$postData = $this->arrayFromPost(array('SearchText', 'BeginDate', 'EndDate'));
			$postData['LogTypeId'] = 1;
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$this->load->model('Mtransactions');
			$rowCount = $this->Mtransactions->getCount($postData);
			$data['listTransactions'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('transaction/list', $data);
		}
		else redirect('proof');
	}

	public function withdrawal(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1){
			$data = $this->commonData($user,
				'Trừ tiền thành viên',
				array(
					'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css','vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js','vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js','vendor/plugins/bootstrap-tagsinput/bootstrap3-typeahead.js', 'js/transaction.js')),
					'pageIcon' => 'cart-icon.png'
				)
			);
			if($this->session->flashdata('messageTransaction')) $data['messageTransaction'] = $this->session->flashdata('messageTransaction');
			$postData = $this->arrayFromPost(array('SearchText', 'BeginDate', 'EndDate'));
			$postData['LogTypeId'] = 7;
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$this->load->model('Mtransactions');
			$rowCount = $this->Mtransactions->getCount($postData);
			$data['listTransactions'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('transaction/withdrawal', $data);
		}
		else redirect('proof');
	}

	public function log(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Lịch sử giao dịch',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transaction_log.js')),
				'pageIcon' => 'cart-icon.png'
			)
		);
		$this->load->model('Mtransactionlogs');
		$postData = $this->arrayFromPost(array('UserId', 'LogTypeId', 'BeginDate', 'EndDate'));
		if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
		if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
		if($user['RoleId'] == 1) $data['listUsers'] = $this->Musers->getListForSelect();
		else{
			$data['listUsers'] = array($user);
			$postData['UserId'] = $user['UserId'];
		}
		$rowCount = $this->Mtransactionlogs->getCount($postData);
		$data['listTransactionLogs'] = array();
		if($rowCount > 0){
			$perPage = DEFAULT_LIMIT;
			$pageCount = ceil($rowCount / $perPage);
			$page = $this->input->post('PageId');
			if(!is_numeric($page) || $page < 1) $page = 1;
			$data['listTransactionLogs'] = $this->Mtransactionlogs->search($postData, $perPage, $page);
			$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
		}
		$this->load->view('transaction/log', $data);
	}

	public function insertRecharge(){ //nap tien
		$user = $this->checkUserLogin(true);
		$phoneNumber = trim($this->input->post('PhoneNumber'));
		$comment = trim($this->input->post('Comment'));
		$paidVN = $this->input->post('PaidVN');
		$paidVN = replacePrice($paidVN);
		if(!empty($phoneNumber) && $paidVN > 0){
			$paidVNText = priceFormat($paidVN);
			$this->loadModel(array('Mtransactions', 'Mafftickets','Mnotifications'));
			$affPercent = $this->Mconfigs->getConfigValue('AFF_PERCENT', 5) / 100;
			$ticketCost = $this->Mconfigs->getConfigValue('TICKET_COST', 20000000);
			$piecesPerTicket = $this->Mconfigs->getConfigValue('PIECES_PER_TICKET', 100);
			$ticketPieces = ceil($ticketCost / $piecesPerTicket);
			$decimalCount = 2;
			if($piecesPerTicket == 1000) $decimalCount = 3;
			elseif($piecesPerTicket == 10) $decimalCount = 1;
			$errors = array();
			$success = array();
			$notifications = array();
			$tokenFCMs = array();
			$phoneNumbers = explode(',', $phoneNumber);
			foreach($phoneNumbers as $pText){
				$parts = explode('(', $pText);
				if(count($parts) > 0){
					$phoneNumber = trim($parts[0]);
					$student = $this->Musers->getBy(array('PhoneNumber' => $phoneNumber, 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2), true, '', 'UserId, FullName, PhoneNumber, Balance, AffUserId, Email, TokenFCM');
					if($student){
						$tokenFCMs[$student['UserId']] = $student['TokenFCM'];
                        $notifications[] = array(
                            'StudentId' => $student['UserId'],
                            'AdminId' => $user['UserId'],
                            'PhoneNumber' => '',
                            'Email' => '',
                            'IsRead' => 1,
                            'IsFromStudent' => 1,
                            'Message' => 'Bạn vừa nạp thành công '.$paidVNText.' đ vào tài khoản.',
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => getCurentDateTime()
                        );
						$postData = array(
							'StudentId' => $student['UserId'],
							'TransactionStatusId' => STATUS_ACTIVED,
							'PaidVN' => $paidVN,
							'LogTypeId' => 1,
							'AffUserId' => $student['AffUserId'],
							'AffPaidVN' => 0,
							'Comment' => $comment,
							'CrUserId'=> $user['UserId'],
							'CrDateTime'=> getCurentDateTime()
						);
						if($student['AffUserId'] > 0){
							$affUser = $this->Musers->getBy(array('UserId' => $student['AffUserId'], 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2), true, '', 'FullName, PhoneNumber, BalanceAff, TicketCount, TokenFCM');
							if($affUser) {
								$tokenFCMs[$student['AffUserId']] = $affUser['TokenFCM'];
								$affPaidVN = $paidVN * $affPercent;
								$postData['AffPaidVN'] = $affPaidVN;
								$affPaidVN += $affUser['BalanceAff'];
								$affUser['TicketCost'] = $ticketCost;
								$affUser['AffTicketCount'] = 0;
								$affUser['OriginalAffPaidVN'] = $affPaidVN;
								if($affPaidVN >= $ticketPieces){
									$affTicketCount = round($affPaidVN/ $ticketCost, $decimalCount, PHP_ROUND_HALF_DOWN);
									if($affTicketCount * $ticketCost > $affPaidVN){
										$unitTicket = 0.01;
										if($decimalCount == 3) $unitTicket = 0.001;
										elseif($decimalCount == 1) $unitTicket = 0.1;
										$affTicketCount -= $unitTicket;
									}
									$affPaidVN = $affPaidVN - $affTicketCount * $ticketCost;
									$affUser['AffTicketCount'] = $affTicketCount;
									//if($affTicketCount > 0){}
								}
								$affUser['AffPaidVN'] = $affPaidVN;
								$student['AffUser'] = $affUser;
								$notifications[] = array(
                                    'StudentId' => $student['AffUserId'],
                                    'AdminId' => $user['UserId'],
                                    'PhoneNumber' => '',
                                    'Email' => '',
                                    'IsRead' => 1,
                                    'IsFromStudent' => 1,
                                    'Message' => 'Thành viên Affiliate '.$student['FullName'].' ('.$student['PhoneNumber'].') vừa nạp '.$paidVNText.' đ vào tài khoản, bạn được cộng '.priceFormat($affUser['OriginalAffPaidVN']).' đ vào tài khoản Affiliate và được quy đổi ra Zticket ⓩ',
                                    'CrUserId' => $user['UserId'],
                                    'CrDateTime' => getCurentDateTime()
                                );
							}
						}
						$flag = $this->Mtransactions->insert($postData, $student);
						if($flag) $success[] = 'Nạp '.$paidVNText.' VNĐ thành công cho tài khoản có SĐT là '.$phoneNumber;
						else $errors[] = "Có lỗi xảy ra khi nạp tiền cho tài khoản có SĐT là ".$phoneNumber;
					}
					else $errors[] = 'Không tìm thấy học viên ứng với SĐT là '.$phoneNumber;
				}
				else $errors[] = $pText.' không đúng định dạng người dùng';
			}
			$this->session->set_flashdata('messageTransaction', array('success' => $success, 'errors' => $errors));
			if(!empty($success)){
                if(!empty($notifications)){
					$this->db->insert_batch('notifications', $notifications);
					foreach ($notifications as $n){
						if(!isset($tokenFCMs[$n['StudentId']])) $tokenFCMs[$n['StudentId']] = $this->Musers->getFieldValue(array('UserId' => $n['StudentId']), 'TokenFCM');
						sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
					}
				} 
				echo json_encode(array('code' => 1, 'message' => "Nạp tiền cho tài khoản thành công"));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertWithdrawal(){ //tru tien
		$user = $this->checkUserLogin(true);
		if($user['RoleId'] == 1){
			$phoneNumber = trim($this->input->post('PhoneNumber'));
			$comment = trim($this->input->post('Comment'));
			$paidVN = $this->input->post('PaidVN');
			$paidVN = replacePrice($paidVN);
			if(!empty($phoneNumber) && $paidVN > 0){
				$paidVNText = priceFormat($paidVN);
				$this->loadModel(array('Mtransactions', 'Mtransactionlogs'));
				$errors = array();
				$success = array();
				$students = array();
				$notifications = array();
				$tokenFCMs = array();
				$phoneNumbers = explode(',', $phoneNumber);
				foreach($phoneNumbers as $pText){
					$parts = explode('(', $pText);
					if(count($parts) > 0){
						$phoneNumber = trim($parts[0]);
						$student = $this->Musers->getBy(array('PhoneNumber' => $phoneNumber, 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2),true, '', 'UserId, FullName, PhoneNumber, Email, Balance, TokenFCM');
						if($student){
							$postData = array(
								'StudentId' => $student['UserId'],
								'TransactionStatusId' => STATUS_ACTIVED,
								'PaidVN' => $paidVN,
								'LogTypeId' => 7,
								'Comment' => $comment,
								'CrUserId'=> $user['UserId'],
								'CrDateTime'=>getCurentDateTime()
							);
							$flag = $this->Mtransactions->insert($postData, $student);
							if($flag){
								$tokenFCMs[$student['UserId']] = $student['TokenFCM'];
								$notifications[] = array(
									'StudentId' => $student['UserId'],
									'AdminId' => $user['UserId'],
									'PhoneNumber' => '',
									'Email' => '',
									'IsRead' => 1,
									'IsFromStudent' => 1,
									'Message' => 'Bạn vừa bị trừ '.$paidVNText.' đ trong tài khoản. Truy cập lịch sử giao dịch để xem lý do.',
									'CrUserId' => $user['UserId'],
									'CrDateTime' => getCurentDateTime()
								);
								$success[] = 'Trừ '.$paidVNText.' VNĐ thành công cho tài khoản có SĐT là '.$phoneNumber;
							} 
							else $errors[] = "Có lỗi xảy ra khi trừ tiền cho tài khoản có SĐT là ".$phoneNumber;
							if($student['Balance'] < $paidVN){
								$student['OwnCost'] = $paidVN - $student['Balance'];
								$students[] = $student;
							}
						}
						else $errors[] = 'Không tìm thấy học viên ứng với SĐT là '.$phoneNumber;
					}
					else $errors[] = $pText.' không đúng định dạng người dùng';
				}
				$this->session->set_flashdata('messageTransaction', array('success' => $success, 'errors' => $errors));
				if(!empty($success)){
                    if(!empty($notifications)){
						$this->db->insert_batch('notifications', $notifications);
						foreach ($notifications as $n){
							if(!isset($tokenFCMs[$n['StudentId']])) $tokenFCMs[$n['StudentId']] = $this->Musers->getFieldValue(array('UserId' => $n['StudentId']), 'TokenFCM');
							sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
						}
					}
                    echo json_encode(array('code' => 1, 'message' => "Trừ tiền tài khoản thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				if(!empty($students)){
					$configs = $this->session->userdata('configs');
					if(!$configs) $configs = array();
					$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
					$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
					foreach($students as $u){
						if(filter_var($u['Email'], FILTER_VALIDATE_EMAIL)){
							$message = "Xin chào {$u['FullName']} ({$u['PhoneNumber']}).<br/> Hiện tại tài khoản của bạn đang nợ học viện với số tiền là ".priceFormat(-$u['OwnCost'])." VNĐ . Xin vui lòng thanh toán cho Học viện";
							$this->sendMail($emailFrom, $companyName, $u['Email'], 'Thanh toán tiền dịch vụ', $message);
						}
					}
				}
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

}