<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Affiliate extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Affiliate',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/affiliate.js')),
                'pageIcon' => 'news.png'
            )
        );
        $postData = $this->arrayFromPost(array('SearchText', 'StatusId', 'AffPaidVNStatusId', 'BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
        if($user['RoleId'] == 1){
            $postData['AffUserId'] = $this->input->post('AffUserId');
            $postData['HasAffUserId'] = 1;
            $data['listAffUsers'] = $this->Musers->getListForSelect(2);
        }
        else{
            $postData['AffUserId'] = $user['UserId'];
            $data['balanceAff'] = $user['BalanceAff'];
        }
        $data['affPercent'] = $data['configs']['AFF_PERCENT'];
        $this->loadModel(array('Mtransactions', 'Mafftickets'));
        $countInfo = $this->Musers->getCountBatch($postData);
        $affUserIds = $countInfo['AffUserIds'];
        if($user['RoleId'] == 1) $data['balanceAff'] = $this->Musers->getSumBalanceAff($affUserIds);
        $rowCount = $countInfo['CountUser'];
        $data['listUsers'] = array();
        $data['totalTicketCount'] = 0;
        $data['todayTicketCount'] = 0;
        if($rowCount > 0){
            $statisticTicket = $this->Mafftickets->statisticForStudentIds($affUserIds);
            $data['totalTicketCount'] = $statisticTicket['TotalTicketCount'];
            $data['todayTicketCount'] = $statisticTicket['TodayTicketCount'];
            $perPage = DEFAULT_LIMIT;
            $pageCount = ceil($rowCount / $perPage);
            $page = $this->input->post('PageId');
            if (!is_numeric($page) || $page < 1) $page = 1;
            $data['listUsers'] = $this->Musers->searchForAff($postData, $perPage, $page);
            $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
        }
        $this->load->view('affiliate/'.($user['RoleId'] == 1 ? 'admin' : 'user'), $data);
    }
}