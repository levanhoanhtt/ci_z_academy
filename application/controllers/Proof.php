<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proof extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 2) {
			$data = $this->commonData($user,
				'Nạp tiền',
				array(
					'scriptFooter' => array('js' => 'js/proof.js'),
					'pageIcon' => 'cart-icon.png'
				)
			);
			$this->load->view('proof/list', $data);
		}
		else redirect('transaction');
	}

	public function requestProof(){
        $user = $this->checkUserLogin(true);
		$paymentTypeId = $this->input->post('PaymentTypeId');
		$imageBase64 = $this->input->post('ImageBase64');
		if($paymentTypeId > 0 && !empty($imageBase64)){
			$this->load->model('Mproofs');
			$imageBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageBase64));
			$crDateTime = getCurentDateTime();
			$imgName = strtotime($crDateTime);
			$imgPath  = "assets/uploads/images/$imgName.png";
			$postData = array(
				'StudentId' => $user['UserId'],
				'PaymentTypeId' => $paymentTypeId,
				'Image' => $imgName.'.png',
				'Comment' => '',
				'CrDateTime' => $crDateTime
			);
			$flag = $this->Mproofs->save($postData);
			if($flag) {
				file_put_contents($imgPath,$imageBase64);
				echo json_encode(array('code' => 1, 'message' => "Gửi bằng chứng thành công"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	public function getList(){
		$user = $this->checkUserLogin(true);
		if($user['RoleId'] == 1) {
			$limit = 10;
			$page = $this->input->post('page') ? intval($this->input->post('page')) : 1;
			$this->load->model('Mproofs');
			$data = $this->Mproofs->search(array(), $limit, $page);
			$maxPage = ceil($this->Mproofs->getCount(array()) / $limit);
			$pymentTypes = $this->Mconstants->paymentTypes;
			for($i = 0; $i < count($data); $i++){
				$data[$i]['CrDateTime'] = ddMMyyyy($data[$i]['CrDateTime'], 'd/m/Y H:i');
				$data[$i]['PaymentTypeName'] = $pymentTypes[$data[$i]['PaymentTypeId']];
			}
			echo json_encode(array(
				'page' => $page,
				'data' => $data,
				'maxPage' => $maxPage
			));
		}
	}
}