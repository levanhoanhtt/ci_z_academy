<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Rút tiền',
			array('pageIcon' => 'cart-icon.png')
		);
		if($user['RoleId'] == 1 || $user['IsMember'] == 2){
			if($user['RoleId'] == 1){
				$data['scriptHeader'] = array('css' => 'vendor/plugins/datepicker/datepicker3.css');
				$data['scriptFooter'] = array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/withdrawal_admin.js'));
				$postData = $this->arrayFromPost(array('SearchText', 'WithdrawalStatusId', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$this->load->model('Mwithdrawals');
				$withdrawalCount = $this->Mwithdrawals->getCount($postData);
				$data['totalWithdrawalCost'] = $withdrawalCount['TotalWithdrawalCost'];
				$data['totalReceiveCost'] = $withdrawalCount['TotalReceiveCost'];
				$data['listWithdrawals'] = array();
				$rowCount = $withdrawalCount['TotalRecord'];
				if($rowCount > 0){
					$perPage = DEFAULT_LIMIT;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listWithdrawals'] = $this->Mwithdrawals->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
			}
			else{
				$data['scriptFooter'] = array('js' => 'js/withdrawal_user.js');
				$configs = $data['configs'];
				$data['affPercent'] = $configs['AFF_PERCENT'];
				$ticketPiece = $configs['PIECES_PER_TICKET'];
				$decimalCount = $this->getDecimalCount($ticketPiece);
				$user['TicketCount'] = round($user['TicketCount'], $decimalCount);
				$data['user'] = $user;
				$data['buyAble'] = $user['Balance'] >= 10000000;
				$this->load->model('Mwithdrawals');
				$data['listWithdrawals'] = $this->Mwithdrawals->getBy(array('WithdrawalStatusId >' => 0, 'StudentId' => $user['UserId']), false, 'CrDateTime');
			}
			$this->load->view('withdrawal/'.($user['RoleId'] == 1 ? 'admin' : 'user'), $data);
		}
		else redirect('user/dashboard');
	}

	public function insert(){
		$user = $this->checkUserLogin(true);
		if($user['RoleId'] == 2 && $user['IsMember'] == 2){
			$postData = $this->arrayFromPost(array('WithdrawalCost', 'ReceiveCost', 'BankName', 'BankHolder', 'BankNumber', 'BranchName', 'AffPercent'));
			$postData['WithdrawalCost'] = replacePrice($postData['WithdrawalCost']);
			$postData['ReceiveCost'] = replacePrice($postData['ReceiveCost']);
			if($postData['WithdrawalCost'] > 0 && $postData['ReceiveCost'] > 0 && !empty($postData['BankName']) && !empty($postData['BankHolder']) && !empty($postData['BranchName']) && $postData['AffPercent'] > 0){
				$convertFulName = $this->toSlug(strtolower($user['FullName']));
				if($this->toSlug(strtolower($postData['BankHolder'])) == $convertFulName){
					if($postData['WithdrawalCost'] >= 10000000 && $postData['WithdrawalCost'] <= $user['Balance']){
						$postData['StudentId'] = $user['UserId'];
						$postData['WithdrawalStatusId'] = 1;
						$postData['CrUserId'] = $user['UserId'];
						$crDateTime = getCurentDateTime();
						$postData['CrDateTime'] = $crDateTime;
                        $notifications = array(
                            'StudentId' => $user['UserId'],
                            'AdminId' => ADMIN_ID,
                            'IsRead' => 1,
                            'IsFromStudent' => 2,
                            'Message' => 'Thành viên '.$user['FullName'].' ('.$user['PhoneNumber'].') yêu cầu rút '.priceFormat($postData['WithdrawalCost']).' đ.',
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
						$this->loadModel(array('Mwithdrawals', 'Mnotifications'));
						$flag = $this->Mwithdrawals->insert($postData, $notifications);
						if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Thông báo rút tiền đã được gửi đến quản lý thành công"));
						else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
					}
					else echo json_encode(array('code' => -1, 'message' => "Bạn không đủ số tiền để rút"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Chủ tài khoản ngân hàng phải trùng tên với chủ tài khoản Z"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$withdrawalId = $this->input->post('WithdrawalId');
		$withdrawalStatusId = $this->input->post('WithdrawalStatusId');
		if($user['RoleId'] == 1 && $withdrawalId > 0 && $withdrawalStatusId > 0){
			$this->loadModel(array('Mwithdrawals', 'Mtransactionlogs', 'Mnotifications'));
			$withdrawal = $this->Mwithdrawals->get($withdrawalId, true, '', 'StudentId, WithdrawalCost, WithdrawalStatusId');
			if($withdrawal && ($withdrawal['WithdrawalStatusId'] == 1 || $withdrawal['WithdrawalStatusId'] == 2) && $withdrawal['WithdrawalStatusId'] != $withdrawalStatusId){
			    $crDateTime = getCurentDateTime();
				$postData = array(
					'WithdrawalStatusId' => $withdrawalStatusId,
					'UpdateUserId' => $user['UserId'],
					'UpdateDateTime' => $crDateTime,
				);
				$metaData = array(
					'StudentId' => $withdrawal['StudentId'],
					'WithdrawalCost' => $withdrawal['WithdrawalCost'],
                    'Notifications' => array()
				);
				if($withdrawalStatusId == 3){
					$student = $this->Musers->get($withdrawal['StudentId'], true, '', 'FullName, PhoneNumber, Balance');
					if($student) {
						if($student['Balance'] >= $withdrawal['WithdrawalCost']) $metaData = array_merge($metaData, $student);
						else {
							echo json_encode(array('code' => -1, 'message' => "Tài khoản học viên hiện không đủ tiền để rút"));
							die();
						}
					}
					else{
						echo json_encode(array('code' => -1, 'message' => "Không tìm thấy học viên"));
						die();
					}
                    $metaData['Notifications'] = array(
                        'StudentId' => $withdrawal['StudentId'],
                        'AdminId' => $user['UserId'],
                        'IsRead' => 1,
                        'IsFromStudent' => 1,
                        'Message' => 'Yêu cầu rút tiền của bạn đã thành công, vui lòng kiểm tra tài khoản',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    );
				}
				elseif($withdrawalStatusId > 3){
                    $metaData['Notifications'] = array(
                        'StudentId' => $withdrawal['StudentId'],
                        'AdminId' => $user['UserId'],
                        'IsRead' => 1,
                        'IsFromStudent' => 1,
                        'Message' => 'Yêu cầu rút tiền của bạn không được chấp nhận',
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $crDateTime
                    );
                }
				$withdrawalId = $this->Mwithdrawals->changeWithdrawalStatus($postData, $withdrawalId, $metaData);
				if($withdrawalId > 0){
				    if(!empty($metaData['Notifications'])){
						$tokenFCM = $this->Musers->getFieldValue(array('UserId' => $metaData['Notifications']['StudentId']), 'TokenFCM');
						sendFCM($metaData['Notifications']['Message'], $tokenFCM);
					}
					$statusName = '<span class="'.$this->Mwithdrawals->labelCss[$withdrawalStatusId].'">'.$this->Mconstants->withdrawalStatus[$withdrawalStatusId].'</span>';
					echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái rút tiền thành công", 'data' => array('StatusName' => $statusName)));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function toSlug($str) {
	    $str = trim(mb_strtolower($str));
	    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
	    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
	    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
	    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
	    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
	    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
	    $str = preg_replace('/(đ)/', 'd', $str);
	    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
	    return $str;
	}
}