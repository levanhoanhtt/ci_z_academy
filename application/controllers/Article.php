<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller{

    public function index($pageId = 1){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Tin tức',
            array(
                'scriptFooter' => array('js' => 'js/article.js'),
                'pageIcon' => 'news.png',
                'listUsers' => $this->Musers->getListForSelect(1)
            )
        );
        if($user['RoleId'] == 1){
            $this->load->model('Marticles');
            $postData = $this->arrayFromPost(array('ArticleTitle', 'ArticleStatusId', 'CrUserId'));
            $postData['UserId'] = $user['UserId'];
            $postData['RoleId'] = $user['RoleId'];
            $rowCount = $this->Marticles->getCount($postData);
            $data['listArticles'] = array();
            if ($rowCount > 0) {
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if (!is_numeric($page) || $page < 1) $page = 1;
                $data['listArticles'] = $this->Marticles->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('article/admin', $data);
        }
        else{
            $this->load->model('Marticles');
            $postData = array('ArticleStatusId' => STATUS_ACTIVED, 'UserId' => $user['UserId'], 'RoleId' => $user['RoleId']);
            $rowCount = $this->Marticles->getCount($postData);
            $data['listArticles'] = array();
            if ($rowCount > 0) {
                $perPage = 10; 
                $pageCount = ceil($rowCount / $perPage);
                if(!is_numeric($pageId) || $pageId < 1) $pageId = 1;
                $data['listArticles'] = $this->Marticles->search($postData, $perPage, $pageId);
                $data['paggingHtml'] = getPaggingHtml($pageId, $pageCount, 'paggingStudent');
            }
            $this->load->view('article/student', $data);
        }
    }

    public function add(){
        $user = $this->checkUserLogin();
        if($user['RoleId'] == 1) {
            $data = $this->commonData($user,
                'Thêm mới Bài viết',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/article_update.js')),
                    'pageIcon' => 'news.png'
                )
            );
            $data['listStudents'] = $this->Musers->search(array('StatusId' => STATUS_ACTIVED, 'RoleId' => 2), 0, 1, false, 'UserId, FullName, PhoneNumber');
            $this->load->view('article/add', $data);
        }
        else redirect('article');
    }

    public function edit($articleId){
        $user = $this->checkUserLogin();
        if($user['RoleId'] == 1 && $articleId > 0) {
            $data = $this->commonData($user,
                'Cật nhật Bài viết',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css'),
                    'scriptFooter' => array('js' => array('ckeditor/ckeditor.js', 'vendor/plugins/datetimepicker/js/bootstrap-datetimepicker.js', 'js/article_update.js')),
                    'pageIcon' => 'news.png'
                )
            );
            $this->loadModel(array('Marticles', 'Marticleusers'));
            $article = $this->Marticles->get($articleId);
            $data['listStudents'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));
            if ($article) {
                $data['articleId'] = $articleId;
                $data['article'] = $article;
                $data['listStudents'] = $this->Musers->search(array('StatusId' => STATUS_ACTIVED, 'RoleId' => 2), 0, 1, false, 'UserId, FullName, PhoneNumber');
                if($article['IsPublic'] == 1) $data['listUserIds'] = $this->Marticleusers->getUserIds($articleId);
                else $data['listUserIds'] = array();
            }
            else {
                $data['articleId'] = 0;
                $data['txtError'] = "Không tìm thấy Bài viết";
            }
            $this->load->view('article/edit', $data);
        }
        else redirect('article');
    }

    public function view($articleId = 0){
        $user = $this->checkUserLogin();
        if($articleId > 0) {
            $data = $this->commonData($user,
                'Xem Bài viết',
                array('pageIcon' => 'news.png')
            );
            $this->loadModel(array('Marticles', 'Marticleusers', 'Marticlereads'));
            $article = $this->Marticles->get($articleId);
            if($article && $article['ArticleStatusId'] > 0) {
                $canView = true;
                if($user['RoleId'] == 2){
                    if($article['ArticleStatusId'] == STATUS_ACTIVED){
                        if($article['IsPublic'] == 1) $canView = $this->Marticleusers->checkCanView($articleId, $user['UserId']);
                        if($canView) $this->Marticlereads->save(array('ArticleId' => $articleId, 'UserId' => $user['UserId']));
                    }
                    else $canView = false;
                }
                if($canView){
                    $data['title'] = $article['ArticleTitle'];
                    $data['articleId'] = $articleId;
                    $data['article'] = $article;
                    $data['crFullName'] = $this->Musers->getFieldValue(array('UserId' => $article['CrUserId']), 'FullName');
                }
                else{
                    $data['articleId'] = 0;
                    $data['txtError'] = "Không tìm thấy Bài viết";
                }
            }
            else {
                $data['articleId'] = 0;
                $data['txtError'] = "Không tìm thấy bài viết";
            }
            $this->load->view('article/view', $data);
        }
        else redirect('article');
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ArticleTitle', 'ArticleSlug', 'ArticleLead', 'ArticleContent', 'ArticleTypeId', 'ArticleStatusId', 'ArticleImage','IsPublic'));
        if (empty($postData['ArticleSlug'])) $postData['ArticleSlug'] = makeSlug($postData['ArticleTitle']);
        else $postData['ArticleSlug'] = makeSlug($postData['ArticleSlug']);
        $postData['ArticleImage'] = replaceFileUrl($postData['ArticleImage']);
        $articleId = $this->input->post('ArticleId');
        $crDateTime = getCurentDateTime();
        $flag = true;
        $publishDateTime = trim($this->input->post('AutoPublishDateTime'));
        if(!empty($publishDateTime)){
            $publishDateTime = ddMMyyyyToDate($publishDateTime, 'd/m/Y H:i', 'Y-m-d H:i');
            if(strtotime($crDateTime) >= strtotime($publishDateTime)) $flag = false;
            else{
                $postData['ArticleStatusId'] = 4;
                $postData['PublishDateTime'] = $publishDateTime;
                $postData['AutoPublishDateTime'] = $publishDateTime;
            }
        }
        elseif($articleId == 0){
            $publishDateTime = trim($this->input->post('PublishDateTime'));
            if (!empty($publishDateTime)) $postData['PublishDateTime'] = ddMMyyyyToDate($publishDateTime, 'd/m/Y H:i', 'Y-m-d H:i');
            else $postData['PublishDateTime'] = $crDateTime;
        }
        if($flag){
            $this->load->model('Marticles');
            $isSendFCM = $postData['ArticleStatusId'] == 2;
            if ($articleId > 0) {
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                if($isSendFCM){
                    $articleStatusIdOld = $this->Marticles->getFieldValue(array('ArticleId' => $articleId), 'ArticleStatusId', STATUS_ACTIVED);
                    $isSendFCM = $articleStatusIdOld != STATUS_ACTIVED;
                }
            }
            else {
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = $crDateTime;
            }
            $userIds = json_decode(trim($this->input->post('UserIds')), true);
            $articleId = $this->Marticles->update($postData, $articleId, $userIds);
            $notifications = array();
            if ($articleId > 0){
                if($isSendFCM) {
                    if ($postData['IsPublic'] == 1) {
                        foreach ($userIds as $userId) {
                            $notifications[] = array(
                                'StudentId' => $userId,
                                'AdminId' => $user['UserId'],
                                'Message' => $postData['ArticleTitle'],
                                'IsRead' => 1,
                                'IsFromStudent' => 1,
                                'ArticleId' => $articleId,
                                'ArticleContent' => $postData['ArticleContent'],
                                'CrUserId' => $user['UserId'],
                                'CrDateTime' => $crDateTime
                            );
                        }
                        $tokenFCMs = $this->Musers->getListToken($userIds);
                        if (!empty($tokenFCMs)) sendFCM($postData['ArticleTitle'], json_encode($tokenFCMs));
                    }
                    else {
                        $notifications[] = array(
                            'StudentId' => 0,
                            'AdminId' => $user['UserId'],
                            'IsRead' => 1,
                            'IsFromStudent' => 1,
                            'ArticleId' => $articleId,
                            'ArticleContent' => $postData['ArticleContent'],
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
                        sendFCM($postData['ArticleTitle'], DEFAULT_TOPIC_NOTIFICATION, true);
                    }
                    if (!empty($notifications)) $this->db->insert_batch('notifications', $notifications);
                }
                echo json_encode(array('code' => 1, 'message' => "Cập nhật bài viết thành công", 'data' => $articleId));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Ngày hẹn xuất bản phải lớn hơn ngày hiện tại"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $articleId = $this->input->post('ArticleId');
        $statusId = $this->input->post('StatusId');
        if ($articleId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->articleStatus)) {
            $this->loadModel(array('Marticles', 'Mnotifications'));
            $flag = $this->Marticles->changeStatus($statusId, $articleId, 'ArticleStatusId', $user['UserId']);
            if ($flag) {
                $statusName = "";
                if ($statusId == 0){
                    $this->db->delete('notifications', array('ArticleId' => $articleId));
                    $txtSuccess = "Xóa bài viết thành công";
                }
                else {
                    $txtSuccess = "Đổi trạng thái thành công";
                    $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->articleStatus[$statusId] . '</span>';
                }
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getCountUnRead(){
        $user = $this->session->userdata('user');
        if($user && $user['RoleId'] == 2){
            $this->load->model('Marticles');
            echo $this->Marticles->getCount(array('ArticleStatusId' => STATUS_ACTIVED, 'UserId' => $user['UserId'], 'RoleId' => $user['RoleId'], 'IsGetCountUnRead' => 1));
        }
        else echo '0';
    }
}
