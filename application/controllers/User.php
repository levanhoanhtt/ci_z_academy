<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function index(){
		if(!$this->session->userdata('user')){
			$data = array('title' => 'Đăng nhập');
			if($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
			$this->load->helper('cookie');
			$data['userName'] = $this->input->cookie('userName', true);
			$data['userPass'] = $this->input->cookie('userPass', true);
			$data['videoGuideUrl'] = $this->Mconfigs->getConfigValue('VIDEO_GUIDE', 'javascript:void(0)');
			$this->load->view('user/login', $data);
		}
		else redirect('user/dashboard');
	}

	public function register($affUserId = 0){
		if($this->session->userdata('user')) redirect('user/dashboard');
		else{
			$data = array('title' => 'Đăng ký thành viên', 'affUserId' => 0, 'affFullName' => '');
			$affUserId -= 10000;
			if($affUserId > 0){
				$affFullName = $this->Musers->getFieldValue(array('UserId' => $affUserId, 'RoleId' => 2), 'FullName');
				if(!empty($affFullName)){
					$data['affUserId'] = $affUserId;
					$data['affFullName'] = $affFullName;
				}
			}
			$this->load->view('user/register', $data);
		}
	}

	public function logout(){
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
		redirect('user');
	}

	public function forgotPass(){
		$this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu'));
	}

	public function sendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				echo json_encode(array('code' => -1, 'message' => "Email không hợp lệ"));
				die();
			}
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName,PhoneNumber");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				//$message = "Xin chào {$user['FullName']}<br/>Xin mời vào địa chỉ ".base_url('user/changePass/'.$token).' để tạo mật khẩu mới.';
				$message = 'Tài khoản của bạn đăng kí với số điện thoại '.$user['PhoneNumber'].'. Bấm <a href="'.base_url('user/changePass/'.$token).'" target="_blank">vào đây</a> để lấy lại mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Quên mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					$this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtSuccess' => 'Vui lòng kiểm tra email (có thể trong hộp Thư rác) và làm theo hướng dẫn'));
				}
			}
			else $this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtError' => 'Không tìm thấy người dùng'));
		}
		else $this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtError' => 'Email không được bỏ trống'));
	}

	public function changePass($token = ''){
		$token = trim($token);
		$data = array('title' => 'Quên mật khẩu', 'token' => $token);
		$isWrongToken = true;
		if(!empty($token)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Token' => $token), true, "", "UserId");
			if($user){
				if($this->input->post('UserPass')) {
					$postData = $this->arrayFromPost(array('UserPass', 'RePass'));
					if (!empty($postData['UserPass']) && $postData['UserPass'] == $postData['RePass']) {
						$this->Musers->save(array('UserPass' => md5($postData['UserPass']), 'Token' => ''), $user['UserId']);
						$this->session->set_flashdata('txtSuccess', "Thay đổi mật khẩu thành công");
						redirect('user');
						exit();
					}
					else $data['txtError'] = "Mật khẩu không trùng";
				}
			}
			else {
				$data['txtError'] = "Mã Token không đúng";
				$isWrongToken = false;
			}
		}
		else {
			$data['txtError'] = "Mã Token không đúng";
			$isWrongToken = false;
		}
		$data['isWrongToken'] = $isWrongToken;
		$this->load->view('user/changepass', $data);
	}

	public function verifyEmail($token = ''){
		$token = trim($token);
		$data = array('title' => 'Xác thực tài khoản', 'token' => $token);
		if(!empty($token)){
			$user = $this->Musers->getBy(array('TokenEmail' => $token), true, "", "UserId, StatusId, SenEmailTime");
			$crDateTime = getCurentDateTime();
			if($user){
				if($user['StatusId'] == STATUS_ACTIVED){
					$flag = $this->Musers->save(array('TokenEmail' => '', 'IsVerifyEmail' => STATUS_ACTIVED, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $user['UserId']);
					if($flag > 0) $data['txtSuccess'] = 'Xác thực email thành công';
					else $data['txtError'] = "Có lỗi xảy ra trong quá trình thực hiện";
				}
				elseif($user['StatusId'] == 1){
					/*$diff = abs(strtotime($crDateTime) - strtotime($user['SenEmailTime']));
					$min  = floor(($diff - floor($diff / 3600) * 3600) / 60);
					if($min <= 10){*/
						$flag = $this->Musers->save(array('StatusId' => STATUS_ACTIVED, 'TokenEmail' => '', 'IsVerifyEmail' => STATUS_ACTIVED, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $user['UserId']);
						if($flag > 0) $data['txtSuccess'] = 'Xác thực email thành công';
						else $data['txtError'] = "Có lỗi xảy ra trong quá trình thực hiện";
					//} else $data['txtError'] = "Mã Token đã hết hạn";
				}
				else $data['txtError'] = "Mã Token không đúng";
			}
			else $data['txtError'] = "Mã Token không đúng";
		}
		else $data['txtError'] = "Mã Token không đúng";
		$this->load->helper('cookie');
		$data['userName'] = $this->input->cookie('userName', true);
		$data['userPass'] = $this->input->cookie('userPass', true);
		$data['videoGuideUrl'] = $this->Mconfigs->getConfigValue('VIDEO_GUIDE', 'javascript:void(0)');
		$this->load->view('user/login', $data);
	}

	public function permission(){
		$this->load->view('user/permission');
	}

	public function profile(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Cài đặt',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/user_profile.js')),
				'pageIcon' => 'key-icon.png'
			)
		);
		$this->load->view('user/profile', $data);
	}

	public function secure(){
		$user = $this->checkUserLogin();
		$this->load->helper('slug');
		require_once APPPATH."/libraries/GoogleAuthenticator.php";
		$ga = new PHPGangsta_GoogleAuthenticator();
		if($user['IsEnabled2FA'] == 2 && !empty($user['SecretCode']))  $secretCode = $user['SecretCode'];
		else $secretCode = $ga->createSecret();
		$qrCodeUrl = $ga->getQRCodeGoogleUrl(str_replace(' ', '%20', $user['PhoneNumber']) . '%20(Z%20Academy)', $secretCode);
		$data = $this->commonData($user,
			'Bảo mật',
			array(
				'scriptFooter' => array('js' => 'js/user_secure.js'),
				'pageIcon' => 'key-yl-icon.png',
				'qrCodeUrl' => $qrCodeUrl,
				'secretCode' => $secretCode
			)
		);
		$this->load->view('user/secure', $data);
	}
	public function checkCode2Fa() {
		$user = $this->checkUserLogin();
		$isEnabled2FA = $this->input->post('IsEnabled2FA');
		$userCode = trim($this->input->post('UserCode'));
		$secretCode = trim($this->input->post('SecretCode'));
		if($isEnabled2FA > 0 && !empty($secretCode)) {
			if($isEnabled2FA == 2 && !empty($userCode)) {
				require_once APPPATH."/libraries/GoogleAuthenticator.php";
				$ga = new PHPGangsta_GoogleAuthenticator();
				$checkResult = $ga->verifyCode($secretCode, $userCode, TIME_SIZE_2FA);
				if ($checkResult) {
					$flag = $this->Musers->save(array('IsEnabled2FA' => 2, 'SecretCode' => $secretCode), $user['UserId']);
					if ($flag) {
						$user['IsEnabled2FA'] = 2;
						$user['SecretCode'] = $secretCode;
						$this->session->set_userdata('user', $user);
						echo json_encode(array('code' => 1, 'message' => "Cài đặt 2FA thành công"));
					}
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Mã token không đúng, vui lòng nhập lại mã"));
			}
			elseif($isEnabled2FA == 1){
				$flag = $this->Musers->save(array('IsEnabled2FA' => 1, 'SecretCode' => ''), $user['UserId']);
				if ($flag) {
					$user['IsEnabled2FA'] = 1;
					$user['SecretCode'] = '';
					$this->session->set_userdata('user', $user);
					echo json_encode(array('code' => 1, 'message' => "Bỏ cài đặt 2FA thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function dashboard(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1) redirect('user/student');
		else redirect('charge');
	}

	public function student(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách tài khoản',
			array(
				'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css', /*'vendor/plugins/switchery/dist/switchery.css'*/)),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js' /*'vendor/plugins/switchery/dist/switchery.js'*/, 'js/user_student.js')),
				'pageIcon' => 'currency-icon.png'
			)
		);
		if($user['RoleId'] == 1){
			$postData = $this->arrayFromPost(array('SearchText', 'StatusId', 'IsMember', 'BeginDate', 'EndDate', 'BalanceStatusId', 'TicketStatusId'));
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$postData['RoleId'] = 2;
			$rowCount = $this->Musers->getCount($postData);
			$data['listStudents'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listStudents'] = $this->Musers->search($postData, $perPage, $page, true);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			if($this->input->post('export')) $this->exportStudent($postData);
			$this->load->view('user/student', $data);
		}
		else redirect('charge');
	}

	private function exportStudent($postData){
		$listStudents = $this->Musers->search($postData, PHP_INT_MAX, 1, true);
		if(!empty($listStudents)){
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$sheet = $this->excel->getActiveSheet();
			$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
			$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$sheet->getPageSetup()->setFitToPage(true);
			$sheet->getPageSetup()->setFitToWidth(1);
			$sheet->getPageSetup()->setFitToHeight(0);
			$sheet->setTitle("DanhSachHocVien");
			$sheet->setCellValue('A1', "Danh sách học viên");
			$sheet->getStyle('A1')->getFont()->setSize(14);
			$sheet->getStyle('A1')->getFont()->setBold(true);
			$sheet->mergeCells('A1:H1');
			$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$border = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$sheet->setCellValue('A2', "STT");
			$sheet->setCellValue('B2', "Họ và tên");
			$sheet->setCellValue('C2', "Số điện thoại");
			$sheet->setCellValue('D2', "Email");
			$sheet->setCellValue('E2', "Z");
			$sheet->setCellValue('F2', "Số tiền");
			$sheet->setCellValue('G2', "Số Zticket");
			$sheet->setCellValue('H2', "Số ghi danh");
			$colLeters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');
			foreach($colLeters as $l) $sheet->getStyle($l.'2')->getFont()->setBold(true);
			$i = 2;
			foreach($listStudents as $u){
				$i++;
				$sheet->setCellValue('A'.$i, $i - 2);
				$sheet->setCellValue('B'.$i, $u['FullName']);
				$sheet->setCellValueExplicit('C' . $i, $u['PhoneNumber'], PHPExcel_Cell_DataType::TYPE_STRING);
				$sheet->setCellValue('D'.$i, $u['Email']);
				$sheet->setCellValue('E'.$i, $u['IsMember'] == 2  ? 'Có' : 'Không');
				$sheet->setCellValue('F'.$i, $u['Balance']);
				$sheet->setCellValue('G'.$i, $u['TicketCount']);
				$sheet->setCellValue('H'.$i, $u['CountRegister']);
			}
			$sheet->getStyle('A2:H'.$i)->applyFromArray($border);
			$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(true);
			foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
			$filename = "DanhSachHocVien.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');
		}
	}

	public function admin(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Danh sách tài khoản',
			array(
				'scriptFooter' => array('js' => 'js/user_admin.js'),
				'pageIcon' => 'currency-icon.png'
			)
		);
		if($user['RoleId'] == 1){
			$postData = $this->arrayFromPost(array('SearchText'));
			$postData['StatusId'] = STATUS_ACTIVED;
			$postData['RoleId'] = 1;
			$rowCount = $this->Musers->getCount($postData);
			$data['listUsers'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listUsers'] = $this->Musers->search($postData, $perPage, $page, false);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('user/admin', $data);
		}
		else redirect('charge');
	}

	public function add(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Thêm người dùng',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/user_update.js'))
			)
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'user/staff')) {
			$this->load->view('user/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($userId = 0){
		if($userId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Cập nhật người dùng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'js/user_update.js'))
				)
			);
			$userEdit = $this->Musers->get($userId);
			if ($userEdit) {
				$listActions = $data['listActions'];
				if ($this->Mactions->checkAccess($listActions, 'user/staff')) {
					$data['canEdit'] = $this->Mactions->checkAccess($listActions, 'user/staff');
					$data['userId'] = $userId;
					$data['userEdit'] = $userEdit;
					$this->load->view('user/edit', $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else {
				$data['userId'] = 0;
				$data['txtError'] = "Không tìm thấy người dùng";
				$this->load->view('user/edit', $data);
			}
		}
		else redirect('user/profile');
	}
}