<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Quản lý nhãn hiệu',
			array('scriptFooter' => array('js' => 'js/brand.js'))
		);
		if($user['RoleId'] == 2 && $user['IsMember'] != 2) $this->load->view('user/permission', $data);
		else {
			$postData = $this->arrayFromPost(array('BrandName', 'UserId', 'ItemStatusId'));
			if($user['RoleId'] == 2) $postData['UserId'] = $user['UserId'];
			else $data['listUsers'] = $this->Musers->getListForSelect(2, 2);
			$this->load->model('Mbrands');
			$rowCount = $this->Mbrands->getCount($postData);
			$data['listBrands'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listBrands'] = $this->Mbrands->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('product/brand', $data);
		}
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('BrandName', 'UserId', 'ItemStatusId'));
		if(!empty($postData['BrandName'])){
			$brandId = $this->input->post('BrandId');
            $this->loadModel(array('Mbrands','Mnotifications'));
			if($user['RoleId'] == 1){
				if($brandId > 0){
					$flag = $this->Mbrands->checkExist($postData['BrandName'], $postData['UserId'], $brandId);
					if(!$flag){
						$postData['UpdateUserId'] = $user['UserId'];
						$postData['UpdateDateTime'] = getCurentDateTime();
						$this->updateBrand($postData, $brandId, $user);
					}
					else echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu này đã tồn tại"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Quản lý không có quyền thêm nhãn hiệu"));
			}
			elseif($user['RoleId'] == 2 && $user['IsMember'] == 2){
				$flag = $this->Mbrands->checkExist($postData['BrandName'], $user['UserId'], $brandId);
				if(!$flag){
					$isValid = false;
					if($brandId > 0){
						$itemStatusId = $this->Mbrands->getFieldValue(array('BrandId' => $brandId), 'ItemStatusId', 0);
						if($itemStatusId == 1){
							$postData['ItemStatusId'] = 1;
							$postData['UpdateUserId'] = $user['UserId'];
							$postData['UpdateDateTime'] = getCurentDateTime();
							$isValid = true;
						}
						else echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu này không có quyền sửa"));
					}
					else{
						$postData['UserId'] = $user['UserId'];
						$postData['ItemStatusId'] = 1;
						$postData['CrUserId'] = $user['UserId'];
						$postData['CrDateTime'] = getCurentDateTime();
						$isValid = true;
					}
					if($isValid) $this->updateBrand($postData, $brandId, $user);
				}
				else echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu này đã tồn tại"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$brandId = $this->input->post('BrandId');
		$itemStatusId = $this->input->post('ItemStatusId');
		if($brandId > 0 && $itemStatusId >= 0 && $itemStatusId < 4){
		    $this->loadModel(array('Mbrands', 'Mnotifications'));
			if($user['RoleId'] == 1) $this->changeStatusBranch($brandId, $itemStatusId, $user);
			elseif($user['RoleId'] == 2 && $user['IsMember'] == 2){
				$itemStatusIdOld = $this->Mbrands->getFieldValue(array('BrandId' => $brandId), 'ItemStatusId', 0);
				if($itemStatusId == 0){
					if($itemStatusIdOld == 1) $this->changeStatusBranch($brandId, $itemStatusId, $user);
					else echo json_encode(array('code' => -1, 'message' => "Nhãn hiệu này không có quyền xóa"));
				}
				elseif($itemStatusId == 2){
					if($itemStatusIdOld == 3) $this->changeStatusBranch($brandId, $itemStatusId, $user);
					else echo json_encode(array('code' => -1, 'message' => "Bạn không có quyền kích hoạt nhãn hiệu"));
				}
				elseif($itemStatusId == 3){
					if($itemStatusIdOld == 2) $this->changeStatusBranch($brandId, $itemStatusId, $user);
					else echo json_encode(array('code' => -1, 'message' => "Bạn không có quyền ngừng kinh doanh nhãn hiệu"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	private function updateBrand($postData, $brandId, $user){
        $notifications = array();
        if($user['RoleId'] == 1){
			if($postData['ItemStatusId'] == 2 || $postData['ItemStatusId'] == 4) {
				$notifications = array(
					'StudentId' => $postData['UserId'],
					'AdminId' => $user['UserId'],
					'IsRead' => 1,
					'IsFromStudent' => 1,
					'Message' => 'Đăng kí Nhãn hiệu ' . $postData['BrandName'] . ' của bạn đã được ' . ($postData['ItemStatusId'] == 2 ? 'đã' : 'không') . '  được phê duyệt',
					'CrUserId' => $user['UserId'],
					'CrDateTime' => getCurentDateTime()
				);
			}
        }
        else{
            if($brandId == 0) {
                $notifications = array(
                    'StudentId' => $user['UserId'],
                    'AdminId' => ADMIN_ID,
                    'IsRead' => 1,
                    'IsFromStudent' => 2,
                    'Message' => 'Thành viên ' . $user['FullName'] . ' (' . $user['PhoneNumber'] . ') đăng kí nhãn hiệu ' . $postData['BrandName'],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => getCurentDateTime()
                );
            }
        }
		$flag = $this->Mbrands->update($postData, $brandId, $notifications);
		if($flag > 0) {
			if(!empty($notifications) && $notifications['IsFromStudent'] == 1){
				$tokenFCM = $this->Musers->getFieldValue(array('UserId' => $notifications['StudentId']), 'TokenFCM');
				sendFCM($notifications['Message'], $tokenFCM);
			}
			$postData['BrandId'] = $flag;
			$postData['StatusName'] = '<span class="'.$this->Mconstants->labelCss[$postData['ItemStatusId']].'">'.$this->Mconstants->itemStatus[$postData['ItemStatusId']].'</span>';
			$postData['IsAdd'] = ($brandId > 0) ? 0 : 1;
			echo json_encode(array('code' => 1, 'message' => "Cập nhật nhãn hiệu thành công", 'data' => $postData));
		}
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	private function changeStatusBranch($brandId, $itemStatusId, $user){
        $notifications = array();
	    if($user['RoleId'] == 1 && $itemStatusId > 0){
	        $brand = $this->Mbrands->get($brandId, true, '', 'BrandName, UserId');
            $notifications = array(
                'StudentId' => $brand['UserId'],
                'AdminId' => $user['UserId'],
                'IsRead' => 1,
                'IsFromStudent' => 1,
                'Message' => 'Đăng kí Nhãn hiệu '.$brand['BrandName'].' của bạn đã được '.($itemStatusId == 2 ? 'đã' : 'không').'  được phê duyệt',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => getCurentDateTime()
            );
        }
		$flag = $this->Mbrands->update(array('ItemStatusId' => $itemStatusId, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), $brandId, $notifications);
		if($flag){
            if(!empty($notifications)){
				$tokenFCM = $this->Musers->getFieldValue(array('UserId' => $notifications['StudentId']), 'TokenFCM');
				sendFCM($notifications['Message'], $tokenFCM);
			}
			if($itemStatusId > 0) echo json_encode(array('code' => 1, 'message' => 'Cập nhật nhãn hiệu thành công', 'data' => array('StatusName' => '<span class="'.$this->Mconstants->labelCss[$itemStatusId].'">'.$this->Mconstants->itemStatus[$itemStatusId].'</span>')));
			else echo json_encode(array('code' => 1, 'message' => "Xóa nhãn hiệu thành công"));
		}
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}