<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

	public function cost(){
		//$this->loadModel(array('Mconfigs', 'Mticketcosts'));
		$this->load->model('Mticketcosts');
		if(!$this->Mticketcosts->checkExist()) {
			$configs = $this->Mconfigs->getListMap(2);
			$oldCost = $configs['TICKET_COST'];
			$newCost = $oldCost + $configs['COST_PER_DAY'];
			$this->Mconfigs->updateTicketCost($oldCost, $newCost);
			echo 'Success' . PHP_EOL;
		}
		else echo 'Existed' . PHP_EOL;
	}

    public function serviceCharge(){
        $this->loadModel(array('Muserservices', 'Mservices', 'Mservicecharges', 'Mtransactionlogs', 'Mservicelogs'));
		$today = date('Y-m-d');
		$serviceCosts = array();
		$serviceNames = array();
		$userIds = array(); //send mail
		$serviceLogs = array();
        $listUserServices = $this->Muserservices->getListCharge($today);
		foreach($listUserServices as $us){
			if(!isset($serviceCosts[$us['ServiceId']])){
				$service = $this->Mservices->get($us['ServiceId'], true, '', 'ServiceName, ServiceCost, StatusId');
				if($service && $service['StatusId'] == STATUS_ACTIVED){
					$serviceNames[$us['ServiceId']] = $service['ServiceName'];
					$serviceCosts[$us['ServiceId']] = $service['ServiceCost'];
				}
				else $serviceCosts[$us['ServiceId']] = 0;
			}
			$serviceCost = $serviceCosts[$us['ServiceId']];
			if($serviceCost > 0) {
				if (!empty($us['EndDate'])){ //den ngay phai tra toan bo
					if($today == $us['BeginDate']){
						$servicePeriod = $this->Muserservices->timeDiff($us['BeginDate'], $us['EndDate'], $us['ServiceTypeId']);
						$serviceCharge = array(
							'UserServiceId' => $us['UserServiceId'],
							'ChargeDate' => $today,
							'ServiceCost' => $serviceCost,
							'ServicePeriod' => $servicePeriod,
							'PaidCost' => $serviceCost * $servicePeriod
						);
						$user = $this->Musers->get($us['UserId'], false, '', 'UserId, FullName, PhoneNumber, Balance');
						if($us['ServiceTypeId'] == 3){
							if(ddMMyyyy($us['BeginDate'], 'm/Y') == ddMMyyyy($us['EndDate'], 'm/Y')) $comment = " tháng ".ddMMyyyy($us['BeginDate'], 'm/Y');
							else $comment = " từ tháng ".ddMMyyyy($us['BeginDate'], 'm/Y')." đến tháng ".ddMMyyyy($us['EndDate'], 'm/Y');
						}
						else $comment = " từ ngày ".ddMMyyyy($today)." đến ngày ".ddMMyyyy($us['EndDate']);
						$serviceInfo = array(
							'ServiceName' => $serviceNames[$us['ServiceId']],
							'ServiceStatusId' => $us['ServiceStatusId'],
							'Comment' => $comment
						);
						$flag = $this->Mservicecharges->insert($serviceCharge, $user, $serviceInfo);
						if($flag) echo $today.' Charge OK ' . $us['UserServiceId'] . PHP_EOL;
						else echo $today. 'Charge Error ' . $us['UserServiceId'] . PHP_EOL;
						if($user['Balance'] < $serviceCharge['PaidCost']){
							$serviceLogs[] = array(
								'UserServiceId' => $us['UserServiceId'],
								'UserId' => $us['UserId'],
								'ServiceId' => $us['ServiceId'],
								'Comment' => "Tài khoản không đủ tiền để sử dụng dịch vụ ".$serviceNames[$us['ServiceId']],
								'CrUserId' => 0,
								'CrDateTime' => getCurentDateTime()
							);
							if(!in_array($us['UserId'], $userIds)) $userIds[] = $us['UserId'];
						}
					}
				}
				else{ //xu ly charge theo thang
					if($us['ServiceTypeId'] == 2){ //gia han vao ngay nay thang do
						$dateBeginDate = ddMMyyyy($us['BeginDate'], 'd');
						$dateToday = ddMMyyyy($today, 'd');
						$flag = $dateToday == $dateBeginDate;
						if(!$flag){
							$monthToday = ddMMyyyy($today, 'm');
							if($dateBeginDate == 31){
								if(in_array($monthToday, array(4, 6, 9, 11))) $flag = $dateToday == 30; //thang nay co 30 ngay
								elseif($monthToday == 2){ // 28 hoac 29 ngay
									if($this->Muserservices->checkLeapYear(ddMMyyyy($today, 'Y'))) $flag = $dateToday == 29; //thang co 29 ngay
									else $flag = $dateToday == 28; //thang co 28 ngay
								}
							}
							elseif($dateBeginDate == 29 && $monthToday == 2){ //dang ky vao 29 thang 2
								if($this->Muserservices->checkLeapYear(ddMMyyyy($today, 'Y'))) $flag = $dateToday == 29; //thang co 29 ngay
								else $flag = $dateToday == 28; //thang co 28 ngay
							}
						}
						if($flag){
							$serviceCharge = array(
								'UserServiceId' => $us['UserServiceId'],
								'ChargeDate' => $today,
								'ServiceCost' => $serviceCost,
								'ServicePeriod' => 1,
								'PaidCost' => $serviceCost
							);
							$user = $this->Musers->get($us['UserId'], false, '', 'UserId, FullName, PhoneNumber, Balance');
							$serviceInfo = array(
								'ServiceName' => $serviceNames[$us['ServiceId']],
								'ServiceStatusId' => $us['ServiceStatusId'],
								'Comment' => " từ ngày ".ddMMyyyy($today)." đến ngày ".date('d/m/Y', strtotime("+1 month", strtotime($today)))
							);
							$flag = $this->Mservicecharges->insert($serviceCharge, $user, $serviceInfo);
							if($flag) echo $today.' Charge OK ' . $us['UserServiceId'] . PHP_EOL;
							else echo $today. 'Charge Error ' . $us['UserServiceId'] . PHP_EOL;
							if($user['Balance'] < $serviceCharge['PaidCost']){
								$serviceLogs[] = array(
									'UserServiceId' => $us['UserServiceId'],
									'UserId' => $us['UserId'],
									'ServiceId' => $us['ServiceId'],
									'Comment' => "Tài khoản không đủ tiền để sử dụng dịch vụ ".$serviceNames[$us['ServiceId']],
									'CrUserId' => 0,
									'CrDateTime' => getCurentDateTime()
								);
								if(!in_array($us['UserId'], $userIds)) $userIds[] = $us['UserId'];
							}
						}
					}
					elseif($us['ServiceTypeId'] == 3){ //gia han vao dau thang hoac ngay dang ky dich vu
						if(ddMMyyyy($today, 'd') == 1 || $today == $us['BeginDate']){
							$serviceCharge = array(
								'UserServiceId' => $us['UserServiceId'],
								'ChargeDate' => $today,
								'ServiceCost' => $serviceCost,
								'ServicePeriod' => 1,
								'PaidCost' => $serviceCost
							);
							$user = $this->Musers->get($us['UserId'], false, '', 'UserId, FullName, PhoneNumber, Balance');
							$serviceInfo = array(
								'ServiceName' => $serviceNames[$us['ServiceId']],
								'ServiceStatusId' => $us['ServiceStatusId'],
								'Comment' => " tháng ".ddMMyyyy($today, 'm/Y')
							);
							$flag = $this->Mservicecharges->insert($serviceCharge, $user, $serviceInfo);
							if($flag) echo $today.' Charge OK ' . $us['UserServiceId'] . PHP_EOL;
							else echo $today. 'Charge Error ' . $us['UserServiceId'] . PHP_EOL;
							if($user['Balance'] < $serviceCharge['PaidCost']){
								$serviceLogs[] = array(
									'UserServiceId' => $us['UserServiceId'],
									'UserId' => $us['UserId'],
									'ServiceId' => $us['ServiceId'],
									'Comment' => "Tài khoản không đủ tiền để sử dụng dịch vụ ".$serviceNames[$us['ServiceId']],
									'CrUserId' => 0,
									'CrDateTime' => getCurentDateTime()
								);
								if(!in_array($us['UserId'], $userIds)) $userIds[] = $us['UserId'];
							}
						}
					}
				}
			}
		}
		if(!empty($serviceLogs)) $this->db->insert_batch('servicelogs', $serviceLogs);
		if(!empty($userIds)){
			$listUsers = $this->Musers->getListSendMailService();
			if(!empty($listUsers)){
				$configs = $this->Mconfigs->getListMap();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
				foreach($listUsers as $u){
					if(filter_var($u['Email'], FILTER_VALIDATE_EMAIL)){
						$message = "Xin chào {$u['FullName']} ({$u['PhoneNumber']}).<br/> Hiện tại tài khoản của bạn đang nợ học viện với số tiền là ".priceFormat(-$u['Balance'])." VNĐ . Xin vui lòng thanh toán cho Học viện";
						$this->sendMail($emailFrom, $companyName, $u['Email'], 'Thanh toán tiền dịch vụ', $message);
					}
				}
			}
		}
    }

	public function updateExpireService(){
		$this->db->query('UPDATE userservices SET ServiceStatusId = 5 WHERE ServiceStatusId = 2 AND EndDate IS NOT NULL AND EndDate < ?', array(date('Y-m-d')));
	}

	public function verifyEmail(){
		$listStudents = $this->Musers->getBy(array('StatusId > 0', 'IsVerifyEmail !=' => 2));
		if(!empty($listStudents)) {
			$configs = $this->Mconfigs->getListMap();
			$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
			$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z_Academy';
			$crDateTime = getCurentDateTime();
			foreach ($listStudents as $u) {
				if(filter_var($u['Email'], FILTER_VALIDATE_EMAIL)) {
					$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
					$token = substr($token, 0, 14);
					$message = "Xin chào {$u['FullName']}<br/>Xin mời vào địa chỉ bên dưới để xác thực lại tài khoản http://hocvienz.com/user/verifyEmail/".$token;
					$flag = $this->sendMail($emailFrom, $companyName, $u['Email'], 'Xác thực tài khoản', $message);
					if ($flag) {
						$this->Musers->save(array('TokenEmail' => $token, 'SenEmailTime' => $crDateTime, 'IsVerifyEmail' => 3, 'UpdateDateTime' => $crDateTime), $u['UserId']);
						echo 'Send mail to ' .$u['Email'].PHP_EOL;
						usleep(1000);
					}
				}
				else{
					$this->Musers->save(array('IsVerifyEmail' => 1, 'UpdateDateTime' => $crDateTime), $u['UserId']);
					echo 'InValid mail ' .$u['Email'].PHP_EOL;
				}
			}
		}
	}
}