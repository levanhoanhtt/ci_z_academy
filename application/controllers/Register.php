<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1){
			$this->load->model('Mregisters');
			$data = $this->commonData($user,
				'Danh sách học viên',
				array(
					'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/register_list.js')),
					'pageIcon' => 'sign-up.png'
				)
			);
			$data['listUsers'] = $this->Musers->getListForSelect();
			$this->load->model('Mregisters');
			$postData = $this->arrayFromPost(array('StudentId', 'ByUserId', 'SearchText', 'CourseId','CourseTypeId'));
			$postData['RegisterStatusId'] = STATUS_ACTIVED;
			$rowCount = $this->Mregisters->getCount($postData);
			$data['listRegisters'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listRegisters'] = $this->Mregisters->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			if($this->input->post('export')) $this->exportRegister($postData);
			$this->load->view('register/list', $data);
		}
		else{
			$data = $this->commonData($user,
				'Ghi danh',
				array(
					'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css','vendor/plugins/congrats-modal/css/animate.min.css', 'vendor/plugins/congrats-modal/css/style.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/congrats-modal/js/EPyQdv.js' , 'vendor/plugins/congrats-modal/js/mo.min.js', "vendor/plugins/congrats-modal/js/index.js", 'js/register.js')),
					'pageIcon' => 'sign-up.png'
				)
			);
			$this->load->model('Mregisters');
			$configs = $data['configs'];
			$decimalCount = $this->getDecimalCount($configs['PIECES_PER_TICKET']);
			$user['TicketCount'] = round($user['TicketCount'], $decimalCount);
			$data['ticketCost'] = $configs['TICKET_COST'];
			$postData = array('StudentId' => $user['UserId'], 'ByUserId' => $user['UserId'], 'RegisterStatusId' => STATUS_ACTIVED, 'SearchOrUser' => 1);
			$rowCount = $this->Mregisters->getCount($postData);
			$data['listRegisters'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listRegisters'] = $this->Mregisters->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('register/add', $data);
		}
	}

	public function exportRegister($postData){
		$listRegisters = $this->Mregisters->search($postData);
		if(!empty($listRegisters)){
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$sheet = $this->excel->getActiveSheet();
			$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
			$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$sheet->getPageSetup()->setFitToPage(true);
			$sheet->getPageSetup()->setFitToWidth(1);
			$sheet->getPageSetup()->setFitToHeight(0);

			$sheet->setTitle("DanhSachHocVien");
			$sheet->setCellValue('A1', "Danh sách học viên đăng ký");
			$sheet->getStyle('A1')->getFont()->setSize(14);
			$sheet->getStyle('A1')->getFont()->setBold(true);
			$sheet->mergeCells('A1:D1');
			$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$border = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$sheet->setCellValue('A2', "STT");
			$sheet->setCellValue('B2', "Họ và tên");
			$sheet->setCellValue('C2', "Ngày sinh");
			$sheet->setCellValue('D2', "Số điện thoại");
			$colLeters = array('A', 'B', 'C', 'D');
			foreach($colLeters as $l) $sheet->getStyle($l.'2')->getFont()->setBold(true);
			$i = 2;
			foreach($listRegisters as $u){
				$i++;
				$sheet->setCellValue('A'.$i, $i - 2);
				$sheet->setCellValue('B'.$i, $u['StudentName']);
				$sheet->setCellValue('C'.$i, ddMMyyyy($u['BirthDay']));
				$sheet->setCellValueExplicit('D' . $i, $u['PhoneNumber'], PHPExcel_Cell_DataType::TYPE_STRING);
			}
			$sheet->getStyle('A2:D'.$i)->applyFromArray($border);
			$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(true);
			foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
			$filename = "DanhSachThanhVien.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');
		}
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$this->loadModel(array('Mregisters', 'Mtransactionlogs', 'Mactionlogs','Mconfigs', 'Mnotifications'));
		$crDateTime = getCurentDateTime();
		if($user['RoleId'] == 1){
			$postData = $this->arrayFromPost(array('RegisterId','CourseId', 'IsDelete'));
			if($postData['RegisterId'] > 0 && !empty($postData['CourseId'])){
				$register = $this->Mregisters->get($postData['RegisterId']);
				if($register){
					if($postData['IsDelete'] == 0){
						$actionLogs = array(
							'ItemId'=> $postData['RegisterId'],
							'ItemTypeId'=> 2,
							'ActionTypeId'=> 2,
							'Comment' => $user['FullName'] . ' đã sửa ' .$register['StudentName'] . ' từ Khóa ' . $register['CourseId'] . ' thành Khóa ' . $postData['CourseId'],
							'CrUserId'=> $user['UserId'],
							'CrDateTime'=> $crDateTime
						);
						$flag = $this->Mregisters->update(array('CourseId' => $postData['CourseId'], 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime), $postData['RegisterId'], $actionLogs);
						if($flag > 0) echo json_encode(array('code' => 1, 'message' => 'Cập nhật khóa học cho thành viên thành công'));
						else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
					}
					else{
					    $byUser = $this->Musers->get($register['ByUserId'], true, '', 'FullName, PhoneNumber, IsMember, TicketCount');
						$ticketCount =  $byUser['TicketCount'] + $register['TicketCount'];
						//if($byUser['IsMember'] == 2) $ticketCount = $ticketCount - 0.05;
						$register['IsMember'] = $byUser['IsMember'];
						$notifications = array(
							'StudentId' => $register['ByUserId'],
							'AdminId' => $user['UserId'],
							'IsRead' => 1,
							'IsFromStudent' => 1,
							'Message' => $user['FullName']." hủy đăng ký khóa {$postData['CourseId']} của {$register['StudentName']} ({$register['PhoneNumber']}), {$byUser['FullName']} được hoàn lại ".priceFormat($register['TicketCount'], true)." Zticket",
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$flag = $this->Mregisters->cancel($register, $user, $ticketCount, $notifications);
						if($flag){
							if(!empty($notifications)){
								$tokenFCM = $this->Musers->getFieldValue(array('UserId' => $notifications['StudentId']), 'TokenFCM');
								sendFCM($notifications['Message'], $tokenFCM);
							}
                            echo json_encode(array('code' => 1, 'message' => "Xóa ghi danh cho {$register['StudentName']} ({$register['PhoneNumber']}) thành công"));
                        }
                        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
					}
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else{
			$postData = $this->arrayFromPost(array('AgeTypeId','StudentName', 'BirthDay', 'IDCardNumber', 'PhoneNumber', 'CourseTypeId'));
			$courseTypeId = $postData['CourseTypeId'];
			if(!empty($postData['IDCardNumber']) && !empty($postData['PhoneNumber']) && !empty($postData['BirthDay']) && $courseTypeId > 0 && $postData['AgeTypeId'] > 0){
				$postData['BirthDay'] = ddMMyyyyToDate($postData['BirthDay']);
				$yearOld = date('Y') - date_format(date_create($postData['BirthDay']), 'Y');
				if($yearOld > 26 && $postData['AgeTypeId'] == 1){
					echo json_encode(array('code' => -1, 'message' => "Bạn chọn sai ngày sinh"));
					die();
				}
				if($yearOld <= 26 && $postData['AgeTypeId'] == 2){
					echo json_encode(array('code' => -1, 'message' => "Bạn chọn sai ngày sinh"));
					die();
				}
				$configs = $this->Mconfigs->getListMap(2);
                if($courseTypeId == 1) $ticketPay = $configs['ALPHA_VALUE'] / 100;
                if($courseTypeId == 2) $ticketPay = $configs['BETA_VALUE'] / 100;
                if($courseTypeId == 3) $ticketPay = $configs['GAMMA_VALUE'] /100;
				$postData['CourseId'] = $configs['COURSE_ID'];
				if($postData['AgeTypeId'] == 2)  $ticketPay = $ticketPay + $ticketPay * $configs['FACTOR_26']/100;
				$user = $this->Musers->get($user['UserId']);
				if($user['TicketCount'] < $ticketPay){
					echo json_encode(array('code' => -1, 'message' => "Bạn không đủ số Zticket đề ghi danh"));
					die();
				}
				
				$isExist = $this->Mregisters->getBy(array('CourseTypeId' => $courseTypeId, 'PhoneNumber' => $postData['PhoneNumber'], 'CourseId' => $postData['CourseId'], 'RegisterStatusId' => 2), true);
				if($isExist){
				    echo json_encode(array('code' => -1, 'message' => "Số điện thoại này đã đăng kí học phần ".$this->Mconstants->courseTypes[$courseTypeId]." khóa ".$postData['CourseId']));
					die();
				}
				//check conditioning Course
				$isCourseBeta = $this->Mregisters->getBy(array('CourseTypeId=' => 1, 'PhoneNumber' => $postData['PhoneNumber'], 'CourseId' => $postData['CourseId'], 'RegisterStatusId' => 2), true);
				if(!$isCourseBeta && $courseTypeId == 2){
				    echo json_encode(array('code' => -1, 'message' => "Số điện thoại này chưa đăng kí phần Alpha khóa ".$postData['CourseId']));
					die();
				}
				$isCourseGamma = $this->Mregisters->getBy(array('CourseTypeId=' => 2, 'PhoneNumber' => $postData['PhoneNumber'], 'CourseId' => $postData['CourseId'], 'RegisterStatusId' => 2), true);
				if(!$isCourseGamma && $courseTypeId == 3){
				    echo json_encode(array('code' => -1, 'message' => "Số điện thoại này chưa đăng kí phần Beta khóa ".$postData['CourseId']));
					die();
				}
				//
				$postData['StudentId'] = $user['UserId'];
				$postData['ByUserId'] = $user['UserId'];
				$student = false;
				if($user['PhoneNumber'] != $postData['PhoneNumber']){
					$studentId = 0;
					$student = $this->Musers->getBy(array('PhoneNumber' => $postData['PhoneNumber'], 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2), true, '', 'UserId, IDCardNumber, FullName, PhoneNumber');
					if($student){
						$studentId = $student['UserId'];
						if(empty($student['IDCardNumber'])) $this->Musers->save(array('IDCardNumber' => $postData['IDCardNumber']), $studentId);
						elseif($student['IDCardNumber'] != $postData['IDCardNumber']){
							echo json_encode(array('code' => -1, 'message' => "Số chứng minh thư không trùng"));
							die();
						}
					}
					$postData['StudentId'] = $studentId;
				}
				$postData['RegisterStatusId'] = STATUS_ACTIVED;
				$postData['PaidVN'] = $configs['TICKET_COST'] * $ticketPay; 
				$postData['TicketCount'] = $ticketPay;
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = getCurentDateTime();
				$flag = $this->Mregisters->update($postData, 0, array(), $user, $student);
				if($flag){
					/*if($user['IsMember'] == 2) echo json_encode(array('code' => 1, 'message' => 'Chúc mừng bạn đã ghi danh thành công - Bạn được tặng 0.05 Zticket', 'bonus' => 1));
					else*/
						echo json_encode(array('code' => 1, 'message' => 'Chúc mừng bạn đã ghi danh thành công', 'bonus' => 0));
				} 
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
	}

	public function getTicketCountFactor26(){
		$factor26 = $this->Mconfigs->getConfigValue('FACTOR_26', 100);
		echo $this->Mconfigs->getConfigValue('ALPHA_VALUE', 100)/100 + $this->Mconfigs->getConfigValue('ALPHA_VALUE', 100)/100 * $factor26/100;
	}

	public function getCourseIdByPhone(){
		$phoneNumber = trim($this->input->post('PhoneNumber'));
		if(!empty($phoneNumber)){
			$this->load->model('Mregisters');
			$register = $this->Mregisters->getBy(array('PhoneNumber' => $phoneNumber), true, 'CrDateTime');
			if($register){
				$register['BirthDay'] = ddMMyyyy($register['BirthDay']);
				echo json_encode(array('code' => 1, 'message' => 'Bạn đã được ghi danh thành công khóa '.$register['CourseId'], 'data' => $register));
			}
			else echo json_encode(array('code' => 0, 'message' => 'Số điện thoại của bạn không có trong trang ghi danh của học viện'));
		}
		else echo json_encode(array('code' => -1, 'message' => "Số điện thoại không được để trống"));
	}
	
	public function getCourse(){
		$alphaCourse = $this->Mconfigs->getConfigValue('ALPHA_VALUE', 100)/100;
		$betaCourse = $this->Mconfigs->getConfigValue('BETA_VALUE', 100)/100;
		$gammaCourse = $this->Mconfigs->getConfigValue('GAMMA_VALUE', 100)/100;
		$type26 = $this->Mconfigs->getConfigValue('FACTOR_26', 100)/100;
		$postData = $this->arrayFromPost(array('Age','CourseTypeId'));
		if($postData['Age'] == 1){
			if($postData['CourseTypeId'] == 1) echo $alphaCourse;
			else if ($postData['CourseTypeId'] == 2) echo $betaCourse;
			else echo $gammaCourse;
		}
		else if(($postData['Age'] == 2)){
			if($postData['CourseTypeId'] == 1) echo $alphaCourse + $alphaCourse * $type26; 
			else if ($postData['CourseTypeId'] == 2) echo $betaCourse + $betaCourse * $type26;
			else echo $gammaCourse + $gammaCourse * $type26;
		}
	}
	
	public function changeIsFinish(){
		$user = $this->checkUserLogin(true);
		if($user['RoleId'] == 1){
		$RegisterId = $this->input->post('RegisterId');
		$isFinish = $this->input->post('IsFinish');
		$this->load->model('Mregisters');
		$RegisterId = $this->Mregisters->update(array('IsFinish' => $isFinish),$RegisterId);
		if($RegisterId) echo json_encode(array('code' => 1, 'message' => 'Thay đổi trạng thái thành viên thành công'));
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	}
}