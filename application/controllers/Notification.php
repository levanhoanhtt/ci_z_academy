<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thông báo',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', "js/notification.js")),
                'pageIcon' => 'bell-icon.png'
            )
        );
        $this->load->model('Mnotifications');
        $postData = $this->arrayFromPost(array('IsRead', 'BeginDate', 'EndDate'));
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        $postData['IsArticle'] = 1;
        if ($user['RoleId'] == 1) {
            $data['listUsers'] = $this->Musers->getListForSelect();
            $postData['SearchText'] = trim($this->input->post('SearchText'));
            $postData['StudentId'] = $this->input->post('StudentId');
            $postData['IsFromStudent'] = 2; // thanh vien gưi cho admin
            $postData['RoleId'] = $user['RoleId'];
        }
        else{
            $postData['StudentId'] = $user['UserId'];
            $postData['IsFromStudent'] = 1; // admin, he thong gui cho thanh vien
            $postData['RoleId'] = $user['RoleId'];
        }
        $rowCount = $this->Mnotifications->getCount($postData);
        $data['listNotifications'] = array();
        if ($rowCount > 0) {
            $perPage = DEFAULT_LIMIT;
            $pageCount = ceil($rowCount / $perPage);
            $page = $this->input->post('PageId');
            if (!is_numeric($page) || $page < 1) $page = 1;
            $data['listNotifications'] = $this->Mnotifications->search($postData, $perPage, $page);
            $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
        }
        $this->load->view('notification/list', $data);
    }

    public function insert(){
        $postData = $this->arrayFromPost(array('StudentId', 'AdminId', 'PhoneNumber', 'Email', 'Message', 'IsFromStudent'));
        if (!empty($postData['Message']) && $postData['IsFromStudent'] > 0) {
            $user = $this->session->userdata('user');
            if($postData['AdminId'] == 0) $postData['AdminId'] = ADMIN_ID;
            $postData['IsRead'] = 1;
            $postData['CrUserId'] = $user ? $user['UserId'] : 0;
            $postData['CrDateTime'] = getCurentDateTime();
            $this->load->model('Mnotifications');
            $flag = $this->Mnotifications->save($postData);
            if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Gửi khiếu nại thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }


    public function changeIsRead(){
        $user = $this->checkUserLogin(true);
        $notificationId = $this->input->post('NotificationId');
        if ($notificationId > 0) {
            $this->loadModel(array('Mnotifications','Mnotificationusers'));
            if($user['RoleId'] ==1 ) $flag = $this->Mnotifications->changeStatus(2, $notificationId, 'IsRead', $user['UserId']);
            else  $flag = $this->Mnotificationusers->save(array('NotificationId' => $notificationId, 'IsRead' => 2, 'UserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime())) > 0;
            if ($flag) {
                $txtSuccess = "Đổi trạng thái thành công";
                $statusName = '<span class="label label-default">Đã xem</span>';
                echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    function changeIsReadAll(){
        $user = $this->checkUserLogin(true);
        $this->load->model('Mnotifications');
        $flag = $this->Mnotifications->changeIsReadAll($user['UserId'], $user['RoleId']);
        if ($flag) {
            $txtSuccess = "Đổi trạng thái thành công";
            $statusName = '<span class="label label-default">Đã xem</span>';
            echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
        }
        else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function getCountUnRead(){
        $user = $this->session->userdata('user');
        if($user){
            $postData = array('IsArticle' => 1);
            if($user['RoleId'] == 1){
                $postData['IsFromStudent'] = 2;
                $postData['IsRead'] = 1;
                $postData['RoleId'] = $user['RoleId'];
            }
            else{
                $postData['StudentId'] = $user['UserId'];
                $postData['IsFromStudent'] = 1;
                $postData['CountIsRead'] = 1;
                $postData['RoleId'] = $user['RoleId'];
            }
            $this->load->model('Mnotifications');
            echo $this->Mnotifications->getCount($postData);
        }
        else echo '0';
    }
}