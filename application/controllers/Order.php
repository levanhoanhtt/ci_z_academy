<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			$user['RoleId'] == 1 ? 'Quản lý thương mại' : 'Quản lý bán hàng',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_list.js')),
				'pageIcon' => 'product.png'
			)
		);
		if($user['RoleId'] == 2 && $user['IsMember'] != 2) $this->load->view('user/permission', $data);
		else{
			$this->loadModel(array('Mproducts', 'Morders'));
			$postData = $this->arrayFromPost(array('ProductId', 'SearchText', 'SellerOrderStatusId', 'CustomerOrderStatusId', 'ZOrderStatusId', 'BeginDate', 'EndDate'));
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			if($user['RoleId'] == 2){
				$postData['SellerId'] = $user['UserId'];
				$data['listProducts'] = $this->Mproducts->getBy(array('ItemStatusId' => STATUS_ACTIVED, 'UserId' => $user['UserId']));
			}
			else{
				$postData['SellerId'] = $this->input->post('SellerId');
				$data['listUsers'] = $this->Musers->getListForSelect(2, 2);
				$data['listProducts'] = $this->Mproducts->getBy(array('ItemStatusId' => STATUS_ACTIVED));
			}
			$rowCount = $this->Morders->getCount($postData);
			$data['listOrders'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('order/list', $data);
		}
	}

	public function mine(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Đơn hàng của tôi',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_list.js')),
				'pageIcon' => 'product.png'
			)
		);
		if($user['RoleId'] == 2) {
			$this->loadModel(array('Mproducts', 'Morders'));
			$postData = $this->arrayFromPost(array('ProductId', 'SearchText', 'SellerOrderStatusId', 'CustomerOrderStatusId', 'ZOrderStatusId', 'BeginDate', 'EndDate'));
			if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$postData['CustomerId'] = $user['UserId'];
			$data['listProducts'] = $this->Mproducts->getBy(array('ItemStatusId' => STATUS_ACTIVED));
			$rowCount = $this->Morders->getCount($postData, 2);
			$data['listOrders'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listOrders'] = $this->Morders->search($postData, $perPage, $page, 2);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('order/mine', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add($productId = 0){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Mua hàng',
			array(
				'scriptFooter' => array('js' => 'js/order_add.js'),
				'pageIcon' => 'product.png'
			)
		);
		if($user['RoleId'] == 2){
			$postData = $this->arrayFromPost(array('ProductName', 'BrandId', 'SearchText', 'SortTypeId'));
			$this->loadModel(array('Mproducts', 'Mbrands'));
			if(!is_numeric($productId) || $productId < 0) $productId = 0;
            if($productId > 10000){
            	$productId -= 10000;
            	$postData['ProductId'] = $productId;
            } 
            else $productId = 0;
            $data['productId'] = $productId;
			$data['listBrands'] = $this->Mbrands->getBy(array('ItemStatusId' => STATUS_ACTIVED));
			$postData['ItemStatusId'] = STATUS_ACTIVED;
			$postData['DifferentUserId'] = $user['UserId'];
			$rowCount = $this->Mproducts->getCount($postData, true);
			$data['listProducts'] = array();
			if ($rowCount > 0) {
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listProducts'] = $this->Mproducts->search($postData, $perPage, $page, true);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('order/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ProductId', 'SellerId', 'Quantity', 'Cost', 'Address', 'Comment', 'PromotionCode', 'DiscountCost'));
		$postData['Quantity'] = replacePrice($postData['Quantity']);
		$postData['Cost'] = replacePrice($postData['Cost']);
		$postData['DiscountCost'] = replacePrice($postData['DiscountCost']);
		if($postData['ProductId'] > 0 && $postData['SellerId'] > 0 && $postData['Quantity'] > 0 && $postData['Cost'] > 0 && !empty($postData['Address'])){
			if($user['UserPass'] != md5(trim($this->input->post('UserPass')))){
				echo json_encode(array('code' => -1, 'message' => "Mật khẩu không trùng"));
				die();
			}
			if($user['IsEnabled2FA'] == 2){
				require_once APPPATH."/libraries/GoogleAuthenticator.php";
				$ga = new PHPGangsta_GoogleAuthenticator();
				$checkResult = $ga->verifyCode($user['SecretCode'], trim($this->input->post('UserCode')), TIME_SIZE_2FA);
				if(!$checkResult){
					echo json_encode(array('code' => -1, 'message' => "Mã 2FA không đúng"));
					die();
				}
			}
			$this->loadModel(array('Morders', 'Mproducts', 'Mpromotions'));
			$promotionCodeId = $this->input->post('PromotionCodeId');
			if(!empty($postData['PromotionCode'])){
				$promotion = $this->Mpromotions->checkPromotionCode($postData['PromotionCode'], $postData['ProductId']);
				$postData['DiscountCost'] = $promotion['DiscountCost'];
				$promotionCodeId = $promotion['PromotionCodeId'];
			}
			else{
				$postData['DiscountCost'] = 0;
				$promotionCodeId = 0;
			}
			$sumCost = $postData['Quantity'] * ($postData['Cost'] - $postData['DiscountCost']);
			if($user['Balance'] < $sumCost){
				echo json_encode(array('code' => -1, 'message' => "Tài khoản của bạn không đủ tiền để thanh toán"));
				die();
			}
			$postData['CustomerId'] = $user['UserId'];
			$postData['IsCustomerWin'] = 0;
			$postData['SellerOrderStatusId'] = 1;
			$postData['CustomerOrderStatusId'] = 1;
			$postData['ZOrderStatusId'] = 1;
			$postData['CrUserId'] = $user['UserId'];
			$crDateTime = getCurentDateTime();
			$postData['CrDateTime'] = $crDateTime;
			$metaData = array(
				'Seller' => $this->Musers->get($postData['SellerId'], true, '', 'FullName, PhoneNumber, Balance, TokenFCM'),
				'Customer' => $user,
				'PromotionCodeId' => $promotionCodeId,
				'SumCost' => $postData['Quantity'] * ($postData['Cost'] - $postData['DiscountCost'])
			);
			$productName = $this->Mproducts->getFieldValue(array('ProductId' => $postData['ProductId']), 'ProductName');
			$notifications = array(
				array(
					'StudentId' => $postData['SellerId'],
					'AdminId' => ADMIN_ID,
					'IsRead' => 1,
					'IsFromStudent' => 1,
					'Message' => $user['FullName'].' ('.$user['PhoneNumber'].') vừa đặt mua sản phẩm '.$productName.' của bạn',
					'CrUserId' => $user['UserId'],
					'CrDateTime' => $crDateTime
				)
			);
            if($promotionCodeId > 0){
				$notifications[] = array(
                    'StudentId' => $postData['SellerId'],
                    'AdminId' => ADMIN_ID,
                    'IsRead' => 1,
                    'IsFromStudent' => 1,
                    'Message' => 'Đơn hàng bạn bán sản phẩm '.$productName.' cho khách hàng '.$user['FullName'].' ('.$user['PhoneNumber'].') mã {ORDER_CODE} đã sử dụng mã giảm giá '.$postData['PromotionCode'],
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
			$metaData['Notifications'] = $notifications;
			$orderId = $this->Morders->add($postData, $metaData);
			if($orderId > 0){
				if(!empty($metaData['Seller']['TokenFCM'])){
					foreach($notifications as $n) sendFCM($n['Message'], $metaData['Seller']['TokenFCM']);
				}
                echo json_encode(array('code' => 1, 'message' => "Thanh toán thành công", "data" => $orderId));
            }
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$orderId = $this->input->post('OrderId');
		$statusId = $this->input->post('StatusId');
		$statusTypeId = $this->input->post('StatusTypeId');
		if($orderId > 0 && $statusId > 0 && $statusTypeId > 0){
			$this->load->model(array('Morders', 'Mproducts', 'Mbrands'));
			$order = $this->Morders->get($orderId);
			if($order){
				$crDateTime = getCurentDateTime();
				$postData = array(
					'UpdateUserId' => $user['UserId'],
					'UpdateDateTime' => $crDateTime
				);
				$statusName = '';
				$zStatusName = '';
				$updateBalanceTypeId = 0;
				$orderCost = $order['Quantity'] * ($order['Cost'] - $order['DiscountCost']);
				$sellerOrderStatusId = $order['SellerOrderStatusId'];
				$customerOrderStatusId = $order['CustomerOrderStatusId'];
				$transactionLogs = array();
				$notifications = array();
				$tokenFCMs = array();
				if($statusTypeId == 2){ // ben ban
					$flag = false;
					if($statusId == 2)$flag = $sellerOrderStatusId == 1 || $sellerOrderStatusId == 3;
                    elseif($statusId == 3)$flag = $sellerOrderStatusId == 1;
					elseif($statusId == 4) $flag = $customerOrderStatusId < 3 && ($sellerOrderStatusId == 1 || $sellerOrderStatusId == 3);
					elseif($statusId == 5) $flag = $customerOrderStatusId == 5 && ($sellerOrderStatusId == 1 || $sellerOrderStatusId == 3);
					elseif($statusId == 6) $flag = $customerOrderStatusId == 4 && ($sellerOrderStatusId == 1 || $sellerOrderStatusId == 3);
					if($flag){
                        $customer = $this->Musers->get($order['CustomerId'], true, '', 'FullName, PhoneNumber, Balance, TokenFCM');
						$seller = $this->Musers->get($order['SellerId'], true, '', 'FullName, PhoneNumber, TokenFCM');
						$tokenFCMs[$order['CustomerId']] = $customer['TokenFCM'];						
						$tokenFCMs[$order['SellerId']] = $seller['TokenFCM'];
                        $product = $this->Mproducts->get($order['ProductId'], true, '', 'ProductName, BrandId');
                        $brandName = $this->Mbrands->getFieldValue(array('BrandId' => $product['BrandId']), 'BrandName');
                        $notifications[] = array(
                            'StudentId' => $order['CustomerId'],
                            'AdminId' => ADMIN_ID,
                            'IsRead' => 1,
                            'IsFromStudent' => 1,
                            'Message' => 'Đơn hàng bạn mua sản phẩm '.$product['ProductName'].' thuộc thương hiệu '.$brandName.' của '.$seller['FullName'].' ('.$seller['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa chuyển trạng thái '.$this->Mconstants->sellerOrderStatus[$statusId],
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
						$postData['SellerOrderStatusId'] = $statusId;
						$statusName = '<span class="'.$this->Morders->labelCss['SellerOrderStatus'][$statusId].'">'.$this->Mconstants->sellerOrderStatus[$statusId].'</span>';
						if($statusId == 4){ // cong lai tien cho ng mua tu tk chinh
							$updateBalanceTypeId = 1;
							$postData['ZOrderStatusId'] = 3;
							$postData['IsCustomerWin'] = 2;
							$zStatusName = '<span class="'.$this->Morders->labelCss['ZOrderStatus'][3].'">'.$this->Mconstants->zOrderStatus[3].'</span>';
							$seller = $this->Musers->get($order['SellerId'], true, '', 'FullName, PhoneNumber, Balance');
							$transactionLogs[] = array(
								'UserId' => $order['CustomerId'],
								'ByUserId' => $user['UserId'],
								'LogTypeId' => 11,
								'Amount' => $orderCost,
								'Balance' => $customer['Balance'] + $orderCost,
								'TicketCount' => 0,
								'Comment' => "{$customer['FullName']} ({$customer['PhoneNumber']}) được hoàn " . priceFormat($orderCost) . " VNĐ từ đơn hàng ".$order['OrderCode'],
								'CrDateTime' => $crDateTime
							);
							$transactionLogs[] = array(
								'UserId' => $order['SellerId'],
								'ByUserId' => $user['UserId'],
								'LogTypeId' => 11,
								'Amount' => $orderCost,
								'Balance' => $seller['Balance'] - $orderCost,
								'TicketCount' => 0,
								'Comment' => "{$seller['FullName']} ({$seller['PhoneNumber']}) hoàn " . priceFormat($orderCost) . " VNĐ cho {$customer['FullName']} ({$customer['PhoneNumber']})",
								'CrDateTime' => $crDateTime
							);
						}
						elseif($statusId == 5 || $statusId == 6){ // cong lai tien cho ng mua tu tk dong bang
							$updateBalanceTypeId = 2;
							$postData['ZOrderStatusId'] = 3;
							$postData['IsCustomerWin'] = 2;
							$zStatusName = '<span class="'.$this->Morders->labelCss['ZOrderStatus'][3].'">'.$this->Mconstants->zOrderStatus[3].'</span>';
							$transactionLogs[] = array(
								'UserId' => $order['CustomerId'],
								'ByUserId' => $user['UserId'],
								'LogTypeId' => 11,
								'Amount' => $orderCost,
								'Balance' => $customer['Balance'] + $orderCost,
								'TicketCount' => 0,
								'Comment' => "{$customer['FullName']} ({$customer['PhoneNumber']}) được hoàn " . priceFormat($orderCost) . " VNĐ từ đơn hàng ".$order['OrderCode'],
								'CrDateTime' => $crDateTime
							);
						}
					}
					else{
						echo json_encode(array('code' => -1, 'message' => "Thay đổi trạng thái không hợp lệ"));
						die();
					}
				}
				elseif($statusTypeId == 3){ // ben mua
					$flag = false;
					if($statusId == 2) $flag = $sellerOrderStatusId < 4 && $customerOrderStatusId == 1;
					elseif($statusId == 3) $flag = $sellerOrderStatusId < 4 && $customerOrderStatusId < 3;
					elseif($statusId == 4) $flag = $sellerOrderStatusId < 4 && $customerOrderStatusId < 3;
					elseif($statusId == 5) $flag = $sellerOrderStatusId < 4 && $customerOrderStatusId < 3;
					if($flag){
						$product = $this->Mproducts->get($order['ProductId'], true, '', 'ProductName, BrandId');
						$customer = $this->Musers->get($order['CustomerId'], true, '', 'FullName, PhoneNumber, TokenFCM');
						$tokenFCMs[$order['CustomerId']] = $customer['TokenFCM'];						
                        $notifications[] = array(
                            'StudentId' => $order['SellerId'],
                            'AdminId' => ADMIN_ID,
                            'IsRead' => 1,
                            'IsFromStudent' => 1,
                            'Message' => 'Đơn hàng bạn bán sản phẩm '.$product['ProductName'].' cho khách hàng '.$customer['FullName'].' ('.$customer['PhoneNumber'].') mã '.$order['OrderCode'].' vừa chuyển trạng thái '.$this->Mconstants->customerOrderStatus[$statusId],
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => $crDateTime
                        );
						$postData['CustomerOrderStatusId'] = $statusId;
						$statusName = '<span class="'.$this->Morders->labelCss['CustomerOrderStatus'][$statusId].'">'.$this->Mconstants->customerOrderStatus[$statusId].'</span>';
						if($statusId == 4 || $statusId == 5){ // mau thuan dc bat - dong bang so tien
							$updateBalanceTypeId = 3;
							$postData['ZOrderStatusId'] = 2;
							$zStatusName = '<span class="'.$this->Morders->labelCss['ZOrderStatus'][2].'">'.$this->Mconstants->zOrderStatus[2].'</span>';
							$seller = $this->Musers->get($order['SellerId'], true, '', 'FullName, PhoneNumber, Balance, TokenFCM');
							$tokenFCMs[$order['SellerId']] = $seller['TokenFCM'];
							$transactionLogs[] = array(
								'UserId' => $order['SellerId'],
								'ByUserId' => $user['UserId'],
								'LogTypeId' => 11,
								'Amount' => $orderCost,
								'Balance' => $seller['Balance'] - $orderCost,
								'TicketCount' => 0,
								'Comment' => "Đóng băng " . priceFormat($orderCost) . " VNĐ của {$seller['FullName']} ({$seller['PhoneNumber']}) từ đơn hàng ".$order['OrderCode'],
								'CrDateTime' => $crDateTime
							);
							$notifications[] = array(
								'StudentId' => $order['SellerId'],
								'AdminId' => ADMIN_ID,
								'IsRead' => 1,
								'IsFromStudent' => 2,
								'Message' => 'Đơn hàng '.$customer['FullName'].' ('.$customer['PhoneNumber'].') mua sản phẩm '.$product['ProductName'].' của '.$seller['FullName'].' ('.$seller['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa chuyển trạng thái Mâu thuẫn',
								'CrUserId' => $user['UserId'],
								'CrDateTime' => $crDateTime
							);
							$notifications[] = array(
								'StudentId' => $order['SellerId'],
								'AdminId' => ADMIN_ID,
								'IsRead' => 1,
								'IsFromStudent' => 1,
								'Message' => 'Đơn hàng bạn bán sản phẩm '.$product['ProductName'].' cho khách hàng '.$customer['FullName'].' ('.$customer['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa chuyển trạng thái Mâu thuẫn',
								'CrUserId' => $user['UserId'],
								'CrDateTime' => $crDateTime
							);
							$brandName = $this->Mbrands->getFieldValue(array('BrandId' => $product['BrandId']), 'BrandName');
							$notifications[] = array(
								'StudentId' => $order['CustomerId'],
								'AdminId' => ADMIN_ID,
								'IsRead' => 1,
								'IsFromStudent' => 1,
								'Message' => 'Đơn hàng bạn mua sản phẩm '.$product['ProductName'].' thuộc thương hiệu '.$brandName.' của '.$seller['FullName'].' ('.$seller['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa chuyển trạng thái Mâu thuẫn',
								'CrUserId' => $user['UserId'],
								'CrDateTime' => $crDateTime
							);
						}
					}
					else{
						echo json_encode(array('code' => -1, 'message' => "Thay đổi trạng thái không hợp lệ"));
						die();
					}

				}
				if(!empty($statusName)) {
					$metaData = array(
						'CustomerId' => $order['CustomerId'],
						'SellerId' => $order['SellerId'],
						'OrderCost' => $orderCost,
						'UpdateBalanceTypeId' => $updateBalanceTypeId,
                        'Notifications' => $notifications
					);
					$flag = $this->Morders->changeOrderStatus($postData, $orderId, $transactionLogs, $metaData);
					if($flag > 0){
						
						if(!isset($tokenFCMs[$order['SellerId']])) $tokenFCMs[$order['SellerId']] = $this->Musers->getFieldValue(array('UserId' => $order['SellerId']), 'TokenFCM');
						foreach($notifications as $n){
							if ($n['IsFromStudent'] == 1){
								if(!isset($tokenFCMs[$n['StudentId']])) $tokenFCMs[$n['StudentId']] = $this->Musers->getFieldValue(array('UserId' => $n['StudentId']), 'TokenFCM');
								sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
							}
						}
                        echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái thành công", 'data' => array('StatusName' => $statusName, 'ZStatusName' => $zStatusName)));
                    }
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getListConflict(){
		$this->checkUserLogin(true);
		$orderId = $this->input->post('OrderId');
		if($orderId > 0){
			$this->load->model('Mconflicts');
			$listConflicts = $this->Mconflicts->getBy(array('OrderId' => $orderId), false, 'CrDateTime');
			for($i = 0; $i < count($listConflicts); $i++){
				if(!empty($listConflicts[$i]['ConflictImage'])) $listConflicts[$i]['ConflictImage'] = IMAGE_PATH.$listConflicts[$i]['ConflictImage'];
				$listConflicts[$i]['CrDateTime'] = ddMMyyyy($listConflicts[$i]['CrDateTime'], 'd/m/Y H:i');
			}
			echo json_encode(array('code' => 1, 'data' => $listConflicts));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertConflict(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('OrderId', 'IsCustomerSend', 'ConflictImage', 'Comment'));
		if($postData['OrderId'] > 0 && $postData['IsCustomerSend'] > 0){
			$crDateTime = getCurentDateTime();
			$this->loadModel(array('Morders', 'Mconflicts','Mproducts', 'Mnotifications'));
			$orderData = array();
			$transactionLogs = array();
			$metaData = array();
			$sellerOrderStatusName = '';
			$customerOrderStatusName = '';
			$zOrderStatusName = '';
			$otherPost = $this->arrayFromPost(array('StatusId', 'StatusTypeId'));
            $order = $this->Morders->get($postData['OrderId'], true, '', 'OrderCode, SellerId, CustomerId, Quantity, Cost, DiscountCost, ProductId, SellerOrderStatusId, CustomerOrderStatusId');
            $seller = $this->Musers->get($order['SellerId'], true, '', 'FullName, PhoneNumber, Balance');
            $customer = $this->Musers->get($order['CustomerId'], true, '', 'FullName, PhoneNumber');
			if($otherPost['StatusId'] == 2 && $otherPost['StatusTypeId'] == 2){
				if($order['SellerOrderStatusId'] == 1 || $order['SellerOrderStatusId'] == 3){
					$orderData = array(
						'SellerOrderStatusId' => $otherPost['StatusId'],
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => $crDateTime
					);
					$sellerOrderStatusName = '<span class="'.$this->Morders->labelCss['SellerOrderStatus'][$otherPost['StatusId']].'">'.$this->Mconstants->sellerOrderStatus[$otherPost['StatusId']].'</span>';
				}
				else{
					echo json_encode(array('code' => -1, 'message' => "Thay đổi trạng thái không hợp lệ"));
					die();
				}
			}
			elseif($otherPost['StatusTypeId'] == 3 && ($otherPost['StatusId'] == 4 || $otherPost['StatusId'] == 5)){
				if($order['CustomerOrderStatusId'] < 4) {
					$orderData = array(
						'CustomerOrderStatusId' => $otherPost['StatusId'],
						'ZOrderStatusId' => 2,
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => $crDateTime
					);
					$customerOrderStatusName = '<span class="' . $this->Morders->labelCss['CustomerOrderStatus'][$otherPost['StatusId']] . '">' . $this->Mconstants->customerOrderStatus[$otherPost['StatusId']] . '</span>';
					$zOrderStatusName = '<span class="' . $this->Morders->labelCss['ZOrderStatus'][2] . '">' . $this->Mconstants->zOrderStatus[2] . '</span>';
					$orderCost = $order['Quantity'] * ($order['Cost'] - $order['DiscountCost']);
					$transactionLogs = array(
						'UserId' => $order['SellerId'],
						'ByUserId' => $user['UserId'],
						'LogTypeId' => 11,
						'Amount' => $orderCost,
						'Balance' => $seller['Balance'] - $orderCost,
						'TicketCount' => 0,
						'Comment' => "Đóng băng " . priceFormat($orderCost) . " VNĐ của {$seller['FullName']} ({$seller['PhoneNumber']}) từ đơn hàng " . $order['OrderCode'],
						'CrDateTime' => $crDateTime
					);
					$metaData = array(
						'OrderCost' => $orderCost,
						'SellerId' => $order['SellerId'],
						'CustomerId' => $order['CustomerId']
					);
				}
				else{
					echo json_encode(array('code' => -1, 'message' => "Thay đổi trạng thái không hợp lệ"));
					die();
				}
			}
			if(!empty($postData['ConflictImage'])) {
				$conflictImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $postData['ConflictImage']));
				$imgName = strtotime($crDateTime);
				$imgPath = "assets/uploads/images/$imgName.png";
				$postData['ConflictImage'] = $imgName . '.png';
				file_put_contents($imgPath, $conflictImage);
			}
			$postData['CrUserId'] = $user['UserId'];
			$postData['CrDateTime'] = $crDateTime;
            $metaData['Notifications'] = array(
                'StudentId' => $user['UserId'],
                'AdminId' => ADMIN_ID,
                'IsRead' => 1,
                'IsFromStudent' => 2,
                'Message' => 'Đơn hàng mâu thuẫn của '.$customer['FullName'].' ('.$customer['PhoneNumber'].') mua sản phẩm '.$this->Mproducts->getFieldValue(array('ProductId' => $order['ProductId']), 'ProductName').' của '.$seller['FullName'].' ('.$seller['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa được gửi thêm bằng chứng',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
			$flag = $this->Mconflicts->insert($postData, $orderData, $transactionLogs, $metaData);
			if($flag){
				if(!empty($postData['ConflictImage'])) $postData['ConflictImage'] = IMAGE_PATH.$postData['ConflictImage'];
				$postData['CrDateTime'] = ddMMyyyy($crDateTime, 'd/m/Y H:i');
				$postData['SellerOrderStatusName'] = $sellerOrderStatusName;
				$postData['CustomerOrderStatusName'] = $customerOrderStatusName;
				$postData['ZOrderStatusName'] = $zOrderStatusName;
				echo json_encode(array('code' => 1, 'message' => "Gửi bằng chứng thành công", "data" => $postData));
			} 
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function solveConflict(){
		$user = $this->checkUserLogin(true);
		$orderId = $this->input->post('OrderId');
		$solveTypeId = $this->input->post('SolveTypeId');
		if($user['RoleId'] == 1 && $orderId > 0 && ($solveTypeId == 1 || $solveTypeId == 2)){
			$this->load->model(array('Morders','Mproducts','Mbrands'));
			$order = $this->Morders->get($orderId, true, '', 'OrderCode, SellerId, CustomerId, CustomerOrderStatusId, ZOrderStatusId, Quantity, Cost, DiscountCost, ProductId');
			if($order){
				if($order['ZOrderStatusId'] == 2 && ($order['CustomerOrderStatusId'] == 4 || $order['CustomerOrderStatusId'] == 5)){
					$crDateTime = getCurentDateTime();
					$postData = array(
						'ZOrderStatusId' => 3,
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => $crDateTime
					);
					$orderCost = $order['Quantity'] * ($order['Cost'] - $order['DiscountCost']);
					$seller = $this->Musers->get($order['SellerId'], true, '', 'FullName, PhoneNumber, Balance, TokenFCM');
					$customer = $this->Musers->get($order['CustomerId'], true, '', 'FullName, PhoneNumber, Balance, TokenFCM');
					$tokenFCMs = array($order['SellerId'] => $seller['TokenFCM'], $order['CustomerId'] => $customer['TokenFCM']);
					$updateBalanceTypeId = 4;
					if($solveTypeId == 1){
						$postData['IsCustomerWin'] = 1;
						$transactionLogs[] = array(
							'UserId' => $order['SellerId'],
							'ByUserId' => $user['UserId'],
							'LogTypeId' => 11,
							'Amount' => $orderCost,
							'Balance' => $seller['Balance'] + $orderCost,
							'TicketCount' => 0,
							'Comment' => "{$seller['FullName']} ({$seller['PhoneNumber']}) được cộng " . priceFormat($orderCost) . " VNĐ từ tài khoản đóng băng do đơn hàng {$order['OrderCode']} được giải quyết",
							'CrDateTime' => $crDateTime
						);
						$message1 = '"Trả tiền cho bên bán"';
						$message2 = '"Trả tiền cho bên bán" hãy kiểm tra tài khoản đóng băng.';
					}
					else {
						$postData['IsCustomerWin'] = 2;
						$updateBalanceTypeId = 2;
						$transactionLogs[] = array(
							'UserId' => $order['CustomerId'],
							'ByUserId' => $user['UserId'],
							'LogTypeId' => 11,
							'Amount' => $orderCost,
							'Balance' => $customer['Balance'] + $orderCost,
							'TicketCount' => 0,
							'Comment' => "{$customer['FullName']} ({$customer['PhoneNumber']}) được cộng " . priceFormat($orderCost) . " VNĐ do đơn hàng {$order['OrderCode']} được giải quyết",
							'CrDateTime' => $crDateTime
						);
						$message1 =  '"Hoàn tiền cho bên mua" hãy kiểm tra tài khoản';
						$message2 = '"Hoàn tiền cho bên mua"';
					}
					$product = $this->Mproducts->get($order['ProductId'], true, '', 'ProductName, BrandId');
					$brandName = $this->Mbrands->getFieldValue(array('BrandId' => $product['BrandId']), 'BrandName');
					$notifications = array(
						array(
							'StudentId' => $order['CustomerId'],
							'AdminId' => $user['UserId'],
							'IsRead' => 1,
							'IsFromStudent' => 1,
							'Message' => 'Đơn hàng mâu thuẫn bạn mua sản phẩm '.$product['ProductName'].' thuộc thương hiệu '.$brandName.' của '.$seller['FullName'].' ('.$seller['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa được phán quyết '.$message1,
							'CrUserId' => $user['UserId'],
							'CrDateTime' => getCurentDateTime()
						),
						array(
							'StudentId' => $order['SellerId'],
							'AdminId' => $user['UserId'],
							'IsRead' => 1,
							'IsFromStudent' => 1,
							'Message' => 'Đơn hàng mâu thuẫn bạn bán sản phẩm '.$product['ProductName'].' cho khách hàng '.$customer['FullName'].' ('.$customer['PhoneNumber'].') mã  '.$order['OrderCode'].' vừa được phán quyết '.$message2,
							'CrUserId' => $user['UserId'],
							'CrDateTime' => getCurentDateTime()
						)
					);
					$metaData = array(
						'CustomerId' => $order['CustomerId'],
						'SellerId' => $order['SellerId'],
						'OrderCost' => $orderCost,
						'UpdateBalanceTypeId' => $updateBalanceTypeId,
						'Notifications' => $notifications
					);
					$flag = $this->Morders->changeOrderStatus($postData, $orderId, $transactionLogs, $metaData);
					if($flag > 0){
                        foreach ($notifications as $n){
							if(!isset($tokenFCMs[$n['StudentId']])) $tokenFCMs[$n['StudentId']] = $this->Musers->getFieldValue(array('UserId' => $n['StudentId']), 'TokenFCM');
							sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
						}
						$statusName = '<span class="'.$this->Morders->labelCss['ZOrderStatus'][3].'">'.$this->Mconstants->zOrderStatus[3].'</span>';
						echo json_encode(array('code' => 1, 'message' => "Đóng thương vụ thành công", 'data' => array('StatusName' => $statusName)));
					}
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}