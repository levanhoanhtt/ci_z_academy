<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sell extends MY_Controller {


	public $labelCss = array(
        'SellerOrderStatus' => array(
            1 => '',
            2 => 'label label-success',
            3 => 'label label-warning',
            4 => 'label label-default',
            5 => 'label label-danger',
            6 => 'label label-info'
        ),
        'CustomerOrderStatus' => array(
            1 => '',
            2 => 'label label-default',
            3 => 'label label-success',
            4 => 'label label-primary',
            5 => 'label label-danger'
        ),
        'ZOrderStatus' => array(
        	1 => '',
        	2 => 'label label-warning',
        	3 => 'label label-danger',
        ),
    );

	public function index(){

		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Quản lý bán hàng',
			array(
					'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css')),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/sell.js')),
					'pageIcon' => 'product.png'
					)
		);
		$data['labelCss'] = $this->labelCss;
		if($user['RoleId'] == 1){
			$postData = $this->arrayFromPost(array('ProductId', 'SearchText', 'BeginDate','EndDate','SellerOrderStatusId','CustomerOrderStatusId','ZOrderStatusId'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$this->load->model(array('Mproducts','Musers', 'Morders','Mbrands','Mconflicts')); 

				$data['listProducts'] = $this->Mproducts->getBy(array('ItemStatusId' => STATUS_ACTIVED));
				$data['listUsers'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));

				$rowCount = $this->Morders->getCount($postData);
				$data['listOrders'] = array();
				if ($rowCount > 0) {
					$perPage = DEFAULT_LIMIT;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if (!is_numeric($page) || $page < 1) $page = 1;
					$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('sell/list', $data);
		}
		else
		{

			if($user['IsMember'] == 2){
				$postData = $this->arrayFromPost(array('ProductId', 'SearchText', 'BeginDate','EndDate','SellerOrderStatusId','CustomerOrderStatusId','ZOrderStatusId'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$postData['SellerId'] = $user['UserId'];
				$this->load->model(array('Mproducts','Musers', 'Morders','Mbrands')); 

				$data['listProducts'] = $this->Mproducts->getBy(array('ItemStatusId' => STATUS_ACTIVED));
				$data['listUsers'] = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED));

				$rowCount = $this->Morders->getCount($postData);
				$data['listOrders'] = array();
				if ($rowCount > 0) {
					$perPage = DEFAULT_LIMIT;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if (!is_numeric($page) || $page < 1) $page = 1;
					$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('sell/list', $data);
				
			}
			else
				$this->load->view('user/permission', $data);
			
		} 
	}

	public function changeStatus(){
		$user = $this->checkUserLogin();
		$orderId = $this->input->post('OrderId');
		if($user['RoleId'] == 1) $zOrderStatusId = $this->input->post('ZOrderStatusId');
		else{
			$sellerOrderStatusId = $this->input->post('SellerOrderStatusId');
			$customerOrderStatusId = $this->input->post('CustomerOrderStatusId');
		} 
		
		if($orderId > 0){
			$postData = array();
			$this->loadModel(array('Morders'));
			if($user['RoleId'] == 1){
				$postData = array(
					'UpdateDateTime' => getCurentDateTime(),
					'UpdateUserId' => $user['UserId'],
					'ZOrderStatusId' => $zOrderStatusId,
				);
			} 
			else{
				$postData = array(
					'UpdateDateTime' => getCurentDateTime(),
					'UpdateUserId' => $user['UserId'],
					'SellerOrderStatusId' => $sellerOrderStatusId,
					'CustomerOrderStatusId' => $customerOrderStatusId
				);
			}
			
			$flag = $this->Morders->updateStatus($postData, $orderId,$user);
			if($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật trạng thái thành công", "data" => $flag));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

	}

	public function requestProof(){
		$user 			= $this->checkUserLogin();
		$conflictId 	= $this->input->post('ConflictId');
		$orderId 		= $this->input->post('OrderId');
		$sellerId 		= $this->input->post('SellerId');
		$isCustomerSend = $this->input->post('IsCustomerSend');
		$conflictImage 	= $this->input->post('ConflictImage');
		$comment 		= $this->input->post('Comment');
		if($orderId > 0 && $sellerId > 0 && !empty($conflictImage)){
			$this->load->model('Mconflicts');
			$conflictImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $conflictImage));
			$crDateTime = getCurentDateTime();
			$imgName = strtotime($crDateTime);
			$imgPath  = "assets/uploads/images/$imgName.png";
			$postData = array(
				'OrderId' => $orderId,
				'SellerId' => $sellerId,
				'IsCustomerSend' => $isCustomerSend,
				'ConflictImage' => $imgName.'.png',
				'Comment' => $comment,
				'CrUserId' => $user['UserId'],
				'CrDateTime' => $crDateTime,
			);
			$flag = $this->Mconflicts->save($postData, $conflictId);
			if($flag >0){
				file_put_contents($imgPath,$conflictImage);
				echo json_encode(array('code' => 1, 'message' => "Gửi bằng chứng thành công", "data" => $flag));
			} 
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}