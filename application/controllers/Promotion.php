<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Mã ưu đãi',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/promotion.js')),
				'pageIcon' => 'product.png'
			)
		);
		if($user['RoleId'] == 2 && $user['IsMember'] != 2) $this->load->view('user/permission', $data);
		else {
			$postData = $this->arrayFromPost(array('UserId'));
			$postData['StatusId'] = STATUS_ACTIVED;
			$this->loadModel(array('Mproducts', 'Mbrands', 'Mpromotions'));
			if($user['RoleId'] == 2){
				$postData['UserId'] = $user['UserId'];
				$data['listProducts'] = $this->Mproducts->getBy(array('UserId' => $user['UserId'], 'ItemStatusId' => STATUS_ACTIVED));
				$data['listBrands'] = $this->Mbrands->getBy(array('UserId' => $user['UserId'], 'ItemStatusId' => STATUS_ACTIVED));
			}
			else $data['listUsers'] = $this->Musers->getListForSelect(2, 2);
			$rowCount = $this->Mpromotions->getCount($postData);
			$data['listPromotions'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listPromotions'] = $this->Mpromotions->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('product/promotion', $data);
		}
	}

	public function checkPromotionCode(){
		$this->checkUserLogin(true);
		$promotionCode = trim($this->input->post('PromotionCode'));
		$productId = $this->input->post('ProductId');
		if(!empty($promotionCode) && $productId > 0){
			$this->load->model('Mpromotions');
			$data = $this->Mpromotions->checkPromotionCode($promotionCode, $productId);
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('UserId', 'ProductId', 'BrandId', 'Quantity', 'DiscountCost', 'BeginDate','EndDate'));
		$postData['Quantity'] = replacePrice($postData['Quantity']);
		$postData['DiscountCost'] = replacePrice($postData['DiscountCost']);
		if($postData['ProductId'] > 0 && $postData['BrandId'] > 0 && $postData['Quantity'] > 0 && $postData['DiscountCost'] > 0 && !empty($postData['BeginDate'])){
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])){
				$postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				if(strtotime($postData['BeginDate']) > strtotime($postData['EndDate'])){
					echo json_encode(array('code' => -1, 'message' => "Ngày hết hiệu lực không hợp lệ"));
					die();
				}
			}
			$promotionId = $this->input->post('PromotionId');
			if($user['RoleId'] == 1){
				$this->load->model('Mpromotions');
				if($promotionId > 0){
					$flag = $this->Mpromotions->checkExist($postData['ProductId'], $postData['BeginDate'], $postData['EndDate'], $promotionId);
					if(!$flag){
						$postData['StatusId'] = STATUS_ACTIVED;
						$postData['UpdateUserId'] = $user['UserId'];
						$postData['UpdateDateTime'] = getCurentDateTime();
						$this->updatePromotion($postData, $promotionId);
					}
					else echo json_encode(array('code' => -1, 'message' => "Ưu đãi này đã tồn tại"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Quản lý không có quyền thêm ưu đãi"));
			}
			elseif($user['RoleId'] == 2 && $user['IsMember'] == 2){
				$this->loadModel(array('Mpromotions', 'Mnotifications', 'Mproducts', 'Mbrands'));
				$flag = $this->Mpromotions->checkExist($postData['ProductId'], $postData['BeginDate'], $postData['EndDate'], $promotionId);
				if(!$flag){
                    $notifications = array();
                    $crDateTime = getCurentDateTime();
					$postData['StatusId'] = STATUS_ACTIVED;
					if($promotionId > 0) {
						$postData['UpdateUserId'] = $user['UserId'];
						$postData['UpdateDateTime'] = $crDateTime;
					}
					else{
						$postData['UserId'] = $user['UserId'];
						$postData['CrUserId'] = $user['UserId'];
						$postData['CrDateTime'] = $crDateTime;
						$productName = $this->Mproducts->getFieldValue(array('ProductId' => $postData['ProductId']), 'ProductName');
						$brandName = $this->Mbrands->getFieldValue(array('BrandId' => $postData['BrandId']), 'BrandName');
                        $notifications = array(
                            'StudentId' => 0,
                            'AdminId' => ADMIN_ID,
                            'IsRead' => 1,
                            'IsFromStudent' => 1,
                            'Message' => "Sản phẩm {$productName} thuộc thương hiệu {$brandName} của {$user['FullName']} ({$user['PhoneNumber']}) đang có chương trình khuyến mại giảm giá ".priceFormat($postData['DiscountCost'])." đ. Hãy liên hệ ngay để có được mã giảm giá",
                            'CrUserId' => $user ? $user['UserId'] : 0,
                            'CrDateTime' => $crDateTime
                        );
					}
					$this->updatePromotion($postData, $promotionId, $notifications);
				}
				else echo json_encode(array('code' => -1, 'message' => "Ưu đãi này đã tồn tại"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));

	}

	public function delete(){
		$user = $this->checkUserLogin(true);
		$promotionId = $this->input->post('PromotionId');
		if($promotionId > 0){
			$this->load->model('Mpromotions');
			$flag = $this->Mpromotions->changeStatus(0, $promotionId, 'StatusId', $user['UserId']);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa mã ưu đãi thành công", 'data' => $flag));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	private function updatePromotion($postData, $promotionId, $notifications = array()){
		$flag = $this->Mpromotions->update($postData, $promotionId, $notifications);
		if($flag > 0) {
			if(!empty($notifications)) sendFCM($notifications['Message'], DEFAULT_TOPIC_NOTIFICATION, true);
			$postData['PromotionId'] = $flag;
			$postData['BeginDate'] = ddMMyyyy($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyy($postData['EndDate']);
			$postData['IsAdd'] = ($promotionId > 0) ? 0 : 1;
			echo json_encode(array('code' => 1, 'message' => "Cập nhật ưu đãi thành công", 'data' => $postData));
		}
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function download($promotionId = 0){
        $this->checkUserLogin();
        if($promotionId > 0){
            $this->load->model('Mpromotioncodes');
            $listPromotionCodes = $this->Mpromotioncodes->getBy(array('PromotionId' => $promotionId));
            if(!empty($listPromotionCodes)) {
				$this->load->library('excel');
				$this->excel->setActiveSheetIndex(0);
				$sheet = $this->excel->getActiveSheet();
				$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
				$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
				$sheet->getPageSetup()->setFitToPage(true);
				$sheet->getPageSetup()->setFitToWidth(1);
				$sheet->getPageSetup()->setFitToHeight(0);
				$sheet->setTitle("MaUuDai");
				$sheet->setCellValue('A1', "Mã ưu đãi");
				$sheet->getStyle('A1')->getFont()->setSize(14);
				$sheet->getStyle('A1')->getFont()->setBold(true);
				$sheet->mergeCells('A1:C1');
				$sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$border = array(
					'borders' => array(
						'allborders' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					)
				);
				$sheet->setCellValue('A2', "STT");
				$sheet->setCellValue('B2', "Mã ưu đãi");
				$sheet->setCellValue('C2', "Trạng thái");
				$colLeters = array('A', 'B', 'C');
				foreach($colLeters as $l) $sheet->getStyle($l.'2')->getFont()->setBold(true);
				$i = 2;
				foreach($listPromotionCodes as $pc){
					$i++;
					$sheet->setCellValue('A'.$i, $i - 2);
					$sheet->setCellValue('B'.$i, $pc['PromotionCode']);
					$sheet->setCellValue('C'.$i, $pc['OrderId'] > 0 ? 'Đã sử dụng' : 'Chưa sử dụng');
				}
				$sheet->getStyle('A2:C'.$i)->applyFromArray($border);
				$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(true);
				foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
				$filename = "MaUuDai.xls";
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
				$objWriter->save('php://output');
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
	}
}