<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charge extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        if($user['RoleId'] == 2) {
            $data = $this->commonData($user,
                'Tài khoản',
                array(
                    'scriptHeader' => array('css' => 'vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css'),
                    'scriptFooter' => array('js' => array('vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js','vendor/plugins/bootstrap-tagsinput/bootstrap3-typeahead.js', 'js/charge.js')),
                    'pageIcon' => 'cart-icon.png'
                )
            );
            if($this->session->flashdata('messageCharge')) $data['messageCharge'] = $this->session->flashdata('messageCharge');
            $configs = $data['configs'];
            $ticketCost = $configs['TICKET_COST'];
            $ticketPiece = $configs['PIECES_PER_TICKET'];
            $decimalCount = $this->getDecimalCount($ticketPiece);
            $user['TicketCount'] = round($user['TicketCount'], $decimalCount);
            $data['user'] = $user;
            $data['ticketCost'] = $ticketCost;
            $data['ticketPiece'] = $ticketPiece;
            $data['buyAble'] = $user['Balance']/$ticketCost > 1/$ticketPiece;
            $this->load->view('charge/list', $data);
        }
        else redirect('user/student');
    }

    public function insert(){
        $user = $this->checkUserLogin(true);
        $chargeTypeId = $this->input->post('ChargeTypeId');
        if($chargeTypeId > 0){
            $this->load->model('Mcharges');
            if($chargeTypeId == 2){ //mua ve
                $paidVN = $this->input->post('PaidVN');
                $ticketCount = $this->input->post('TicketCount');
                $paidVN = replacePrice($paidVN);
                $ticketCount = replacePrice($ticketCount);
                $user = $this->Musers->get($user['UserId']);
                if($paidVN > $user['Balance']) echo json_encode(array('code' => -1, 'message' => "Bạn không đủ tiền để mua. Vui lòng nạp tiền vào tài khoản"));
                elseif($paidVN > 0 && $ticketCount > 0){
                    $totalTicket = $this->Mconfigs->getConfigValue('TOTAL_TICKET', 0);
                    if($totalTicket >= $ticketCount) {
                        $postData = array(
                            'StudentId' => $user['UserId'],
                            'ToStudentId' => 0,
                            'ChargeTypeId' => $chargeTypeId,
                            'ChargeStatusId' => STATUS_ACTIVED,
                            'PaidVN' => $paidVN,
                            'TicketCount' => $ticketCount,
                            'CrUserId' => $user['UserId'],
                            'CrDateTime' => getCurentDateTime()
                        );
                        $flag = $this->Mcharges->insert($postData, array(), $user);
                        if ($flag) echo json_encode(array('code' => 1, 'message' => "Bạn đã mua {$ticketCount} Zticket thành công"));
                        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Không đủ Zticket trong kho"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            elseif($chargeTypeId == 1){//ban ve
                $ticketCount = $this->input->post('TicketCount');
                $phoneNumber = trim($this->input->post('PhoneNumber'));
                $userPass = trim($this->input->post('UserPass'));
                if($ticketCount <= 0 || empty($phoneNumber) || empty($userPass)) echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                else {
                    $user = $this->Musers->get($user['UserId']);
                    $flag = false;
                    if($user['IsEnabled2FA'] == 2 && !empty($user['SecretCode'])){
                        $userCode = trim($this->input->post('UserCode'));
                        if(!empty($userCode)){
                            require_once APPPATH."/libraries/GoogleAuthenticator.php";
                            $ga = new PHPGangsta_GoogleAuthenticator();
                            $checkResult = $ga->verifyCode($user['SecretCode'], $userCode, TIME_SIZE_2FA);
                            if ($checkResult) $flag = true;
                            else echo json_encode(array('code' => -1, 'message' => "Mã token không đúng, vui lòng nhập lại mã"));
                        }
                        else echo json_encode(array('code' => -1, 'message' => "Mã 2FA không được bỏ trống"));
                    }
                    else $flag = true;
                    if($flag) {
                        $phoneNumbers = explode(',', $phoneNumber);
                        if ($ticketCount * count($phoneNumbers) > $user['TicketCount']) echo json_encode(array('code' => -1, 'message' => "Bạn không đủ Zticket để chuyển. Vui lòng mua Zticket vào tài khoản"));
                        else {
                            $errors = array();
                            $success = array();
                            $notifications = array();
                            $tokenFCMs = array();
                            $crDateTime = getCurentDateTime();
                            foreach($phoneNumbers as $pText) {
                                $parts = explode('(', $pText);
                                if (count($parts) > 0) {
                                    $phoneNumber = trim($parts[0]);
                                    $toStudent = $this->Musers->getBy(array('PhoneNumber' => $phoneNumber, 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2), true, '', 'UserId, FullName, PhoneNumber, TicketCount, TokenFCM');
                                    if($toStudent){
                                        if($toStudent['UserId'] != $user['UserId']) {
                                            $user = $this->Musers->get($user['UserId']);
                                            $decimalCount = $this->getDecimalCount();
                                            $ticketCount = round($ticketCount, $decimalCount);
                                            $ticketCost = $this->Mconfigs->getConfigValue('TICKET_COST', 20000000);
                                            $paidVN = round($ticketCount * $ticketCost, 0);
                                            $postData = array(
                                                'StudentId' => $user['UserId'],
                                                'ToStudentId' => $toStudent['UserId'],
                                                'ChargeTypeId' => $chargeTypeId,
                                                'ChargeStatusId' => STATUS_ACTIVED,
                                                'PaidVN' => $paidVN,
                                                'TicketCount' => $ticketCount,
                                                'CrUserId' => $user['UserId'],
                                                'CrDateTime' => $crDateTime
                                            );
                                            $flag = $this->Mcharges->insert($postData, $toStudent, $user);
                                            if ($flag){
                                                $notifications[] = array(
                                                    'StudentId' => $toStudent['UserId'],
                                                    'AdminId' => ADMIN_ID,
                                                    'IsRead' => 1,
                                                    'IsFromStudent' => 1,
                                                    'Message' => 'Bạn vừa nhận được '.$ticketCount.' Zticket từ '.$user['FullName'].' ('.$user['PhoneNumber'].')',
                                                    'CrUserId' => $user['UserId'],
                                                    'CrDateTime' => $crDateTime
                                                );
                                                $tokenFCMs[$toStudent['UserId']] = $toStudent['TokenFCM'];                                                
                                                $success[] = "Bạn đã chuyển {$ticketCount} Zticket cho {$phoneNumber} thành công";                                                
                                            }
                                            else $errors[] = "Có lỗi xảy ra khi chuyển Zticket cho tài khoản có SĐT là '.$phoneNumber;";
                                        }
                                        else $errors[] = "Không được chuyển Zticket cho chính mình";
                                    }
                                    else $errors[] = 'Không tìm thấy học viên ứng với SĐT là '.$phoneNumber;
                                }
                                else $errors[] = $pText.' không đúng định dạng người dùng';
                            }
                            $this->session->set_flashdata('messageCharge', array('success' => $success, 'errors' => $errors));
                            if(!empty($success)){
                                if(!empty($notifications)){
                                    $this->db->insert_batch('notifications', $notifications);
                                    foreach($notifications as $n){
                                        if(isset($tokenFCMs[$n['StudentId']])) sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
                                    }
                                }
                                echo json_encode(array('code' => 1, 'message' => "Bán Zticket cho tài khoản khác thành công"));
                            }
                            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                        }
                    }
                }
            }
            elseif($chargeTypeId == 4){ //chuyen tien giua 2 thanh vien
                $phoneNumber = trim($this->input->post('PhoneNumber'));
                $userPass = trim($this->input->post('UserPass'));
                $paidVN = $this->input->post('PaidVN');
                $paidVN = replacePrice($paidVN);
                if($paidVN <= 0 || empty($phoneNumber) || empty($userPass)) echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                else{
                    $user = $this->Musers->get($user['UserId']);
                    $flag = false;
                    if($user['IsEnabled2FA'] == 2 && !empty($user['SecretCode'])){
                        $userCode = trim($this->input->post('UserCode'));
                        if(!empty($userCode)){
                            require_once APPPATH."/libraries/GoogleAuthenticator.php";
                            $ga = new PHPGangsta_GoogleAuthenticator();
                            $checkResult = $ga->verifyCode($user['SecretCode'], $userCode, TIME_SIZE_2FA);
                            if ($checkResult) $flag = true;
                            else echo json_encode(array('code' => -1, 'message' => "Mã token không đúng, vui lòng nhập lại mã"));
                        }
                        else echo json_encode(array('code' => -1, 'message' => "Mã 2FA không được bỏ trống"));
                    }
                    else $flag = true;
                    if($flag) {
                        $phoneNumbers = explode(',', $phoneNumber);
                        if($paidVN * count($phoneNumbers) > $user['Balance']) echo json_encode(array('code' => -1, 'message' => "Bạn không đủ tiền để chuyển. Vui lòng nạp tiền vào tài khoản"));
                        else{
                            $errors = array();
                            $success = array();
                            $notifications = array();
                            $tokenFCMs = array();
                            $crDateTime = getCurentDateTime();
                            foreach($phoneNumbers as $pText) {
                                $parts = explode('(', $pText);
                                if (count($parts) > 0) {
                                    $phoneNumber = trim($parts[0]);
                                    $toStudent = $this->Musers->getBy(array('PhoneNumber' => $phoneNumber, 'StatusId' => STATUS_ACTIVED, 'RoleId' => 2), true, '', 'UserId, FullName, PhoneNumber, Balance, TokenFCM');
                                    if($toStudent){
                                        if($toStudent['UserId'] != $user['UserId']) {
                                            $user = $this->Musers->get($user['UserId']);                
                                            $postData = array(
                                                'StudentId' => $user['UserId'],
                                                'ToStudentId' => $toStudent['UserId'],
                                                'ChargeTypeId' => $chargeTypeId,
                                                'ChargeStatusId' => STATUS_ACTIVED,
                                                'PaidVN' => $paidVN,
                                                'TicketCount' => 0,
                                                'Comment' => trim($this->input->post('Comment')),
                                                'CrUserId' => $user['UserId'],
                                                'CrDateTime' => $crDateTime
                                            );
                                            $flag = $this->Mcharges->insert($postData, $toStudent, $user);
                                            if ($flag){
                                                $notifications[] = array(
                                                    'StudentId' => $toStudent['UserId'],
                                                    'AdminId' => ADMIN_ID,
                                                    'IsRead' => 1,
                                                    'IsFromStudent' => 1,
                                                    'Message' => 'Bạn vừa nhận được chuyển khoản '.priceFormat($paidVN).' đ từ '.$user['FullName'].' ('.$user['PhoneNumber'].').',
                                                    'CrUserId' => $user['UserId'],
                                                    'CrDateTime' => $crDateTime
                                                );
                                                $tokenFCMs[$toStudent['UserId']] = $toStudent['TokenFCM'];
                                                $success[] = "Bạn đã chuyển ".priceFormat($paidVN)." VNĐ cho {$phoneNumber} thành công";
                                            }
                                            else $errors[] = "Có lỗi xảy ra khi chuyển tiền cho tài khoản có SĐT là '.$phoneNumber;";
                                        }
                                        else $errors[] = "Không được chuyển tiền cho chính mình";
                                    }
                                    else $errors[] = 'Không tìm thấy học viên ứng với SĐT là '.$phoneNumber;
                                }
                                else $errors[] = $pText.' không đúng định dạng người dùng';
                            }
                            $this->session->set_flashdata('messageCharge', array('success' => $success, 'errors' => $errors));
                            if(!empty($success)){
                                if(!empty($notifications)){
                                    $this->db->insert_batch('notifications', $notifications);
                                    foreach($notifications as $n){
                                        if(isset($tokenFCMs[$n['StudentId']])) sendFCM($n['Message'], $tokenFCMs[$n['StudentId']]);
                                    }
                                }
                                echo json_encode(array('code' => 1, 'message' => "Chuyển tiền cho tài khoản khác thành công"));
                            }
                            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
                        }
                    }
                }
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }
}