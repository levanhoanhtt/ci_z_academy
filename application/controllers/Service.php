<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
        $prefix = 'student';
        $listUsers = array();
        if($user['RoleId'] == 1){
            $prefix = 'admin';
            $listUsers = $this->Musers->getListForSelect(2, 2);
        }
		$data = $this->commonData($user,
			'Dịch vụ',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css', 'vendor/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'vendor/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js', "js/service_{$prefix}.js")),
                'listUsers' => $listUsers
            )
		);
        $this->load->model('Mservices');
        $postData = $this->arrayFromPost(array('ServiceName', 'ServiceTypeId'));
        if(empty($postData['ServiceName'])) unset($postData['ServiceName']);
        if(empty($postData['ServiceTypeId']) || $postData['ServiceTypeId'] <= 0) unset($postData['ServiceTypeId']);
        if($user['RoleId'] == 1) $postData['StatusId >'] = 0;
        else $postData['StatusId'] = STATUS_ACTIVED;
        $data['listServices'] = $this->Mservices->getBy($postData);
        $this->load->view('service/'.$prefix, $data);
	}

    public function log(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            $user['RoleId'] == 1 ? 'Đăng ký dịch vụ' : 'Dịch vụ của tôi',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', "js/service_log.js"))
            )
        );
        $this->loadModel(array('Mservices', 'Muserservices'));
        $listUsers = array();
        if($user['RoleId'] == 1) $listUsers = $this->Musers->getListForSelect();
        else $listUsers[] = $user;
        $data['listUsers'] = $listUsers;
        $data['listServices'] = $this->Mservices->getBy(array('StatusId >=' => 0));
        $postData = $this->arrayFromPost(array('UserId', 'ServiceId', 'ServiceStatusId', 'BeginDate', 'EndDate'));
        if($user['RoleId'] == 2) $postData['UserId'] = $user['UserId'];
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        $rowCount = $this->Muserservices->getCount($postData);
        $data['listUserServices'] = array();
        if($rowCount > 0){
            $perPage = DEFAULT_LIMIT;
            $pageCount = ceil($rowCount / $perPage);
            $page = $this->input->post('PageId');
            if(!is_numeric($page) || $page < 1) $page = 1;
            $data['listUserServices'] = $this->Muserservices->search($postData, $perPage, $page, false);
            $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
        }
        $this->load->view('service/log', $data);
    }

    public function notice(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Thông báo dịch vụ',
            array(
                'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', "js/service_log.js"))
            )
        );
        $this->loadModel(array('Mservices', 'Mservicelogs'));
        $listUsers = array();
        if($user['RoleId'] == 1) $listUsers = $this->Musers->getListForSelect();
        else $listUsers[] = $user;
        $data['listUsers'] = $listUsers;
        $data['listServices'] = $this->Mservices->getBy(array('StatusId >=' => 0));
        $postData = $this->arrayFromPost(array('UserId', 'ServiceId', 'BeginDate', 'EndDate'));
        if($user['RoleId'] == 2) $postData['UserId'] = $user['UserId'];
        if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 00:00:00');
        $rowCount = $this->Mservicelogs->getCount($postData);
        $data['listServiceLogs'] = array();
        if($rowCount > 0){
            $perPage = DEFAULT_LIMIT;
            $pageCount = ceil($rowCount / $perPage);
            $page = $this->input->post('PageId');
            if(!is_numeric($page) || $page < 1) $page = 1;
            $data['listServiceLogs'] = $this->Mservicelogs->search($postData, $perPage, $page, false);
            $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
        }
        $this->load->view('service/notice', $data);
    }

    public function get(){
        $this->checkUserLogin(true);
        $serviceId = $this->input->post('ServiceId');
        if($serviceId > 0){
            $this->load->model('Mservices');
            $service = $this->Mservices->get($serviceId);
            if($service){
                $service['ServiceTypeName'] = $this->Mconstants->serviceTypes[$service['ServiceTypeId']];
                echo json_encode(array('code' => 1, 'data' => $service));
            }
            else echo json_encode(array('code' => -1, 'message' => "Không tìm thấy dịch vụ"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ServiceName', 'ServiceDesc', 'ServiceCost', 'ServiceTypeId', 'IsRequireCarNumber'));
        $postData['ServiceCost'] = replacePrice($postData['ServiceCost']);
        if(!empty($postData['ServiceName']) && $postData['ServiceCost'] > 0 && $postData['ServiceTypeId'] > 0){
            $imageBase64 = $this->input->post('ServiceImage');
            if(!empty($imageBase64) && $imageBase64 != IMAGE_PATH.NO_IMAGE) {
                $crDateTime = getCurentDateTime();
                $imageBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $imageBase64));
                $imgName = strtotime($crDateTime);
                $imgPath =  IMAGE_PATH . $imgName . ".png";
                file_put_contents($imgPath, $imageBase64);
                $postData['ServiceImage'] = $imgName . '.png';
            }
            $serviceId = $this->input->post('ServiceId');
            if($serviceId > 0){
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = getCurentDateTime();
            }
            else{
                $postData['StatusId'] = STATUS_ACTIVED;
                $postData['CrUserId'] = $user['UserId'];
                $postData['CrDateTime'] = getCurentDateTime();
            }
            $this->load->model('Mservices');
            $serviceId = $this->Mservices->save($postData, $serviceId);
            if($serviceId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật dịch vụ thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $serviceId = $this->input->post('ServiceId');
        $statusId = $this->input->post('StatusId');
        if($serviceId > 0 && $statusId >= 0){
            $this->load->model('Mservices');
            $flag = $this->Mservices->changeStatus($statusId, $serviceId, 'StatusId', $user['UserId']);
            if($flag){
                $msg = "Xóa dịch vụ thành công";
                $statusName = '';
                if($statusId > 0) {
                    $statusName = '<span class="'.$this->Mconstants->labelCss[$statusId].'">'.$this->Mconstants->status[$statusId].'</span>';
                    if($statusId == 1) $msg = "Dừng dịch vụ thành công";
                    elseif($statusId == 2) $msg = "Kích hoạt dịch vụ thành công";
                }
                echo json_encode(array('code' => 1, 'message' => $msg, 'data' => array('StatusName' => $statusName)));
            }
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

	/*public function delete(){
		$user = $this->checkUserLogin(true);
		$serviceId = $this->input->post('ServiceId');
		if($serviceId > 0){
			$this->load->model('Mservices');
			$flag = $this->Mservices->changeStatus(0, $serviceId, 'StatusId', $user['UserId']);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa dịch vụ thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

    public function status(){
        $user = $this->checkUserLogin(true);
        $serviceId = $this->input->post('ServiceId');
        $statusId = $this->input->post('StatusId');
        if($serviceId > 0 && $statusId > 0){
            $this->load->model('Mservices');
            if($statusId == 2) $statusId = 1;
            else $statusId = 2;
            $flag = $this->Mservices->changeStatus($statusId, $serviceId, 'StatusId', $user['UserId']);
            if($flag) echo json_encode(array('code' => 1, 'message' => $statusId == 1 ? "Dừng dịch vụ thành công":"Kích hoạt dịch vụ thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }*/

	public function register(){
        $user = $this->checkUserLogin(true);
        $postData = $this->arrayFromPost(array('ServiceId', 'ServiceTypeId', 'BeginDate', 'EndDate', 'PaidCost', 'Comment', 'CarNumber'));
        $serviceCost = replacePrice($this->input->post('ServiceCost'));
        if($user['RoleId'] == 1) $userId = $this->input->post('UserId');
        else $userId = $user['UserId'];
        if($userId > 0 && $postData['ServiceId'] > 0 && $postData['ServiceTypeId'] > 0 && $serviceCost > 0 && !empty($postData['BeginDate'])){
            $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
            $postData['UserId'] = $userId;
            $this->loadModel(array('Mservices', 'Muserservices', 'Mtransactionlogs', 'Mservicecharges', 'Mservicelogs'));
            if($this->Muserservices->checkExist($postData['UserId'], $postData['ServiceId'])){
                echo json_encode(array('code' => -1, 'message' => "Bạn đang đăng ký dịch vụ này rồi"));
                die();
            }
            $postData['PaidCost'] = replacePrice($postData['PaidCost']);
            $postData['CrDateTime'] = getCurentDateTime();
            $toDay = date("Y-m-d");
            $serviceStatusId = 2;
            if(strtotime($toDay) < strtotime($postData['BeginDate'])) $serviceStatusId = 1;
            if(!empty($postData['EndDate'])){
                if(strtotime($toDay) >= strtotime($postData['EndDate'])) $serviceStatusId = 3;
            }
            $postData['ServiceStatusId'] = $serviceStatusId;
            $postData['PaidCost'] = $postData['PaidCost'] > 0 ? $postData['PaidCost'] : $serviceCost;
            $transactionLogs = array();
            $serviceCharge = array();
            $serviceLog = array();
            $userEdit = $this->Musers->get($userId, true, '', 'FullName, PhoneNumber, Email, Balance');
            $isSendMail = false;
            if($serviceStatusId == 2 || $serviceStatusId == 3){
                $comment = '';
                $serviceName = $this->Mservices->getFieldValue(array('ServiceId' => $postData['ServiceId']), 'ServiceName');
                if(!empty($postData['EndDate'])){
                    $endDate = ddMMyyyy($postData['EndDate']);
                    $servicePeriod = $this->Muserservices->timeDiff($postData['BeginDate'], $postData['EndDate'], $postData['ServiceTypeId']);
                    if($postData['ServiceTypeId'] == 3){
                        if(ddMMyyyy($postData['BeginDate'], 'm/Y') == ddMMyyyy($postData['EndDate'], 'm/Y')) $comment =  "{$userEdit['FullName']} ({$userEdit['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi đăng ký dịch vụ {$serviceName} tháng ".ddMMyyyy($postData['BeginDate'], 'm/Y');
                        else $comment =  "{$userEdit['FullName']} ({$userEdit['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi đăng ký dịch vụ {$serviceName} từ tháng ".ddMMyyyy($postData['BeginDate'], 'm/Y')." đến tháng ".ddMMyyyy($postData['EndDate'], 'm/Y');
                    }
                    else $comment =  "{$userEdit['FullName']} ({$userEdit['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi đăng ký dịch vụ {$serviceName} từ ngày ".ddMMyyyy($postData['BeginDate'])." đến ngày ".$endDate;
                }
                else{
                    $servicePeriod = $this->Muserservices->timeDiff($postData['BeginDate'], $toDay, $postData['ServiceTypeId']);
                    if($postData['ServiceTypeId'] == 2){
                        $endDate = date('d/m/Y', strtotime("+{$servicePeriod} month", strtotime($postData['BeginDate'])));
                        $comment =  "{$userEdit['FullName']} ({$userEdit['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi đăng ký dịch vụ {$serviceName} từ ngày ".ddMMyyyy($postData['BeginDate'])." đến ngày ".$endDate;
                    }
                    elseif($postData['ServiceTypeId'] == 3){
                        $comment =  "{$userEdit['FullName']} ({$userEdit['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi đăng ký dịch vụ ".$serviceName;
                        if($servicePeriod > 1) $comment .=  " từ tháng ".ddMMyyyy($postData['BeginDate'], 'm/Y')." đến tháng ".ddMMyyyy($toDay, 'm/Y');
                        else $comment .=  " tháng ".ddMMyyyy($postData['BeginDate'], 'm/Y');
                    }
                }
                $transactionLogs = array(
                    'UserId' => $userId,
                    'ByUserId' => 0,
                    'LogTypeId' => 6,
                    'Amount' => $postData['PaidCost'],
                    'Balance' => $userEdit['Balance'] - $postData['PaidCost'],
                    'TicketCount' => 0,
                    'Comment' => $comment,
                    'CrDateTime' => $postData['CrDateTime']
                );
                $serviceCharge = array(
                    'ChargeDate' => date('Y-m-d'),
                    'ServiceCost' => $serviceCost,
                    'ServicePeriod' => $servicePeriod,
                    'PaidCost' => $postData['PaidCost']
                );
                if($postData['PaidCost'] > 0 && $userEdit['Balance'] < $postData['PaidCost']) {
                    $isSendMail = true;
                    $serviceLog = array(
                        'UserId' => $userId,
                        'ServiceId' => $postData['ServiceId'],
                        'Comment' => "Tài khoản không đủ tiền để sử dụng dịch vụ " . $serviceName,
                        'CrUserId' => $user['UserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    );
                }
            }
            $flag = $this->Muserservices->register($postData, $transactionLogs, $serviceCharge, $serviceLog);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Đăng ký dịch vụ thành công"));
            else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            if($isSendMail && filter_var($userEdit['Email'], FILTER_VALIDATE_EMAIL)){
                $message = "Xin chào {$userEdit['FullName']} ({$userEdit['PhoneNumber']}).<br/> Hiện tại tài khoản của bạn đang nợ học viện với số tiền là ".priceFormat(-$transactionLogs['Balance'])." VNĐ . Xin vui lòng thanh toán cho Học viện";
                $configs = $this->session->userdata('configs');
                if(!$configs) $configs = array();
                $emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'minhngocbka@me.com';
                $companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Z Academy';
                $this->sendMail($emailFrom, $companyName, $userEdit['Email'], 'Thanh toán tiền dịch vụ', $message);
            }
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function updateRegister(){
        $user = $this->checkUserLogin(true);
        if($user['RoleId'] == 1){
            $postData = $this->arrayFromPost(array('UserId', 'ServiceId', 'BeginDate', 'EndDate', 'ServiceStatusId', 'Comment', 'CarNumber'));
            $userServiceId = $this->input->post('UserServiceId');
            if($userServiceId > 0 && $postData['UserId'] > 0 && $postData['ServiceId'] > 0 && !empty($postData['BeginDate']) && $postData['ServiceStatusId']){
                $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
                if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
                $crDateTime = getCurentDateTime();
                $postData['UpdateUserId'] = $user['UserId'];
                $postData['UpdateDateTime'] = $crDateTime;
                $comment = 'Hệ thống cập nhật dịch vụ '.$this->Mservices->getFieldValue(array('ServiceId' => $postData['ServiceId']), 'ServiceName').' của '.$this->Musers->getFieldValue(array('UserId' => $postData['UserId']), 'FullName');
                $commentTmp = trim($this->input->post('CommentUpdate'));
                if(!empty($commentTmp)) $comment .= ' với lý do: '.$commentTmp;
                $serviceLog = array(
                    'UserServiceId' => $userServiceId,
                    'UserId' => $postData['UserId'],
                    'ServiceId' => $postData['ServiceId'],
                    'Comment' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
                $this->loadModel(array('Mservices', 'Muserservices', 'Mservicelogs'));
                $flag = $this->Muserservices->updateRegister($postData, $userServiceId, $serviceLog);
                if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật đăng ký dịch vụ thành công"));
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function cancelService() {
        $user = $this->checkUserLogin(true);
        $userServiceId = $this->input->post('UserServiceId');
        $serviceId = $this->input->post('ServiceId');
        if($user['RoleId'] == 1) $userId = $this->input->post('UserId');
        else $userId = $user['UserId'];
        if($userServiceId > 0 && $serviceId > 0 && $userId > 0){
            $this->loadModel(array('Mservices', 'Muserservices', 'Mservicelogs'));
            $serviceStatusId = $this->Muserservices->getFieldValue(array('UserServiceId' => $userServiceId), 'ServiceStatusId', 0);
            if($serviceStatusId == 1 || $serviceStatusId == 2){
                $serviceName = $this->Mservices->getFieldValue(array('ServiceId' => $serviceId), 'ServiceName');
                $statusId = $user['RoleId'] == 1 ? 6 : 3;
                $serviceLog = array(
                    'UserServiceId' => $userServiceId,
                    'UserId' => $userId,
                    'ServiceId' => $serviceId,
                    'Comment' => $statusId == 6 ? 'Hệ thống dừng dịch vụ '.$serviceName.' với lý do: '.$this->input->post('Comment') : 'Tự tạm dừng sử dụng dịch vụ '.$serviceName,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => getCurentDateTime()
                );
                $flag = $this->Muserservices->stopService($userServiceId, $user['UserId'], $statusId, $serviceLog);
                if($flag){
                    $statusName = '<span class="' . $this->Mservices->labelCss[$statusId] . '">' . $this->Mconstants->serviceStatus[$statusId] . '</span>';
                    echo json_encode(array('code' => 1, 'message' => "Hủy dịch vụ thành công", 'data' => array('StatusName' => $statusName, 'EndDate' => date('d/m/Y'))));
                }
                else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Dịch vụ này không hủy được"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
    }

    public function timeDiff(){
        $beginDate = trim($this->input->post('BeginDate'));
        $endDate = trim($this->input->post('EndDate'));
        $serviceTypeId = $this->input->post('ServiceTypeId');
        if(!empty($beginDate) && !empty($endDate) && $serviceTypeId > 0){
            if(strtotime($beginDate) > strtotime($endDate)) echo 0;
            else {
                $this->load->model('Muserservices');
                echo $this->Muserservices->timeDiff($beginDate, $endDate, $serviceTypeId);
            }
        }
        else echo 0;
    }
}