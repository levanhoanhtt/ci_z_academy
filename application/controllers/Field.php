<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Lĩnh vực',
			array('scriptFooter' => array('js' => 'js/field.js'))
		);
		if($user['RoleId'] == 1){
			$this->load->model('Mfields');
			$data['listFields'] = $this->Mfields->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/field', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FieldName'));
		if(!empty($postData['FieldName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$fieldId = $this->input->post('FieldId');
			$this->load->model('Mfields');
			$flag = $this->Mfields->save($postData, $fieldId);
			if ($flag > 0) {
				$postData['FieldId'] = $flag;
				$postData['IsAdd'] = ($fieldId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật lĩnh vực thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function delete(){
		$this->checkUserLogin(true);
		$fieldId = $this->input->post('FieldId');
		if($fieldId > 0){
			$this->load->model('Mfields');
			$flag = $this->Mfields->changeStatus(0, $fieldId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa  lĩnh vực thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}