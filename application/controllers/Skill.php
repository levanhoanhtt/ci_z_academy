<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Kỹ năng',
			array('scriptFooter' => array('js' => 'js/skill.js'))
		);
		if($user['RoleId'] == 1){
			$this->load->model('Mskills');
			$data['listSkills'] = $this->Mskills->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/skill', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('SkillName','SkillTypeId'));
		if(!empty($postData['SkillName']) && $postData['SkillTypeId'] > 0) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$skillId = $this->input->post('SkillId');
			$this->load->model('Mskills');
			$flag = $this->Mskills->save($postData, $skillId);
			if ($flag > 0) {
				$postData['SkillId'] = $flag;
				$postData['IsAdd'] = ($skillId > 0) ? 0 : 1;
				$postData['SkillTypeName'] = '<span class="'.$this->Mconstants->labelCss[$postData['SkillTypeId']].'">'.$this->Mconstants->skillTypes[$postData['SkillTypeId']].'</span>';
				echo json_encode(array('code' => 1, 'message' => "Cập nhật kỹ năng thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$skillId = $this->input->post('SkillId');
		if($skillId > 0){
			$this->load->model('Mskills');
			$flag = $this->Mskills->changeStatus(0, $skillId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa  kỹ năng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
