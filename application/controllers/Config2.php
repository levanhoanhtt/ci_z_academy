<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config2 extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Điều chỉnh',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', "js/config.js")),
			)
		);
		if($user['RoleId'] == 1) {
			$configs = $this->session->userdata('configs');
			if(!$configs){
				$configs = $this->Mconfigs->getListMap();
				$this->session->set_userdata('configs', $configs);
			}
			$data['configs'] = array_merge($data['configs'], $configs);
			$this->load->model('Mactionlogs');
			$rowCount = $this->Mactionlogs->getCount(1);
			$data['listActionLogs'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listActionLogs'] = $this->Mactionlogs->getList(1, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('config/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1) {
			$data = $this->input->post('data');
			if(is_array($data) && !empty($data)) {
				$crDateTime = getCurentDateTime();
				foreach ($data as $k => $v) {
					$v = replacePrice($v);
					if($k == 5){
						$postData = array(
							'TicketCount' => $v,
							'UpdateUserId' => $user['UserId'],
							'UpdateDateTime' => $crDateTime
						);
						$actionLogs = array(
							'ItemId' => $k,
							'ItemTypeId' => 1,
							'ActionTypeId' => 2,
							'Comment' => $user['FullName'] . ' đã mở bán thêm '. priceFormat($v, true).' Zticket',
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$flag = $this->Mconfigs->update($postData, $k, $actionLogs);
						if ($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin thành công"));
						else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
					}
					else{
						$configItem = $this->Mconfigs->get($k);
						if ($configItem) {
							if($configItem['ConfigValue'] != $v) {
								$postData = array(
									'ConfigValue' => $v,
									'UpdateUserId' => $user['UserId'],
									'UpdateDateTime' => $crDateTime
								);
								$actionLogs = array(
									'ItemId' => $k,
									'ItemTypeId' => 1,
									'ActionTypeId' => 2,
									'Comment' => $user['FullName'] . ' đã sửa ' . $configItem['ConfigName'] . ' từ ' . priceFormat($configItem['ConfigValue']) . ' thành ' . priceFormat($v),
									'CrUserId' => $user['UserId'],
									'CrDateTime' => $crDateTime
								);
								$flag = $this->Mconfigs->update($postData, $k, $actionLogs);
								if ($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin thành công"));
								else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
							}
							else echo json_encode(array('code' => 0, 'message' => "Xin mời thay đổi giá trị"));
						}
						else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy cấu hình"));
					}
					break;
				}
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateText(){
		$user = $this->checkUserLogin();
		if($user['RoleId'] == 1) {
			$data = $this->input->post('data');
			if (is_array($data) && !empty($data)) {
				$crDateTime = getCurentDateTime();
				foreach ($data as $k => $v) {
					$postData = array(
						'ConfigValue' => $v,
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => $crDateTime
					);
					$flag = $this->Mconfigs->save($postData, $k);
					if ($flag){
						$this->session->set_userdata('configs', $this->Mconfigs->getListMap());
						echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin thành công"));
					}
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getTicketCost(){
		header('Access-Control-Allow-Origin: *');
		echo priceFormat(2 * $this->Mconfigs->getConfigValue('TICKET_COST', 0));
	}
        public function khoahoc(){
		header('Access-Control-Allow-Origin: *');
		echo $this->Mconfigs->getConfigValue('COURSE_ID', 20000000);
	}
	public function datekhoahoc(){
		header('Access-Control-Allow-Origin: *');
		echo $this->Mconfigs->getConfigValue('COURSE_DATE', 20000000);
	}
	public function alpha(){
		header('Access-Control-Allow-Origin: *');
		echo priceFormat(0.3 * $this->Mconfigs->getConfigValue('TICKET_COST', 0));
	}
       public function alphabeta(){
		header('Access-Control-Allow-Origin: *');
		echo priceFormat(0.85 * $this->Mconfigs->getConfigValue('TICKET_COST', 0));
	}
}