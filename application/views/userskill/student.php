<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<?php $this->load->view('includes/balance'); ?>
		<section class="content">
			<style>
				.tdText{line-height: 34px !important;padding-left: 5px !important;}
				.tdSelect{padding: 0 !important;}
			</style>
			<div class="row">
				<div class="col-sm-4">
					<div class="box box-widget widget-user-2">
						<div class="widget-user-header bg-yellow">
							<div class="widget-user-image">
								<img class="img-circle" src="assets/vendor/dist/img/logo-z.png" alt="<?php echo $user['FullName']; ?>">
							</div>
							<h3 class="widget-user-username"><?php echo $user['FullName']; ?></h3>
							<h5 class="widget-user-desc"><?php if($user['IsMember'] == 2) echo 'Thành viên Z - '; echo $user['PhoneNumber']; ?></h5>
						</div>
						<div class="box-footer no-padding">
							<ul class="nav nav-stacked">
								<li><a href="javascript:void(0)">Số điện thoại <span class="pull-right badge bg-green"><?php echo $user['PhoneNumber']; ?></span></a></li>
								<li><a href="javascript:void(0)">Email <span class="pull-right badge bg-aqua"><?php echo $user['Email']; ?></span></a></li>
								<li><a href="javascript:void(0)">Số Zticket <span class="pull-right badge bg-blue"><?php echo $user['TicketCount']; ?></span></a></li>
								<li><a href="javascript:void(0)">Tiền mặt <span class="pull-right badge bg-aqua"><?php echo priceFormat($user['Balance']); ?></span></a></li>
								<li><a href="javascript:void(0)">Ghi danh <span class="pull-right badge bg-green"><?php echo priceFormat($user['CountRegister']); ?></span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Kỹ năng Công nghệ</h3>
						</div>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody>
								<?php foreach($listSkills as $s){
									if($s['SkillTypeId'] == 1){ ?>
										<tr>
											<td class="tdText"><?php echo $s['SkillName']; ?></td>
											<td class="tdSelect" data-id="<?php echo $s['SkillId']; ?>" data-type="1">
												<?php $skillLevelId = $this->Mconstants->getObjectValue($listUserSkills, 'SkillId',  $s['SkillId'], 'SkillLevelId');
												if(empty($skillLevelId)) $skillLevelId = 0;
												echo $this->Mconstants->selectConstants('skillLevels', 'SkillLevel_'.$s['SkillId'], $skillLevelId, $skillLevelId == 0, 'Chọn mức độ'); ?>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Kỹ năng Mềm</h3>
						</div>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody>
								<?php foreach($listSkills as $s){
									if($s['SkillTypeId'] == 2){ ?>
										<tr>
											<td class="tdText"><?php echo $s['SkillName']; ?></td>
											<td class="tdSelect" data-id="<?php echo $s['SkillId']; ?>" data-type="2">
												<?php $skillLevelId = $this->Mconstants->getObjectValue($listUserSkills, 'SkillId',  $s['SkillId'], 'SkillLevelId');
												if(empty($skillLevelId)) $skillLevelId = 0;
												echo $this->Mconstants->selectConstants('skillLevels', 'SkillLevel_'.$s['SkillId'], $skillLevelId, $skillLevelId == 0, 'Chọn mức độ'); ?>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Mối quan hệ</h3>
						</div>
						<?php echo form_open('userskill/updateRelationship', array('id' => 'relationshipForm')); ?>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody id="tbodyRelationship">
								<?php foreach($listRelationships as $r){ ?>
									<tr id="relationship_<?php echo $r['RelationshipId']; ?>">
										<td class="tdText" id="fieldName_<?php echo $r['RelationshipId']; ?>"><?php echo $this->Mconstants->getObjectValue($listFields, 'FieldId', $r['FieldId'], 'FieldName'); ?></td>
										<td class="tdText" id="comment_<?php echo $r['RelationshipId']; ?>"><?php echo $r['Comment']; ?></td>
										<td class="actions">
											<a href="javascript:void(0)" class="link_edit" data-id="<?php echo $r['RelationshipId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
											<a href="javascript:void(0)" class="link_delete" data-id="<?php echo $r['RelationshipId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
											<input type="text" hidden="hidden" id="fieldId_<?php echo $r['RelationshipId']; ?>" value="<?php echo $r['FieldId']; ?>">
										</td>
									</tr>
								<?php } ?>
								<tr>
									<td><?php $this->Mconstants->selectObject($listFields, 'FieldId', 'FieldName', 'FieldId'); ?></td>
									<td><input type="text" class="form-control hmdrequired" id="comment" name="Comment" placeholder="Ghi chú" value="" data-field="Ghi chú"></td>
									<td class="actions">
										<a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
										<a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
										<input type="text" name="RelationshipId" id="relationshipId" value="0" hidden="hidden">
										<input type="text" id="deleteRelationshipUrl" value="<?php echo base_url('userskill/deleteRelationship'); ?>" hidden="hidden">
									</td>
								</tr>
								</tbody>
							</table>
						</div>
						<?php echo form_close(); ?>
					</div>
					<input type="text" hidden="hidden" id="updateUserSkillUrl" value="<?php echo base_url('userskill/update'); ?>">
				</div>
			</div>
		</section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>