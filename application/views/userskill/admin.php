<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <style>.control-label{color: #000;font-size: 14px;}</style>
                    <div class="box-body row-margin">
                        <?php echo form_open('userskill'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Kỹ năng công nghệ</label>
                                    <?php echo $this->Mconstants->selectObject($listTechSkills, 'SkillId', 'SkillName', 'SkillId_1', set_value('SkillId_1'), true, "--Chọn--"); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Xếp loại</label>
                                    <?php echo $this->Mconstants->selectConstants('skillLevels', 'SkillLevelId_1', set_value('SkillLevelId_1'), true, "--Chọn--"); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Kỹ năng mềm</label>
                                    <?php echo $this->Mconstants->selectObject($listSoftSkills, 'SkillId', 'SkillName', 'SkillId_2', set_value('SkillId_2'), true, "--Chọn--"); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Xếp loại</label>
                                    <?php echo $this->Mconstants->selectConstants('skillLevels', 'SkillLevelId_2', set_value('SkillLevelId_2'), true, "--Chọn--"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php echo $this->Mconstants->selectObject($listFields, 'FieldId', 'FieldName', 'FieldId', set_value('FieldId'), true, "Lĩnh vực quan hệ"); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên hoặc SĐT">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày sinh</th>
                                <th>Họ và tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Số Zticket</th>
                                <th>Tiền mặt</th>
                                <th>Ghi danh</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUser">
                            <?php $i = 0;
                            foreach($listStudents as $u){
                                $i++; ?>
                                <tr id="user_<?php echo $u['UserId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ddMMyyyy($u['BirthDay']); ?></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$u['UserId']); ?>"><?php echo $u['FullName']; ?></a></td>
                                    <td><?php echo $u['PhoneNumber']; ?></td>
                                    <td><?php echo $u['Email']; ?></td>
                                    <td><?php echo priceFormat($u['TicketCount'], true); ?></td>
                                    <td><?php echo priceFormat($u['Balance']); ?></td>
                                    <td><?php echo priceFormat($u['CountRegister']); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>