<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<?php if($userId > 0){ ?>
		<section class="content">
			<?php $this->load->view('includes/balance'); ?>
			<?php $labelCss = $this->Mskills->labelCss;
			$skillLevels = $this->Mconstants->skillLevels;
			$skillLevels[0] = ''; ?>
			<div class="row row-n-pd">
				<div class="col-sm-4">
					<div class="box box-widget widget-user-2">
						<div class="widget-user-header bg-yellow">
							<div class="widget-user-image">
								<img class="img-circle" src="assets/vendor/dist/img/logo-z.png" alt="<?php echo $userEdit['FullName']; ?>">
							</div>
							<h3 class="widget-user-username"><?php echo $userEdit['FullName']; ?></h3>
							<h5 class="widget-user-desc"><?php if($userEdit['IsMember'] == 2) echo 'Thành viên Z - '; echo $userEdit['PhoneNumber']; ?></h5>
						</div>
						<div class="box-footer no-padding">
							<ul class="nav nav-stacked">
								<li><a href="javascript:void(0)">Số điện thoại <span class="pull-right badge bg-green"><?php echo $userEdit['PhoneNumber']; ?></span></a></li>
								<li><a href="javascript:void(0)">Email <span class="pull-right badge bg-aqua"><?php echo $userEdit['Email']; ?></span></a></li>
								<li><a href="javascript:void(0)">Số Zticket <span class="pull-right badge bg-blue"><?php echo $userEdit['TicketCount']; ?></span></a></li>
								<li><a href="javascript:void(0)">Tiền mặt <span class="pull-right badge bg-aqua"><?php echo priceFormat($userEdit['Balance']); ?></span></a></li>
								<li><a href="javascript:void(0)">Ghi danh <span class="pull-right badge bg-green"><?php echo priceFormat($userEdit['CountRegister']); ?></span></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Kỹ năng Công nghệ</h3>
						</div>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody>
								<?php foreach($listSkills as $s){
									if($s['SkillTypeId'] == 1){ ?>
										<tr>
											<td class="tdText"><?php echo $s['SkillName']; ?></td>
											<td class="tdText">
												<?php $skillLevelId = $this->Mconstants->getObjectValue($listUserSkills, 'SkillId',  $s['SkillId'], 'SkillLevelId');
												if(empty($skillLevelId)) $skillLevelId = 0; ?>
												<span class="<?php echo $labelCss[$skillLevelId]; ?>"><?php echo $skillLevels[$skillLevelId]; ?></span>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Kỹ năng Mềm</h3>
						</div>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody id="tbodySoftSkills">
								<?php foreach($listSkills as $s){
									if($s['SkillTypeId'] == 2){ ?>
										<tr>
											<td class="tdText mbw-75"><?php echo $s['SkillName']; ?></td>
											<td class="tdText mbw-25">
												<?php $skillLevelId = $this->Mconstants->getObjectValue($listUserSkills, 'SkillId',  $s['SkillId'], 'SkillLevelId');
												if(empty($skillLevelId)) $skillLevelId = 0; ?>
												<span class="<?php echo $labelCss[$skillLevelId]; ?>"><?php echo $skillLevels[$skillLevelId]; ?></span>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="box box-default padding15">
						<div class="box-header with-border">
							<h3 class="box-title">Mối quan hệ</h3>
						</div>
						<div class="box-body table-responsive no-padding divTable">
							<table class="table table-hover table-bordered">
								<tbody id="tbodyRelationship">
								<?php foreach($listRelationships as $r){ ?>
									<tr id="relationship_<?php echo $r['RelationshipId']; ?>">
										<td class="tdText" id="fieldName_<?php echo $r['RelationshipId']; ?>"><?php echo $this->Mconstants->getObjectValue($listFields, 'FieldId', $r['FieldId'], 'FieldName'); ?></td>
										<td class="tdText" id="comment_<?php echo $r['RelationshipId']; ?>"><?php echo $r['Comment']; ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php } else{ ?>
			<section class="content"><?php $this->load->view('includes/notice'); ?></section>
		<?php } ?>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>