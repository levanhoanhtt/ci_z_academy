<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <?php echo form_open('article/update', array('id' => 'articleForm')); ?>
            <div class="row">
                <div class="col-sm-8 no-padding">
                    <div class="bg-box-gradient-re pd-15-25">
                        <div class="form-group">
                            <label class="control-label">Tiêu đề <span class="required">*</span></label>
                            <input type="text" name="ArticleTitle" class="form-control" id="articleTitle" value="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Trích dẫn</label>
                            <textarea name="ArticleLead" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nội dung</label>
                            <textarea name="ArticleContent" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="bg-box-gradient-re pd-15-25">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label">Ngày xuất bản</label>
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </span>
                                    <input type="text" class="form-control datetimepicker" id="publishDateTime" name="PublishDateTime" value="<?php echo date('d/m/Y H:i'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Hẹn ngày xuất bản</label>
                                <div class="input-group">
                                     <span class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </span>
                                    <input type="text" class="form-control datetimepicker" id="autoPublishDateTime" name="AutoPublishDateTime" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label normal">Trạng thái</label>
                                <?php $this->Mconstants->selectConstants('articleStatus', 'ArticleStatusId', 2); ?>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="IsPublic" id="isPublic">
                                <label class="control-label normal">Chỉ hiển thị cho 1 số học viên</label>
                            </div>
                            <style>#divStudent input.select2-search__field{color: #000;}</style>
                            <div class="form-group" id="divStudent" style="display: none;">
                                <label class="control-label normal">Danh sách học viên</label>
                                <select class="form-control select2" id="userId" multiple="multiple" data-placeholder="Chọn Học viên">
                                    <?php foreach($listStudents as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                                <?php //$this->Mconstants->selectObject($listStudents, 'UserId', 'FullName', 'UserId', 0, false, '', ' select2', ' multiple="multiple" data-placeholder="Chọn Học viên"'); ?>
                            </div>
                        </div>
                        <ul class="list-inline pull-right" style="margin-top: 35px;">
                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Cập nhật"></li>
                            <li><a href="<?php echo base_url('article'); ?>" id="articleListUrl" class="btn btn-default">Đóng</a></li>
                            <input type="text" hidden="hidden" id="articleId" name="ArticleId" value="0">
                            <input type="text" hidden="hidden" id="articleTypeId" value="1">
                        </ul>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>