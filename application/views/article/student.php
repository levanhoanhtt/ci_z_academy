<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<section class="content">
			<?php $this->load->view('includes/balance'); ?>
			<style>
				#articleList span.username, #articleList span.description{margin-left: 0;}
				#articleList .box-body{color: #555;}
			</style>
			<div class="text-right" style="margin-bottom: 20px;">
				<?php echo isset($paggingHtml) ? $paggingHtml : '' ?>
				<div class="clearfix"></div>
			</div>
			<div id="articleList">
				<?php foreach($listArticles as $a){ ?>
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<span class="username"><a href="<?php echo base_url('article/view/'.$a['ArticleId']); ?>"><?php echo $a['ArticleTitle']; ?></a></span>
							<span class="description"><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $a['CrUserId'], 'FullName'); ?> - <?php echo ddMMyyyy($a['PublishDateTime'], 'd/m/Y H:i'); ?></span>
						</div>
						<div class="box-tools">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body" style="display: block;">
						<?php echo $a['ArticleLead']; ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="text-right" style="margin-bottom: 20px;">
				<?php echo isset($paggingHtml) ? $paggingHtml : '' ?>
				<div class="clearfix"></div>
				<input type="text" hidden="hidden" id="articleListUrl" value="<?php echo base_url('article'); ?>">
			</div>
		</section>
	</div>
</div>
<?php $this->load->view('includes/footer'); ?>