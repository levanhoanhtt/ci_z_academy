<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
		<section class="content">
			<style>h2.dp-il{display: none;}</style>
			<?php $this->load->view('includes/balance'); ?>
			<div id="articleList">
				<?php $this->load->view('includes/notice'); ?>
				<?php if($articleId > 0){ ?>
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<span class="username"><a href="javascript:void(0)"><?php echo $article['ArticleTitle']; ?></a></span>
							<span class="description"><?php echo $crFullName; ?> - <?php echo ddMMyyyy($article['PublishDateTime'], 'd/m/Y H:i'); ?></span>
						</div>
					</div>
					<div class="box-body" style="display: block;">
						<?php echo $article['ArticleContent']; ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</section>
	</div>
</div>
<?php $this->load->view('includes/footer'); ?>