<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('affiliate'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control select2" name="AffUserId">
                                    <option value="0">Người giới thiệu</option>
                                    <?php foreach($listAffUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('AffUserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="SĐT hoặc Email">
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'),true,'--Trạng thái--' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control" name="AffPaidVNStatusId">
                                    <option value="0">--Doanh thu--</option>
                                    <option value="1"<?php if(set_value('AffPaidVNStatusId') == 1) echo ' selected="selected"'; ?>>Lớn hơn 0</option>
                                    <option value="3"<?php if(set_value('AffPaidVNStatusId') == 3) echo ' selected="selected"'; ?>>Bằng 0</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="row" style="margin-bottom: 0;">
                            <div class="col-sm-3">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Bonus Affiliate</h4>
                                        <h4><span class="spanCost"><?php echo $affPercent; ?></span> %</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Tiền chưa quy đổi</h4>
                                        <h4><span class="spanCost"><?php echo priceFormat($balanceAff); ?></span> đ</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Doanh thu hôm nay</h4>
                                        <h4><span class="spanCost"><?php echo priceFormat($todayTicketCount, true); ?></span> Zticket</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Tổng doanh thu</h4>
                                        <h4><span class="spanCost"><?php echo priceFormat($totalTicketCount, true); ?></span> Zticket</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày khởi tạo TK</th>
                                <th>Người giới thiệu</th>
                                <th>Họ và tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Tổng nạp</th>
                                <th>Doanh thu</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUser">
                            <?php $i = 0;
                            foreach($listUsers as $u){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ddMMyyyy($u['CrDateTime'], 'H:i:s d/m/Y'); ?></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$u['AffUserId']); ?>"><?php echo $this->Mconstants->getObjectValue($listAffUsers, 'UserId', $u['AffUserId'], 'FullName'); ?></a></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$u['UserId']); ?>"><?php echo $u['FullName']; ?></a></td>
                                    <td><?php echo $u['PhoneNumber']; ?></td>
                                    <td><?php echo $u['Email']; ?></td>
                                    <td><?php echo priceFormat($u['TotalPaidVN']); ?> đ</td>
                                    <td><?php echo priceFormat($u['TotalAffPaidVN']); ?> đ</td>
                                    <td><span class="<?php echo $this->Mconstants->labelCss[$u['StatusId']] ?>"><?php echo $this->Mconstants->status[$u['StatusId']] ?></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>