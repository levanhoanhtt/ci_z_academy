<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <?php $roleId = $user['RoleId']; ?>
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="box box-default">
                <?php sectionTitleHtml('Tìm kiếm'); ?>
                <div class="box-body row-margin">
                    <?php echo form_open('notification'); ?>
                    <div class="row">
                        <?php if($roleId == 1){ ?>
                        <div class="col-sm-4">
                            <select class="form-control select2" name="StudentId">
                                <option value="0">Học viên</option>
                                <?php foreach($listUsers as $u){ ?>
                                    <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('StudentId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php } ?>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <select class="form-control" name="IsRead">
                                <option value="0">--Trạng thái--</option>
                                <option value="1"<?php if(set_value('IsRead') == 1) echo ' selected="selected"'; ?>>Chưa xem</option>
                                <option value="2"<?php if(set_value('IsRead') == 2) echo ' selected="selected"'; ?>>Đã xem</option>
                            </select>
                        </div>
                        <!--<div class="col-sm-3">
                            <select class="form-control" name="IsFromStudent">
                                <option value="0">--Từ--</option>
                                <option value="1"<?php //if(set_value('IsFromStudent') == 1) echo ' selected="selected"'; ?>>Hệ thống</option>
                                <option value="2"<?php //if(set_value('IsFromStudent') == 2) echo ' selected="selected"'; ?>>Học viên</option>
                            </select>
                        </div>-->
                        <?php if($roleId == 1){ ?>
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="SĐT hoặc Email">
                            </div>
                        <?php } ?>
                        <div class="col-sm-4">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            <button type="button" class="btn btn-info pull-right" id="btnReadAll">Đọc tất cả</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>

        </section>
        <section class="content">
            <div class="box box-success">
                <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Ngày gửi</th>
                            <?php if($roleId == 1){ ?><th>Học viên</th><?php } ?>
                            <th>Tin nhắn</th>
                            <th class="text-center">Từ</th>
                            <th class="text-center" style="width: 72px;">Trạng thái</th>
                            <th class="text-center" style="width: 80px;">Hành động</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyNotification">
                        <?php foreach($listNotifications as $n){ ?>
                            <tr id="notification_<?php echo $n['NotificationId']; ?>">
                                <td><?php echo ddMMyyyy($n['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                <?php if($roleId == 1){ ?>
                                    <td>
                                        <?php if($n['StudentId'] > 0){ ?>
                                            <a href="<?php echo base_url('userskill/view/'.$n['StudentId']); ?>"><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $n['StudentId'], 'FullName'); ?></a>
                                        <?php }
                                        elseif(!empty($n['PhoneNumber'])){
                                            echo $n['PhoneNumber'];
                                            if(!empty($n['Email'])) echo ' ('.$n['Email'].')';
                                        } ?>
                                    </td>
                                <?php } ?>
                                <td><?php echo $n['Message']; ?>.</td>
                                <td class="text-center"><?php if($n['IsFromStudent'] == 2) echo '<span class="label label-default">Học viên</span>'; else echo '<span class="label label-success">Hệ thống</span>'; ?></td>
                                <td class="tdIsRead text-center" id="isRead_<?php echo $n['NotificationId']; ?>">
                                    <?php if($roleId != 1){ ?>
                                    <?php if($n['UserIsRead'] == 2) echo '<span class="label label-default">Đã xem</span>'; else echo '<span class="label label-success">Chưa xem</span>'; ?></td>
                                    <?php }else{ ?>
                                        <?php if($n['IsRead'] == 2) echo '<span class="label label-default">Đã xem</span>'; else echo '<span class="label label-success">Chưa xem</span>'; ?></td>
                                    <?php } ?>
                                <td class="actions text-center">
                                	<?php
                                    if($roleId != 1){
                                        if($n['UserIsRead'] == NULL){ ?>
                                    		<a href="javascript:void(0)" class="link_isRead" data-id="<?php echo $n['NotificationId']; ?>" title="Đã xem"><i class="fa fa-eye"></i></a>
                                        <?php }}else{ 
                                            if($n['IsRead'] == 1){
                                        ?>
                                        <a href="javascript:void(0)" class="link_isRead" data-id="<?php echo $n['NotificationId']; ?>" title="Đã xem"><i class="fa fa-eye"></i></a>
                                    <?php }} ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
                <input type="text" hidden="hidden" id="changeIsReadUrl" value="<?php echo base_url('notification/changeIsRead'); ?>">
                <input type="text" hidden="hidden" id="changeIsReadAllUrl" value="<?php echo base_url('notification/changeIsReadAll'); ?>">
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>