<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('order'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listProducts, 'ProductId', 'ProductName', 'ProductId', set_value('ProductId'), true, '--Sản phẩm--', ' select2'); ?>
                            </div>
                            <?php if($user['RoleId'] == 1){ ?>
                                <div class="col-sm-4">
                                    <select class="form-control select2" name="SellerId">
                                        <option value="0">Người bán</option>
                                        <?php foreach($listUsers as $u){ ?>
                                            <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('SellerId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên, SĐT người mua">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('sellerOrderStatus', 'SellerOrderStatusId', set_value('SellerOrderStatusId'), true, '--Phản hồi bên bán--'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('customerOrderStatus', 'CustomerOrderStatusId', set_value('CustomerOrderStatusId'), true, '--Phản hồi bên mua--'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('zOrderStatus', 'ZOrderStatusId', set_value('ZOrderStatusId'), true, '--Phản hồi từ Z--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <?php if($user['RoleId'] == 2){ ?>
                            <div class="row" style="margin-bottom: 0;">
                                <div class="col-sm-4">
                                    <div class="small-box primary-gradient box-statistic-money">
                                        <div class="inner text-center db-font">
                                            <h4>Tiền đang đóng băng</h4>
                                            <h4><span class="spanCost"><?php echo priceFormat($user['BalanceSuspend']); ?></span> đ</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="small-box primary-gradient box-statistic-money">
                                        <div class="inner text-center db-font">
                                            <h4>Số lần yêu cầu hoàn tiền</h4>
                                            <h4><span class="spanCost"><?php echo $user['RefundTime']; ?></span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <style>#tbodyOrder ul.dropdown-menu{top: -240%;left: 48px;}</style>
                <div class="box box-default">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($user['RoleId'] == 2) echo '<th>Hành<br/>động</th>'; ?>
                                <th>Thời gian</th>
                                <th>Sản phẩm</th>
                                <?php if($user['RoleId'] == 1) echo '<th>Người bán</th>'; ?>
                                <th>Khách hàng</th>
                                <th class="text-center" style="width: 68px;">Số lần<br/>yêu cầu<br/>hoàn tiền</th>
                                <th>SL</th>
                                <th>Địa chỉ giao hàng</th>
                                <th>Ghi chú</th>
                                <th class="text-center" style="width: 70px;">Mã đơn hàng</th>
                                <th class="text-center" style="width: 103px;">Phản hồi<br/>bên mua</th>
                                <th class="text-center" style="width: 103px;">Phản hồi<br/>bên bán</th>
                                <th class="text-center" style="width: 103px;">Phản hồi<br/>từ Z</th>
                                <th class="text-center" style="width: 50px;">Bằng<br/>chứng</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $customerOrderStatus = $this->Mconstants->customerOrderStatus;
                            $sellerOrderStatus = $this->Mconstants->sellerOrderStatus;
                            $zOrderStatus = $this->Mconstants->zOrderStatus;
                            $labelCss = $this->Morders->labelCss;
                            $productNames = array();
                            foreach($listOrders as $o){
                                $statusTypeId = 0;
                                if($user['RoleId'] == 1) $statusTypeId = 1;//quan tri ?>
                                <tr id="trOrder_<?php echo $o['OrderId']; ?>">
                                    <?php if($user['RoleId'] == 2){ ?>
                                    <td class="actions">
                                        <?php if ($user['UserId'] == $o['SellerId']) {
                                            $statusTypeId = 2;//ng ban ?>
                                            <div class="btn-group" id="btnGroup_<?php echo $o['OrderId']; ?>">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <?php foreach ($sellerOrderStatus as $j => $v) { ?>
                                                        <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $o['OrderId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                    </td>
                                    <?php } ?>
                                    <td><?php echo ddMMyyyy($o['CrDateTime'], 'H:i:s d/m/Y') ; ?></td>
                                    <td>
                                        <?php $productName = $this->Mconstants->getObjectValue($listProducts, 'ProductId', $o['ProductId'], 'ProductName');
                                        if(empty($productName)){
                                            if(!isset($productNames[$o['ProductId']])) $productNames[$o['ProductId']] = $this->Mproducts->getFieldValue(array('ProductId' => $o['ProductId']), 'ProductName');
                                            $productName = $productNames[$o['ProductId']];
                                        }
                                        echo $productName; ?>
                                    </td>
                                    <?php if($user['RoleId'] == 1){ ?>
                                        <td id="sellerName_<?php echo $o['OrderId']; ?>"><a href="<?php echo base_url('userskill/view/'.$o['SellerId']); ?>"><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $o['SellerId'], 'FullName'); ?></a></td>
                                    <?php } ?>
                                    <td id="customerName_<?php echo $o['OrderId']; ?>"><a href="<?php echo base_url('userskill/view/'.$o['CustomerId']); ?>"><?php echo $o['FullName']; ?></a></td>
                                    <td class="text-center" style="width: 68px;"><?php echo $o['RefundTime']; ?></td>
                                    <td><?php echo priceFormat($o['Quantity']); ?></td>
                                    <td class="break-word" style="max-width: 250px;"><?php echo $o['Address']; ?></td>
                                    <td class="break-word" style="max-width: 250px;"><?php echo $o['Comment']; ?></td>
                                    <td class="text-center" style="width: 70px;"><?php echo $o['OrderCode']; ?></td>
                                    <td class="tdCustomerOrderStatus text-center"><span class="<?php echo $labelCss['CustomerOrderStatus'][$o['CustomerOrderStatusId']]; ?>"><?php echo $customerOrderStatus[$o['CustomerOrderStatusId']]; ?></span></td>
                                    <td class="tdSellerOrderStatus text-center"><span class="<?php echo $labelCss['SellerOrderStatus'][$o['SellerOrderStatusId']]; ?>"><?php echo $sellerOrderStatus[$o['SellerOrderStatusId']]; ?></span></td>
                                    <td class="tdZOrderStatus text-center"><span class="<?php echo $labelCss['ZOrderStatus'][$o['ZOrderStatusId']]; ?>"><?php echo $zOrderStatus[$o['ZOrderStatusId']]; ?></span></td>
                                    <td class="actions text-center">
                                        <a href="javascript:void(0)" class="link_evident" title="Bằng chứng" data-id="<?php echo $o['OrderId']; ?>"<?php if($o['ZOrderStatusId'] == 1) echo ' style="display: none;"'; ?>><i class="fa fa-gavel"></i></a>
                                        <input type="text" hidden="hidden" id="statusTypeId_<?php echo $o['OrderId']; ?>" value="<?php echo $statusTypeId; ?>">
                                        <input type="text" hidden="hidden" id="sellerId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['SellerId']; ?>">
                                        <input type="text" hidden="hidden" id="customerId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CustomerId']; ?>">
                                        <input type="text" hidden="hidden" id="zOrderStatusId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['ZOrderStatusId']; ?>">
                                        <input type="text" hidden="hidden" id="isCustomerWin_<?php echo $o['OrderId']; ?>" value="<?php echo $o['IsCustomerWin']; ?>">
                                        <?php if($user['RoleId'] == 2){ ?>
                                            <span style="display: none;" id="sellerName_<?php echo $o['OrderId']; ?>"><?php echo $user['FullName']; ?></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <?php $this->load->view('order/modal'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('order/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>