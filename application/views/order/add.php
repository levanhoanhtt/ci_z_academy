<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('order/add'); ?>
                        <p style="color: #000;margin: 5px 0 15px 0;">Danh sách sản phẩm bảo hộ bởi ⓩ TM</p>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="ProductName" class="form-control" value="<?php echo set_value('ProductName'); ?>" placeholder="Sản phẩm">
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listBrands, 'BrandId', 'BrandName', 'BrandId', set_value('BrandId'), true, '--Nhãn hiệu--'); ?>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên, SĐT hoặc Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $sortTypes = array(
                                    1 => 'Giá thấp nhất',
                                    2 => 'Giá cao nhất',
                                    3 => 'Bán chạy nhất'
                                ); ?>
                                <select class="form-control" name="SortTypeId">
                                    <option value="0">--Xắp xếp theo--</option>
                                    <?php foreach($sortTypes as $i => $v){ ?>
                                        <option value="<?php echo $i; ?>"<?php if($i == set_value('SortTypeId')) echo ' selected="selected"'; ?>><?php echo $v; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên sản phẩm</th>
                                <th>Nhãn hiệu</th>
                                <th>Người bán</th>
                                <th>Giá</th>
                                <th class="text-center" style="width: 150px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $i = 0;
                            foreach ($listProducts as $p){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td class="tdProductName"><?php echo $p['ProductName']; ?></td>
                                    <td class="tdBrandName"><?php echo $this->Mconstants->getObjectValue($listBrands, 'BrandId', $p['BrandId'], 'BrandName'); ?></td>
                                    <td class="tdSeller"><a href="<?php echo base_url('userskill/view/'.$p['UserId']); ?>"><?php echo $p['FullName']; ?></a></td>
                                    <td><span class="spanCost"><?php echo priceFormat($p['Price']) ; ?></span> Đ</td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" class="btn btn-info btn-sm aOrder" data-id="<?php echo $p['ProductId'] ?>">Thanh toán</a>
                                        <input type="text" hidden="hidden" id="sellerId_<?php echo $p['ProductId'] ?>" value="<?php echo $p['UserId'] ?>">
                                    </td>
                                </tr>
                            <?php }	?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('order/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="productBuyId" value="<?php echo $productId; ?>">
                    <div id="orderModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content bg-box-gradient">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" style="color: #fff !important;">&times;</button>
                                    <h3 class="modal-title" style="color: #fff !important;">Thanh toán</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4 class="control-label" style="color: #fff !important;">Thông tin đặt hàng</h4>
                                            <div class="form-group">
                                                <input type="text" id="productName" class="form-control" readonly placeholder="Tên sản phẩm">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="brandName" class="form-control" readonly placeholder="Thương hiệu">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="seller" class="form-control" readonly placeholder="Người bán">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="promotionCode" class="form-control" placeholder="Mã ưu đãi">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="cost" readonly class="form-control" placeholder="Giá">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="quantity" class="form-control" placeholder="Số lượng" value="1">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="sumCost" class="form-control" readonly value="0" placeholder="Thành tiền">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4 class="control-label" style="color: #fff !important;">Thông tin khách hàng</h4>
                                            <div class="form-group">
                                                <input type="text" id="fullName" class="form-control" readonly placeholder="Họ và tên" value="<?php echo $user['FullName']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="email" class="form-control" readonly placeholder="Email" value="<?php echo $user['Email']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="phoneNumber" class="form-control" readonly placeholder="Số điện thoại" value="<?php echo $user['PhoneNumber']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" id="password" class="form-control hmdrequired" data-field="Mật khẩu" placeholder="Mật khẩu" value="">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="userCode" class="form-control" placeholder="Mã bảo mật Authenticator (nếu bật)" value="">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="address" class="form-control hmdrequired" data-field="Địa chỉ nhận hàng" placeholder="Địa chỉ nhận hàng">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" id="comment" class="form-control" placeholder="Ghi chú">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-danger" id="divPromotionError" style="display: none;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer text-center">
                                    <button type="button" style="color: #fff !important;" class="btn btn-buy" id="btnPayOrder">Thanh toán</button>
                                    <input type="text" hidden="hidden" id="productId" value="0">
                                    <input type="text" hidden="hidden" id="sellerId" value="0">
                                    <input type="text" hidden="hidden" id="promotionCodeId" value="0">
                                    <input type="text" hidden="hidden" id="discountCost" value="0">
                                    <input type="text" hidden="hidden" id="checkPromotionCodeUrl" value="<?php echo base_url('promotion/checkPromotionCode'); ?>">
                                    <input type="text" hidden="hidden" id="updateOrderUrl" value="<?php echo base_url('order/update'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
