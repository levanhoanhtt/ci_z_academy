<div class="modal fade" id="modalProof" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bằng chứng</h4>
            </div>
            <div class="modal-body">
                <style>
                    #tblProof img{max-height: 100px;}
                    #divConflictNotify{margin: auto;float: none; padding: 5px;border-radius: 25px;color:#fff;}
                </style>
                <?php if($user['RoleId'] == 2){ ?>
                <div class="bank-guide" style="margin-bottom: 15px;">
                    <div class="form-group">
                        <label>Ghi chú</label>
                        <input type="text" class="form-control" id="comment" value="">
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh bằng chứng</label>
                        <button class="text-center form-control" id="selectImage">Bấm để chọn tệp tin vào để tải lên</button>
                        <div class="progress" id="progress" style="display: none">
                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <input type="file" style="display: none" id="inputImage">
                    </div>
                    <div>
                        <span style="font-size: 10px">Chỉ hỗ trợ hình ảnh</span>
                        <div class="text-center">
                            <img src="" id="preView" style="width: 100%;height: auto"/>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Mốc thời gian</th>
                        <th>Họ tên</th>
                        <th>Ảnh</th>
                        <th>Ghi chú</th>
                    </tr>
                    </thead>
                    <tbody id="tblProof"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="text-center col-sm-4" id="divConflictNotify"><span></span></div>
                <?php if($user['RoleId'] == 1){ ?>
                    <button type="button" class="btn btn-primary btnSolve" data-id="1">Bên bán thắng và Đóng thương vụ</button>
                    <button type="button" class="btn btn-primary btnSolve" data-id="2">Bên mua thắng và Đóng thương vụ</button>
                <?php } else{ ?>


                    <button id="sendProof" type="button" class="btn btn-primary">Gửi</button>
                <?php } ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="text" hidden="hidden" id="orderId" value="0">
                <input type="text" hidden="hidden" id="statusId" value="0">
                <input type="text" hidden="hidden" id="statusTypeId" value="0">
                <input type="text" hidden="hidden" id="getConflictUrl" value="<?php echo base_url('order/getListConflict'); ?>">
                <input type="text" hidden="hidden" id="insertConflictUrl" value="<?php echo base_url('order/insertConflict'); ?>">
                <input type="text" hidden="hidden" id="solveConflictUrl" value="<?php echo base_url('order/solveConflict'); ?>">
            </div>
        </div>
    </div>
</div>