<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('product'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="ProductName" class="form-control" value="<?php echo set_value('ProductName'); ?>" placeholder="Sản phẩm">
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listBrands, 'BrandId', 'BrandName', 'BrandId', set_value('BrandId'), true, '--Nhãn hiệu--'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', set_value('ItemStatusId'), true, '--Trạng thái--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <?php if($user['RoleId'] == 1){ ?>
                                <div class="col-sm-4">
                                    <select class="form-control select2" name="UserId">
                                        <option value="0">Học viên</option>
                                        <?php foreach($listUsers as $u){ ?>
                                            <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                </div>
                            <?php } else{ ?>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                </div>
                            <?php } ?>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <?php echo form_open('product/update', array('id' => 'productForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<th>Họ và tên</th><th>Số điện thoại</th>'; ?>
                                <th>Sản phẩm</th>
                                <th>Nhãn hiệu</th>
                                <th>Giá</th>
                                <th>Link nhúng thanh toán</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center" style="width: 125px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct">
                            <?php $itemStatus = $this->Mconstants->itemStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listProducts as $p){ ?>
                                <tr id="product_<?php echo $p['ProductId']; ?>">
                                    <?php if($user['RoleId'] == 1){
                                        $pUser = false;
                                        foreach($listUsers as $u){
                                            if($u['UserId'] == $p['UserId']){
                                                $pUser = $u;
                                                break;
                                            }
                                        } ?>
                                        <td id="fullName_<?php echo $p['ProductId']; ?>"><a href="<?php echo base_url('userskill/view/'.$p['UserId']); ?>"><?php echo $pUser ? $pUser['FullName'] : ''; ?></a></td>
                                        <td id="phoneNumber_<?php echo $p['ProductId']; ?>"><?php echo $pUser ? $pUser['PhoneNumber'] : ''; ?></td>
                                    <?php } ?>
                                    <td id="productName_<?php echo $p['ProductId']; ?>"><?php echo $p['ProductName']; ?></td>
                                    <td id="brandName_<?php echo $p['ProductId']; ?>"><?php echo $this->Mconstants->getObjectValue($listBrands, 'BrandId', $p['BrandId'], 'BrandName'); ?></td>
                                    <td id="price_<?php echo $p['ProductId']; ?>"><?php echo priceFormat($p['Price']) ; ?></td>
                                    <td id="shortLink_<?php echo $p['ProductId']; ?>"><?php echo base_url('thanhtoan/'.($p['ProductId'] + 10000)); ?></td>
                                    <td id="statusName_<?php echo $p['ProductId']; ?>" class="text-center"><label class="<?php echo $labelCss[$p['ItemStatusId']]; ?>"><?php echo $itemStatus[$p['ItemStatusId']]; ?></label></td>
                                    <td class="actions text-center">
                                        <?php if($p['ItemStatusId'] == 1 || $user['RoleId'] == 1){ ?>
                                            <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['ProductId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                            <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <?php } elseif($user['RoleId'] == 2){
                                            if($p['ItemStatusId'] == 2){ ?>
                                                <a href="javascript:void(0)" class="link_status" data-id="<?php echo $p['ProductId']; ?>" data-status="3">Ngừng kinh doanh</i></a>
                                            <?php }
                                            elseif($p['ItemStatusId'] == 3){ ?>
                                                <a href="javascript:void(0)" class="link_status" data-id="<?php echo $p['ProductId']; ?>" data-status="2">Kinh doanh</i></a>
                                            <?php }
                                        } ?>
                                        <input type="text" hidden="hidden" id="itemStatusId_<?php echo $p['ProductId']; ?>" value="<?php echo $p['ItemStatusId']; ?>">
                                        <input type="text" hidden="hidden" id="userId_<?php echo $p['ProductId']; ?>" value="<?php echo $p['UserId']; ?>">
                                        <input type="text" hidden="hidden" id="brandId_<?php echo $p['ProductId']; ?>" value="<?php echo $p['BrandId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<td id="fullName"></td><td id="phoneNumber"></td>'; ?>
                                <td><input type="text" class="form-control hmdrequired" id="productName" name="ProductName" value="" data-field="Sản phẩm"></td>
                                <td>
                                    <select class="form-control" name="BrandId" id="productBrandId">
                                        <option value="0">--Chọn--</option>
                                        <?php foreach($listBrands as $b){ ?>
                                            <option value="<?php echo $b['BrandId']; ?>" data-id="<?php echo $b['UserId']; ?>"<?php if($user['RoleId'] == 1) echo ' style="display: none;"'; ?>><?php echo $b['BrandName']; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td><input type="text" class="form-control hmdrequired" id="price" name="Price" value="0" data-field="Giá sản phẩm"></td>
                                <td id="shortLink"></td>
                                <td><?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', 1); ?></td>
                                <td class="actions text-center">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductId" id="productId" value="0" hidden="hidden">
                                    <input type="text" name="UserId" id="userId" value="0" hidden="hidden">
                                    <input type="text" id="changeStatusUrl" value="<?php echo base_url('product/changeStatus'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>