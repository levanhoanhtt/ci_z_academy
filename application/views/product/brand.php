<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('brand'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="BrandName" class="form-control" value="<?php echo set_value('BrandName'); ?>" placeholder="Nhãn hiệu">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', set_value('ItemStatusId'), true, '--Trạng thái--'); ?>
                            </div>
                            <?php if($user['RoleId'] == 1){ ?>
                            <div class="col-sm-3">
                                <select class="form-control select2" name="UserId">
                                    <option value="0">Học viên</option>
                                    <?php foreach($listUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <?php echo form_open('brand/update', array('id' => 'brandForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<th>Họ và tên</th><th>Số điện thoại</th>'; ?>
                                <th>Nhãn hiệu</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center" style="width: 125px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyBrand">
                            <?php $itemStatus = $this->Mconstants->itemStatus;
                            $labelCss = $this->Mconstants->labelCss; 
                            foreach($listBrands as $b){ ?>
                                <tr id="brand_<?php echo $b['BrandId']; ?>">
                                    <?php if($user['RoleId'] == 1){
                                        $bUser = false;
                                        foreach($listUsers as $u){
                                            if($u['UserId'] == $b['UserId']){
                                                $bUser = $u;
                                                break;
                                            }
                                        } ?>
                                        <td id="fullName_<?php echo $b['BrandId']; ?>"><a href="<?php echo base_url('userskill/view/'.$b['UserId']); ?>"><?php echo $bUser ? $bUser['FullName'] : ''; ?></a></td>
                                        <td id="phoneNumber_<?php echo $b['BrandId']; ?>"><?php echo $bUser ? $bUser['PhoneNumber'] : ''; ?></td>
                                    <?php } ?>
                                    <td id="brandName_<?php echo $b['BrandId']; ?>"><?php echo $b['BrandName']; ?></td>
                                    <td id="statusName_<?php echo $b['BrandId']; ?>" class="text-center"><label class="<?php echo $labelCss[$b['ItemStatusId']]; ?>"><?php echo $itemStatus[$b['ItemStatusId']]; ?></label></td>
                                    <td class="actions text-center">
                                        <?php if($b['ItemStatusId'] == 1 || $user['RoleId'] == 1){ ?>
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $b['BrandId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $b['BrandId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <?php } elseif($user['RoleId'] == 2){
                                            if($b['ItemStatusId'] == 2){ ?>
                                                <a href="javascript:void(0)" class="link_status" data-id="<?php echo $b['BrandId']; ?>" data-status="3">Ngừng kinh doanh</i></a>
                                            <?php }
                                            elseif($b['ItemStatusId'] == 3){ ?>
                                                <a href="javascript:void(0)" class="link_status" data-id="<?php echo $b['BrandId']; ?>" data-status="2">Kinh doanh</i></a>
                                            <?php }
                                        } ?>
                                        <input type="text" hidden="hidden" id="itemStatusId_<?php echo $b['BrandId']; ?>" value="<?php echo $b['ItemStatusId']; ?>">
                                        <input type="text" hidden="hidden" id="userId_<?php echo $b['BrandId']; ?>" value="<?php echo $b['UserId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<td id="fullName"></td><td id="phoneNumber"></td>'; ?>
                                <td><input type="text" class="form-control hmdrequired" id="brandName" name="BrandName" value="" data-field="Nhãn hiệu"></td>
                                <td><?php $this->Mconstants->selectConstants('itemStatus', 'ItemStatusId', 1); ?></td>
                                <td class="actions text-center">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="BrandId" id="brandId" value="0" hidden="hidden">
                                    <input type="text" name="UserId" id="userId" value="0" hidden="hidden">
                                    <input type="text" id="changeStatusUrl" value="<?php echo base_url('brand/changeStatus'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>