<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <?php if($user['UserId'] == 1){ ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('promotion'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control select2" name="UserId">
                                    <option value="0">Học viên</option>
                                    <?php foreach($listUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <?php } else{ ?>
                    <div style="display: none;">
                        <?php echo form_open('promotion'); ?>
                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        <?php echo form_close(); ?>
                    </div>
                <?php } ?>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php echo form_open('promotion/update', array('id' => 'promotionForm')); ?>
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<th>Họ và tên</th><th>Số điện thoại</th>'; ?>
                                <th>Tên sản phẩm</th>
                                <th>Nhãn hiệu</th>
                                <th>Giảm giá</th>
                                <th>Số lượng</th>
                                <th>Hiệu lực từ ngày</th>
                                <th>Ngày hết hiệu lực</th>
                                <th style="width: 80px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPromotion">
                            <?php foreach($listPromotions as $p){ ?>
                                <tr id="promotion_<?php echo $p['PromotionId']; ?>">
                                    <?php if($user['RoleId'] == 1){
                                        $bUser = false;
                                        foreach($listUsers as $u){
                                            if($u['UserId'] == $p['UserId']){
                                                $bUser = $u;
                                                break;
                                            }
                                        } ?>
                                        <td id="fullName_<?php echo $p['PromotionId']; ?>"><a href="<?php echo base_url('userskill/view/'.$p['UserId']); ?>"><?php echo $bUser ? $bUser['FullName'] : ''; ?></a></td>
                                        <td id="phoneNumber_<?php echo $p['PromotionId']; ?>"><?php echo $bUser ? $bUser['PhoneNumber'] : ''; ?></td>
                                    <?php } ?>
                                    <td id="productName_<?php echo $p['PromotionId']; ?>"><?php echo $p['ProductName']; ?></td>
                                    <td id="brandName_<?php echo $p['PromotionId']; ?>"><?php echo $p['BrandName']; ?></td>
                                    <td id="discountCost_<?php echo $p['PromotionId']; ?>"><?php echo priceFormat($p['DiscountCost']) ; ?></td>
                                    <td id="quantity_<?php echo $p['PromotionId']; ?>"><?php echo priceFormat($p['Quantity']) ; ?></td>
                                    <td id="beginDate_<?php echo $p['PromotionId']; ?>"><?php echo ddMMyyyy($p['BeginDate']) ; ?></td>
                                    <td id="endDate_<?php echo $p['PromotionId']; ?>"><?php echo ddMMyyyy($p['EndDate']); ?></td>
                                    <td class="actions text-center">
                                        <a href="<?php echo base_url('promotion/download/'.$p['PromotionId']); ?>" target="_blank" title="Tải về"><i class="fa fa-download"></i></a>
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $p['PromotionId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['PromotionId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="userId_<?php echo $p['PromotionId']; ?>" value="<?php echo $p['UserId']; ?>">
                                        <input type="hidden" id="productId_<?php echo $p['PromotionId']; ?>" value="<?php echo $p['ProductId']; ?>">
                                        <input type="hidden" id="brandId_<?php echo $p['PromotionId']; ?>" value="<?php echo $p['BrandId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php if($user['RoleId'] == 1) echo '<td id="fullName"></td><td id="phoneNumber"></td>'; ?>
                                <td id="productName">
                                    <?php if($user['RoleId'] == 2){ ?>
                                        <select class="form-control" name="ProductId" id="productId">
                                            <option value="0" data-id="0">--Chọn sản phẩm--</option>
                                            <?php foreach($listProducts as $p){ ?>
                                                <option value="<?php echo $p['ProductId']; ?>" data-id="<?php echo $p['BrandId']; ?>"><?php echo $p['ProductName']; ?></option>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                </td>
                                <td id="brandName"><?php if($user['UserId'] == 2) echo $this->Mconstants->selectObject($listBrands, 'BrandId', 'BrandName', 'ProductBrandId', 0, true, "--Chọn nhãn hiệu--", '', ' disabled'); ?></td>
                                <td><input type="text" class="form-control hmdrequired cost" id="discountCost" name="DiscountCost" value="" data-field="Giảm giá"></td>
                                <td><input type="text" class="form-control hmdrequired cost" id="quantity" name="Quantity" value="" data-field="Số lượng"></td>
                                <td><input type="text" class="form-control hmdrequired datepicker" id="beginDate" name="BeginDate" value="" data-field="Hiệu lực từ ngày"></td>
                                <td><input type="text" class="form-control datepicker" id="endDate" name="EndDate" value=""></td>
                                <td class="actions text-center">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PromotionId" id="promotionId" value="0" hidden="hidden">
                                    <input type="text" name="UserId" id="userId" value="0" hidden="hidden">
                                    <input type="text" name="BrandId" id="brandId" value="0" hidden="hidden">
                                    <?php if($user['RoleId'] == 1) echo '<input type="text" name="ProductId" id="productId" value="0" hidden="hidden">'; ?>
                                    <input type="text" id="deletePromotionUrl" value="<?php echo base_url('promotion/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="deletePromotionUrl" value="<?php echo base_url('promotion/delete'); ?>">
                    <input type="text" hidden="hidden" id="downloadPromotionUrl" value="<?php echo base_url('promotion/download'); ?>">
                    <?php if($user['UserId'] == 2){
                        foreach($listProducts as $p){ ?>
                            <input type="text" hidden="hidden" id="price_<?php echo $p['ProductId']; ?>" value="<?php echo $p['Price']; ?>">
                        <?php }
                    } ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>