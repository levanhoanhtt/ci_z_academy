<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="row mgt-25" style="margin-bottom: 20px;">
                <div class="col-md-6">
                    <div class="medium-box-n boxst-1 success-gradient">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">Zticket<!-- | VNĐ <?php //echo priceFormat($user['TicketCount']*$ticketCost, true) ?> Đ--></h2>
                            <h2><?php echo priceFormat($user['TicketCount'], true); ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="medium-box-n boxst-1 btn-primary">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">VNĐ<!-- | <?php //echo priceFormat(round($user['Balance']/$ticketCost, 3), true); ?>--></h2>
                            <h2><?php echo priceFormat($user['Balance'], true); ?> Đ</h2>
                        </div>
                        <button type="button" class="btn-buy buy-ticket" id="btnDraw">Rút tiền</button>
                        <input type="text" hidden="hidden" id="buyAble" value="<?php echo $buyAble ? 1 : 0; ?>">
                        <input type="text" hidden="hidden" id="totalPercent" value="<?php echo $affPercent + 1; ?>">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="row mgt-25">
            	<?php echo form_open('withdrawal/insert', array('id' => 'withdrawalForm')); ?>
                <div class="col-md-12">
                    <div class="bg-box-gradient pd-15-25" id="divSell">
                        <h3 class="title-form">Thông tin tài khoản ngân hàng</h3>
                        <div class="form-group mgt-10 divTagInput">
                            <input type="text" id="bankName" class="form-control hmdrequired" value="" placeholder="Ngân hàng" data-field="Ngân hàng">
                        </div>
                        <div class="form-group mgt-10">
                            <input type="text" id="bankHolder" class="form-control hmdrequired"  value="" placeholder="Chủ tài khoản" data-field="Chủ tài khoản">
                        </div>
                        <div class="form-group mgt-10">
                            <input type="text" id="bankNumber" class="form-control hmdrequired" value="" placeholder="Số tài khoản" data-field="Số tài khoản">
                        </div>

                        <div class="form-group mgt-10">
                            <input type="text" id="branchName" class="form-control hmdrequired" value="" placeholder="Chi nhánh" data-field="Chi nhánh">
                        </div>
                    </div>
                </div>
                <div id="drawModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content bg-box-gradient">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" style="color: #fff !important;">&times;</button>
                                <h4 class="modal-title" style="color: #fff !important;">Rút tiền</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" style="color: #fff !important;">Số tiền rút</label>
                                            <input min="0" type="text" id="withdrawalCost" class="form-control" value="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label" style="color: #fff !important;">Ước tính thưc hiện</label>
                                            <input type="text" id="receiveCost" readonly class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer text-center">
                                <button type="button" class="btn btn-primary" id="btnWithdrawal">Rút</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        	<div class="box-body">
        		<h3 class="box-title text-grey" style="margin: 15px 0;">Lưu ý:</h3>
        		<p class="text-grey">1- Chủ tài khoản ngân hàng phải trùng tên với chủ tài khoản Z.</p>
        		<p class="text-grey">2- Số tiền rút tối thiểu là 10.000.000 đ</p>
        		<p class="text-grey">3- Phí Affiliate: <?php echo $affPercent; ?>% (do số tiền được nạp vào hệ thống học viên đã phải trả <?php echo $affPercent; ?>% phí tiếp thị cho người giới thiệu khởi tạo tài khoản nên khi bạn rút tiền ra khỏi hệ thống số tiền được rút sẽ bị trừ <?php echo $affPercent; ?>% phí Affiliate đó).</p>
        		<p class="text-grey">4- Phí dịch vụ ngân hàng: học viện Z trả</p>
        		<p class="text-grey">5- Phí dịch vụ Z: 1%</p>
        		<p class="text-grey">6- Tiền sẽ được trả về tài khoản trong tối đa 24h (không kể thứ 7 và Chủ Nhật).</p>
        	</div>
            <div class="box box-default mgt-25">
                <?php sectionTitleHtml('Lịch sử rút tiền'); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Thời gian tạo lệnh</th>
                            <th>Số tiền rút</th>
                            <th>Thực nhận</th>
                            <th>Ngân hàng</th>
                            <th>Số tài khản</th>
                            <th>Chi nhánh</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $labelCss = $this->Mwithdrawals->labelCss;
                        $withdrawalStatus = $this->Mconstants->withdrawalStatus;
                        foreach($listWithdrawals as $w){ ?>
                            <tr>
                                <td><?php echo ddMMyyyy($w['CrDateTime'], 'H:i:s d/m/Y'); ?></td>
                                <td><?php echo priceFormat($w['WithdrawalCost']); ?> Đ</td>
                                <td><?php echo priceFormat($w['ReceiveCost']); ?> Đ</td>
                                <td><?php echo $w['BankName']; ?></td>
                                <td><?php echo $w['BankNumber']; ?></td>
                                <td><?php echo $w['BranchName']; ?></td>
                                <td class="tdStatus"><span class="<?php echo $labelCss[$w['WithdrawalStatusId']]; ?>"><?php echo $withdrawalStatus[$w['WithdrawalStatusId']]; ?></span></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>