<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('withdrawal'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên, SĐT hoặc Email">
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('withdrawalStatus', 'WithdrawalStatusId', set_value('WithdrawalStatusId'), true, '--Trạng thái--'); ?>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="row" style="margin-bottom: 0;">
                            <div class="col-sm-4">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Tổng rút thành công</h4>
                                        <h4><span class="spanCost" id="spanWithdrawalCost"><?php echo priceFormat($totalWithdrawalCost); ?></span> đ</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="small-box primary-gradient box-statistic-money">
                                    <div class="inner text-center db-font">
                                        <h4>Tổng rút thực nhận</h4>
                                        <h4><span class="spanCost" id="spanReceiveCost"><?php echo priceFormat($totalReceiveCost); ?></span> đ</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Thời gian tạo lệnh</th>
                                <th>Họ và tên</th>
                                <th>Số tiền rút</th>
                                <th>Thực nhận</th>
                                <th>Ngân hàng</th>
                                <th>Số tài khản</th>
                                <th>Chi nhánh</th>
                                <th>Trạng thái</th>
                                <th style="width: 50px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyWithdrawal">
                            <?php $labelCss = $this->Mwithdrawals->labelCss;
                            $withdrawalStatus = $this->Mconstants->withdrawalStatus;
                            foreach($listWithdrawals as $w){ ?>
                                <tr id="withdrawal_<?php echo $w['WithdrawalId']; ?>">
                                    <td><?php echo ddMMyyyy($w['CrDateTime'], 'H:i:s d/m/Y'); ?></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$w['StudentId']); ?>"><?php echo $w['FullName']; ?></a></td>
                                    <td><span class="spanWithdrawalCost"><?php echo priceFormat($w['WithdrawalCost']); ?></span> Đ</td>
                                    <td><span class="spanReceiveCost"><?php echo priceFormat($w['ReceiveCost']); ?></span> Đ</td>
                                    <td><?php echo $w['BankName']; ?></td>
                                    <td><?php echo $w['BankNumber']; ?></td>
                                    <td><?php echo $w['BranchName']; ?></td>
                                    <td class="tdStatus"><span class="<?php echo $labelCss[$w['WithdrawalStatusId']]; ?>"><?php echo $withdrawalStatus[$w['WithdrawalStatusId']]; ?></span></td>
                                    <td class="actions text-center">
                                    	<?php if($w['WithdrawalStatusId'] == 1 || $w['WithdrawalStatusId'] == 2){ ?>
                                            <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $w['WithdrawalId']; ?>"><i class="fa fa-pencil"></i></a>
                                            <input type="text" hidden="hidden" id="withdrawalStatusId_<?php echo $w['WithdrawalId']; ?>" value="<?php echo $w['WithdrawalStatusId']; ?>">
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
                <div class="modal fade" id="modalWithdrawalStatus" tabindex="-1" role="dialog" aria-labelledby="modalWithdrawalStatus">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Cập nhật trạng thái</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <?php $this->Mconstants->selectConstants('withdrawalStatus', 'WithdrawalStatusIdUpdate', 0, true, '--Chọn trạng thái--'); ?>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button type="button" class="btn btn-primary" id="btnUpdateStatus">Cập nhật</button>
                                <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('withdrawal/changeStatus'); ?>">
                                <input type="text" hidden="hidden" id="withdrawalId" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
