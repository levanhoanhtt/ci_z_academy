<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="box box-default">
                <?php sectionTitleHtml('Tìm kiếm'); ?>
                <div class="box-body row-margin">
                    <?php echo form_open('register'); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="text" name="CourseId" class="form-control" value="<?php echo set_value('CourseId'); ?>" placeholder="Khóa">
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên hoặc SĐT">
                        </div>
						<div class="col-sm-4">
                            <select class="form-control select2" name="CourseTypeId" >
                                <option value="0">Học phần</option>
                                <option value="1"<?php if(set_value('CourseTypeId') == 1) echo ' selected="selected"'; ?>>Alpha</option>
								<option value="2"<?php if(set_value('CourseTypeId') == 2) echo ' selected="selected"'; ?>>Beta</option>
								<option value="3"<?php if(set_value('CourseTypeId') == 3) echo ' selected="selected"'; ?>>Gamma</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <select class="form-control select2" name="StudentId">
                                <option value="0">Học viên</option>
                                <?php foreach($listUsers as $u){ ?>
                                    <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('StudentId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control select2" name="ByUserId">
                                <option value="0">Ghi danh bởi</option>
                                <?php foreach($listUsers as $u){ ?>
                                    <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('ByUserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="submit" name="export" class="btn btn-default" value="Xuất file">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box box-default">
                <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mốc thời gian</th>
                            <th>Khóa</th>
                            <th>Họ và tên</th>
                            <th>Ngày sinh</th>
                            <th>Số CMT</th>
                            <th>Số điện thoại</th>
                            <th>Bởi</th>
                            <th width="30px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;
                        $fullNames = array();
                        foreach($listRegisters as $u) {
                            $i++;
                            if($u['ByUserId'] > 0 && !isset($fullNames[$u['ByUserId']])) $fullNames[$u['ByUserId']] = $this->Musers->getFieldValue(array('UserId' => $u['ByUserId']), 'FullName'); ?>
                            <tr id="register_<?php echo $u['RegisterId']; ?>">
                                <td><?php echo ddMMyyyy($u['CrDateTime']); ?></td>
                                <td><?php echo $u['CourseId'];?>
								<?php if($u['CourseTypeId'] == 1) echo " (Alpha)" ;
									  if($u['CourseTypeId'] == 2) echo " (Beta)" ;
									  if($u['CourseTypeId'] == 3) echo " (Gamma)";
								?>
								</td>
                                <td>
                                    <?php if($u['StudentId'] > 0){ ?>
                                        <a href="<?php echo base_url('userskill/view/'.$u['StudentId']); ?>"><?php echo $u['StudentName']; ?></a>
                                    <?php } else echo $u['StudentName']; ?>
                                </td>
                                <td><?php echo ddMMyyyy($u['BirthDay']); ?></td>
                                <td><?php echo $u['IDCardNumber']; ?></td>
                                <td><?php echo $u['PhoneNumber']; ?></td>
                                <td>
                                    <?php if($u['ByUserId'] > 0){ ?>
                                        <a href="<?php echo base_url('userskill/view/'.$u['ByUserId']); ?>"><?php echo $fullNames[$u['ByUserId']]; ?></a>
                                    <?php } ?>
                                </td>
                                <td><span class="glyphicon glyphicon-pencil editRegister" data-id="<?php echo $u['RegisterId']; ?>" data-course="<?php echo $u['CourseId']; ?>" data-name="<?php echo $u['StudentName']; ?>"></span></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
            </div>
        </section>
        <div id="changeRegisterModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thành viên: <span id="nameStudentMd"></span></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-9 text-right" style="color:#444">
                                <p style="line-height: 30px;font-weight: 600;">Thay đổi khóa hiện tại sang:</p>
                            </div>
                            <div class="col-md-3"><input type="text" class="form-control" name="courseIdMd" id="courseIdMd" style="width:80px"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-9 text-right" style="color:#444">
                                <p style="line-height: 30px;font-weight: 600;">Xóa thành viên khỏi lớp học và hoàn lại Zticket<!-- <br>(Tích vào ô và ấn Save)--></p>
                            </div>
                            <div class="col-md-3" style="padding-top: 5px;">
                                <input type="checkbox" class="iCheck" name="deleteRegisterMd" id="deleteRegisterMd">
                                <label class="control-label"></label>
                            </div>
                        </div>
                        <input type="hidden" name="RegisterIdMd" id="RegisterIdMd">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnUpdateRegiter">Cập nhật</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        <input type="text" hidden="hidden" id="updateRegisterUrl" value="<?php echo base_url('register/update'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>