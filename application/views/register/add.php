<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="row mgt-25">
                <div class="col-sm-6">
                    <div class="medium-box-n boxst-1 success-gradient">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">Zticket<!-- | VNĐ <?php //echo priceFormat($user['TicketCount']*$ticketCost, true) ?> Đ--></h2>
                            <h2><?php echo priceFormat($user['TicketCount'], true); ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="medium-box-n boxst-1 success-gradient">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">Khóa <?php echo $configs['COURSE_ID']; ?></h2>
                            <h2 style="font-size: 24px;">Khai giảng: <?php echo $configs['COURSE_DATE']; ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row mgt-25">
                <div class="col-sm-12">
                    <div class="bg-box-gradient pd-15-25">
                        <?php echo form_open('register/update', array('id' => 'registerForm')); ?>
                        <div class="pull-left gender" id="divYear">
                            <div class="radio dp-il">
                                <h3 class="title-form">GHI DANH</h3>
                            </div>
                        </div>
                        <div class="pull-left gender" id="divYear">
                            <div class="radio dp-il">
                                <label class="fz-20">
                                    <input type="radio" name="CourseTypeId" id ="CourseType1" value="1" checked=""> Alpha
                                </label>
                            </div>
                            <div class="radio dp-il">
                                <label class="fz-20">
                                    <input type="radio" name="CourseTypeId" id ="CourseType2" value="2"> Beta
                                </label>
                            </div>
							<div class="radio dp-il">
                                <label class="fz-20">
                                    <input type="radio" name="CourseTypeId" id ="CourseType3" value="3"> Gamma
                                </label>
                            </div>
                        </div>
                        <div class="pull-right gender" id="divYear">
                            <div class="radio dp-il">
                                <label class="fz-20">
                                    <input type="radio" name="AgeTypeId" id="ageType1" value="1" checked="">
                                    Dưới 26 tuổi
                                </label>
                            </div>
                            <div class="radio dp-il">
                                <label class="fz-20">
                                    <input type="radio" name="AgeTypeId" id="ageType2" value="2">
                                    Trên 26 tuổi
                                    </label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-center">
                            <button type="button" class="btn-n gradient-success-top pd-10-30 bd-rd-10 fz-18 dp-il">
                                Cần thanh toán <span class="ticketPay"><?php echo $this->Mconfigs->getConfigValue('ALPHA_VALUE', 100)/100; ?></span> Zticket
                            </button>
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Số điện thoại</label>
                            <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="" data-field="Số điện thoại">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Họ và tên</label>
                            <input type="text" name="StudentName" id="studentName" class="form-control hmdrequired" value="" data-field="Họ và tên">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Ngày sinh</label>
                            <input type="text" name="BirthDay" id="birthDay" class="form-control hmdrequired datepicker" value="" data-field="Ngày sinh" style="padding: 6px 12px;">
                        </div>

                        <div class="form-group mgt-10">
                            <label class="control-label">4 số cuối chứng minh thư</label>
                            <input type="text" name="IDCardNumber" id="iDCardNumber" class="form-control hmdrequired" value="" data-field="4 số cuối chứng minh thư">
                        </div>
                        <div class="text-center">
                            <button class="btn-n pd-10-30 fz-20 gradient-success-top regiter-submit">GHI DANH</button>
                        </div>
                        <?php echo form_close(); ?>
                        <input type="text" hidden="hidden" id="getTicketCountFactor26Url" value="<?php echo base_url('register/getTicketCountFactor26'); ?>">
                        <input type="text" hidden="hidden" id="getUserByPhoneUrl" value="<?php echo base_url('api/user/searchByPhone'); ?>">
                        <input type="text" hidden="hidden" id="yourPhone" value="<?php echo $user['PhoneNumber'] ?>">
                        <input type="text" hidden="hidden" id="yourName" value="<?php echo $user['FullName'] ?>">
                        <input type="text" hidden="hidden" id="yourIDCardNumber" value="<?php echo $user['IDCardNumber'] ?>">
                        <input type="text" hidden="hidden" id="yourYearOld" value="<?php echo date("Y") - date_format(date_create($user['BirthDay']), 'Y') ?>">
                        <input type="text" hidden="hidden" id="yourBirthDay" value="<?php echo ddMMyyyy($user['BirthDay']); ?>">
						<input type="text" hidden="hidden" id="getCourse" value="<?php echo base_url('register/getCourse'); ?>">
                    </div>
                </div>
            </div>
            <div class="box box-default mgt-25">
                <?php sectionTitleHtml('Lịch sử ghi danh', isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mốc thời gian</th>
                            <th>Khóa</th>
                            <th>Họ và tên</th>
                            <th>Ngày sinh</th>
                            <th>Số CMT</th>
                            <th>Số điện thoại</th>
                            <th>Bởi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;
                        $fullNames = array();
                        foreach($listRegisters as $u) {
                            $i++;
                            if($u['ByUserId'] > 0 && !isset($fullNames[$u['ByUserId']])) $fullNames[$u['ByUserId']] = $this->Musers->getFieldValue(array('UserId' => $u['ByUserId']), 'FullName'); ?>
                            <tr id="register_<?php echo $u['RegisterId']; ?>">
                                <td><?php echo ddMMyyyy($u['CrDateTime']); ?></td>
                                <td><?php echo $u['CourseId']." (".$this->Mconstants->courseTypes[$u['CourseTypeId']].")"; ?></td>
                                <td>
                                    <?php if($u['StudentId'] > 0){ ?>
                                        <a href="<?php echo base_url('userskill/view/'.$u['StudentId']); ?>"><?php echo $u['StudentName']; ?></a>
                                    <?php } else echo $u['StudentName']; ?>
                                </td>
                                <td><?php echo ddMMyyyy($u['BirthDay']); ?></td>
                                <td><?php echo $u['IDCardNumber']; ?></td>
                                <td><?php echo $u['PhoneNumber']; ?></td>
                                <td>
                                    <?php if($u['ByUserId'] > 0){ ?>
                                    <a href="<?php echo base_url('userskill/view/'.$u['ByUserId']); ?>"><?php echo $fullNames[$u['ByUserId']]; ?></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
            </div>
        </section>
        <div class="modal fade fade-scale congrat-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <canvas height='1' id='confetti' width='1' style="display: none;"></canvas>
                    <canvas id="drawing_canvas"></canvas>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="anim_container">
                            <canvas id="myCanvas" data-completed="100" data-width="200" data-height="200" height="170" style="display: none"></canvas>
                            <div class="toto"></div>
                            <div id="count" class="round"><div class="num">0</div></div>
                            <div class="tata"><p><img src="assets/vendor/dist/img/your-halong-trustworthy-icon.png" width="80px"></p></div>
                        </div>
                        <h1 class="modal-title" style="color: #000;">Ghi danh thành công !</h1>
                        <h3 class="modal-subtitle sub1">Chúc mừng bạn được tặng 0.1 Zticket</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>