<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
        </section>
		<section class="content">
            <div class="bs-example" data-example-id="thumbnails-with-custom-content">
                <div class="row">
                    <?php $labelCss = $this->Mconstants->labelCss;
                    $serviceTypes = $this->Mconstants->serviceTypes;
                    foreach($listServices as $s){ ?>
                        <div class="col-sm-6 col-md-4">
                            <div class="thumbnail">
                                <div class="img_thumb_holder float-l"
                                     style="
                                        width:100%;
                                        height:200px;
                                        background-size: 300px;
                                        background-position: center center;
                                        background-image: url('<?php echo IMAGE_PATH.$s['ServiceImage']; ?>')">
                                </div>
                                <div class="caption">
                                    <h4><?php echo $s['ServiceName']; ?></h4>
                                    <button class="btn btn-primary btnRegisterService" type="button" data-id="<?php echo $s['ServiceId']; ?>">Đăng ký</button>
                                    <p class="pull-right" style="font-size: 20px;margin-top: -3px;"><?php echo priceFormat($s['ServiceCost']); ?> VNĐ</p>
                                    <div class="clearfix"></div>
                                    <p class="pull-right"><span class="<?php echo $labelCss[$s['ServiceTypeId']]; ?>"><?php echo $serviceTypes[$s['ServiceTypeId']]; ?></span></p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <input type="text" hidden="hidden" id="getServiceDetailUrl" value="<?php echo base_url('service/get'); ?>">
            <input type="text" hidden="hidden" id="registerServiceUrl" value="<?php echo base_url('service/register'); ?>">
            <input type="text" hidden="hidden" id="getTimeDiffUrl" value="<?php echo base_url('service/timeDiff'); ?>">
            <input type="text" hidden="hidden" id="serviceId" value="0">
            <input type="text" hidden="hidden" id="serviceTypeId" value="0">
            <input type="text" hidden="hidden" id="isRequireCarNumber" value="0">
            <div class="modal fade" id="modalRegisterService" tabindex="-1" role="dialog" aria-labelledby="modalRegisterService">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Đăng ký dịch Vụ</h4>
                        </div>
                        <div class="modal-body" style="color: #000;">
                            <div class="alert alert-success">
                                <p>Tên dịch vụ: <span id="spanServiceName"></span></p>
                                <p>Mô tả: <span id="spanServiceDesc"></span></p>
                                <p>Phí dịch vụ: <span id="spanServiceCost">0</span> VNĐ</p>
                                <p>Loại dịch vụ: <span id="spanServiceTypeName"></span></p>
                            </div>
                            <div class="alert alert-warning">
                                <p>Chú thích về loại dịch vụ</p>
                                <p><b>1. Theo ngày (không gia hạn)</b>: Phải chọn ngày kết thúc, tài khoản bị trừ tiền luôn</p>
                                <p><b>2. Theo tháng (gia hạn vào ngày đăng ký)</b>:<br/>
                                    - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                    - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày đăng ký ở hàng tháng
                                </p>
                                <p><b>3. Theo tháng (gia hạn vào đầu tháng)</b>:<br/>
                                    - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                    - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày mồng 1 hàng tháng
                                </p>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Ngày bắt đầu</label>
                                        <input type="text" class="form-control datepicker" id="beginDate" value="<?php echo date('d/m/Y'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Ngày kết thúc</label>
                                        <input type="text" class="form-control datepicker" id="endDate" value="" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Số tiền</label>
                                        <input type="text" class="form-control" id="paidCost" value="0" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4" id="divCarNumber" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">Biển số xe</label>
                                        <input type="text" class="form-control" id="carNumber" value="">
                                    </div>
                                </div>
                                <div class="col-sm-8" id="divServiceComment">
                                    <div class="form-group">
                                        <label class="control-label">Ghi chú</label>
                                        <input type="text" class="form-control" id="comment" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <button type="button" class="btn btn-primary" id="btnRegisterService">Đăng ký</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	</div>
</div>
<?php $this->load->view('includes/footer'); ?>

