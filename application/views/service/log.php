<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $roleId = $user['RoleId']; ?>
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('service/log'); ?>
                        <div class="row">
                            <?php if($roleId == 1){ ?>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="UserId">
                                    <option value="0">Học viên</option>
                                    <?php foreach($listUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <?php $listServiceActiive = array();
                                foreach($listServices as $s){
                                    if($s['StatusId'] == STATUS_ACTIVED) $listServiceActiive[] = $s;
                                }
                                $this->Mconstants->selectObject($listServiceActiive, 'ServiceId', 'ServiceName', 'ServiceId', set_value('ServiceId'), true, 'Dịch vụ', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('serviceStatus', 'ServiceStatusId', set_value('ServiceStatusId'), true, 'Trạng thái'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="TìmF kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Học viên</th>
                                <th>Số điện thoại</th>
                                <th>Dịch vụ</th>
                                <th>Từ ngày</th>
                                <th>Đến ngày</th>
                                <th>Trạng thái</th>
                                <th>Ghi chú</th>
                                <th style="width: 50px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUserService">
                            <?php $i = 0;
                            $labelCss = $this->Mservices->labelCss;
                            $serviceStatus = $this->Mconstants->serviceStatus;
                            $serviceTypes = $this->Mconstants->serviceTypes;
                            //$lis
                            foreach($listUserServices as $us){
                                $i++; ?>
                                <tr id="userService_<?php echo $us['UserServiceId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$us['UserId']); ?>"><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $us['UserId'], 'FullName'); ?></a></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $us['UserId'], 'PhoneNumber'); ?></td>
                                    <td>
                                        <span class="serviceName"><?php echo $this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceName'); ?></span>
                                        <span class="serviceDesc" style="display: none;"><?php echo $this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceDesc'); ?></span>
                                        <span class="serviceCost" style="display: none;"><?php echo priceFormat($this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceCost')); ?></span>
                                        <span class="serviceTypeName" style="display: none;"><?php echo $serviceTypes[$this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceTypeId')] ?></span>
                                        <span class="paidCost" style="display: none;"><?php echo priceFormat($us['PaidCost']); ?></span>
                                    </td>
                                    <td><?php echo ddMMyyyy($us['BeginDate']); ?></td>
                                    <td class="tdEndDate"><?php echo ddMMyyyy($us['EndDate']); ?></td>
                                    <td class="tdStatusName"><span class="<?php echo $labelCss[$us['ServiceStatusId']]; ?>"><?php echo $serviceStatus[$us['ServiceStatusId']]; ?></span></td>
                                    <td>
                                        <?php if(!empty($us['CarNumber'])){
                                            echo '<span class="spanCarNumber">'.$us['CarNumber'].'</span>';
                                            if(!empty($us['Comment'])) echo '. <span class="spanComment">'.$us['Comment'].'</span>';
                                        }
                                        else echo '<span class="spanComment">'.$us['Comment'].'</span>'; ?>
                                    </td>
                                    <td class="actions">
                                        <?php if($roleId == 1){ ?>
                                            <a class="link_edit" href="javascript:void(0)"
                                               data-id="<?php echo $us['UserServiceId']; ?>"
                                               data-status="<?php echo $us['ServiceStatusId'] ?>"
                                               data-type="<?php echo $this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceTypeId'); ?>"
                                               data-car="<?php echo $this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'IsRequireCarNumber'); ?>"
                                               title="Cập nhật"><i class="fa fa-edit"></i></a>
                                        <?php } ?>
                                        <?php if($us['ServiceStatusId'] == 1 || $us['ServiceStatusId'] == 2){ ?>
                                            <a class="link_delete" href="javascript:void(0)" title="Hủy dịch vụ" data-id="<?php echo $us['UserServiceId']; ?>"><i class="fa fa-times"></i></a>
                                        <?php } ?>
                                        <input type="text" hidden="hidden" id="userId_<?php echo $us['UserServiceId']; ?>" value="<?php echo $us['UserId']; ?>">
                                        <input type="text" hidden="hidden" id="serviceId_<?php echo $us['UserServiceId']; ?>" value="<?php echo $us['ServiceId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="roleId" value="<?php echo $roleId ?>">
                </div>
                <div class="modal fade" id="modalCancelService" tabindex="-1" role="dialog" aria-labelledby="modalCancelService" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content" style="width: 300px!important;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Hủy dịch vụ</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Lý do</label>
                                            <textarea class="form-control" id="comment1" name="Comment" rows="2"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button type="button" class="btn btn-primary" id="btnCancelService">Hủy dịch vụ</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <input type="text" hidden="hidden" id="cancelServiceUrl" value="<?php echo base_url('service/cancelService'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalRegisterService" tabindex="-1" role="dialog" aria-labelledby="modalRegisterService" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Chỉnh sửa dịch Vụ</h4>
                            </div>
                            <div class="modal-body" style="color: #000;">
                                <div class="alert alert-success">
                                    <p>Học viên: <span id="spanStudentName"></span></p>
                                    <p>Số điện thoại: <span id="spanStudentPhone"></span></p>
                                </div>
                                <div class="alert alert-success">
                                    <p>Tên dịch vụ: <span id="spanServiceName"></span></p>
                                    <p>Mô tả: <span id="spanServiceDesc"></span></p>
                                    <p>Phí dịch vụ: <span id="spanServiceCost">0</span> VNĐ</p>
                                    <p>Loại dịch vụ: <span id="spanServiceTypeName"></span></p>
                                </div>
                                <!--<div class="alert alert-warning">
                                    <p>Chú thích về loại dịch vụ</p>
                                    <p><b>1. Theo ngày (không gia hạn)</b>: Phải chọn ngày kết thúc, tài khoản bị trừ tiền luôn</p>
                                    <p><b>2. Theo tháng (gia hạn vào ngày đăng ký)</b>:<br/>
                                        - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                        - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày đăng ký ở hàng tháng
                                    </p>
                                    <p><b>3. Theo tháng (gia hạn vào đầu tháng)</b>:<br/>
                                        - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                        - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày mồng 1 hàng tháng
                                    </p>
                                </div>-->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ngày bắt đầu</label>
                                            <input type="text" class="form-control datepicker" id="beginDate" value="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ngày kết thúc</label>
                                            <input type="text" class="form-control datepicker" id="endDate" value="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Số tiền</label>
                                            <input type="text" class="form-control" id="paidCost" value="0" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Trạng thái</label>
                                            <?php $this->Mconstants->selectConstants('serviceStatus', 'ServiceStatusId2', 1); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" id="divCarNumber" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label">Biển số xe</label>
                                            <input type="text" class="form-control" id="carNumber" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú</label>
                                            <input type="text" class="form-control" id="comment2" value="">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú thay đổi</label>
                                            <input type="text" class="form-control" id="commentTmp" value="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button type="button" class="btn btn-primary" id="btnUpdateService">Cập nhật</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                <input type="text" hidden="hidden" id="updateRegisterUrl" value="<?php echo base_url('service/updateRegister'); ?>">
                                <input type="text" hidden="hidden" id="getTimeDiffUrl" value="<?php echo base_url('service/timeDiff'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>