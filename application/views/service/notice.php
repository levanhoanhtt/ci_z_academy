<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $roleId = $user['RoleId']; ?>
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('service/notice'); ?>
                        <div class="row">
                            <?php if($roleId == 1){ ?>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="UserId">
                                    <option value="0">Học viên</option>
                                    <?php foreach($listUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <?php $listServiceActiive = array();
                                foreach($listServices as $s){
                                    if($s['StatusId'] == STATUS_ACTIVED) $listServiceActiive[] = $s;
                                }
                                $this->Mconstants->selectObject($listServiceActiive, 'ServiceId', 'ServiceName', 'ServiceId', set_value('ServiceId'), true, 'Dịch vụ', ' select2'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày</th>
                                <th>Học viên</th>
                                <th>Số điện thoại</th>
                                <th>Dịch vụ</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            foreach($listServiceLogs as $us){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ddMMyyyy($us['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td><a href="<?php echo base_url('userskill/view/'.$us['UserId']); ?>"><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $us['UserId'], 'FullName'); ?></a></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listUsers, 'UserId', $us['UserId'], 'PhoneNumber'); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listServices, 'ServiceId', $us['ServiceId'], 'ServiceName'); ?></td>
                                    <td><?php echo $us['Comment']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>