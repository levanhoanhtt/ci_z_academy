<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('service'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ServiceName" class="form-control" value="<?php echo set_value('ServiceName'); ?>" placeholder="Tên dịch vụ">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('serviceTypes', 'ServiceTypeId', set_value('ServiceTypeId'), true, 'Loại dịch vụ'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                            <div class="col-sm-3">
                                <a href="javascript:void(0)" id="aAddService" class="btn btn-info">Thêm mới</a>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên dịch vụ</th>
                                <th>Mô tả</th>
                                <th>Phí dịch vụ</th>
                                <th class="text-center">Loại dịch vụ</th>
                                <th class="text-center">Trạng thái</th>
                                <th style="width: 76px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyService">
                            <?php $i = 0;
                            $labelCss = $this->Mconstants->labelCss;
                            $status = $this->Mconstants->status;
                            $serviceTypes = $this->Mconstants->serviceTypes;
                            foreach($listServices as $s){
                                $i++; ?>
                                <tr id="service_<?php echo $s['ServiceId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $s['ServiceName']; ?></td>
                                    <td><?php echo $s['ServiceDesc']; ?></td>
                                    <td><span><?php echo priceFormat($s['ServiceCost']); ?></span> VNĐ</td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$s['ServiceTypeId']]; ?>"><?php echo $serviceTypes[$s['ServiceTypeId']]; ?></span></td>
                                    <td class="text-center tdStatusName"><span class="<?php echo $labelCss[$s['StatusId']]; ?>"><?php echo $status[$s['StatusId']]; ?></span></td>
                                    <td class="text-center">
                                        <a class="link_edit" href="javascript:void(0)" data-id="<?php echo $s['ServiceId']; ?>" title="Cập nhật"><i class="fa fa-edit"></i></a>
                                        <?php if($s['StatusId'] == 2){ ?>
                                            <a class="link_register" href="javascript:void(0)" data-id="<?php echo $s['ServiceId'] ?>" data-type="<?php echo $s['ServiceTypeId']; ?>" data-car="<?php echo $s['IsRequireCarNumber']; ?>" title="Thêm dịch vụ cho học viên"><i class="fa fa-plus"></i></a>
                                        <?php } ?>
                                        <a class="link_delete" href="javascript:void(0)" data-id="<?php echo $s['ServiceId']; ?>" title="Xóa"><i class="fa fa-times"></i></a>
                                        <a class="link_status" href="javascript:void(0)" data-id="<?php echo $s['ServiceId']; ?>" title="<?php echo $s['StatusId'] == 2 ? 'Dừng dịch vụ' : 'Kích hoạt dịch vụ'; ?>" data-status="<?php echo $s['StatusId'] == 2 ? 3 : 2; ?>"><i class="fa fa-power-off"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="getServiceDetailUrl" value="<?php echo base_url('service/get'); ?>">
                <input type="text" hidden="hidden" id="updateServiceUrl" value="<?php echo base_url('service/update'); ?>">
                <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('service/changeStatus'); ?>">
                <input type="text" hidden="hidden" id="registerServiceUrl" value="<?php echo base_url('service/register'); ?>">
                <input type="text" hidden="hidden" id="getTimeDiffUrl" value="<?php echo base_url('service/timeDiff'); ?>">
                <input type="text" hidden="hidden" id="imagePath" value="<?php echo IMAGE_PATH; ?>">
                <input type="text" hidden="hidden" id="noImage" value="<?php echo IMAGE_PATH.NO_IMAGE; ?>">
                <input type="text" hidden="hidden" id="serviceId" value="0">
                <div class="modal fade" id="modalUpdateService" tabindex="-1" role="dialog" aria-labelledby="modalUpdateService" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Sửa Dịch Vụ</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary pull-right" id="btnUpdateService">Cập nhật</button>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tên:</label>
                                    <input type="text" class="form-control" id="serviceName" value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mô tả:</label>
                                    <textarea class="form-control" rows="2" id="serviceDesc"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Phí dịch vụ (VNĐ):</label>
                                            <input type="text" class="form-control" id="serviceCost" value="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Loại dịch vụ:</label>
                                            <?php $this->Mconstants->selectConstants('serviceTypes', 'ServiceTypeId1'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Bắt buộc ghi biển số xe? &nbsp;&nbsp;<input type="checkbox" id="cbIsRequireCarNumber"></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ảnh đại diện:</label>
                                    <button class="text-center form-control" id="selectUpdateImage">Bấm để chọn tệp tin vào để tải lên</button>
                                    <div class="progress" id="progressUpdateImage" style="display: none">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <input type="file" style="display: none" id="inputUpdateImage">
                                </div>
                                <div>
                                    <span style="font-size: 10px">Chỉ hỗ trợ hình ảnh</span>
                                    <div class="text-center">
                                        <img src="<?php echo IMAGE_PATH.NO_IMAGE; ?>" id="preViewUpdate" style="width: 100%;height: auto"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalRegisterService" role="dialog" aria-labelledby="modalRegisterService" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Đăng ký dịch Vụ</h4>
                            </div>
                            <div class="modal-body" style="color: #000;">
                                <div class="alert alert-success">
                                    <p>Tên dịch vụ: <span id="spanServiceName"></span></p>
                                    <p>Mô tả: <span id="spanServiceDesc"></span></p>
                                    <p>Phí dịch vụ: <span id="spanServiceCost">0</span> VNĐ</p>
                                    <p>Loại dịch vụ: <span id="spanServiceTypeName"></span></p>
                                </div>
                                <div class="alert alert-warning">
                                    <p>Chú thích về loại dịch vụ</p>
                                    <p><b>1. Theo ngày (không gia hạn)</b>: Phải chọn ngày kết thúc, tài khoản bị trừ tiền luôn</p>
                                    <p><b>2. Theo tháng (gia hạn vào ngày đăng ký)</b>:<br/>
                                        - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                        - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày đăng ký ở hàng tháng
                                    </p>
                                    <p><b>3. Theo tháng (gia hạn vào đầu tháng)</b>:<br/>
                                        - Chọn ngày kết thúc: Tài khoản bị trừ tiền luôn<br/>
                                        - Không chọn ngày kết thúc: Tài khoản bị trừ tiền vào ngày mồng 1 hàng tháng
                                    </p>
                                </div>
                                <div class="form-group">
                                    <select class="form-control select2" name="StudentId" id="studentId">
                                        <option value="0">--Chọn học viên--</option>
                                        <?php foreach($listUsers as $u){ ?>
                                            <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ngày bắt đầu</label>
                                            <input type="text" class="form-control datepicker" id="beginDate" value="<?php echo date('d/m/Y'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Ngày kết thúc</label>
                                            <input type="text" class="form-control datepicker" id="endDate" value="" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label">Số tiền</label>
                                            <input type="text" class="form-control" id="paidCost" value="0" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" id="divCarNumber" style="display: none;">
                                        <div class="form-group">
                                            <label class="control-label">Biển số xe</label>
                                            <input type="text" class="form-control" id="carNumber" value="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8" id="divServiceComment">
                                        <div class="form-group">
                                            <label class="control-label">Ghi chú</label>
                                            <input type="text" class="form-control" id="comment" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer" style="text-align: center;">
                                <button type="button" class="btn btn-primary" id="btnRegisterService">Đăng ký</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>