<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="row mgt-25" style="margin-bottom: 20px;">
                <div class="col-md-6">
                    <div class="medium-box-n boxst-1 success-gradient">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">Zticket<!-- | VNĐ <?php //echo priceFormat($user['TicketCount']*$ticketCost, true) ?> Đ--></h2>
                            <h2><?php echo priceFormat($user['TicketCount'], true); ?></h2>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="medium-box-n boxst-1 primary-gradient">
                        <img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
                        <div class="right-s">
                            <h2 class="pd-15">VNĐ<!-- | <?php //echo priceFormat(round($user['Balance']/$ticketCost, 3), true); ?>--></h2>
                            <h2><?php echo priceFormat($user['Balance'], true); ?> Đ</h2>
                        </div>
                        <button class="btn-buy buy-ticket" id="btnBuyTicket">Mua Zticket</button>
                        <input type="text" hidden="hidden" id="buyAble" value="<?php echo $buyAble ? 1 : 0; ?>">
                        <input type="text" hidden="hidden" id="ticketCost" value="<?php echo $ticketCost; ?>">
                        <input type="text" hidden="hidden" id="ticketPiece" value="<?php echo $ticketPiece; ?>">
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php if(isset($messageCharge)){
                foreach($messageCharge['success'] as $text){ ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $text; ?>
                    </div>
                <?php }
                foreach($messageCharge['errors'] as $text){ ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $text; ?>
                    </div>
                <?php }
            } ?>
            <div class="row mgt-25">
                <div class="col-md-12">
                    <div class="bg-box-gradient pd-15-25" id="divSell">
                        <h3 class="title-form">Chuyển Zticket</h3>
                        <div class="form-group mgt-10 divTagInput">
                            <label class="control-label">Đến tài khoản (số điện thoại)</label>
                            <input type="text" id="phoneNumber" class="form-control hmdrequired tagsinput-typeahead" value="" data-field="Số điện thoại">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Số lượng Zticket</label>
                            <input type="text" id="ticketCountSell" class="form-control hmdrequired" min="0" step="<?php echo 1/$ticketPiece; ?>" value="0" data-field="Số lượng Zticket">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Mật khẩu</label>
                            <input type="password" id="userPass" class="form-control hmdrequired" value="" data-field="Mật khẩu">
                        </div>

                        <div class="form-group mgt-10">
                            <label class="control-label">Mã 2FA</label>
                            <input type="text" id="userCode" class="form-control" value="" placeholder="Mã bảo mật Authenticator (nếu bật)">
                        </div>
                        <div class="text-center">
                            <button class="btn-buy pd-10-30 fz-20" id="btnSell">Chuyển</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mgt-25">
                <div class="col-md-12">
                    <div class="bg-box-gradient pd-15-25" id="sendMoney">
                        <h3 class="title-form">Chuyển tiền</h3>
                        <div class="form-group mgt-10 divTagInput">
                            <label class="control-label">Đến tài khoản (số điện thoại)</label>
                            <input type="text" id="phoneNumber-s" class="form-control hmdrequired tagsinput-typeahead" value="" data-field="Số điện thoại">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Số tiền</label>
                            <input type="text" id="money-s" class="form-control hmdrequired" value="0" data-field="Số tiền">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Mật khẩu</label>
                            <input type="password" id="userPass-s" class="form-control hmdrequired" value="" data-field="Mật khẩu">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Lý do </label>
                            <input type="text" id="reason-s" class="form-control" value="">
                        </div>

                        <div class="form-group mgt-10">
                            <label class="control-label">Mã 2FA</label>
                            <input type="text" id="userCode-s" class="form-control" value="" placeholder="Mã bảo mật Authenticator (nếu bật)">
                        </div>
                        <div class="text-center">
                            <button class="btn-buy pd-10-30 fz-20" id="btnSendMoney">Chuyển</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="buyModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content bg-box-gradient">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" style="color: #fff !important;">&times;</button>
                        <h4 class="modal-title" style="color: #fff !important;">Mua Zticket</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label" style="color: #fff !important;">Số Zticket</label>
                                    <input min="0" type="text" name="TicketBuy" id="ticket_buy" class="form-control" step="<?php echo 1/$ticketPiece; ?>">
                                </div>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="control-label" style="color: #fff !important;">Số tiền</label>
                                    <input type="text" id="money_cost" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <p class="text-left" style="position:relative;">Số Zticket được mua lẻ tối thiểu là 0.01 Zticket</p>
                        <button type="button" class="btn btn-primary" id="btnBuy">Mua</button>
                        <input type="text" hidden="hidden" id="insertChargeUrl" value="<?php echo base_url('charge/insert'); ?>">
                        <input type="hidden" id="searchByNameOrPhoneUrl" value="<?php echo base_url('api/user/searchByNameOrPhone'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>