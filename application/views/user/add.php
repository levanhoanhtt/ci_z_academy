<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
                <div class="row mgt-25">
                    <div class="col-md-6">
                        <div class="bg-box-gradient-re pd-15-25" id="divProfile">
                            <h3 class="title-form">Thông tin</h3>
                            <!--<div class="form-group mgt-10">
                                <label class="control-label">Tên đăng nhập</label>
                                <input type="text" name="UserName" class="form-control hmdrequired" value="" data-field="Tên đăng nhập">
                            </div>-->
                            <div class="form-group mgt-10">
                                <label class="control-label">Họ tên</label>
                                <input type="text" name="FullName" class="form-control hmdrequired" value="" data-field="Họ tên">
                            </div>
                            <div class="form-group mgt-10">
                                <label class="control-label">Số điện thoại (dùng để đăng nhập)</label>
                                <input type="text" name="PhoneNumber"  id="phoneNumber" class="form-control hmdrequired" value="" data-field="Số điện thoại">
                            </div>
                            <div class="form-group mgt-10">
                                <label class="control-label">Email (dùng để lấy lại mật khẩu)</label>
                                <input type="text" name="Email" class="form-control hmdrequired" value="" data-field="Email">
                            </div>
                            <div class="form-group mgt-10">
                                <label class="control-label">Ngày sinh</label>
                                <input type="text" name="BirthDay" class="form-control datepicker hmdrequired" value="" data-field="Ngày sinh" style="padding: 6px 12px;">
                            </div>
                            <div class="form-group mgt-10">
                                <label class="control-label">4 số cuối chứng minh thư</label>
                                <input type="text" name="IDCardNumber" id="idCardNumber" class="form-control hmdrequired" value="" data-field="4 số cuối chứng minh thư">
                            </div>
                            <!--<div class="form-group mgt-10">
                                <label class="control-label">Country</label>
                                <select class="form-control">
                                    <option>Vietnam</option>
                                </select>
                            </div>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bg-box-gradient-re pd-15-25" id="divPass">
                            <h3 class="title-form">Bảo mật</h3>
                            <div class="form-group mgt-10">
                                <label class="control-label">Mật khẩu mới</label>
                                <input type="password" id="newPass" name="UserPass" class="form-control hmdrequired" value="" data-field="Mật khẩu mới">
                            </div>

                            <div class="form-group mgt-10">
                                <label class="control-label">Gõ lại mật khẩu</label>
                                <input type="password" id="rePass" class="form-control hmdrequired" value="" data-field="Gõ lại mật khẩu">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <input type="text" name="UserId" id="userId" hidden="hidden" value="0">
                    <input type="text" name="RoleId" id="roleId" hidden="hidden" value="1">
                    <input type="text" name="StatusId" hidden="hidden" value="<?php echo STATUS_ACTIVED; ?>">
                    <input type="text" name="IsVerifyEmail" hidden="hidden" value="2">
                    <input type="text" name="Avatar" hidden="hidden" value="<?php echo NO_IMAGE; ?>">
                    <input type="text" name="AffUserId" hidden="hidden" value="0">
                    <input type="text" id="userEditUrl" hidden="hidden" value="<?php echo base_url('user/admin'); ?>">
                    <button type="button" class="btn btn-primary submit" id="btnUpdate">Cập nhật</button>
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>