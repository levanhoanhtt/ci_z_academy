<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="row mgt-25">
                            <div class="col-md-12">
                                <div class="bg-box-gradient-re pd-15-25">
                                <h4 class="text-white"><strong>Two Factor Setup</strong></h4>
                                <p>To setup two factor authentication you first need to download Google Authenticator:</p>
                                <p>
                                    <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank"><img src="assets/vendor/dist/img/appstore.png" width="100px"></a>
                                    <a href="https://itunes.apple.com/vn/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="assets/vendor/dist/img/googleplay.png" width="100px"></a>
                                </p>
                                <p>Then scan the below barcode or, if you are not able to scan the barcode, you can enter the "Security Key" manually.</p>
                                <h4>Security Key: <span style="font-family:Courier;color:#fff;letter-spacing:2px;font-size:22px;"><?php echo $secretCode; ?></span> <small class="text-black"><em>(Time Based Code)</em></small></h4>

                                
                                <p>Enter the 6 digit code generated by Google Authenticator in the 2FA Code box and switch "Enable Two-Factor" to On</p>
                                <p><span class="label label-danger">Important</span> Save this secret code for future reference</p>
                                <p><em>Note: No Google account is required to use Google Authenticator; skip any Google logins</em></p>
                                </div>
                            </div>
                        </div>   
                        <br>
                        <?php $videoUrl = '';
                        $configs1 = $this->session->userdata('configs');
                        if($configs1){
                            if(isset($configs1['VIDEO_2FA'])) $videoUrl = 'https://www.youtube.com/embed/' . getVideoYoutubeId($configs1['VIDEO_2FA']);
                        }
                        if(!empty($videoUrl)){ ?>
                            <img src="assets/vendor/dist/img/help.png" style="margin-left: 15px;"> <span style="color:#547dff; font-weight: bold;">HƯỚNG DẪN</span>
                            <iframe width="96%" style="margin: 2%" height="400px" src="<?php echo $videoUrl; ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <div class="row mgt-25">
                            <div class="bg-box-gradient-re pd-15-25">
                                <h3 class="title-form">Account Security</h3>
                                <p class="mgbt-40">Two Factor Authentication</p>
                                <div class="row">
                                    <label for="enable2fa" class="control-label col-md-4 text-white">Enable Two-Factor</label>
                                    <div class="col-md-8">
                                        <label class="radio-inline text-white"><input name="enable2fa" type="radio" value="2" id="enable2fa"<?php if($user['IsEnabled2FA'] == 2) echo ' checked'; ?>> On</label>
                                        <label class="radio-inline text-white"><input name="enable2fa" type="radio" value="1" id="disable2fa"<?php if($user['IsEnabled2FA'] != 2) echo ' checked'; ?>> Off</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <div class="form-group div2FA" style="display: none;margin-top: 10px;">
                                            <img src="<?php echo $qrCodeUrl; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group div2FA" style="display: none;">
                                    <div class="row">
                                        <label for="2fa" class="control-label col-md-4 text-white">Enter 2FA Code</label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-qrcode fa-lg"></i></span>
                                                <input class="form-control" id="userCode" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-4">
                                        <button class="btn btn-primary" id="btn2FA">Submit</button>
                                        <!--<button class="btn btn-default">Cancel</button>-->
                                        <input type="text" hidden="hidden" id="secretCode" value="<?php echo $secretCode; ?>">
                                        <input type="text" hidden="hidden" id="checkCode2FaUrl" value="<?php echo base_url('user/checkCode2Fa'); ?>">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="row mgt-25">
                            <div class="bg-box-gradient-re pd-15-25">
                                <h4 class="text-white" style="padding-left: 25px"><strong>Google Authenticator Guide</strong></h4>
                                <style type="text/css">ol > li {margin-bottom: 10px;}</style>
                                <ol>
                                    <li>Install Google Authenticator for <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank" class="text-black">Android</a> or <a href="https://itunes.apple.com/en/app/google-authenticator/id388497605?mt=8" target="_blank" class="text-black">Apple</a> and open Google Authenticator</li>
                                    <li>Go to <code>Menu</code> -&gt; <code>Setup Account</code></li>
                                    <li>Choose <code>Scan a barcode</code> option, and scan the barcode shown on this page</li>
                                    <li><em><b>If you are unable to scan the barcode</b>: Choose <code>Enter provided key</code> and type in the "Security Key"</em></li>
                                    <li>A six digit number will now appear in your Google Authenticator app home screen, enter this code into the 2FA form on this page</li>
                                    <li>Every time you login to hextracoin.co you must enter the new 2FA code from your Google Authenticator into the 2FA box on the login form</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>