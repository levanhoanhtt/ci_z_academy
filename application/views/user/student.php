<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('user/student'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên, SĐT hoặc Email">
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="IsMember">
                                    <option value="0">--Z--</option>
                                    <option value="1"<?php if(set_value('IsMember') == 1) echo ' selected="selected"'; ?>>Not Z</option>
                                    <option value="2"<?php if(set_value('IsMember') == 2) echo ' selected="selected"'; ?>>Member</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <select class="form-control" name="BalanceStatusId">
                                    <option value="0">--Số dư tài khoản--</option>
                                    <option value="1"<?php if(set_value('BalanceStatusId') == 1) echo ' selected="selected"'; ?>>Lớn hơn 0</option>
                                    <option value="2"<?php if(set_value('BalanceStatusId') == 2) echo ' selected="selected"'; ?>>Nhỏ hơn 0</option>
                                    <option value="3"<?php if(set_value('BalanceStatusId') == 3) echo ' selected="selected"'; ?>>Bằng 0</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select class="form-control" name="TicketStatusId">
                                    <option value="0">--Số Zticket--</option>
                                    <option value="1"<?php if(set_value('TicketStatusId') == 1) echo ' selected="selected"'; ?>>Lớn hơn 0</option>
                                    <option value="3"<?php if(set_value('TicketStatusId') == 3) echo ' selected="selected"'; ?>>Bằng 0</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, '--Trạng thái--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="submit" name="export" class="btn btn-default" value="Xuất file">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày sinh</th>
                                <th>Họ và tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Số Zticket</th>
                                <th>Tiền mặt</th>
                                <th>Ghi danh</th>
                                <th>Z</th>
                                <th>Block</th>
                                <th class="text-center">Trạng thái</th>
                                <th class="text-center" style="width: 50px;">Xóa</th>
                                <th class="text-center" style="width: 50px;">Email</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUser">
                            <?php $i = 0;
                            $labelCss = $this->Mconstants->labelCss;
                            $status = $this->Mconstants->status;
                            foreach($listStudents as $u){
                                $i++; ?>
                                <tr id="user_<?php echo $u['UserId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ddMMyyyy($u['BirthDay']); ?></td>
                                    <td>
                                        <a href="<?php echo base_url('userskill/view/'.$u['UserId']); ?>"><?php echo $u['FullName']; ?></a>
                                    </td>
                                    <td><?php echo $u['PhoneNumber']; ?></td>
                                    <td><?php echo $u['Email']; ?></td>
                                    <td><?php echo priceFormat($u['TicketCount'], true); ?></td>
                                    <td><?php echo priceFormat($u['Balance']); ?></td>
                                    <td><?php echo priceFormat($u['CountRegister']); ?></td>
                                    <td><input type="checkbox" value="<?php echo $u['UserId']; ?>" class="js-switch cbMember"<?php if($u['IsMember'] == 2) echo ' checked'; ?>/></td>
                                    <td><input type="checkbox" value="<?php echo $u['UserId']; ?>" class="js-switch cbBlock"<?php if($u['StatusId'] == 3) echo ' checked'; ?>/></td>
                                    <td class="text-center" id="tdStatusName_<?php echo $u['UserId']; ?>"><span class="<?php echo $labelCss[$u['StatusId']]; ?>"><?php echo $status[$u['StatusId']]; ?></span></td>
                                    <td class="text-center"><?php if($u['TicketCount'] == 0 && $u['Balance'] == 0){ ?><a class="link_delete" href="javascript:void(0)" data-id="<?php echo $u['UserId']; ?>"><i class="fa fa-remove"></i></a><?php } ?></td>
                                    <td class="text-center">
                                        <?php if($u['IsVerifyEmail'] == 1) echo '<a class="link_mail" href="javascript:void(0)" data-id="'.$u['UserId'].'"><i class="fa fa-envelope tooltip1" title="Chưa xác thực"></i></a>';
                                        if($u['IsVerifyEmail'] == 2) echo '<a href="javascript:void(0)"><i class="fa fa-check tooltip1" title="Đã xác thực"></i></a>';
                                        elseif($u['IsVerifyEmail'] == 3) echo '<a class="link_mail" href="javascript:void(0)" data-id="'.$u['UserId'].'"><i class="fa fa-share tooltip1" title="Đã gửi mail xác thực"></i></a>'; ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('api/user/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="changeIsMemberUrl" value="<?php echo base_url('api/user/changeIsMember'); ?>">
                    <input type="text" hidden="hidden" id="verifyEmailUrl" value="<?php echo  base_url('api/user/verifyEmail') ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>