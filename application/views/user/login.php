<!--<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'vi', includedLanguages: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>-->
<?php $this->load->view('includes/user/header'); ?>
    <div class="login-box-body">
        <?php $this->load->view('includes/notice'); ?>
        <?php echo form_open('api/user/checkLogin', array('id' => 'userForm')); ?>
        <div class="form-group has-feedback">
            <input type="text" name="UserName" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $userName; ?>" placeholder="Số điện thoại" data-field="Số điện thoại">
            <!-- <span class="glyphicon glyphicon-phone form-control-feedback"></span> -->
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="UserPass" class="form-control hmdrequired" value="<?php echo $userPass; ?>" placeholder="Mật khẩu" data-field="Mật khẩu">
            <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
        </div>
        <div class="form-group">
            <input type="text" name="UserCode" class="form-control" value="" placeholder="Mã bảo mật Authenticator (nếu bật)">
        </div>
        <div class="row">
            <div class="col-xs-8" style="display: none">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="IsRemember" class="iCheck" checked="checked"> Remember
                    </label>
                </div>
            </div>
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat bd-rd-10">Đăng nhập</button>
                <input type="text" hidden="hidden" name="IsGetConfigs" value="1">
            </div>
        </div>
        <?php echo form_close(); ?>
        <p class="text-center mgt-5 fz-13">Bạn chưa có tài khoản? Bấm <a href="<?php echo base_url('user/register'); ?>">vào đây</a> để đăng ký</p>
        <p class="text-center"><a href="<?php echo base_url('user/forgotpass'); ?>">Quên mật khẩu</a></p>
        <input type="text" hidden="hidden" id="dashboardUrl" value="<?php echo base_url('user/dashboard'); ?>">
        <input type="text" hidden="hidden" id="fullNameLoginId" value="Z Academy">
        <p class="text-center mgt-10"><a  href="<?php echo $videoGuideUrl; ?>" target="_bank">Hướng dẫn sử dụng website</a></p>
        <p class="text-center mgt-10">Tra cứu tình chứng chỉ học phần</p>
        <p class="text-center">
            <?php echo form_open('register/getCourseIdByPhone', array('id' => 'checkPhoneForm')); ?>
            <input type="text" name="PhoneNumber" id="phoneNumber1" class="form-control dp-il w-80 hmdrequired flash" value="" data-field="Số điện thoại">
            <button type="submit" class="btn btn-primary checkregister-btn btn-block btn-flat bd-rd-10 mgt-5">Tra cứu</button>
            <?php echo form_close(); ?>
        </p>
		</br>
		<img src="assets/vendor/dist/img/line.png">
    </div>
    <div class="register-success text-center" style="display: none;color: #009900;margin-top: 40px;">
        <h4 style="font-size: 20px;">Tài khoản đã khởi tạo thành công nhưng chưa được xác minh.</h4>
        <h4 style="font-size: 20px;">Bạn vui lòng truy cập email xác minh tài khoản trước khi đăng nhập và sử dụng dịch vụ.</h4>
        <p  style="font-size: 15px;"><i>Lưu ý: Nếu bạn không nhận được email xác minh trong hộp thư đến, hãy kiểm tra thêm hòm thư spam. Xin cảm ơn!</i></p>
    </div>
    <div class="login-block text-center text-danger" style="display: none;;margin-top: 40px;">
        <h4 style="font-size: 20px;">Tài khoản của bạn đang bị block.</h4>
        <h4 style="font-size: 20px;">Bạn vui lòng liên hệ học viện để giải quyết.</h4>
        <p  style="font-size: 15px;"><i>Lưu ý: Nếu bạn không nhận được email xác minh trong hộp thư đến, hãy kiểm tra thêm hòm thư spam. Xin cảm ơn!</i></p>
    </div>

</div>
<div class="modal fade" id="modalRegisterNotice" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
		<div class="trung" style="border: 10px solid;outline: 14px solid #ffffff;border-image: linear-gradient(to top, #2b83ce 0%, #0b518c 100%);border-image-slice: 1;color:#0c518c;">
            <div class="modal-header" style="text-align: center;border-bottom: none!important;">
			<div class="row" style="margin-right:0px">
			<div class="col-md-12" style="bottom: 15px;"> <h3 style="font-family:phosphate;">HỌC VIỆN KIẾN THỨC NỀN - KHỞI NGHIỆP THÔNG MINH</h3> </div>
			 <button type="button" style="color: #333;" class="close" data-dismiss="modal">&times;</button>
			</div>
				<div class="row" style="margin-right:0px">
					<div class="col-md-2"></div>
					<div class="col-md-8">
					<img width="100px" src="assets/vendor/dist/img/Z-certification.png">
										<h3 style="font-weight: bold;" >CHỨNG NHẬN</h3>
										<h1 style="font-weight: bold;margin-top:10px;" id="bFullName"></h1>
					</div>
					<div class="col-md-2"></div>
				</div>
               <!-- <h4 class="modal-title" id="h4Notice" style="font-weight: bold;"></h4> !-->
            </div>
            <div class="modal-body" style="padding:0px">
                <div class="row">
                    <div class="col-md-12" style="text-align: center;bottom: 30px;">
                        <h4>Ngày sinh: <b id="bBirthDay"></b></h4>
                        <h4>Số điện thoại: <b id="bPhoneNumber"></b></h4>
                        <h4>4 số cuối thẻ định danh (CMT,thẻ căn cước): <b id="bIDCardNumber"></b></h4>
                    </div>
                </div>
				<div class="row">
				<div class="col-md-2"></div>
                    <div class="col-md-8" style="text-align: center;bottom: 20px;">
                        <table class="table table-bordered " style="color:#eee;background:linear-gradient( #5fb6ff 0%, #4F98D5 100%);">
    <thead>
      <tr>
        <th>Học phần</th>
        <th>Trạng thái</th>
        <th>Tốt nghiệp</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>Alpha</b></td>
        <td><b id="IsAlpha"></b></td>
        <td><b id="IsFinishA"></b></td>
      </tr>
      <tr>
        <td><b>Beta</b></td>
        <td><b id="IsBeta"></b></td>
        <td><b id="IsFinishB"></b></td>
      </tr>
      <tr>
        <td><b>Gamma</b></td>
        <td><b id="IsGamma"></b></td>
        <td><b id="IsFinishG"></b></td>
      </tr>
    </tbody>
  </table>
                    </div>
					<div class="col-md-2"></div>
                </div>
				<p style="text-align:right"><i>Thông tin chi tiết đào tạo học phần xem tại <u><a href="http://kienthucnen.com">kienthucnen.com</a></u></i></p>
            </div>
        </div>
</div>
    </div>
</div>
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript" src="assets/js/user_login.js?2"></script>
<?php $this->load->view('includes/footer login'); ?>
</body>
</html>