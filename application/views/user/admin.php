<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('user/admin'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên, SĐT hoặc Email">
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                            <div class="col-sm-4">
                                <a href="<?php echo base_url('user/add'); ?>" class="btn btn-info">Thêm mới</a>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ và tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th style="width: 50px;"></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyUser">
                            <?php $i = 0;
                            foreach($listUsers as $u){
                                $i++; ?>
                                <tr id="user_<?php echo $u['UserId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $u['FullName']; ?></td>
                                    <td><?php echo $u['PhoneNumber']; ?></td>
                                    <td><?php echo $u['Email']; ?></td>
                                    <td><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $u['UserId']; ?>" title="Xóa"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('api/user/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>