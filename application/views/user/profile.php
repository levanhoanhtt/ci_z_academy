<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="row mgt-25">
                <div class="col-md-6">
                    <div class="bg-box-gradient-re pd-15-25" id="divProfile">
                        <h3 class="title-form">Thông tin</h3>
                        <!--<div class="form-group mgt-10">
                            <label class="control-label">Tên đăng nhập</label>
                            <input type="text" id="userName" class="form-control hmdrequired" value="<?php //echo $user['UserName']; ?>" data-field="Tên đăng nhập">
                        </div>-->
                        <div class="form-group mgt-10">
                            <label class="control-label">Họ tên</label>
                            <input type="text" id="fullName" class="form-control hmdrequired" value="<?php echo $user['FullName']; ?>" data-field="Họ tên">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Số điện thoại (dùng để đăng nhập)</label>
                            <input type="text" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $user['PhoneNumber']; ?>" data-field="Số điện thoại">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Email (dùng để lấy lại mật khẩu)</label>
                            <input type="text" id="email" class="form-control hmdrequired" value="<?php echo $user['Email']; ?>" data-field="Email"<?php if($user['RoleId'] == 2) echo ' disabled'; ?>>
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">Ngày sinh</label>
                            <input type="text" id="birthDay" class="form-control datepicker hmdrequired" value="<?php echo ddMMyyyy($user['BirthDay']); ?>" data-field="Ngày sinh" style="padding: 6px 12px;">
                        </div>
                        <div class="form-group mgt-10">
                            <label class="control-label">4 số cuối chứng minh thư</label>
                            <input type="text" id="iDCardNumber" class="form-control hmdrequired" value="<?php echo $user['IDCardNumber']; ?>" data-field="4 số cuối chứng minh thư">
                        </div>
                        <div>
                            <button class="btn btn-primary" id="btnProfile">Cập nhật</button>
                            <input type="text" hidden="hidden" id="updateProfileUrl" value="<?php echo base_url('api/user/updateProfile'); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="bg-box-gradient-re pd-15-25" id="divPass">
                        <h3 class="title-form">Bảo mật</h3>
                        <div class="form-group mgt-10">
                            <label class="control-label">Mật khẩu cũ</label>
                            <input type="password" id="userPass" class="form-control hmdrequired" value="" data-field="Mật khẩu cũ">
                        </div>

                        <div class="form-group mgt-10">
                            <label class="control-label">Mật khẩu mới</label>
                            <input type="password" id="newPass" class="form-control hmdrequired" value="" data-field="Mật khẩu mới">
                        </div>

                        <div class="form-group mgt-10">
                            <label class="control-label">Gõ lại mật khẩu mới</label>
                            <input type="password" id="rePass" class="form-control hmdrequired" value="" data-field="Gõ lại mật khẩu mới">
                        </div>
                        <div>
                            <button class="btn btn-primary" id="btnPassword">Cập nhật</button>
                            <input type="text" hidden="hidden" id="changePassUrl" value="<?php echo base_url('api/user/changePass'); ?>">
                        </div>
                    </div>
                    <div class="alert alert-danger alert-dismissible" style="margin-top: 25px;">
                        Nếu điền thông tin không chính xác sẽ bị block tài khoản
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>