<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url();?>"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="title" content="Chào mừng bạn đến với Học Viện Z">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link href="assets/vendor/plugins/datepicker/datepicker3.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/style.css">
    <script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top: 5px;">
    <p class="text-center"><img width="130px" src="assets/vendor/dist/img/logo-z.png"></p>
        <div class="login-logo">
            <span class="logo-lg"><span class="round-letter-login">Z</span>Academy</span>
        </div>
    <div class="login-box-body">
        <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
        <div class="form-group has-feedback">
            <input type="text" name="FullName" class="form-control hmdrequired" value="" placeholder="Họ và tên" data-field="Họ và tên">
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="Email" id="email" class="form-control" value="" placeholder="Email (dùng để lấy lại mật khẩu)">
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="" placeholder="Số điện thoại (dùng để đăng nhập)" data-field="Số điện thoại">
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="BirthDay" id="birthDay" class="form-control datepicker hmdrequired" value="" placeholder="Ngày sinh" data-field="Ngày sinh" style="padding: 6px 12px;">
        </div>
        <div class="form-group has-feedback">
            <input type="text" name="IDCardNumber" id="iDCardNumber" class="form-control hmdrequired" value="" placeholder="4 số cuối chứng minh thư" data-field="4 số cuối chứng minh thư">
        </div>
        <div class="form-group has-feedback">
            <input type="password" id="newPass" name="UserPass" class="form-control hmdrequired" value="" placeholder="Mật khẩu" data-field="Mật khẩu">
        </div>
        <div class="form-group has-feedback">
            <input type="password" id="rePass" class="form-control hmdrequired" value="" placeholder="Nhập lại Mật khẩu" data-field="Mật khẩu">
        </div>
        <?php if($affUserId > 0){ ?>
            <div class="form-group has-feedback">
                <label style="font-weight: normal;">Người giới thiệu</label>
                <input type="text" class="form-control" disabled value="<?php echo $affFullName; ?>">
            </div>
        <?php } ?>
        <div class="g-recaptcha" data-sitekey="6LdlbzoUAAAAAN1aFHhACORAO_O0VIn_jpwz-tcA"></div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-xs-8" style="display: none">
                <div class="checkbox icheck">
                    <label class="control-label"><input type="checkbox" name="IsSendPass" class="iCheck" checked="checked"> Gửi mật khẩu vào email</label>
                </div>
            </div>
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat submit bd-rd-10">Đăng ký</button>
                <input type="text" name="UserId" id="userId" hidden="hidden" value="0">
                <input type="text" name="RoleId" id="roleId" hidden="hidden" value="2">
                <input type="text" name="StatusId" hidden="hidden" value="1">
                <input type="text" name="IsVerifyEmail" hidden="hidden" value="1">
                <input type="text" name="IsVerifyPhone" hidden="hidden" value="1">
                <input type="text" name="Avatar" hidden="hidden" value="<?php echo NO_IMAGE; ?>">
                <input type="text" name="AffUserId" hidden="hidden" value="<?php echo $affUserId; ?>">
                <input type="text" hidden="hidden" id="appFbId" value="<?php echo APP_FB_ID; ?>">
                <input type="text" id="userEditUrl" hidden="hidden" value="<?php echo base_url('user/index'); ?>">
                <input type="text" id="verifyPhoneUrl" hidden="hidden" value="<?php echo base_url('api/user/verifyPhone'); ?>">
            </div>
        </div>
        <?php echo form_close(); ?>
        <?php echo form_open('notification/insert', array('id' => 'notificationForm', 'style' => 'display: none;')); ?>
        <p class="text-center text-danger">Tài khoản của bạn đã có người đăng ký trong hệ thống. Nếu không phải bạn, vui lòng điền vào form bên dưới.</p>
        <div class="form-group has-feedback">
            <textarea class="form-control hmdrequired" id="message" rows="4" style="height: 100%!important;" placeholder="Ghi chú khiếu nại" data-field="Ghi chú khiếu nại"></textarea>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat bd-rd-10" id="btnComplaint">Gửi khiếu nại</button>
        </div>
        <br>
        <?php echo form_close(); ?>
        <p class="text-center fz-13 mgt-5">Bạn đã có tài khoản? Bấm <a href="<?php echo base_url('user'); ?>" id="aLogin">vào đây</a> để đăng nhập</p>
        <p class="text-center"><a href="<?php echo base_url('user/forgotpass'); ?>">Quên mật khẩu</a></p>
        <input type="text" hidden="hidden" id="fullNameLoginId" value="Z Academy">
    </div>
    <div class="register-success text-center" id="divRegister" style="display: none;color: #009900;margin-top: 40px;">
        <h4 style="font-size: 20px;">Bạn vừa khởi tạo tài khoản thành công.</h4>
        <h4 style="font-size: 20px;">Bạn vui lòng kiểm tra email để xác minh tài khoản.</h4>
        <p  style="font-size: 15px;"><i>Lưu ý: Nếu bạn không nhận được email xác minh trong hộp thư đến, hãy kiểm tra thêm hòm thư spam. Xin cảm ơn!</i></p>
    </div>
    <div class="register-success text-center" id="divNotification" style="display: none;color: #009900;margin-top: 40px;">
        <h4 style="font-size: 20px;">Bạn vừa gửi khiếu nại về tại khoản thành công.</h4>
        <h4 style="font-size: 20px;">Chùng tôi sẽ xem xét để xác minh tài khoản của bạn sớm nhất.</h4>
    </div>
    <hr class="mgt-20" style="border-color:#ddd !important">
    <p class="text-center"><span class="z-round-small">z</span> Academy All Right Reserved 2017</p>
</div>

<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="assets/js/user_update.js"></script>
</body>
</html>