<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $roleId = $user['RoleId']; ?>
            <section class="content">
                <?php $this->load->view('includes/balance'); ?>
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('transaction/log'); ?>
                        <div class="row">
                            <?php if($roleId == 1){ ?>
                            <div class="col-sm-4">
                                <select class="form-control select2" name="UserId">
                                    <option value="0">Học viên</option>
                                    <?php foreach($listUsers as $u){ ?>
                                        <option value="<?php echo $u['UserId']; ?>"<?php if(set_value('UserId') == $u['UserId']) echo ' selected="selected"'; ?>><?php echo $u['FullName']; ?> (<?php echo $u['PhoneNumber']; ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('logTypes', 'LogTypeId', set_value('LogTypeId'), true, '--Loại giao dịch--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày</th>
                                <th>Họ và tên</th>
                                <th>Loại giao dịch</th>
                                <th>Số tiền/ Zticket</th>
                                <th>Hiện tại</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            $labelCss = $this->Mtransactionlogs->labelCss;
                            $logTypes = $this->Mconstants->logTypes;
                            $ticketLogTypes = array(2, 3, 4, 12);
                            foreach($listTransactionLogs as $tl){
                                $i++;
                                $isTicket = in_array($tl['LogTypeId'], $ticketLogTypes); ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo ddMMyyyy($tl['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td>
                                        <a href="<?php echo base_url('userskill/view/'.$tl['UserId']); ?>">
                                            <?php echo $roleId == 2 ? $user['FullName'] : $this->Mconstants->getObjectValue($listUsers, 'UserId', $tl['UserId'], 'FullName'); ?>
                                        </a>
                                    </td>
                                    <td><span class="<?php echo $labelCss[$tl['LogTypeId']]; ?>"><?php echo $logTypes[$tl['LogTypeId']]; ?></span></td>
                                    <td>
                                        <?php if($isTicket) echo priceFormat($tl['Amount'], true) . ' Zticket';
                                        else echo priceFormat($tl['Amount']) . ' VNĐ'; ?>
                                    </td>
                                    <td>
                                        <?php if($isTicket) echo priceFormat($tl['TicketCount'], true) . ' Zticket';
                                        else echo priceFormat($tl['Balance']) . ' VNĐ'; ?>
                                    </td>
                                    <td><?php echo $tl['Comment']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php $this->load->view('includes/pagging_footer'); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>