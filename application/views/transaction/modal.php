<div class="modal fade" id="modalProof" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Danh sách bằng chứng thanh toán</h4>
            </div>
            <div class="modal-body">
                <style>#tblProof img{max-height: 100px;}</style>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Mốc thời gian</th>
                        <th>Họ tên</th>
                        <th>Số điện thoại</th>
                        <th>Ảnh</th>
                        <th>Kiểu thanh toán</th>
                    </tr>
                    </thead>
                    <tbody id="tblProof"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="hidden" value="<?php echo base_url('proof/getList')?>" id="getListProofUrl">
                <input type="hidden" value="assets/uploads/images/" id="imagePath">
            </div>
        </div>
    </div>
</div>