<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <style>.bootstrap-tagsinput{height: 65px;}</style>
            <div class="row text-center mgt-25">
                <div class="col-md-9">
                    <?php echo form_open('transaction/insertRecharge', array('id' => 'transactionForm')); ?>
                    <p><input class="nt-input hmdrequired tagsinput-typeahead" type="text" name="PhoneNumber" placeholder="Số điện thoại" id="phoneNumber" data-field="Số điện thoại"></p>
                    <p><input class="hmdrequired nt-input" style="width: 100%!important;" type="text" name="Comment" placeholder="Ghi chú" id="comment" data-field="Ghi chú"></p>
                    <p><input class="nt-input hmdrequired" type="text" name="PaidVN" placeholder="Số tiền" id="paidVN" data-field="Số tiền"></p>
                    <p><button class="btn btn-primary bd-rd-10 pd-10-30 fz-18" id="btnInsertTransaction">Xác nhận</button></p>
                    <input type="hidden" id="searchByNameOrPhoneUrl" value="<?php echo base_url('api/user/searchByNameOrPhone'); ?>">
                    <?php echo form_close(); ?>
                    <?php if(isset($messageTransaction)){
                        foreach($messageTransaction['success'] as $text){ ?>
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $text; ?>
                            </div>
                        <?php }
                        foreach($messageTransaction['errors'] as $text){ ?>
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?php echo $text; ?>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="col-md-3">
                    <div class="small-box-n primary-gradient mgt-20"><a id="showPopupProof" style="color: white;display: block" href="javascript:;">Xem bằng chứng</a></div>
                </div>
            </div>
            <div class="box box-default">
                <?php sectionTitleHtml('Tìm kiếm'); ?>
                <div class="box-body row-margin">
                    <?php echo form_open('transaction'); ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" name="SearchText" class="form-control" value="<?php echo set_value('SearchText'); ?>" placeholder="Tên hoặc SĐT">
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="box box-default">
                <?php sectionTitleHtml('Danh sách nạp tiền', isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mốc thời gian</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Số tiền</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 0;
                        foreach ($listTransactions as $t) {  ?>
                            <tr>
                                <td><?php echo ddMMyyyy($t['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                <td><?php echo $t['FullName']; ?></td>
                                <td><?php echo $t['PhoneNumber']; ?></td>
                                <td><?php echo priceFormat($t['PaidVN']); ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
            </div>
            <?php $this->load->view('transaction/modal'); ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>