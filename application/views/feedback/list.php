<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <?php $roleId = $user['RoleId']; ?>
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="box box-default">
                <?php sectionTitleHtml('Tìm kiếm'); ?>
                <div class="box-body row-margin">
                    <?php echo form_open('feedback'); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php $this->Mconstants->selectConstants('feedbackStatus' . $roleId, 'FeedbackStatusId', set_value('FeedbackStatusId'), true, 'Trạng thái'); ?>
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                        <?php if ($roleId == 2) { ?>
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAddFeedback">Tạo hỗ trợ</button>
                            </div>
                        <?php } ?>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="box box-default">
                <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mốc thời gian</th>
                            <th>Họ và tên</th>
                            <th>Số điện thoại</th>
                            <th>Tiêu đề</th>
                            <th>Nội dung</th>
                            <th>Trả lời</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="tbodyFeedback">
                        <?php $labelCss = $this->Mconstants->labelCss;
                        $status1 = $this->Mconstants->feedbackStatus1;
                        $status2 = $this->Mconstants->feedbackStatus2;
                        foreach ($listFeedbacks as $f) { ?>
                            <tr>
                                <td><?php echo ddMMyyyy($f['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                <td><?php echo $f['FullName']; ?></td>
                                <td><?php echo $f['PhoneNumber']; ?></td>
                                <td><?php echo $f['Title']; ?></td>
                                <td><?php echo $this->Mfeedbacks->cutWords($f["Question"], 3); ?></td>
                                <td><?= $this->Mfeedbacks->cutWords($f["Reply"], 3) ?></td>
                                <td><span class="<?php echo $labelCss[$f['FeedbackStatusId']]; ?>"><?php echo $roleId == 1 ? $status1[$f['FeedbackStatusId']] : $status2[$f['FeedbackStatusId']]; ?></span></td>
                                <td><a href="javascript:void(0)" class="link_edit" data-id="<?php echo $f['FeedbackId']; ?>" title="Xem"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php $this->load->view('includes/pagging_footer'); ?>
                <input type="text" hidden="hidden" id="getFeedbackUrl" value="<?php echo base_url('feedback/get'); ?>">
                <input type="text" hidden="hidden" id="updateFeedbackUrl" value="<?php echo base_url('feedback/update'); ?>">
            </div>
            <div class="modal fade" id="modalAddFeedback" tabindex="-1" role="dialog" aria-labelledby="modalAddFeedback">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Hỗ trợ</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Tiêu đề:</label>
                                <input type="text" class="form-control" id="feedbackTitle1">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung:</label>
                                <textarea class="form-control" rows="8" id="feedbackQuestion1"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <button type="button" class="btn btn-primary" id="btnAddFeedback">Gửi</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modalReplyFeedback" tabindex="-1" role="dialog" aria-labelledby="modalReplyFeedback">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Nội dung Hỗ trợ</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Tiêu đề:</label>
                                <input type="text" class="form-control" id="feedbackTitle2" disabled>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nội dung:</label>
                                <textarea class="form-control" rows="8" id="feedbackQuestion2" disabled></textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phản hồi:</label>
                                <textarea class="form-control" rows="8" id="feedbackReply2"></textarea>
                            </div>
                            <input type="text" hidden="hidden" id="feedbackId" value="0">
                        </div>
                        <?php if ($roleId == 1){ ?>
                        <div class="modal-footer" style="text-align: center;">
                            <button type="button" class="btn btn-primary" id="btnReplyFeedback">Trả lời</button>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>

