<hr class="mgt-20" style="border-color:#ddd !important">
    <p class="text-center"><span class="z-round-small">z</span> Academy All Right Reserved 2017</p>
</div>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
</script>
</body>
</html>