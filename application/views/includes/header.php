<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/select2/select2.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/all.css">
    <?php if (isset($scriptHeader)) outputScript($scriptHeader); ?>
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <link href="assets/vendor/plugins/marquee/marquee.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/vendor/dist/css/style.css?20171229">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="<?php echo base_url(); ?>" class="logo">
            <span class="logo-mini"><b>Z</b>A</span>
            <span class="logo-lg"><span class="round-letter">Z</span><b>Academy</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </nav>
    </header>
    <aside class="main-sidebar">
        <div class="logo text-center">
            <img width="130px" src="assets/vendor/dist/img/logo-z.png">
            <div class="user-name"><?php echo $user['FullName']; ?></div>
        </div>
        <section class="sidebar">
            <?php $roleId = $user['RoleId'];
            $isMemberAdmin = $roleId == 1 || ($roleId == 2 && $user['IsMember'] == 2); ?>
            <ul class="sidebar-menu">
                <?php if($roleId == 1){ ?>
                    <li>
                        <a href="<?php echo base_url('userskill'); ?>">
                            <i class="fa fa-user"></i> <span>Hồ sơ Z</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                        </a>
                    </li>
                <?php } elseif($roleId == 2 && $user['IsMember'] == 2){ ?>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class="fa fa-user"></i> <span>Hồ sơ Z</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('userskill'); ?>"><i class="fa fa-circle-o"></i> Tìm kiếm hồ sơ Z</a></li>
                            <li><a href="<?php echo base_url('userskill/profile'); ?>"><i class="fa fa-circle-o"></i> Cập nhật hồ sơ Z</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li class="treeview">
                    <a href="javascript:void(0)">
                        <i class="fa fa-usd"></i> <span>Tài khoản</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo $roleId == 1 ? base_url('user/student') : base_url('charge'); ?>"><i class="fa fa-circle-o"></i> Tài khoản</a></li>
                        <li><a href="<?php echo base_url('transaction/log'); ?>"><i class="fa fa-history"></i> Lịch sử giao dịch</a></li>
                        <li><a href="<?php echo base_url('affiliate'); ?>"><i class="fa fa-globe"></i> Affiliate</a></li>
                        <?php if($isMemberAdmin){ ?>
                            <li><a href="<?php echo base_url('withdrawal'); ?>"><i class="fa fa-money"></i> Rút tiền</a></li>
                        <?php } ?>
                        <li><a href="<?php echo $roleId == 1 ? base_url('transaction') : base_url('proof'); ?>"><i class="fa fa-money"></i> Nạp tiền</a></li>
                        <?php if($roleId == 1){ ?>
                            <li><a href="<?php echo base_url('transaction/withdrawal'); ?>"><i class="fa fa-money"></i> Trừ tiền</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('register'); ?>">
                        <i class="fa fa-registered"></i> <span>Ghi danh</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="javascript:void(0)">
                        <i class="fa fa-shopping-cart"></i> <span>Chợ Z</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <?php if($isMemberAdmin){  ?>
                        <li><a href="<?php echo base_url('brand'); ?>"><i class="fa fa-bandcamp"></i> Nhãn hiệu</a></li>
                        <li><a href="<?php echo base_url('product'); ?>"><i class="fa fa-product-hunt"></i> Sản phẩm</a></li>
                        <li><a href="<?php echo base_url('promotion'); ?>"><i class="fa fa-gift"></i> Mã ưu đãi</a></li>
                        <li><a href="<?php echo base_url('order'); ?>"><i class="fa fa-id-card"></i><?php echo ($roleId == 1) ?  'Quản lý thương mại': 'Quản lý bán hàng'; ?></a></li>
                        <?php }
                        if($roleId == 2){ ?>
                        <li><a href="<?php echo base_url('order/add'); ?>"><i class="fa fa-handshake-o"></i> Mua hàng</a></li>
                        <li><a href="<?php echo base_url('order/mine'); ?>"><i class="fa fa-suitcase"></i> Đơn hàng của tôi</a></li>
                        <?php } ?>
                        <li>
                            <?php $tradeUrl = '';
                            $configs1 = $this->session->userdata('configs');
                            if($configs1){
                                if(isset($configs1['TRADE_URL'])) $tradeUrl = $configs1['TRADE_URL'];
                            } ?>
                            <a href="<?php echo $tradeUrl; ?>" target="_blank"><i class="fa fa-facebook"></i> Cộng đồng</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url('feedback'); ?>">
                        <i class="fa fa-question-circle"></i> <span>Hỗ trợ</span>
                        <?php if($roleId == 1){ ?>
                            <span class="label label-success pull-right-container" id="spanFeedbackCount" style="right: 40px !important;display: none;">0</span>
                            <input type="text" hidden="hidden" id="getFeedbackUnReadCountUrl" value="<?php echo base_url('feedback/getCountUnRead'); ?>">
                        <?php } ?>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('article'); ?>">
                        <i class="fa fa-newspaper-o"></i> <span>Tin tức</span>
                        <?php if($roleId == 2){ ?>
                            <span class="label label-success pull-right-container" id="spanArticleCount" style="right: 40px !important;display: none;">0</span>
                            <input type="text" hidden="hidden" id="getArticleUnReadCountUrl" value="<?php echo base_url('article/getCountUnRead'); ?>">
                        <?php } ?>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('notification'); ?>">
                        <i class="fa fa-bell-o"></i> <span>Thông báo</span>
                        <span class="label label-success pull-right-container" id="spanNotificationCount" style="right: 40px !important;display: none;">0</span>
                        <input type="text" hidden="hidden" id="getNotificationUnReadCountUrl" value="<?php echo base_url('notification/getCountUnRead'); ?>">
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                    </a>
                </li>
                 <?php //if($isMemberAdmin){ ?>
                <li class="treeview">
                    <a href="javascript:void(0)">
                        <i class="fa fa-circle-o"></i> <span>Dịch vụ</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('service'); ?>"><i class="fa fa-circle-o"></i> Tất cả dịch vụ</a></li>
                        <li><a href="<?php echo base_url('service/log'); ?>"><i class="fa fa-circle-o"></i> <?php echo $roleId == 1 ? 'Đăng ký dịch vụ' : 'Dịch vụ của tôi'; ?></a></li>
                        <li><a href="<?php echo base_url('service/notice'); ?>"><i class="fa fa-circle-o"></i> Thông báo dịch vụ</a></li>
                    </ul>
                </li>
                <?php //}// ?>
                <li class="treeview">
                    <a href="javascript:void(0)">
                        <i class="fa fa-cogs"></i> <span>Cài đặt</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('user/profile'); ?>"><i class="fa fa-user"></i> Thông tin cá nhân</a></li>
                        <li><a href="<?php echo base_url('user/secure'); ?>"><i class="fa fa-lock"></i> Bảo mật</a></li>
                        <?php if($roleId == 1){ ?>
                            <li><a href="<?php echo base_url('config1'); ?>"><i class="fa fa-cogs"></i> Cấu hình</a></li>
                            <li><a href="<?php echo base_url('user/admin'); ?>"><i class="fa fa-user"></i> Admin</a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php if($roleId == 1){ ?>
                    <li class="treeview">
                        <a href="javascript:void(0)">
                            <i class="fa fa-cogs"></i> <span>Tham số</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-right pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo base_url('skill'); ?>"><i class="fa fa-circle-o"></i> Kỹ năng</a></li>
                            <li><a href="<?php echo base_url('field'); ?>"><i class="fa fa-circle-o"></i> Lĩnh vực</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?php echo base_url('user/logout'); ?>">
                        <i class="fa fa-sign-out"></i> <span>Đăng xuất</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-right pull-right"></i>
                        </span>
                    </a>
                </li>
            </ul>
        </section>
    </aside>