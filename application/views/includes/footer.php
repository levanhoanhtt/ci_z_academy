<?php $tradeUrl = '';
      $configs1 = $this->session->userdata('configs');
      if($configs1){
      if(isset($configs1['TRADE_URL'])) $tradeUrl = $configs1['TRADE_URL'];
      } 
?>
<div id="footer" style="z-index:1;height:80px;background:#ffffffcf;">
    <div>
      <p class="str2"><?php echo $this->Mconfigs->getConfigValue('FOOTER_ID',0);?></p>
    </div>
</div>
<div id="footer" style="z-index:2;height: 80px;line-height:105px;background:#ffffff00">
    <div style="position: absolute;right: 0;">
		<a target="_blank" style="color:#00a65a" href="https://kienthucnen.com"><u><p class="menuktn">Môn học</p></u></a>
		<a target="_blank" style="color:#00a65a" href="https://drive.google.com/file/d/1imIHbpmcUnIz4EISQ8ndgKLYwHhitLaq/view"><u><p class="menuktn">Z-Partner</p></u></a>
		<img style="width: 35px;height: 50px;" src="assets/vendor/dist/img/ktn.png">
    </div>
    <div class="left" style="position: absolute;">
		<a href="<?php echo $tradeUrl; ?>" target="_blank">
		<img style="width: 25px;height: 25px;" src="assets/vendor/dist/img/facebook.png"></a>
	</div>
</div>
<?php $siteName = 'Z_Academy';
$email = 'minhngocbka@me.com';
$configs = $this->session->userdata('configs');
if($configs){
    if(isset($configs['SITE_NAME'])) $siteName = $configs['SITE_NAME'];
    if(isset($configs['EMAIL_COMPANY'])) $email = $configs['EMAIL_COMPANY'];
} ?>
<input type="text" hidden="hidden" id="rootPath" value="<?php echo ROOT_PATH; ?>">
<input type="text" hidden="hidden" id="siteName" value="<?php echo $siteName; ?>">
<input type="text" hidden="hidden" id="userImagePath" value="<?php echo USER_PATH; ?>">
<?php if($user){ ?>
    <input type="text" hidden="hidden" id="userLoginId" value="<?php echo $user['UserId']; ?>">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="<?php echo $user['FullName']; ?>">
    <input type="text" hidden="hidden" id="roleLoginId" value="<?php echo $user['RoleId']; ?>">
<?php } else { ?>
    <input type="text" hidden="hidden" id="userLoginId" value="0">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="">
    <input type="text" hidden="hidden" id="roleLoginId" value="0">
<?php } ?>
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/fastclick/fastclick.js"></script>
<script src="assets/vendor/dist/js/app.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.min.js"></script>
<script src="assets/vendor/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/marquee/marquee.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<?php if(isset($scriptFooter)) outputScript($scriptFooter); ?>
</body>
</html>