<div class="row">
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            Ngày mai 1 Zticket = <?php echo priceFormat(20683 + $configs['TICKET_COST']); ?> đ
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            +<?php echo priceFormat($configs['COST_PER_DAY']); ?> đ/ngày
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            Hôm nay 1 Zticket = <?php echo priceFormat($configs['TICKET_COST']); ?> đ
        </div>
    </div>
</div>
<div class="row mgt-30 mgbt-40">
    <div class="col-md-8 title-page">
        <img src="assets/vendor/dist/img/<?php echo isset($pageIcon) ? $pageIcon : 'currency-icon.png'; ?>">
        <h2 class="dp-il"><?php echo $title; ?></h2>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient"><?php echo date('d/m/Y'); ?></div>
    </div>
</div>