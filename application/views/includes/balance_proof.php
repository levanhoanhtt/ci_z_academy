<div class="row">
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            1 Zticket = <?php echo priceFormat($configs['TICKET_COST']); ?> đ
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            +<?php echo priceFormat($configs['COST_PER_DAY']); ?> đ/ngày
        </div>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient">
            Kho Zticket còn: <?php echo priceFormat($configs['TOTAL_TICKET'], true); ?>
        </div>
    </div>
</div>
<div class="row mgt-30 mgbt-40">
    <div class="col-md-8 title-page">
        <img src="assets/vendor/dist/img/<?php echo isset($pageIcon) ? $pageIcon : 'currency-icon.png'; ?>">
        <h2 class="dp-il"><?php echo $title; ?></h2>
    </div>
    <div class="col-md-4">
        <div class="small-box-n primary-gradient"><?php echo date('d/m/Y'); ?></div>
        <?php if($_SESSION['user']['RoleId'] == 1):?>
        <div class="small-box-n primary-gradient mgt-20"><a id="showPopupProof" style="color: white;display: block" href="javascript:;">Xem bằng chứng</a></div>
        <?php endif?>
    </div>
</div>
<div class="modal fade" id="proof" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background: #251b08;color: white">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Danh sách bằng chứng thanh toán</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Ngày tháng</th>
                        <th>Họ tên</th>
                        <th>Số điện thoại</th>
                        <th>Ảnh</th>
                        <th>Kiểu thanh toán</th>
                    </tr>
                    </thead>
                    <tbody id="tableContent">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="hidden" value="<?=base_url('/transaction/showProof')?>" id="urlShow">
                <input type="hidden" value="<?=base_url('/assets/uploads/images')?>" id="pathStore">
            </div>
        </div>
    </div>
</div>