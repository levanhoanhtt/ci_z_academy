<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="row mgt-25">
                <div class="col-md-8">
                    <table class="table table-bordered">
                        <tr>
                            <td width="240px" class="text-center"><img width="160px" src="assets/vendor/dist/img/bank1.png"></td>
                            <td class="bank-info" style="color:#42924d">
                                <p>Ngân hàng Vietcombank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 0611001919947</p>
                                <p>CN: Ba Đình - Hà Nội</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank2.png"></td>
                            <td class="bank-info" style="color:#214099">
                                <p>Ngân hàng BIDV</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 45010006070959</p>
                                <p>CN: Hà Tây</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank3.png"></td>
                            <td class="bank-info" style="color:#04519f">
                                <p>Ngân hàng Viettinbank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 108004649317</p>
                                <p>CN: Hai Bà Trưng - Hà Nội</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank4.png"></td>
                            <td class="bank-info" style="color:#c94242 ">
                                <p>Ngân hàng VP Bank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 61801898</p>
                                <p>CN: Thăng Long - Hà Nội</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank5.png"></td>
                            <td class="bank-info" style="color:#214099">
                                <p>Ngân hàng MB Bank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 0050100061888</p>
                                <p>CN: Trần Duy Hưng - Hà Nội</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank6.png"></td>
                            <td class="bank-info" style="color:#992540">
                                <p>Ngân hàng AgriBank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 2904205010220</p>
                                <p>CN: Kim Bảng - Hà Nam</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank7.jpg"></td>
                            <td class="bank-info" style="color:#214099">
                                <p>Ngân hàng Techcombank</p>
                                <p>CTK: Nguyễn Minh Ngọc</p>
                                <p>STK: 19133048258019</p>
                                <p>CN: Hoàng Gia - Hà Nội</p>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="col-md-4 bank-guide">
                    <p>
                        Nội dung chuyển khoản:<br>
                        “<b>số điện thoại nạp vào Z</b>”<br><br>
                        Ví dụ: <b>0978737871 nạp vào Z</b> <br><br>
                        Lưu ý: số điện thoại là số đăng kí tài
                        khoản thành viên
                    </p>
                    <br>
                    <img src="assets/vendor/dist/img/lock.png" class="img-responsive mgt-40 mgbt-40">

                    <p>Quá trình đối soát nhanh hơn nếu
                        bạn có gửi thêm bằng chứng thanh
                        toán.</p>
                    <a href="javascript:;" class="send-proof primary-gradient-re mgt-20"> Gửi bằng chứng</a>
                    <div class="modal fade" id="popupProof" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header" style="color: white">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Gửi bằng chứng thanh toán</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Hình thức trả</label>
                                        <?php $this->Mconstants->selectConstants('paymentTypes', 'PaymentTypeId', 1); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Hóa đơn thanh toán</label>
                                        <button class="text-center form-control" id="selectImage">Bấm để chọn tệp tin vào để tải lên</button>
                                        <div class="progress" id="progress" style="display: none">
                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <input type="file" style="display: none" id="inputImage">
                                    </div>
                                    <div>
                                        <span style="font-size: 10px">Chỉ hỗ trợ hình ảnh</span>
                                        <div class="text-center">
                                            <img src="" id="preView" style="width: 100%;height: auto"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="sendProof" type="button" class="btn btn-primary">Gửi</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                                    <input type="hidden" value="<?=base_url('proof/requestProof')?>" id="urlSend">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>