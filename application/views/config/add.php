<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
	<div class="container-fluid">
        <section class="content">
			<?php $this->load->view('includes/balance'); ?>
			<div class="row mgbt-40 mgt-40">
				<div class="text-center item">
					<label class="color-dc">Giá Zticket</label>
					<input class="input-arange input-number" type="text" value="<?php echo priceFormat($configs['TICKET_COST']); ?>" name="TicketCost" data-id="3">
					<a href="" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Khóa học hiện tại</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['COURSE_ID']; ?>" name="CourseId" data-id="4">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Ngày khai giảng</label>
					<input class="input-arange datepicker" type="text" value="<?php echo $configs['COURSE_DATE']; ?>" name="CourseDate"  data-id="12">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Mở bán thêm</label>
					<input class="input-arange input-number" type="text" value="0" data-id="5">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Số tiền tăng theo ngày</label>
					<input class="input-arange input-number" type="text" value="<?php echo priceFormat($configs['COST_PER_DAY']); ?>" name="CostPerDay" data-id="6">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Chia 1 Zticket thành</label>
					<input class="input-arange input-number" type="text" value="<?php echo priceFormat($configs['PIECES_PER_TICKET']); ?>" name="PiecesPerTicket" data-id="7">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<!--<div class="mgt-10 text-center item">
					<label class="color-dc">Thưởng triết khấu</label>
					<input class="input-arange input-number" type="text" value="<?php //echo priceFormat($configs['DISCOUNT_BONUS']); ?>" name="Discount_Bonus" data-id="8">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>-->
				<div class="mgt-10 text-center item">
					<label class="color-dc">Hệ số HP > 26 tuổi (%)</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['FACTOR_26']; ?>" name="Factor26"  data-id="9">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Affiliate (%)</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['AFF_PERCENT']; ?>" name="AffPercent"  data-id="14">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Hệ số Zticket Alpha (%)</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['ALPHA_VALUE']; ?>" name="AlphaValue"  data-id="16">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Hệ số Zticket Beta (%)</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['BETA_VALUE']; ?>" name="BetaValue"  data-id="17">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc">Hệ số Zticket Gamma (%)</label>
					<input class="input-arange input-number" type="text" value="<?php echo $configs['GAMMA_VALUE']; ?>" name="Gamma	Value"  data-id="18">
					<a href="javascript:void(0)" class="change_config btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<hr/>
				<div class="mgt-10 text-center item">
					<label class="color-dc" style="width:110px;">Email</label>
					<input class="input-arange" type="text" value="<?php echo $configs['EMAIL_COMPANY']; ?>" name="Email"  style="width: 60% !important"  data-id="1">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc" style="width:110px;">Trade URL</label>
					<input class="input-arange" type="text" value="<?php echo $configs['TRADE_URL']; ?>" name="TradeURL"  style="width: 60% !important"  data-id="10">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc" style="width:110px;">Video 2FA</label>
					<input class="input-arange" type="text" value="<?php echo $configs['VIDEO_2FA']; ?>" name="Video2FA"  style="width: 60% !important"  data-id="11">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<div class="mgt-10 text-center item">
					<label class="color-dc" style="width:110px;">Video HD</label>
					<input class="input-arange" type="text" value="<?php echo $configs['VIDEO_GUIDE']; ?>" name="VideoGuide"  style="width: 60% !important"  data-id="13">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
					<div class="mgt-10 text-center item">
					<label class="color-dc" style="width:110px;">Footer TB</label>
					<input class="input-arange" type="text" value="<?php echo $configs['FOOTER_ID']; ?>" name="FooterTB"  style="width: 60% !important"  data-id="15">
					<a href="javascript:void(0)" class="change_config_text btn btn-light-primary">Áp dụng</a>
					<div class="clearfix"></div>
				</div>
				<input type="text" hidden="hidden" id="updateConfigUrl" value="<?php echo base_url('config1/update'); ?>">
				<input type="text" hidden="hidden" id="updateConfigTextUrl" value="<?php echo base_url('config1/updateText'); ?>">
			</div>
			<div class="box box-default">
				<div style="display: none;">
					<?php echo form_open('config1'); ?>
					<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
					<input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
					<?php echo form_close(); ?>
				</div>
				<?php sectionTitleHtml('Lịch sử thay đổi', isset($paggingHtml) ? $paggingHtml : ''); ?>
				<div class="box-body table-responsive no-padding divTable">
					<table class="table table-hover table-bordered">
						<thead>
						<tr>
							<th>Mốc thời gian</th>
							<th>Thay đổi</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach ($listActionLogs as $v): ?>
							<tr>
								<td><?php echo ddMMyyyy($v['CrDateTime'], 'd/m/Y H:i'); ?></td>
								<td><?php echo $v['Comment']; ?></td>
							</tr>
						<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<?php $this->load->view('includes/pagging_footer'); ?>
			</div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>