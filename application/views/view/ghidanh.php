<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = 26.404.120 đ 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+21.890 đ/ngày 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: 124,03 
						</div>
					</div>
				</div>
				<div class="row mgt-30">
					<div class="col-md-6 title-page">
						<img src="assets/vendor/dist/img/sign-up.png">
						<h2 class="dp-il">Tài khoản</h2>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">01/02/2018</div>
					</div>
				</div>
				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<div class="medium-box-n boxst-1 success-gradient">
									<img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
									<div class="right-s">
										<h2 class="pd-15">Zticket | VNĐ 0 Đ</h2>
										<h2>0,00</h2>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="bg-box-gradient pd-15-25">
							<h3 class="title-form">
								GHI DANH
								<div class="pull-right gender">
									<div class="radio dp-il">
									  <label class="fz-20">
									    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
									    Dưới 26 tuổi
									  </label>
									</div>
									<div class="radio dp-il">
									  <label class="fz-20">
									    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
									    Trên 26 tuổi
									  </label>
									</div>
								</div>
							</h3>
							
							                  
							                
							                
							<div class="form-group mgt-10">
							    <label class="control-label">Họ và tên</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Ngày sinh</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Số chứng minh thư</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Số điện thoại</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>
							<div class="text-center">
								<button class="btn-n pd-10-30 fz-20 gradient-success-top">GHI DANH</button>
							</div>
						</div>
					</div>	
				</div>
        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>