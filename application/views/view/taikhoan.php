<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = <?php echo $ticketCost ?> VND
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+<?php echo $costPerDay ?> đ/ngày
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: <?php echo $ticketRemain ?>
						</div>
					</div>
				</div>
				<div class="row mgt-30">
					<div class="col-md-6 title-page">
						<img src="assets/vendor/dist/img/currency-icon.png">
						<h2 class="dp-il">Tài khoản</h2>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient"><?php echo date('d/m/Y') ?></div>
					</div>
				</div>
				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6">
								<div class="medium-box-n boxst-1 success-gradient">
									<img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
									<div class="right-s">
										<h2 class="pd-15">Zticket | <?php echo $user['TicketCount']*$ticketCost ?> VND</h2>
										<h2><?php echo $user['TicketCount']?></h2>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="medium-box-n boxst-1 primary-gradient">
									<img class="pull-left" width="60px" src="assets/vendor/dist/img/z-icon.png">
									<div class="right-s">
										<h2 class="pd-15">Zticket | VNĐ <?php echo $user['Balance']/$ticketCost ?> Đ</h2>
										<h2><?php echo $user['Balance']?> Đ</h2>
									</div>
									<button class="btn-n buy-ticket">Mua Zticket</button>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="bg-box-gradient pd-15-25">
							<h3 class="title-form">Bán Zticket</h3>
							<div class="form-group mgt-10">
							    <label class="control-label">Đến tài khoản (số điện thoại)</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Số lượng Zticket</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Mật khẩu</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>

							<div class="form-group mgt-10">
							    <label class="control-label">Mã 2FA</label>
							    <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
							</div>
							<div class="text-center">
								<button class="btn-n pd-10-30 fz-20">Bán</button>
							</div>
						</div>
					</div>	
				</div>
        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>