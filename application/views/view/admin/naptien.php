<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = 26.404.120 đ 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+21.890 đ/ngày 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: 124,03 
						</div>
					</div>
				</div>
				<div class="row mgt-30">
						<div class="col-md-6 title-page">
							<img src="assets/vendor/dist/img/cart-icon.png">
							<h2 class="dp-il">Nạp tiền</h2>
						</div>
						<div class="col-md-3">
							<div class="small-box-n primary-gradient">01/02/2018</div>
						</div>
					

				</div>

				<div class="row text-center mgt-25">
					<div class="col-md-9">
					<form>
						<p><input class="nt-input" type="text" placeholder="Số điện thoại" name=""></p>
						<p><input class="nt-input" type="text" placeholder="Số tiền" name=""></p>
						<p><button class="btn btn-primary bd-rd-10 pd-10-30 fz-18">Xác nhận</button></p>
					</form>
					</div>
				</div>

				<div class="row mgt-10">
					<div class="col-md-9">
						<h3 style="color:#777">Lịch sử</h3>
						<table class="table table-bordered table-striped mgt-30 pd-8">
							<thead>
								<tr>
									<th>Mốc thời gian</th>
									<th>Họ và tên</th>
									<th>Số điện thoại</th>
									<th>Tiêu đề</th>
									<th>Nội dung</th>
									<th>Trả lời</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>23:21:15 12/1/2018</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td>23:21:15 12/1/2018</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td>23:21:15 12/1/2018</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
							</tbody>
						</table>
					</div>	
				</div>
        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>