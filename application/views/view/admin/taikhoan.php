<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = 26.404.120 đ 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+21.890 đ/ngày 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: 124,03 
						</div>
					</div>
				</div>
				<div class="row mgt-30">
					<div class="col-md-6 title-page">
						<img src="assets/vendor/dist/img/currency-icon.png">
						<h2 class="dp-il">Danh sách tài khoản</h2>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">01/02/2018</div>
					</div>
				</div>
				<div class="row mgt-25">
					<div class="col-md-9">
						<table class="table table-bordered table-striped mgt-30 pd-8">
							<thead>
								<tr style="background: #007100;color:#fff">
									<th width="35px">STT</th>
									<th>Họ và tên</th>
									<th>Số điện thoại</th>
									<th>Tiêu đề</th>
									<th>Nội dung</th>
									<th>Trả lời</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center bg-green-stt">1</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">2</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">3</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">3</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">3</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">3</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
								<tr>
									<td class="text-center bg-green-stt">3</td>
									<td>Nguyễn Minh Ngọc</td>
									<td>0978737871</td>
									<td>Đổi lịch học</td>
									<td>Do công việc… </td>
									<td>Chờ trả lời</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>