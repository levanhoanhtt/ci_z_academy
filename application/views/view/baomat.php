<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = 26.404.120 đ 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+21.890 đ/ngày 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: 124,03 
						</div>
					</div>
				</div>
				<div class="row mgt-30">
					<div class="col-md-6 title-page">
						<img src="assets/vendor/dist/img/key-yl-icon.png">
						<h2 class="dp-il">Cài đặt</h2>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">01/02/2018</div>
					</div>
				</div>

				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="bg-box-gradient-re pd-15-25">
							<h3 class="title-form">Account Security</h3>
							<p class="mgbt-40">Two Factor Authentication</p>
							   
							    <div class="form-group">
							        <div class="row">
							        	<label for="enable2fa" class="control-label col-md-3 text-white">Enable Two-Factor</label>
							        	<div class="col-md-9">
							        	    <label class="radio-inline text-white">
							        	        <input name="enable2fa" type="radio" value="1" id="enable2fa"> On</label>
							        	    <label class="radio-inline text-white">
							        	        <input checked="checked" name="enable2fa" type="radio" value="0" id="enable2fa"> Off</label>
							        	</div>
							        </div>
							    </div>

							
							<div class="form-group">
								<div class="row">
							    <label for="2fa" class="control-label col-md-3 text-white">Enter 2FA Code</label>
							    <div class="col-md-9">
							        <div class="input-group">
							            <span class="input-group-addon"><i class="fa fa-qrcode fa-lg"></i></span>
							            <input class="form-control" name="one_time_password" type="text">
							        </div>
							    </div>
							    </div>
							</div>
							
								<div class="row">
									<div class="col-md-offset-3 col-md-3">
										<button class="btn btn-primary">Submit</button>
										<button class="btn btn-default">Cancel</button>
									</div>
								</div>
								
							
							<div class="clearfix"></div>
						</div>
					</div>	
				</div>
        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>