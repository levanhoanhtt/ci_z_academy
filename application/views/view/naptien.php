<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <section class="content">
				<div class="row">
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							1 Zticket = 26.404.120 đ 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							+21.890 đ/ngày 
						</div>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">
							Kho Zticket còn: 124,03 
						</div>
					</div>
				</div>
				<div class="row mgt-30">
					<div class="col-md-6 title-page">
						<img src="assets/vendor/dist/img/cart-icon.png">
						<h2 class="dp-il">Nạp tiền</h2>
					</div>
					<div class="col-md-3">
						<div class="small-box-n primary-gradient">01/02/2018</div>
					</div>
				</div>
				<div class="row mgt-25">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-8">
								<table class="table table-bordered">
									<tr>
										<td width="240px" class="text-center"><img width="160px" src="assets/vendor/dist/img/bank1.png"></td>
										<td class="bank-info" style="color:#42924d">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>
									<tr>
										<td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank2.png"></td>
										<td class="bank-info" style="color:#214099">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>
									<tr>
										<td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank3.png"></td>
										<td class="bank-info" style="color:#04519f">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>
									<tr>
										<td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank4.png"></td>
										<td class="bank-info" style="color:#c94242 ">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>
									<tr>
										<td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank5.png"></td>
										<td class="bank-info" style="color:#214099">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>
									<tr>
										<td class="text-center"><img width="160px" src="assets/vendor/dist/img/bank6.png"></td>
										<td class="bank-info" style="color:#992540">
											<p>Ngân hàng Vietcombank</p>
											<p>CTK: Nguyễn Minh Ngọc</p>
											<p>STK: 9123091092829</p>
											<p>CN: Ba Đình - Hà Nội</p>
										</td>
									</tr>

								</table>
							</div>
							<div class="col-md-4 bank-guide">
								<p>
									Nội dung chuyển khoản:<br>
									“<b>số điện thoại, KTN</b>”<br><br>
									Ví dụ: <b>0978737871, KTN</b> <br><br>
									Lưu ý: số điện thoại là số đăng kí tài
									khoản thành viên
								</p>
								<br>
								<img src="assets/vendor/dist/img/lock.png" class="img-responsive mgt-40 mgbt-40">
								

								<p>Quá trình đối soát nhanh hơn nếu
								bạn có gửi thêm bằng chứng thanh
								toán.</p>

								<a href="#" class="send-proof primary-gradient-re mgt-20"> Gửi bằng chứng</a>
							</div>
						</div>
					</div>
				</div>

				
        </section>
    </div>
<?php $this->load->view('includes/footer'); ?>