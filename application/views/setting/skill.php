<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="box box-success">
                <?php echo form_open('skill/update', array('id' => 'skillForm')); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Kỹ năng</th>
                            <th>Loại</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody id="tbodySkill">
                        <?php $labelCss = $this->Mconstants->labelCss;
                        $skillTypes = $this->Mconstants->skillTypes;
                        foreach($listSkills as $s){ ?>
                            <tr id="skill_<?php echo $s['SkillId']; ?>">
                                <td id="skillName_<?php echo $s['SkillId']; ?>"><?php echo $s['SkillName']; ?></td>
                                <td id="skillTypeName_<?php echo $s['SkillId']; ?>"><span class="<?php echo $labelCss[$s['SkillTypeId']]; ?>"><?php echo $skillTypes[$s['SkillTypeId']]; ?></span></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $s['SkillId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $s['SkillId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    <input type="text" hidden="hidden" id="skillTypeId_<?php echo $s['SkillId']; ?>" value="<?php echo $s['SkillTypeId']; ?>">
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td><input type="text" class="form-control hmdrequired" id="skillName" name="SkillName" value="" data-field="Kỹ năng"></td>
                            <td><?php $this->Mconstants->selectConstants('skillTypes', 'SkillTypeId'); ?></td>
                            <td class="actions">
                                <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                <input type="text" name="SkillId" id="skillId" value="0" hidden="hidden">
                                <input type="text" id="deleteSkillUrl" value="<?php echo base_url('skill/delete'); ?>" hidden="hidden">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php echo form_close(); ?>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>