<div class="box box-default padding15">
    <div class="box-header with-border">
        <h3 class="box-title">Khởi tạo mã ưu đãi</h3>
    </div>
    <?php echo form_open('promotion/update', array('id' => 'promotionForm')); ?>
    <div class="box-body table-responsive no-padding divTable">
        <div class="box-body table-responsive no-padding divTable">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Nhãn hiệu</th>
                    <th>Giảm giá</th>
                    <th>SL</th>
                    <th>Hiệu lực từ ngày</th>
                    <th>Ngày hết hiệu lực</th>
                    <th>Hành động</th>
                </tr>
                </thead>
                <tbody id="tbodyPromotion">
                
                    <?php echo form_open('promotion/update', array('id' => 'productPromotion')); ?>
                    <tr>
                        <th><?php $this->Mconstants->selectObject($listProductsIsActive, 'ProductId', 'ProductName', 'PromotionProductId',0,true,'Lựa chọn sản phẩm'); ?></th>
                        <th><?php //if(count($listProductsIsActive) > 0) echo $this->Mbrands->get($listProductsIsActive[0]['BrandId'])['BrandName'] ?></th>
                        <th><input type="number" class="form-control hmdrequired" id="discountCost" name="DiscountCost" value="" data-field="Giảm Giá"></th>
                        <th><input type="number" class="form-control hmdrequired" id="quantity" name="Quantity" value="" data-field="Số Lượng"></th>
                        <th>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker hmdrequired" name="BeginDate" id="beginDate" value="<?php echo set_value('BeginDate'); ?>" data-field="Ngày bắt đầu" autocomplete="off">
                            </div>
                        </th>
                        <th><div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control datepicker hmdrequired" name="EndDate" id="endDate" value="<?php echo set_value('BeginDate'); ?>" data-field="Ngày kết thúc" autocomplete="off">
                            </div>
                        </th>
                        <th>
                            <input type="submit" class="btn btn-primary" id="createPromotion" value="Khởi tạo & Tải về">
                        </th>
                    </tr>
                    <?php echo form_close(); ?>
               
                </tbody>
            </table>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<input type="text" hidden="hidden" id="updatePromotionUrl" value="<?php echo base_url('promotion/update'); ?>">
