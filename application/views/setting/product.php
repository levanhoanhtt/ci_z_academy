<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <?php $roleId = $user['RoleId']; ?>
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
        </section>
        <section class="content">
            <?php $this->load->view('setting/brand') ?>

            <div class="box box-default padding15">
                <div class="box-header with-border">
                    <h3 class="box-title">Khởi tạo sản phẩm</h3>
                </div>
                <?php echo form_open('product/update', array('id' => 'productForm')); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th width="50px">STT</th>
                                <th>Thêm sản phẩm</th>
                                <th>Nhãn hiệu</th>
                                <th width="150px">Giá</th>
                                <th>Link nhúng thanh toán</th>
                                <th>Hành động</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct">
                            <?php
                            $itemStatus = $this->Mconstants->itemStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listProducts as $k => $pr){?>
                                <tr id="product_<?php echo $pr['ProductId']; ?>">
                                    <td><?php echo $k + 1 ?></td>
                                    <td id="productName_<?php echo $pr['ProductId'];?>"><?php echo $pr['ProductName']; ?></td>
                                    <td id="productBrandName_<?php echo $pr['ProductId'];?>"><?php echo $this->Mbrands->get($pr['BrandId'],true)['BrandName'] ?></td>
                                    <td id="price_<?php echo $pr['ProductId'];?>"><?php echo number_format($pr['Price']) ?></td>
                                    <td></td>
                                    <td class="actions">
                                        <?php if($pr['ItemStatusId'] == 1){ ?>
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $pr['ProductId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $pr['ProductId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <?php }
                                        elseif($pr['ItemStatusId'] > 0){ ?>
                                            <div class="btn-group" id="btnGroup_<?php echo $pr['ProductId']; ?>">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                                <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $pr['ProductId']; ?>" data-status="3">Ngừng kinh doanh</a></li>
                                                        <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $pr['ProductId']; ?>" data-status="2">Tiếp tục kinh doanh</a></li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                        <input type="hidden" id="productBrandId_<?php  echo $pr['ProductId']; ?>" value="<?php echo $pr['BrandId'] ?>">
                                    </td>
                                    <td id="statusName_<?php echo $pr['ProductId']; ?>"><span class="<?php echo $labelCss[$pr['ItemStatusId']]; ?>"><?php echo $itemStatus[$pr['ItemStatusId']]; ?></span></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td><input type="text" class="form-control hmdrequired" id="productName" name="ProductName" value="" data-field="Tên sản phẩm"></td>
                                <td><?php $this->Mconstants->selectObject($listBrandsIsActive, 'BrandId', 'BrandName', 'ProductBrandId'); ?></td>
                                <td><input type="number" class="form-control hmdrequired" id="price" name="Price" value="" data-field="Giá"></td>
                                <td></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductId" id="productId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductUrl" value="<?php echo base_url('product/delete'); ?>" hidden="hidden">
                                    <input type="text" id="changeProductUrl" value="<?php echo base_url('product/changeStatus'); ?>" hidden="hidden">
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <input type="text" hidden="hidden" id="updateProductUrl" value="<?php echo base_url('product/update'); ?>">
            

            <?php $this->load->view('setting/promotion') ?>


        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>

