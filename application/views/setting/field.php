<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <?php $this->load->view('includes/balance'); ?>
            <div class="box box-success">
                <?php echo form_open('field/update', array('id' => 'fieldForm')); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Lĩnh vực</th>
                            <th>Hành động</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyField">
                        <?php foreach($listFields as $s){ ?>
                            <tr id="field_<?php echo $s['FieldId']; ?>">
                                <td id="fieldName_<?php echo $s['FieldId']; ?>"><?php echo $s['FieldName']; ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $s['FieldId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $s['FieldId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td><input type="text" class="form-control hmdrequired" id="fieldName" name="FieldName" value="" data-field="Lĩnh vực"></td>
                            <td class="actions">
                                <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                <input type="text" name="FieldId" id="fieldId" value="0" hidden="hidden">
                                <input type="text" id="deleteFieldUrl" value="<?php echo base_url('field/delete'); ?>" hidden="hidden">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php echo form_close(); ?>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>