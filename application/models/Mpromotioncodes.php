<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpromotioncodes extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "promotioncodes";
        $this->_primary_key = "PromotionCodeId";
    }
}