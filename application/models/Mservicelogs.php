<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservicelogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "servicelogs";
        $this->_primary_key = "ServiceLogId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND UserId=".$postData['UserId'];
        if(isset($postData['ServiceId']) && $postData['ServiceId'] > 0) $query.=" AND ServiceId=".$postData['ServiceId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime <= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime >= '{$postData['EndDate']}'";
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM servicelogs WHERE 1=1" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }
}