<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marticles extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "articles";
        $this->_primary_key = "ArticleId";
    }

    public function getCount($postData){
        $query = "ArticleStatusId > 0 " . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM  articles WHERE ArticleStatusId > 0" . $this->buildQuery($postData).' ORDER BY PublishDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ArticleTitle']) && !empty($postData['ArticleTitle'])) $query.=" AND ArticleTitle LIKE '%{$postData['ArticleTitle']}%'";
        if(isset($postData['ArticleStatusId']) && $postData['ArticleStatusId'] > 0)  $query.=" AND ArticleStatusId=".$postData['ArticleStatusId'];
        if(isset($postData['CrUserId']) && $postData['CrUserId'] > 0)  $query.=" AND CrUserId=".$postData['CrUserId'];
        if(isset($postData['RoleId']) && $postData['RoleId'] == 2 && isset($postData['UserId']) && $postData['UserId'] > 0){
            $query .= " AND (IsPublic = 2 OR ArticleId IN(SELECT ArticleId FROM articleusers WHERE UserId = {$postData['UserId']}))";
            if(isset($postData['IsGetCountUnRead']) && $postData['IsGetCountUnRead'] == 1) $query .= " AND ArticleId NOT IN(SELECT ArticleId FROM articlereads WHERE UserId = {$postData['UserId']})";
        }
        return $query;
    }

    public function update($postData, $articleId, $userIds = array()){
        $isUpdate = $articleId > 0 ? true : false;
        $this->db->trans_begin();
        $articleId = $this->save($postData, $articleId);
            if($articleId > 0){
                if($isUpdate) $this->db->delete('articleusers', array('ArticleId' => $articleId));
                if($postData['IsPublic'] == 1 && !empty($userIds)){
                    $articleUsers = array();
                    foreach($userIds as $userId) $articleUsers[] = array('ArticleId' => $articleId, 'UserId' => $userId);
                    if(!empty($articleUsers)) $this->db->insert_batch('articleusers', $articleUsers);
                }
            }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $articleId;
        }
    }
}
