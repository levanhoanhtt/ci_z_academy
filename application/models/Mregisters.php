<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mregisters extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "registers";
        $this->_primary_key = "RegisterId";
    }

    public function getCount($postData){
        $query = "RegisterStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SearchOrUser']) && $postData['SearchOrUser'] == 1){
            if (isset($postData['StudentId']) && $postData['StudentId'] > 0 && isset($postData['ByUserId']) && $postData['ByUserId'] > 0) $query .= " AND (StudentId={$postData['StudentId']} OR ByUserId={$postData['ByUserId']})";
        }
        else {
            if (isset($postData['StudentId']) && $postData['StudentId'] > 0) $query .= " AND StudentId=" . $postData['StudentId'];
            if (isset($postData['ByUserId']) && $postData['ByUserId'] > 0) $query .= " AND ByUserId=" . $postData['ByUserId'];
        }
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (StudentName LIKE '%{$postData['SearchText']}%' OR PhoneNumber LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['RegisterStatusId']) && $postData['RegisterStatusId'] > 0) $query.=" AND RegisterStatusId=".$postData['RegisterStatusId'];
		if(isset($postData['CourseTypeId'])&& $postData['CourseTypeId'] > 0 ) $query.=" AND CourseTypeId=".$postData['CourseTypeId'];
        if(isset($postData['CourseId'])){
            if($postData['CourseId'] > 0) $query.=" AND CourseId=".$postData['CourseId'];
            elseif(!empty($postData['CourseId'])) $query.=" AND CourseId = 0";
        }
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM registers WHERE RegisterStatusId > 0" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function update($postData, $registerId, $actionLogs = array(), $user = array(), $student = false){
        $courseTypeName = '('.$this->Mconstants->courseTypes[$postData['CourseTypeId']].')';
        $isInsert = $registerId == 0;
        $this->db->trans_begin();
        $registerId = $this->save($postData, $registerId);
        if($registerId > 0){
            if(!empty($actionLogs)) $this->Mactionlogs->save($actionLogs);
            if($isInsert){
                $ticketCount = $postData['TicketCount'];
                //if($user['IsMember'] == 2) $ticketCount = $ticketCount - 0.05;
                $this->db->query('UPDATE users SET TicketCount = TicketCount - ? WHERE UserId = ?', array($ticketCount, $user['UserId']));
                if(!$student) $comment = $user['FullName']." đã đăng ký khóa {$postData['CourseId']}{$courseTypeName}, {$user['FullName']} ({$user['PhoneNumber']}) bị trừ {$ticketCount} Zticket";
                else $comment = $user['FullName']." đã đăng ký khóa {$postData['CourseId']}{$courseTypeName} cho {$student['FullName']} ({$student['PhoneNumber']}), {$user['FullName']} ({$user['PhoneNumber']}) bị trừ {$ticketCount} Zticket";
                $this->Mtransactionlogs->save(array(
                    'UserId' => $user['UserId'],
                    'ByUserId' => $user['UserId'],
                    'LogTypeId' => 2,
                    'Amount' => $ticketCount,
                    'Balance' => 0,
                    'TicketCount' => $user['TicketCount'] -  $ticketCount,
                    'Comment' => $comment,
                    'CrDateTime' => $postData['CrDateTime']
                ));
                if(!$student) $comment = "Tài khoản {$user['FullName']} ({$user['PhoneNumber']}) vừa ghi danh khoá ".$postData['CourseId'].$courseTypeName;
                else $comment = "Tài khoản {$user['FullName']} ({$user['PhoneNumber']}) vừa ghi danh khoá {$postData['CourseId']}{$courseTypeName} cho {$student['FullName']} ({$student['PhoneNumber']})";
                $this->Mnotifications->save(array(
                    'StudentId' => $user['UserId'],
                    'AdminId' => ADMIN_ID,
                    'IsRead' => 1,
                    'IsFromStudent' => 2,
                    'Message' => $comment,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $postData['CrDateTime']
                ));
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function cancel($register, $user, $curentTicketCount, $notifications){
        $this->db->trans_begin();
        $flag = $this->changeStatus(0, $register['RegisterId'], 'RegisterStatusId', $user['UserId']);
        if($flag){
            $ticketCount = $register['TicketCount'];
            //if($register['IsMember'] == 2) $ticketCount = $ticketCount - 0.05;
            $this->db->query('UPDATE users SET TicketCount = TicketCount + ? WHERE UserId = ?', array($ticketCount, $register['ByUserId']));
            $this->Mtransactionlogs->save(array(
                'UserId' => $register['ByUserId'],
                'ByUserId' => $user['UserId'],
                'LogTypeId' => 2,
                'Amount' => $ticketCount,
                'Balance' => 0,
                'TicketCount' => $curentTicketCount,
                'Comment' => $notifications['Message'],
                'CrDateTime' => getCurentDateTime()
            ));
            $this->Mnotifications->save($notifications);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}
