<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserskills extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "userskills";
        $this->_primary_key = "UserSkillId";
    }

    public function getListByIds($userSkillIds){
        return $this->getByQuery('SELECT * FROM userskills WHERE UserSkillId IN ?', array($userSkillIds));
    }

    public function update($skillId,$userId,$skillLevelId)
    {
    	// $this->getByQuery('DELETE FROM userskills WHERE UserId = ? AND SkillId = ?',array($userId,$skillId));
		$this->load->model('Muserskills');
		$this->Muserskills->deleteMultiple(array('UserId' => $userId,'SkillId' => $skillId));
    	$data = array(
    		'UserId' => $userId,
    		'SkillId' => $skillId,
    		'SkillLevelId' => $skillLevelId,
    		'CrDateTime' => getCurentDateTime(),
    	);
    	$UserSkillId = $this->Muserskills->save($data,0);
    	return $UserSkillId;
    }
}