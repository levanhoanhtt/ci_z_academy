<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //USER
    /*public $roles = array(
        1 => 'Admin',
        2 => 'Học viên'
    );*/

    public $status = array(
        2 => 'Đã duyệt',
        1 => 'Chưa duyệt',
        3 => 'Block'
    );

    public $itemStatus = array(
        2 => 'Đã duyệt',
        1 => 'Chờ duyệt',
        3 => 'Ngừng kinh doanh',
        4 => 'Không được duyệt'
    );

    public $withdrawalStatus = array(
        1 => 'Chờ xử lý',
        2 => 'Đang xử lý',
        3 => 'Thành công',
        4 => 'Thất bại',
        5 => 'Hủy'
    );

    public $sellerOrderStatus = array(
        1 => 'Không vấn đề',
        2 => 'Đã giao hàng',
        3 => 'Chưa giao hàng',
        4 => 'Hết hàng',
        5 => 'Đã hủy đơn',
        6 => 'Đồng ý hoàn tiền'
    );

    public $customerOrderStatus = array(
        1 => 'Không vấn đề',
        2 => 'Chậm hàng',
        3 => 'Đã hài lòng',
        4 => 'Yêu cầu hoàn tiền',
        5 => 'Hủy đơn'
    );

    public $zOrderStatus = array(
        1 => 'Không vấn đề',
        2 => 'Mâu thuẫn',
        3 => 'Đóng thương vụ'
    );

    /*public $isVerifyEmails = array(
        1 => 'Chưa xác minh',
        2 => 'Đã xác minh',
        3 => 'Đang gửi xác minh'
    );*/

    public $feedbackStatus1 = array(
        1 => 'Chờ xử lý',
        2 => 'Đã xử lý'
    );

    public $feedbackStatus2 = array(
        1 => 'Chờ trả lới',
        2 => 'Đã trả lời'
    );

    public $paymentTypes = array(
        1 => 'Internet Banking',
        2 => 'Mobile Banking (App trên iPhone/ Android)',
        3 => 'Bank Plus (Thuê bao Viettel)',
        4 => 'Nộp tiền mặt vào tài khoản ngân hàng'
    );

    /*public $chargeTypes = array(
        1 => 'Bán Zticket',
        2 => 'Mua Zticket',
        3 => 'Hoàn lại Zticket',
        4 => 'Chuyển tiền'
    );*/

    public $ageTypes = array(
        1 => 'Dưới 26',
        2 => 'Trên 26'
    );

    public $registerStatus = array(
        2 => 'Thành công',
        1 => 'Xóa và hoàn lại Zticket'
    );

    public $courseTypes = array(
        1 => 'Alpha',
        2 => 'Beta',
        3 => 'Gamma'
    );

    /*public $itemTypes = array(
        1 => 'Config',
        2 => 'Register',
        3 => 'Transaction'
    );*/

    public $skillLevels = array(
        1 => 'Trung bình',
        2 => 'Khá',
        3 => 'Tốt'
    );

    public $skillTypes = array(
        1 => 'Kỹ năng công nghệ',
        2 => 'Kỹ năng mềm'
    );

    public $articleStatus = array(
        1 => 'Chưa xuất bản',
        2 => 'Xuất bản',
        3 => 'Hủy',
        4 => 'Chờ xuất bản'
    );

    public $serviceTypes = array(
        1 => 'Theo ngày (không gia hạn)', //phai co ngay ket thuc, ko co gia han
        2 => 'Theo tháng (gia hạn vào ngày đăng ký)',
        3 => 'Theo tháng (gia hạn vào đầu tháng)',
    );

    public $serviceStatus = array(
        1 => 'Chưa đến thời điểm sử dụng dịch vụ',
        2 => 'Đang sử dụng dịch vụ',
        3 => 'Tự tạm dừng dịch vụ',
        //4 => 'Tạm dừng do không đủ tiền', //bo
        5 => 'Hết hạn dịch vụ',
        6 => 'ⓩ tạm dừng dịch vụ của bạn'
    );

    public $logTypes = array(
        1 => 'Nạp tiền',
        2 => 'Ghi danh',
        3 => 'Mua Zticket từ ⓩ ',
        4 => 'Mua - Bán Zticket',
        5 => 'Chuyển tiền',
        6 => 'Dịch vụ',
        7 => 'Trừ tiền',
        8 => 'Affiliate',
        12 => 'Affiliate Zticket',
        9 => 'Rút tiền',
        10 => 'Bán hàng',
        11 => 'Đóng băng'
    );

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        7 => 'label label-success',
        6 => 'label label-warning',
        8 => 'label label-danger',
        9 => 'label label-default',
        10 => 'label label-success',
        11 => 'label label-warning',
        12 => 'label label-danger'
    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả'){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'">';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue) return $obj[$objKeyReturn];
        }
        return '';
    }
}