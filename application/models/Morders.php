<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morders extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "orders";
        $this->_primary_key = "OrderId";
    }

    public $labelCss = array(
        'SellerOrderStatus' => array(
            1 => 'label label-default',
            2 => 'label label-success',
            3 => 'label label-warning',
            4 => 'label label-orange',
            5 => 'label label-danger',
            6 => 'label label-green'
        ),
        'CustomerOrderStatus' => array(
            1 => 'label label-default',
            2 => 'label label-warning',
            3 => 'label label-success',
            4 => 'label label-orange',
            5 => 'label label-danger'
        ),
        'ZOrderStatus' => array(
            1 => 'label label-default',
            2 => 'label label-danger',
            3 => 'label label-success'
        )
    );

    public function getCount($postData, $joinTypeId = 1){
        if($joinTypeId == 1) $query = "SELECT OrderId FROM orders LEFT JOIN users ON orders.CustomerId = users.UserId WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData);
        elseif($joinTypeId == 2) $query = "SELECT OrderId FROM orders LEFT JOIN users ON orders.SellerId = users.UserId WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData);
        else $query = "SELECT OrderId FROM orders WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData);
        return count($this->getByQuery($query));
    }

    public function search($postData, $perPage = 0, $page = 1, $joinTypeId = 1){
        if($joinTypeId == 1) $query = "SELECT orders.*, users.FullName, users.PhoneNumber, users.RefundTime FROM orders LEFT JOIN users ON orders.CustomerId = users.UserId WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY orders.CrDateTime DESC';
        elseif($joinTypeId == 2) $query = "SELECT orders.*, users.FullName, users.PhoneNumber, users.RefundTime FROM orders LEFT JOIN users ON orders.SellerId = users.UserId WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY orders.CrDateTime DESC';
        else $query = "SELECT * FROM orders WHERE SellerOrderStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY CrDateTime DESC';
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.="  AND (users.FullName LIKE '%{$postData['SearchText']}%' OR users.PhoneNumber LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['SellerOrderStatusId']) && $postData['SellerOrderStatusId'] > 0) $query.=" AND orders.SellerOrderStatusId=".$postData['SellerOrderStatusId'];
        if(isset($postData['CustomerOrderStatusId']) && $postData['CustomerOrderStatusId'] > 0) $query.=" AND orders.CustomerOrderStatusId=".$postData['CustomerOrderStatusId'];
        if(isset($postData['ZOrderStatusId']) && $postData['ZOrderStatusId'] > 0) $query.=" AND orders.ZOrderStatusId=".$postData['ZOrderStatusId'];
        if(isset($postData['ProductId']) && $postData['ProductId'] > 0) $query.=" AND orders.ProductId=".$postData['ProductId'];
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND orders.CustomerId=".$postData['CustomerId'];
        if(isset($postData['SellerId']) && $postData['SellerId'] > 0) $query.=" AND orders.SellerId=".$postData['SellerId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND orders.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND orders.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function add($postData, $metaData){
        $this->db->trans_begin();
        $orderId = $this->save($postData, 0, array('UpdateUserId', 'UpdateDateTime'));
        if($orderId > 0){
            $orderCode = $this->genOrderCode($orderId);
            $this->db->update('orders', array('OrderCode' => $orderCode), array('OrderId' => $orderId));
            if($metaData['PromotionCodeId'] > 0) $this->db->update('promotioncodes', array('OrderId' => $orderId), array('PromotionCodeId' => $metaData['PromotionCodeId']));
            $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($metaData['SumCost'], $postData['CustomerId']));
            $this->db->query('UPDATE users SET Balance = Balance + ? WHERE UserId = ?', array($metaData['SumCost'], $postData['SellerId']));
            if(!empty($metaData['Notifications'])){
                for($i = 0; $i < count($metaData['Notifications']); $i++){
                    $metaData['Notifications'][$i]['Message'] = str_replace('{ORDER_CODE}', $orderCode, $metaData['Notifications'][$i]['Message']);
                }
                $this->db->insert_batch('notifications', $metaData['Notifications']);
            }
            $logs = array(
                array(
                    'UserId' => $postData['CustomerId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 10,
                    'Amount' => $metaData['SumCost'],
                    'Balance' => $metaData['Customer']['Balance'] - $metaData['SumCost'],
                    'TicketCount' => 0,
                    'Comment' => "{$metaData['Customer']['FullName']} ({$metaData['Customer']['PhoneNumber']}) thanh toán " . priceFormat($metaData['SumCost']) . " VNĐ cho đơn hàng ".$orderCode,
                    'CrDateTime' => $postData['CrDateTime']
                ),
                array(
                    'UserId' => $postData['SellerId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 10,
                    'Amount' => $metaData['SumCost'],
                    'Balance' => $metaData['Seller']['Balance'] + $metaData['SumCost'],
                    'TicketCount' => 0,
                    'Comment' => "{$metaData['Seller']['FullName']} ({$metaData['Seller']['PhoneNumber']}) được cộng " . priceFormat($metaData['SumCost']) . " VNĐ từ đơn hàng ".$orderCode,
                    'CrDateTime' => $postData['CrDateTime']
                )
            );
            $this->db->insert_batch('transactionlogs', $logs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $orderId;
        }
    }

    public function changeOrderStatus($postData, $orderId, $transactionLogs, $metaData){
        $this->db->trans_begin();
        $this->save($postData, $orderId);
        if($metaData['UpdateBalanceTypeId'] == 1){
            $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['SellerId']));
            $this->db->query('UPDATE users SET Balance = Balance + ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['CustomerId']));
        }
        elseif($metaData['UpdateBalanceTypeId'] == 2){
            $this->db->query('UPDATE users SET BalanceSuspend = BalanceSuspend - ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['SellerId']));
            $this->db->query('UPDATE users SET Balance = Balance + ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['CustomerId']));
        }
        elseif($metaData['UpdateBalanceTypeId'] == 3){
            $this->db->query('UPDATE users SET BalanceSuspend = BalanceSuspend + ?, Balance = Balance - ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['OrderCost'], $metaData['SellerId']));
            if(isset($postData['CustomerOrderStatusId']) && $postData['CustomerOrderStatusId'] == 4){
                $this->db->query('UPDATE users SET RefundTime = RefundTime + 1 WHERE UserId = ?', array($metaData['CustomerId']));
            }
        }
        elseif($metaData['UpdateBalanceTypeId'] == 4){
            $this->db->query('UPDATE users SET BalanceSuspend = BalanceSuspend - ?, Balance = Balance + ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['OrderCost'], $metaData['SellerId']));
        }
        if(!empty($transactionLogs)) $this->db->insert_batch('transactionlogs', $transactionLogs);
        if(!empty($metaData['Notifications'])) $this->db->insert_batch('notifications', $metaData['Notifications']);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    private function genOrderCode($orderId){
        return 'DH-' . ($orderId + 10000);
    }
}