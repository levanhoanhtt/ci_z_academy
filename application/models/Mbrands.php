<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbrands extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "brands";
        $this->_primary_key = "BrandId";
    }

    public function checkExist($brandName, $userId, $brandId = 0){
        $query = "SELECT BrandId FROM brands WHERE BrandId != ? AND ItemStatusId > 0 AND BrandName = ? AND UserId = ? LIMIT 1";
        $brands = $this->getByQuery($query, array($brandId, $brandName, $userId));
        if(!empty($brands)) return true;
        return false;
    }

    public function getCount($postData){
        $query = "ItemStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM brands WHERE ItemStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY BrandId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['BrandName']) && !empty($postData['BrandName'])) $query.=" AND BrandName LIKE '%{$postData['BrandName']}%'";
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND UserId=".$postData['UserId'];
        if(isset($postData['ItemStatusId']) && $postData['ItemStatusId'] > 0) $query.=" AND ItemStatusId=".$postData['ItemStatusId'];
        return $query;
    }

    public function update($postData, $brandId, $notifications = array()){
        $isUpdate = $brandId > 0;
        $this->db->trans_begin();
        $brandId = $this->save($postData, $brandId);
        if($brandId > 0){
            if($isUpdate){
                if(isset($postData['ItemStatusId']) && $postData['ItemStatusId'] != STATUS_ACTIVED){
                    $this->db->update('products', array('ItemStatusId' => $postData['ItemStatusId'], 'UpdateUserId' => $postData['UpdateUserId'], 'UpdateDateTime' => $postData['UpdateDateTime']), array('BrandId' => $brandId, 'ItemStatusId != ' => 1));
                }
            }
            if(!empty($notifications)) $this->Mnotifications->save($notifications);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $brandId;
        }
    }
}