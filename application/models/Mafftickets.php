<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mafftickets extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "afftickets";
        $this->_primary_key = "AffTicketId";
    }

    public function statisticForStudentIds($userIds){
        $totalTicketCount = 0;
        $todayTicketCount = 0;
        if(!empty($userIds)) {
            $today = date('d/m/Y');
            $affTickets = $this->getByQuery('SELECT TicketCount, CrDateTime FROM afftickets WHERE AffUserId IN ?', array($userIds));
            foreach ($affTickets as $at) {
                $totalTicketCount += $at['TicketCount'];
                if ($today == ddMMyyyy($at['CrDateTime'])) $todayTicketCount += $at['TicketCount'];
            }
        }
        return array(
            'TotalTicketCount' => $totalTicketCount,
            'TodayTicketCount' => $todayTicketCount
        );
    }
}