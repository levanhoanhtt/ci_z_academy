<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mactionlogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "actionlogs";
        $this->_primary_key = "ActionLogId";
    }

    public function getCount($itemTypeId){
        return $this->countRows('ItemTypeId='.$itemTypeId);
    }

    public function getList($itemTypeId, $perPage = 0, $page = 1){
        $query = 'SELECT * FROM actionlogs WHERE ItemTypeId = ? ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query, array($itemTypeId));
    }
}