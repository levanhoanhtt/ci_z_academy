<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfeedbacks extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "feedbacks";
        $this->_primary_key = "FeedbackId";
    }

    public function getCount($postData){
        $query = "FeedbackStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT feedbacks.*, users.FullName, users.PhoneNumber FROM feedbacks INNER JOIN  users ON feedbacks.StudentId = users.UserId WHERE FeedbackStatusId > 0" . $this->buildQuery($postData).' ORDER BY FeedbackId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FeedbackStatusId']) && $postData['FeedbackStatusId'] > 0) $query.=" AND FeedbackStatusId=".$postData['FeedbackStatusId'];
        if(isset($postData['StudentId']) && $postData['StudentId'] > 0) $query.=" AND feedbacks.StudentId=".$postData['StudentId'];
        return $query;
    }

    public function cutWords($string, $wordsreturned){
        $retval = $string;
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
        $string = str_replace("\n", " ", $string);
        $array = explode(" ", $string);
        if (count($array)<=$wordsreturned) $retval = $string;
        else{
            array_splice($array, $wordsreturned);
            $retval = implode(" ", $array)." ...";
        }
        return $retval;
    }
}