<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnotificationusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "notificationusers";
        $this->_primary_key = "NotificationUserId";
    }
}