<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marticleusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "articleusers";
        $this->_primary_key = "ArticleUserId";
    }

    public function getUserIds($articleId){
        $retVal = array();
        $sus = $this->getBy(array('ArticleId' => $articleId), false, '', 'UserId');
        foreach($sus as $su) $retVal[] = $su['UserId'];
        return $retVal;
    }

    public function checkCanView($articleId, $userId){
        $sus = $this->getByQuery('SELECT ArticleUserId FROM articleusers WHERE ArticleId = ? AND UserId = ?', array($articleId, $userId));
        return !empty($sus);
    }
}