<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservicecharges extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "servicecharges";
        $this->_primary_key = "ServiceChargeId";
    }

    public function insert($postData, $user, $serviceInfo){
        $this->db->trans_begin();
        $serviceChargeId = $this->save($postData);
        if($serviceChargeId > 0){
            $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($postData['PaidCost'], $user['UserId']));
            if($serviceInfo['ServiceStatusId'] == 1) $this->Muserservices->save(array('ServiceStatusId' => 2), $postData['UserServiceId']);
            $this->Mtransactionlogs->save(array(
                'UserId' => $user['UserId'],
                'ByUserId' => 0,
                'LogTypeId' => 6,
                'Amount' => $postData['PaidCost'],
                'Balance' => $user['Balance'] - $postData['PaidCost'],
                'TicketCount' => 0,
                'Comment' => "{$user['FullName']} ({$user['PhoneNumber']}) bị trừ " . priceFormat($postData['PaidCost']) . " VNĐ khi sử dụng dịch vụ ".$serviceInfo['ServiceName'].$serviceInfo['Comment'],
                'CrDateTime' => getCurentDateTime()
            ));
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}