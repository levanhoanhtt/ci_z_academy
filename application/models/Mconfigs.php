<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconfigs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "configs";
        $this->_primary_key = "ConfigId";
    }

    public function getListMap($autoLoad = 1){
        $configs = $this->getBy(array('AutoLoad' => $autoLoad), false, "", "ConfigCode,ConfigValue,ConfigDecimal");
        $retVal = array();
        foreach($configs as $cf){
            if($cf['ConfigCode'] == 'TOTAL_TICKET') $retVal['TOTAL_TICKET'] = $cf['ConfigDecimal'];
            else $retVal[$cf['ConfigCode']] = $cf['ConfigValue'];
        }
        return $retVal;
    }

    public function getConfigValue($configCode, $defaultValue){
        if($configCode == 'TOTAL_TICKET') return $this->getFieldValue(array('ConfigCode' => $configCode), 'ConfigDecimal', $defaultValue);
        return $this->getFieldValue(array('ConfigCode' => $configCode), 'ConfigValue', $defaultValue);
    }

    public function updateBatch($valueData){
        if(!empty($valueData)) $this->db->update_batch('configs', $valueData, 'ConfigId');
        return true;
    }

    public function update($postData, $configId, $actionLogs = array(), $curentTicket = 0){
        $this->load->model('Mactionlogs');
        $this->db->trans_begin();
        if($configId == 5) $this->db->query('UPDATE configs SET ConfigDecimal = ConfigDecimal + ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ConfigId = 5', array($postData['TicketCount'], $postData['UpdateUserId'], $postData['UpdateDateTime']));
        else $configId = $this->save($postData, $configId);
        if($configId > 0){
            if(!empty($actionLogs)) $this->Mactionlogs->save($actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateTicketCost($oldCost, $newCost){
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $configId = $this->save(array(
            'ConfigValue' => $newCost,
            'UpdateUserId' => 0,
            'UpdateDateTime' => $crDateTime
        ), 3);
        if($configId > 0){
            $this->Mticketcosts->save(array(
                'OldCost' => $oldCost,
                'NewCost' => $newCost,
                'CrDateTime' => $crDateTime
            ));
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}