<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfields extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "fields";
        $this->_primary_key = "FieldId";
    }
}