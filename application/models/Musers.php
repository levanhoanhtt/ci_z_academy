<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "users";
        $this->_primary_key = "UserId";
    }

    public function login($userName, $userPass){
        if(!empty($userName) && !empty($userPass)){
            $query = "SELECT * FROM users WHERE UserPass=? AND PhoneNumber=? AND StatusId > 0 LIMIT 1";
            $users = $this->getByQuery($query, array(md5($userPass), $userName));
            if(!empty($users)) return $users[0];
        }
        return false;
    }

    public function checkExist($userId, $userName, $email, $phoneNumber){
        $query = "SELECT UserId, StatusId, Email, FullName FROM users WHERE UserId!=? AND StatusId > 0 AND (UserName=? OR Email=? OR PhoneNumber=?) LIMIT 1";
        $users = $this->getByQuery($query, array($userId, $userName, $email, $phoneNumber));
        if (!empty($users)) return $users[0];
        return false;
    }

    public function getCountBatch($postData){
        $affUserIds = array();
        if(isset($postData['HasAffUserId']) && $postData['HasAffUserId'] == 1) unset($postData['HasAffUserId']);
        if(isset($postData['AffPaidVNStatusId'])){
            $having = '';
            if($postData['AffPaidVNStatusId'] == 1) $having = ' HAVING TotalAffPaidVN > 0';
            elseif($postData['AffPaidVNStatusId'] == 3) $having = ' HAVING (TotalAffPaidVN = 0 OR TotalAffPaidVN IS NULL)';
            $query = "SELECT UserId, users.AffUserId, SUM(PaidVN) AS TotalPaidVN, SUM(AffPaidVN) AS TotalAffPaidVN 
                      FROM users LEFT JOIN transactions ON (transactions.StudentId = users.UserId AND transactions.LogTypeId = 1 AND transactions.AffPaidVN > 0) WHERE StatusId > 0" . $this->buildQuery($postData) .
                    " GROUP BY UserId" . $having;
        }
        else $query = "SELECT UserId, AffUserId FROM users WHERE StatusId > 0" . $this->buildQuery($postData);
        $users = $this->getByQuery($query);
        foreach($users as $u){
            if($u['AffUserId'] > 0){
                if(!in_array($u['AffUserId'], $affUserIds)) $affUserIds[] = $u['AffUserId'];
            }
        }
        return array(
            'CountUser' => count($users),
            'AffUserIds' => $affUserIds
        );
    }

    public function getSumBalanceAff($userIds){
        $retVal = 0;
        if(!empty($userIds)){
            $users = $this->getByQuery("SELECT SUM(BalanceAff) AS SumBalanceAff FROM users WHERE UserId IN ?", array($userIds));
            if(!empty($users)) $retVal = $users[0]['SumBalanceAff'];
        }
        return $retVal;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $isCountRegister = false, $select = '*'){
        if($isCountRegister) $query = "SELECT users.{$select}, (SELECT COUNT(1) FROM registers WHERE ByUserId = users.UserId) AS CountRegister FROM users WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY UserId DESC';
        else $query = "SELECT {$select} FROM users WHERE StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY UserId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function searchForAff($postData, $perPage = 0, $page = 1){
        $having = '';
        if(isset($postData['AffPaidVNStatusId'])){
            if($postData['AffPaidVNStatusId'] == 1) $having = ' HAVING TotalAffPaidVN > 0';
            elseif($postData['AffPaidVNStatusId'] == 3) $having = ' HAVING (TotalAffPaidVN = 0 OR TotalAffPaidVN IS NULL)';
        }
        $query = "SELECT UserId, FullName, PhoneNumber, Email, users.AffUserId, users.CrDateTime, StatusId, IsVerifyEmail, SUM(PaidVN) AS TotalPaidVN, SUM(AffPaidVN) AS TotalAffPaidVN 
                  FROM users LEFT JOIN transactions ON (transactions.StudentId = users.UserId AND transactions.LogTypeId = 1 AND transactions.AffPaidVN > 0) WHERE StatusId > 0" . $this->buildQuery($postData) .
                " GROUP BY UserId{$having} ORDER BY UserId DESC";
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){ 
        $query = '';
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (FullName LIKE '%{$postData['SearchText']}%' OR PhoneNumber LIKE '%{$postData['SearchText']}%' OR Email LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['RoleId']) && $postData['RoleId'] > 0) $query.=" AND RoleId=".$postData['RoleId'];
        if(isset($postData['IsMember']) && $postData['IsMember'] > 0) $query.=" AND IsMember=".$postData['IsMember'];
        if(isset($postData['BalanceStatusId']) && $postData['BalanceStatusId'] > 0){
            if($postData['BalanceStatusId'] == 1) $query .= " AND Balance > 0";
            elseif($postData['BalanceStatusId'] == 2) $query .= " AND Balance < 0";
            elseif($postData['BalanceStatusId'] == 3) $query .= " AND Balance = 0";
        }
        if(isset($postData['TicketStatusId']) && $postData['TicketStatusId'] > 0){
            if($postData['TicketStatusId'] == 1) $query .= " AND TicketCount > 0";
            elseif($postData['TicketStatusId'] == 2) $query .= " AND TicketCount < 0";
            elseif($postData['TicketStatusId'] == 3) $query .= " AND TicketCount = 0"; 
        }
        if(isset($postData['AffUserId']) && $postData['AffUserId'] > 0) $query.=" AND users.AffUserId=".$postData['AffUserId'];
        if(isset($postData['IsVerifyPhone']) && $postData['IsVerifyPhone'] > 0) $query.=" AND users.IsVerifyPhone=".$postData['IsVerifyPhone'];
        if(isset($postData['IsVerifyEmail']) && $postData['IsVerifyEmail'] > 0) $query.=" AND users.IsVerifyEmail=".$postData['IsVerifyEmail'];
        if(isset($postData['HasAffUserId']) && $postData['HasAffUserId'] == 1) $query.=" AND users.AffUserId > 0";
        /*if(isset($postData['TicketStatusId']) && $postData['TicketStatusId'] > 0){
            if($postData['TicketStatusId'] == 1) $query .= " AND TicketCount > 0";
            elseif($postData['TicketStatusId'] == 2) $query .= " AND TicketCount < 0";
            elseif($postData['TicketStatusId'] == 3) $query .= " AND TicketCount = 0";
        }*/
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";

        if(isset($postData['SkillId_1']) && $postData['SkillId_1'] > 0 && isset($postData['SkillLevelId_1']) && $postData['SkillLevelId_1'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillId = {$postData['SkillId_1']} AND SkillLevelId = {$postData['SkillLevelId_1']} AND SkillTypeId = 1)";
        elseif(isset($postData['SkillId_1']) && $postData['SkillId_1'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillId = {$postData['SkillId_1']} AND SkillTypeId = 1)";
        elseif(isset($postData['SkillLevelId_1']) && $postData['SkillLevelId_1'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillLevelId = {$postData['SkillLevelId_1']} AND SkillTypeId = 1)";

        if(isset($postData['SkillId_2']) && $postData['SkillId_2'] > 0 && isset($postData['SkillLevelId_2']) && $postData['SkillLevelId_2'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillId = {$postData['SkillId_2']} AND SkillLevelId = {$postData['SkillLevelId_2']} AND SkillTypeId = 2)";
        elseif(isset($postData['SkillId_2']) && $postData['SkillId_2'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillId = {$postData['SkillId_2']} AND SkillTypeId = 2)";
        elseif(isset($postData['SkillLevelId_2']) && $postData['SkillLevelId_2'] > 0) $query.=" AND UserId IN(SELECT UserId FROM userskills WHERE SkillLevelId = {$postData['SkillLevelId_2']} AND SkillTypeId = 2)";

        if(isset($postData['FieldId']) && $postData['FieldId'] > 0) $query.=" AND UserId IN(SELECT UserId FROM relationships WHERE FieldId = {$postData['FieldId']})";

        return $query;
    }

    public function getListForSelect($roleId = 2, $isMember = 0){
        $where = array('StatusId' => STATUS_ACTIVED, 'RoleId' => $roleId);
        if($isMember > 0) $where['IsMember'] = $isMember;
        return $this->getBy($where, false, "UserId", "UserId, FullName, PhoneNumber");
    }

    public function getListSendMailService($userIds){
        return $this->getByQuery('SELECT UserId, FullName, PhoneNumber, Email, Balance FROM users WHERE Balance < 0 AND UserId IN ?', array($userIds));
    }

    public function getListToken($userIds){
        $retVal = array();
        $users = $this->getByQuery('SELECT TokenFCM FROM users WHERE UserId IN ?', array($userIds));
        foreach($users as $u){
            if(!empty($u['TokenFCM'])){
                $tokenFCMs = json_decode($u['TokenFCM'], true);
                foreach($tokenFCMs as $tokenFCM){
                    if(!in_array($tokenFCM, $retVal)) $retVal[] = $tokenFCM;
                }
            }
        }
        return $retVal;
    }

    public function update($postData, $userId, $notifications = array()){
        $this->db->trans_begin();
        $userId = $this->save($postData, $userId);
        if(!empty($notifications)) $this->Mnotifications->save($notifications);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $userId;
        }
    }
    public function updateIsVerifyPhone($phoneNumber){
        $this->db->update('users', array('IsVerifyPhone' => 2), array('PhoneNumber' => $phoneNumber));
    }
}