<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducts extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "products";
        $this->_primary_key = "ProductId";
    }

    public function checkExist($productName, $brandId, $productId = 0){
        $query = "SELECT ProductId FROM products WHERE ProductId != ? AND ItemStatusId > 0 AND ProductName = ? AND BrandId = ? LIMIT 1";
        $products = $this->getByQuery($query, array($productId, $productName, $brandId));
        if(!empty($products)) return true;
        return false;
    }
    
    public function getCount($postData, $isJoinUser = false){
        if($isJoinUser){
            $query = "SELECT ProductId FROM products LEFT JOIN users ON products.UserId = users.UserId WHERE ItemStatusId > 0" . $this->buildQuery($postData);
            $retVal = count($this->getByQuery($query));
        }
        else {
            $query = "ItemStatusId > 0" . $this->buildQuery($postData);
            $retVal = $this->countRows($query);
        }
        return $retVal;
    }

    public function search($postData, $perPage = 0, $page = 1, $isJoinUser = false){
        if($isJoinUser){
            $query = 'SELECT products.*, users.FullName, users.PhoneNumber FROM products INNER JOIN users ON products.UserId = users.UserId  WHERE ItemStatusId > 0' . $this->buildQuery($postData);
            if(isset($postData['SortTypeId'])){
                if($postData['SortTypeId'] == 1) $query .= ' ORDER BY Price ASC';
                elseif($postData['SortTypeId'] == 2) $query .= ' ORDER BY Price DESC';
                elseif($postData['SortTypeId'] == 3){
                    $query = 'SELECT products.*, users.FullName, users.PhoneNumber, SUM(orders.Quantity) AS SumQuantity FROM products INNER JOIN users ON products.UserId = users.UserId INNER JOIN orders ON products.ProductId = orders.ProductId
                            WHERE ItemStatusId > 0'.$this->buildQuery($postData).' GROUP BY products.ProductId ORDER BY SumQuantity DESC';
                }
            }
            else $query .= ' ORDER BY ProductId DESC';
        }
        else $query = 'SELECT * FROM products WHERE ItemStatusId > 0' . $this->buildQuery($postData) . ' ORDER BY ProductId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ProductName']) && !empty($postData['ProductName'])) $query.=" AND products.ProductName LIKE '%{$postData['ProductName']}%'";
        if(isset($postData['ItemStatusId']) && $postData['ItemStatusId'] > 0) $query.=" AND products.ItemStatusId=".$postData['ItemStatusId'];
        if(isset($postData['BrandId']) && $postData['BrandId'] > 0) $query.=" AND products.BrandId=".$postData['BrandId'];
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND products.UserId=".$postData['UserId'];
        if(isset($postData['ProductId']) && $postData['ProductId'] > 0) $query.=" AND products.ProductId=".$postData['ProductId'];
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.="  AND (users.FullName LIKE '%{$postData['SearchText']}%' OR users.PhoneNumber LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['DifferentUserId']) && $postData['DifferentUserId'] > 0) $query.=" AND products.UserId != ".$postData['DifferentUserId'];
        return $query;
    }

    public function update($postData, $productId, $notifications = array()){
        $this->db->trans_begin();
        $productId = $this->save($postData, $productId);
        if(!empty($notifications)) $this->db->insert_batch('notifications', $notifications);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $productId;
        }
    }
}