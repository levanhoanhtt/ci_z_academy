<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcharges extends MY_Model{
    function __construct() {
        parent::__construct();
        $this->_table_name = "charges";
        $this->_primary_key = "ChargeId";
    }

    public function insert($postData, $toStudent = array(), $user = array()){
        $this->db->trans_begin();
        $chargeId = $this->save($postData);
        if($chargeId > 0){
            $logs = array();
            if($postData['ChargeTypeId'] == 2){ //mua ve
                $this->db->query('UPDATE users SET Balance = Balance - ?, TicketCount = TicketCount + ? WHERE UserId = ?', array($postData['PaidVN'], $postData['TicketCount'], $postData['StudentId']));
                //tru trong kho ve
                $this->db->query('UPDATE configs SET ConfigDecimal = ConfigDecimal - ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ConfigId = 5', array($postData['TicketCount'], $postData['CrUserId'], $postData['CrDateTime']));
                $logs[] = array(
                    'UserId' => $postData['StudentId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 3,
                    'Amount' => $postData['TicketCount'],
                    'Balance' => $user['Balance'] - $postData['PaidVN'],
                    'TicketCount' => $user['TicketCount'] + $postData['TicketCount'],
                    'Comment' => "{$user['FullName']} ({$user['PhoneNumber']}) mua " . $postData['TicketCount']. " Zticket",
                    'CrDateTime' => $postData['CrDateTime']
                );
            }
            elseif($postData['ChargeTypeId'] == 1){ //ban ve
                $this->db->query('UPDATE users SET TicketCount = TicketCount - ? WHERE UserId = ?', array($postData['TicketCount'], $postData['StudentId']));
                $this->db->query('UPDATE users SET TicketCount = TicketCount + ? WHERE UserId = ?', array($postData['TicketCount'], $toStudent['UserId']));
                $logs[] = array(
                    'UserId' => $postData['StudentId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 4,
                    'Amount' => $postData['TicketCount'],
                    'Balance' => 0,
                    'TicketCount' => $user['TicketCount'] - $postData['TicketCount'],
                    'Comment' => "{$user['FullName']} ({$user['PhoneNumber']}) bán " . $postData['TicketCount'] . " Zticket cho {$toStudent['FullName']} ({$toStudent['PhoneNumber']})",
                    'CrDateTime' => $postData['CrDateTime']
                );
                $logs[] = array(
                    'UserId' => $toStudent['UserId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 4,
                    'Amount' => $postData['TicketCount'],
                    'Balance' => 0,
                    'TicketCount' => $toStudent['TicketCount'] + $postData['TicketCount'],
                    'Comment' => "{$toStudent['FullName']} ({$toStudent['PhoneNumber']}) mua " . $postData['TicketCount'] . " Zticket từ {$user['FullName']} ({$user['PhoneNumber']})",
                    'CrDateTime' => $postData['CrDateTime']
                );
            }
            elseif($postData['ChargeTypeId'] == 4){ //chuyen tien giua 2 thanh vien
                $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($postData['PaidVN'], $postData['StudentId']));
                $this->db->query('UPDATE users SET Balance = Balance + ? WHERE UserId = ?', array($postData['PaidVN'], $toStudent['UserId']));
                $comment = $postData['Comment'];
                if(!empty($comment)) $comment = ', với lý do: '. $comment;
                $logs[] = array(
                    'UserId' => $postData['StudentId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 5,
                    'Amount' => $postData['PaidVN'],
                    'Balance' => $user['Balance'] - $postData['PaidVN'],
                    'TicketCount' => 0,
                    'Comment' => "{$user['FullName']} ({$user['PhoneNumber']}) chuyển " . priceFormat($postData['PaidVN']) . " VNĐ cho {$toStudent['FullName']} ({$toStudent['PhoneNumber']})".$comment,
                    'CrDateTime' => $postData['CrDateTime']
                );
                $logs[] = array(
                    'UserId' => $toStudent['UserId'],
                    'ByUserId' => $postData['CrUserId'],
                    'LogTypeId' => 5,
                    'Amount' => $postData['PaidVN'],
                    'Balance' => $toStudent['Balance'] + $postData['PaidVN'],
                    'TicketCount' => 0,
                    'Comment' => "{$toStudent['FullName']} ({$toStudent['PhoneNumber']}) nhận " . priceFormat($postData['PaidVN']) . " VNĐ từ {$user['FullName']} ({$user['PhoneNumber']})".$comment,
                    'CrDateTime' => $postData['CrDateTime']
                );
            }
            if(!empty($logs)) $this->db->insert_batch('transactionlogs', $logs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}