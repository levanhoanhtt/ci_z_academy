<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mticketcosts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ticketcosts";
        $this->_primary_key = "TicketCostId";
    }

    public function checkExist(){
        $tcs = $this->getByQuery('SELECT TicketCostId FROM ticketcosts WHERE DATE(CrDateTime) = ? LIMIT 1', array(date('Y-m-d')));
        if(!empty($tcs)) return true;
        return false;
    }
}