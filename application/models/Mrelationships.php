<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrelationships extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "relationships";
        $this->_primary_key = "RelationshipId";
    }
}