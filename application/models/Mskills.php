<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mskills extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "skills";
        $this->_primary_key = "SkillId";
    }

    public $labelCss = array(
        0 => 'label label-default',
        3 => 'label label-success',
        2 => 'label label-warning',
        1 => 'label label-danger'
    );
}