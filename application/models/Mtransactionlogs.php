<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionlogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionlogs";
        $this->_primary_key = "TransactionLogId";
    }

    public $labelCss = array(
        1 => 'label label-success',
        2 => 'label label-success',
        3 => 'label label-green',
        4 => 'label label-yellow',
        5 => 'label label-red',
        6 => 'label label-orange',
        7 => 'label label-red',
        8 => 'label label-green',
        12 => 'label label-green',
        9 => 'label label-danger',
        10 => 'label label-orange',
        11 => 'label label-danger'
    );

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        //if (isset($postData['UserId']) && $postData['UserId'] > 0) $query .= " AND (UserId={$postData['UserId']} OR ByUserId={$postData['UserId']})";
        if (isset($postData['UserId']) && $postData['UserId'] > 0) $query .= " AND UserId=" . $postData['UserId'];
        if (isset($postData['ByUserId']) && $postData['ByUserId'] > 0) $query .= " AND ByUserId=" . $postData['ByUserId'];
        if(isset($postData['LogTypeId']) && $postData['LogTypeId'] > 0) $query.=" AND LogTypeId=".$postData['LogTypeId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM transactionlogs WHERE 1=1" . $this->buildQuery($postData).' ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }
}