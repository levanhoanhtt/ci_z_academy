<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactions extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "transactions";
        $this->_primary_key = "TransactionId";
    }

    public function getCount($postData){
        $query = "SELECT 1 FROM transactions INNER JOIN users ON users.UserId = transactions.StudentId WHERE 1=1" . $this->buildQuery($postData);
        return count($this->getByQuery($query));
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (users.FullName LIKE '%{$postData['SearchText']}%' OR users.PhoneNumber LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['LogTypeId']) && !empty($postData['LogTypeId'])) $query .= " AND LogTypeId = '{$postData['LogTypeId']}'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT transactions.*, users.FullName, users.PhoneNumber FROM transactions INNER JOIN users ON users.UserId = transactions.StudentId WHERE 1=1" . $this->buildQuery($postData).' ORDER BY transactions.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function insert($postData, $student){
        if($postData['LogTypeId'] == 1){
            $comment =  "{$student['FullName']} ({$student['PhoneNumber']}) nạp " . priceFormat($postData['PaidVN']) . " VNĐ vào học viện ⓩ";
            if(!empty($postData['Comment'])) $comment .= '. Lý do: '.$postData['Comment'];
        }
        else{
            $comment = "Học viện ⓩ trừ " . priceFormat($postData['PaidVN']) . " VNĐ từ {$student['FullName']} ({$student['PhoneNumber']})";
            if(!empty($postData['Comment'])) $comment .= '. Lý do: '.$postData['Comment'];
        }
        $transactionLogs = array();
        $this->db->trans_begin();
        $transactionId = $this->save($postData);
        if($transactionId > 0){
            if($postData['LogTypeId'] == 1){
                $this->db->query('UPDATE users SET Balance = Balance + ? WHERE UserId = ?', array($postData['PaidVN'], $postData['StudentId']));
                if($postData['AffUserId'] > 0){
                    $affUser = $student['AffUser'];
                    $this->db->query('UPDATE users SET BalanceAff = ?, TicketCount = TicketCount + ? WHERE UserId = ?', array($affUser['AffPaidVN'], $affUser['AffTicketCount'], $postData['AffUserId']));
                    if($affUser['AffPaidVN'] > 0) {
                        $transactionLogs[] = array(
                            'UserId' => $postData['AffUserId'],
                            'ByUserId' => $postData['CrUserId'],
                            'LogTypeId' => 8,
                            'Amount' => $postData['AffPaidVN'],//can sua lai, phai la so tien khi tru ve
                            'Balance' => $affUser['AffPaidVN'],
                            'TicketCount' => 0,
                            'Comment' => "{$affUser['FullName']} ({$affUser['PhoneNumber']}) được cộng " . priceFormat($affUser['AffPaidVN']) . " VNĐ vào tài khoản Affiliate",
                            'CrDateTime' => $postData['CrDateTime']
                        );
                    }
                    if($affUser['AffTicketCount'] > 0){
                        //tru trong kho ve
                        $this->db->query('UPDATE configs SET ConfigDecimal = ConfigDecimal - ?, UpdateUserId = ?, UpdateDateTime = ? WHERE ConfigId = 5', array($affUser['AffTicketCount'], $postData['CrUserId'], $postData['CrDateTime']));
                        $transactionLogs[] = array(
                            'UserId' => $postData['AffUserId'],
                            'ByUserId' => $postData['CrUserId'],
                            'LogTypeId' => 12,
                            'Amount' => $affUser['AffTicketCount'],
                            'Balance' => 0,
                            'TicketCount' => $affUser['TicketCount'] + $affUser['AffTicketCount'],
                            'Comment' => "{$affUser['FullName']} ({$affUser['PhoneNumber']}) được cộng {$affUser['AffTicketCount']} Zticket từ Affiliate",
                            'CrDateTime' => $postData['CrDateTime']
                        );
                        $this->Mafftickets->save(array(
                            'TransactionId' => $transactionId,
                            'StudentId' => $postData['StudentId'],
                            'AffUserId' => $postData['AffUserId'],
                            'AffPaidVN' => $affUser['OriginalAffPaidVN'],
                            'TicketCost' => $affUser['TicketCost'],
                            'TicketCount' => $affUser['AffTicketCount'],
                            'CrUserId' => $postData['CrUserId'],
                            'CrDateTime' => $postData['CrDateTime']
                        ));
                    }
                }
            } 
            else $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($postData['PaidVN'], $postData['StudentId']));
            $transactionLogs[] = array(
                'UserId' => $postData['StudentId'],
                'ByUserId' => $postData['CrUserId'],
                'LogTypeId' => $postData['LogTypeId'],
                'Amount' => $postData['PaidVN'],
                'Balance' => $postData['LogTypeId'] == 1 ? $student['Balance'] + $postData['PaidVN'] : $student['Balance'] - $postData['PaidVN'],
                'TicketCount' => 0,
                'Comment' => $comment,
                'CrDateTime' => $postData['CrDateTime']
            );
            $this->db->insert_batch('transactionlogs', $transactionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}