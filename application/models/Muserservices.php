<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserservices extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "userservices";
        $this->_primary_key = "UserServiceId";
    }

    public function checkExist($userId, $serviceId){
        $us = $this->getByQuery('SELECT UserServiceId FROM userservices WHERE UserId = ? AND ServiceId = ? AND ServiceStatusId IN(1,2)', array($userId, $serviceId));
        if(!empty($us)) return true;
        return false;
    }

    public function register($postData, $transactionLogs = array(), $serviceCharge = array(), $serviceLog = array()){
        $this->db->trans_begin();
        $userServiceId = $this->save($postData, 0, array('EndDate', 'UpdateUserId', 'UpdateDateTime'));
        if($userServiceId > 0){
            if($postData['ServiceStatusId'] == 2 || $postData['ServiceStatusId'] == 3){
                $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($postData['PaidCost'], $postData['UserId']));
                if(!empty($transactionLogs)) $this->Mtransactionlogs->save($transactionLogs);
                if(!empty($serviceCharge)){
                    $serviceCharge['UserServiceId'] = $userServiceId;
                    $this->Mservicecharges->save($serviceCharge);
                }
                if(!empty($serviceLog)){
                    $serviceLog['UserServiceId'] = $userServiceId;
                    $this->Mservicelogs->save($serviceLog);
                }
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateRegister($postData, $userServiceId, $serviceLog = array()){
        $this->db->trans_begin();
        $userServiceId = $this->save($postData, $userServiceId, array('EndDate'));
        if($userServiceId > 0){
            if(!empty($serviceLog)) $this->Mservicelogs->save($serviceLog);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function stopService($userServiceId, $userId, $serviceStatusId, $serviceLog = array()){
        $updateDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $userServiceId = $this->save(array('EndDate' => $updateDateTime, 'ServiceStatusId' => $serviceStatusId, 'UpdateUserId' => $userId, 'UpdateDateTime' => $updateDateTime), $userServiceId);
        if($userServiceId > 0){
            if(!empty($serviceLog)) $this->Mservicelogs->save($serviceLog);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getListCharge($today = ''){
        if(empty($today)) $today = date('Y-m-d');
        return $this->getByQuery('SELECT * FROM userservices WHERE ServiceStatusId IN(1, 2) AND (EndDate IS NULL OR EndDate >= ?)', array($today));
        //return $this->getByQuery('SELECT * FROM userservices WHERE ServiceStatusId = 1 AND (EndDate IS NULL OR EndDate >= ?)', array($today));
    }

    public function getCount($postData){
        $query = "ServiceStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND UserId=".$postData['UserId'];
        if(isset($postData['ServiceId']) && $postData['ServiceId'] > 0) $query.=" AND ServiceId=".$postData['ServiceId'];
        if(isset($postData['ServiceStatusId']) && $postData['ServiceStatusId'] > 0) $query.=" AND ServiceStatusId=".$postData['ServiceStatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND BeginDate <= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND EndDate >= '{$postData['EndDate']}'";
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM userservices WHERE ServiceStatusId > 0" . $this->buildQuery($postData).' ORDER BY BeginDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function timeDiff($beginDate, $endDate, $serviceTypeId){
        $retVal = 0;
        if($serviceTypeId == 3){
            $unixBeginDate = strtotime($beginDate);
            $unixEndDate = strtotime($endDate);
            $d1Y = date('Y', $unixBeginDate);
            $d2Y = date('Y', $unixEndDate);
            $d1M = date('m', $unixBeginDate);
            $d2M = date('m', $unixEndDate);
            if($d1Y == $d2Y) $retVal = $d2M - $d1M + 1;
            else if($d2Y > $d1Y){
                $retVal = ($d2M+12*$d2Y)-($d1M+12*$d1Y);
                if($d1M != $d2M) $retVal++;
            }
        }
        else{
            $dStart = new DateTime($beginDate);
            $dEnd = new DateTime($endDate);
            $diff = $dStart->diff($dEnd);
            if ($serviceTypeId == 1) $retVal = $diff->days + 1;
            elseif ($serviceTypeId == 2){
                $months = $diff->y * 12 + $diff->m + 1;//$diff->d / 30;
                $retVal = round($months);
            }
        }
        return $retVal;
    }

    public function checkLeapYear($year){
        $retVal = false;
        if($year % 100 == 0){
            if($year % 400 == 0) $retVal = true;
        }
        elseif($year % 4 == 0) $retVal = true;
        return $retVal;
    }
}