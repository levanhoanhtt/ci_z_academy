<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconflicts extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "conflicts";
        $this->_primary_key = "ConflictId";
    }

    public function insert($postData, $orderData, $transactionLogs = array(), $metaData = array()){
        $this->db->trans_begin();
        $this->save($postData);
        if(!empty($orderData)) $this->Morders->save($orderData, $postData['OrderId']);
        if(!empty($transactionLogs)){
            $this->db->query('UPDATE users SET BalanceSuspend = BalanceSuspend + ?, Balance = Balance - ? WHERE UserId = ?', array($metaData['OrderCost'], $metaData['OrderCost'], $metaData['SellerId']));
            $this->load->model('Mtransactionlogs');
            $this->Mtransactionlogs->save($transactionLogs);
        }
        if(isset($orderData['CustomerOrderStatusId']) && $orderData['CustomerOrderStatusId'] == 4){
            $this->db->query('UPDATE users SET RefundTime = RefundTime + 1 WHERE UserId = ?', array($metaData['CustomerId']));
        }
        if(!empty($metaData['Notifications'])) $this->Mnotifications->save($metaData['Notifications']);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}