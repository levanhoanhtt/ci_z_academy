<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproofs extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "proofs";
        $this->_primary_key = "ProofId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    private function buildQuery($postData){
        $query = '';
        //if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (StudentName LIKE '%{$postData['SearchText']}%' OR PhoneNumber LIKE '%{$postData['SearchText']}%')";
        return $query;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT proofs.*, users.FullName, users.PhoneNumber FROM proofs INNER JOIN users ON users.UserId = proofs.StudentId WHERE 1=1" . $this->buildQuery($postData).' ORDER BY proofs.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }
}