<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservices extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "services";
        $this->_primary_key = "ServiceId";
    }

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-warning',
        5 => 'label label-danger',
        6 => 'label label-red'
    );
}