<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnotifications extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "notifications";
        $this->_primary_key = "NotificationId";
    }

    public function getCount($postData){
        if($postData['RoleId'] == 1){
            $query = "ArticleId = 0 " . $this->buildQuery($postData);
            return $this->countRows($query);
        }
        else{
            $query = "SELECT 1 FROM  notifications LEFT JOIN notificationusers ON notificationusers.NotificationId = notifications.NotificationId AND notificationusers.UserId = ".$postData['StudentId']." WHERE 1=1" . $this->buildQuery($postData). "  GROUP BY notifications.NotificationId";
            return count($this->getByQuery($query));
        }
    }

    public function search($postData, $perPage = 0, $page = 1){
        if($postData['RoleId'] == 1){
            $query = "SELECT notifications.* FROM  notifications  WHERE 1=1 AND notifications.ArticleId = 0  " . $this->buildQuery($postData).' ORDER BY notifications.CrDateTime DESC';
        }
        else{
            $query = "SELECT notificationusers.IsRead AS UserIsRead, notifications.* FROM  notifications LEFT JOIN notificationusers ON notificationusers.NotificationId = notifications.NotificationId AND notificationusers.UserId = ".$postData['StudentId']." WHERE 1=1" . $this->buildQuery($postData).' GROUP BY notifications.NotificationId ORDER BY notifications.CrDateTime DESC';
        }
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['StudentId']) && $postData['StudentId'] > 0)  $query.=" AND (notifications.StudentId = {$postData['StudentId']} OR notifications.StudentId = 0)";
        //if(isset($postData['AdminId']) && $postData['AdminId'] > 0)  $query.=" AND AdminId=".$postData['AdminId'];
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (notifications.PhoneNumber LIKE '%{$postData['SearchText']}%' OR notifications.Email LIKE '%{$postData['SearchText']}%')";
        if($postData['RoleId'] == 1){
            if(isset($postData['IsRead']) && $postData['IsRead'] > 0)  $query.=" AND notifications.IsRead=".$postData['IsRead'];
        }
        else{
            if(isset($postData['IsRead']) && $postData['IsRead'] > 0){
                if($postData['IsRead'] != 1) $query.=" AND notificationusers.IsRead=".$postData['IsRead'];
                else $query.=" AND notificationusers.IsRead  IS NULL";
            }  
            if(isset($postData['CountIsRead']) && $postData['CountIsRead'] > 0)  $query.=" AND notificationusers.UserId IS NULL";
        }
        if(isset($postData['IsFromStudent']) && $postData['IsFromStudent'] > 0)  $query.=" AND notifications.IsFromStudent=".$postData['IsFromStudent'];
        if(isset($postData['IsArticle'])){
            if($postData['IsArticle'] == 1) $query.=" AND notifications.ArticleId = 0";
            elseif($postData['IsArticle'] == 2) $query.=" AND notifications.ArticleId > 0";
        }
        return $query;
    }

    function changeIsReadAll($userId, $roleId){
        $crDateTime = getCurentDateTime();
        if($roleId == 1) $this->db->query('UPDATE notifications SET IsRead = 2, UpdateUserId = ?, UpdateDateTime = ? WHERE IsRead = 1 AND IsFromStudent = 2', array($userId, $crDateTime));
        else{
            $listNotifications = $this->getByQuery('SELECT NotificationId FROM  notifications  WHERE (StudentId = ? OR StudentId = 0) AND IsFromStudent = 1', array($userId));
            $notificationUsers = array();
            foreach($listNotifications as $n) {
               $notificationUsers[] = array(
                    'NotificationId' => $n['NotificationId'],
                    'UserId' => $userId,
                    'IsRead' => 2,
                    'UpdateDateTime' => $crDateTime
               );
            }
            if(!empty($notificationUsers)){
                $this->db->delete('notificationusers', array('UserId' => $userId));
                $this->db->insert_batch('notificationusers', $notificationUsers);
            }
        } 
        return true;
    }
}