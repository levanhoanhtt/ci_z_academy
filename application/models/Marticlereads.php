<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marticlereads extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "articlereads";
        $this->_primary_key = "ArticleReadId";
    }
}