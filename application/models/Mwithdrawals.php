<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwithdrawals extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "withdrawals";
        $this->_primary_key = "WithdrawalId";
    }

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-yellow',
        3 => 'label label-success',
        4 => 'label label-warning',
        5 => 'label label-danger'
    );

    public function getCount($postData){
        $totalWithdrawalCost = 0;
        $totalReceiveCost = 0;
        $query = "SELECT withdrawals.WithdrawalCost, withdrawals.ReceiveCost, withdrawals.WithdrawalStatusId FROM withdrawals LEFT JOIN users ON users.UserId = withdrawals.StudentId WHERE withdrawals.WithdrawalStatusId > 0" . $this->buildQuery($postData);
        $listWithdrawals = $this->getByQuery($query);
        foreach($listWithdrawals as $w){
            if($w['WithdrawalStatusId'] == 3){
                $totalWithdrawalCost += $w['WithdrawalCost'];
                $totalReceiveCost += $w['ReceiveCost'];
            }
        }
        return array(
            'TotalWithdrawalCost' => $totalWithdrawalCost,
            'TotalReceiveCost' => $totalReceiveCost,
            'TotalRecord' => count($listWithdrawals)
        );
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT withdrawals.*, users.FullName, users.PhoneNumber FROM withdrawals LEFT JOIN users ON users.UserId = withdrawals.StudentId WHERE withdrawals.WithdrawalStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY withdrawals.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['SearchText']) && !empty($postData['SearchText'])) $query.=" AND (users.FullName LIKE '%{$postData['SearchText']}%' OR users.PhoneNumber LIKE '%{$postData['SearchText']}%' OR users.Email LIKE '%{$postData['SearchText']}%')";
        if(isset($postData['StudentId']) && $postData['StudentId'] > 0) $query.=" AND withdrawals.StudentId=".$postData['StudentId'];
        if(isset($postData['WithdrawalStatusId']) && $postData['WithdrawalStatusId'] > 0) $query.=" AND withdrawals.WithdrawalStatusId=".$postData['WithdrawalStatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND withdrawals.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND withdrawals.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function changeWithdrawalStatus($postData, $withdrawalId, $metaData = array()){
    	$this->db->trans_begin();
        $withdrawalId = $this->save($postData, $withdrawalId);
        if(!empty($metaData['Notifications'])) $this->Mnotifications->save($metaData['Notifications']);
        if($withdrawalId > 0 && $postData['WithdrawalStatusId'] == 3){
            $this->db->query('UPDATE users SET Balance = Balance - ? WHERE UserId = ?', array($metaData['WithdrawalCost'], $metaData['StudentId']));
            $this->Mtransactionlogs->save(array(
                'UserId' => $metaData['StudentId'],
                'ByUserId' => 0,
                'LogTypeId' => 9,
                'Amount' => $metaData['WithdrawalCost'],
                'Balance' => $metaData['Balance'] - $metaData['WithdrawalCost'],
                'TicketCount' => 0,
                'Comment' => "{$metaData['FullName']} ({$metaData['PhoneNumber']}) rút thành công " . priceFormat($metaData['WithdrawalCost']) . " VNĐ",
                'CrDateTime' => getCurentDateTime()
            ));
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $withdrawalId;
        }
    }

    public function insert($postData, $notifications = array()){
        $this->db->trans_begin();
        $withdrawalId = $this->save($postData);
        if(!empty($notifications)) $this->Mnotifications->save($notifications);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $withdrawalId;
        }
    }
}