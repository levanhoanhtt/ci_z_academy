<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpromotions extends MY_Model{

    function __construct(){
        parent::__construct();
        $this->_table_name = "promotions";
        $this->_primary_key = "PromotionId";
    }

    public function checkExist($productId, $beginDate, $endDate, $promotionId = 0){
        $param = array($promotionId, $productId);
        $query = "SELECT PromotionId FROM promotions WHERE PromotionId != ? AND StatusId > 0 AND ProductId = ? AND (EndDate IS NULL OR EndDate > ?) LIMIT 1";
        if(empty($endDate)) $param[] = $beginDate;
        else $param[] = $endDate;
        $promotions = $this->getByQuery($query, $param);
        if(!empty($promotions)) return true;
        return false;
    }

    public function checkPromotionCode($promotionCode, $productId){
        $retVal = array(
            'PromotionCodeId' => 0,
            'DiscountCost' => 0
        );
        $today = date('Y-m-d');
        $query = 'SELECT DiscountCost, PromotionCodeId FROM promotions INNER JOIN promotioncodes ON promotioncodes.PromotionId = promotions.PromotionId WHERE promotions.StatusId = 2 AND promotioncodes.PromotionCode = ? AND promotions.ProductId = ? AND promotioncodes.OrderId = 0 AND (promotions.EndDate IS NULL OR (promotions.BeginDate <= ? AND promotions.EndDate >= ?))';
        $promotions = $this->getByQuery($query, array($promotionCode, $productId, $today, $today));
        if(!empty($promotions)) $retVal = $promotions[0];
        return $retVal;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

     public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT promotions.*, products.ProductName, brands.BrandName FROM promotions INNER JOIN products ON products.ProductId = promotions.ProductId INNER JOIN brands ON promotions.BrandId = brands.BrandId WHERE promotions.StatusId > 0" . $this->buildQuery($postData) . ' ORDER BY promotions.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND promotions.StatusId=".$postData['StatusId'];
        if(isset($postData['ProductId']) && $postData['ProductId'] > 0) $query.=" AND promotions.ProductId=".$postData['ProductId'];
        if(isset($postData['BrandId']) && $postData['BrandId'] > 0) $query.=" AND promotions.BrandId=".$postData['BrandId'];
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND promotions.UserId=".$postData['UserId'];
        return $query;
    }

    public function update($postData, $promotionId = 0, $notifications = array()){
        $isUpdate = $promotionId > 0;
        $this->db->trans_begin();
        $promotionId = $this->save($postData, $promotionId, array('EndDate', 'UpdateUserId', 'UpdateDateTime'));
        if ($promotionId > 0){
            if(!empty($notifications)) $this->Mnotifications->save($notifications);
            if(!$isUpdate) {
                $promotionCodes = array();
                for ($i = 1; $i <= $postData['Quantity']; $i++) {
                    $promotionCodes[] = array(
                        'PromotionId' => $promotionId,
                        'PromotionCode' => uniqid(),
                        'OrderId' => 0
                    );
                }
                if (!empty($promotionCodes)) $this->db->insert_batch('promotioncodes', $promotionCodes);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $promotionId;
        }
    }
}